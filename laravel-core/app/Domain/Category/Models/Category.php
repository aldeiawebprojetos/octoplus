<?php

namespace Octoplus\Domain\Category\Models;

use Spatie\Activitylog\Traits\LogsActivity;
use Octoplus\App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Octoplus\Domain\Item\Models\Item;

class Category extends Model
{
    use ForTenants, LogsActivity;

    protected $fillable = [
        'company_id',
        'name',
        'color',
        'icon',
        'type',
        'id',
        'enabled',
        'parent_id',
    ];

    protected static $logFillable = true;
    protected static $logName = 'general.category';

    public function items()
    {
        return $this->belongsToMany(Item::class,'item_categories');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');
    }
}
