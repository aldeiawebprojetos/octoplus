<?php

namespace Octoplus\Domain\Currency;

use Octoplus\App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use ForTenants;

    protected $fillable = [
        'company_id',
        'name',
        'code',
        'rate',
        'precision',
        'symbol',
        'symbol_first',
        'decimal_mark',
        'thousands_separator',
        'enabled',
    ];

}
