<?php

namespace Octoplus\Domain\Currency;

use Spatie\Activitylog\Traits\LogsActivity;
use Octoplus\App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;


class Tax extends Model
{
    use ForTenants,LogsActivity;

    protected $fillable = [
    'company_id',
    'name',
    'rate',
    'enabled',
];
protected static $logFillable = true;
protected static $logName = 'general.tax';


}
