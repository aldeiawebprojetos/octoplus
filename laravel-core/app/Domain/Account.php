<?php

namespace Octoplus\Domain;

use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Model;
use Octoplus\App\Tenant\Traits\ForTenants;

class Account extends Model
{
    use LogsActivity, ForTenants;

    protected $fillable = [
        'company_id',
        'currency_id',
        'name',
        'number',
        'balance',
        'default',
        'bank_name',
        'bank_phone',
        'enabled',
    ];

    protected static $logFillable = true;
    protected static $logName = 'financial::general.account';
}
