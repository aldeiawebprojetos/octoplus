<?php

namespace Octoplus\Domain\Company\Models;

use Illuminate\Database\Eloquent\Model;
use Octoplus\App\Tenant\Traits\IsTenant;
use Octoplus\Domain\Projects\Models\Project;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Octoplus\Domain\Module\Models\Module;
use Octoplus\Domain\Users\Models\User;
use Plank\Mediable\Mediable;

class Company extends Model
{
    use IsTenant, Mediable;

    protected $fillable = [
        'name',
        'country',
        'email',
        'phone',
        'document',
        'address',
    ];

    /**
     * Get projects owned by company.
     * 
     * @return HasMany
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function modules()
    {
        return $this->belongsToMany(Module::class,'company_modules');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'company_users');
    }
}
