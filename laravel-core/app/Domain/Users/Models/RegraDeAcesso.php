<?php

namespace Octoplus\Domain\Users\Models;

use Octoplus\App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;


class RegraDeAcesso extends Model
{
   use ForTenants;

   protected $fillable = [
    'name',
    'company_id'
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'regra_de_acesso_permissions');
    }
}
