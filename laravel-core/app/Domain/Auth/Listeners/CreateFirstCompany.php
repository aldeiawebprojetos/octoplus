<?php

namespace Octoplus\Domain\Auth\Listeners;

use Illuminate\Support\Facades\Artisan;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Users\Models\Permission;

class CreateFirstCompany
{
    public function handle($event)
    {
        //Coloca as primeiras permissões necessárias para o usuário
        $permissions = Permission::where('name','LIKE', 'octoplus%')->pluck('id');
        $company = new Company;
        $company->fill(['name' => trans('general.personal_finances'),'email'=> $event->user->email]);
        $company->save();

        $event->user->permissions()->sync($permissions);
        $event->user->roles()->sync([1]);
        $event->user->companies()->syncWithoutDetaching($company->id);

        session()->put('tenant', $company->id);
        session()->put('company_id', $company->id);
        settings(['unitTypes' => ['un' => trans('general.unitTypes.un'),'kg' => trans('general.unitTypes.kg')]])->save();

        Artisan::call('db:seed', ['--class' => 'CurrencySeeder']);
    }
}