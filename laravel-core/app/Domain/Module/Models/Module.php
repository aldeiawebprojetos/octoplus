<?php

namespace Octoplus\Domain\Module\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'alias',
        'status'
    ];
}
