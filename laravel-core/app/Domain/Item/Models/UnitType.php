<?php

namespace Octoplus\Domain\Item\Models;

use Octoplus\App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;

class UnitType extends Model
{
    use ForTenants;

    protected $fillable = [
        'company_id',
        'name',
        'code',
    ];

    
}
