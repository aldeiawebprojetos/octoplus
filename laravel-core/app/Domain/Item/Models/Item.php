<?php

namespace Octoplus\Domain\Item\Models;

use Modules\SaleRent\Models\Invoice;
use Octoplus\Domain\Category\Models\Category;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Octoplus\App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Item extends Model
{
    use ForTenants, Mediable, LogsActivity, SoftDeletes;

    protected $fillable = [
        'company_id',
        'name',
        'sku',
        'code',
        'id',
        "rent_status",
        'description',
        'sale_price',
        'purchase_price',
        'quantity',
        'category_id',
        'enabled',
        'storage',
        'storage_min',
        'barcode',
        'unit_type',
        'item_type',
    ];

    protected static $logFillable = true;
    protected static $logName = 'general.item';
    protected static $logAttributes = ['categories'];
   

    public function categories()
    {
        return $this->belongsToMany(Category::class,'item_categories');
    }
    
    public function invoices()
    {
        return $this->belongsToMany(Invoice::class,'invoice_items');
    }
}
