<?php

namespace Octoplus\Http\Categories\Controllers;

use Illuminate\Http\Request;
use Octoplus\App\Controllers\Controller;
use Octoplus\Domain\Category\Models\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:octoplus.categories_create')->only(['create','store']);
        $this->middleware('can:octoplus.categories_update')->only(['edit','update']);
        $this->middleware('can:octoplus.categories_show')->only(['index']);
        $this->middleware('can:octoplus.categories_delete')->only(['delete']);
    }
    
    public function index(Request $request)
    {
        $query = Category::orderBy('name','ASC');
        
        if(isset($request->status))
            {
               $query->where('enabled',$request->status);
            }
        if(isset($request->category))
            {
                $query->where('parent_id', $request->category);
            }
        
        $categories_select = Category::all();
        $categories = $query->paginate();
        $types = categories_types();

        return view('categories.index',compact('categories','types','categories_select'));
    }

    public function create()
    {
        $categories = Category::all()->pluck('name','id');
        return view('categories.create',compact('categories'));
    }

    public function store(Request $request)
    {
       
       $category = Category::create($this->getCategoryArray($request));

        if($category)
        {
           if($request->wantsJson())
           {

                return response()->json($category);

           }else{

                sendNotify(trans('alerts.success').'!','success',trans_choice('alerts.item_created',2,['item' => trans("general.category")]));
                return redirect()->route('tenant.items.categories.index');
           
            }
        }
    }

    public function edit(Request $request){
        $category = Category::findOrFail($request->category_id);
        $categories = Category::all()->pluck('name','id');
        
        return view('categories.edit', compact('category','categories'));
    }

    public function show(Request $request){
        $category = Category::findOrFail($request->category_id);
        $categories = Category::all()->pluck('name','id');
        
        return view('categories.show', compact('category','categories'));
    }

    public function update(Request $request)
    {
        $category = Category::findOrFail($request->category_id);
        if($category->update($this->getCategoryArray($request)))
        {
            
            sendNotify(trans('alerts.success').'!','success',trans_choice('alerts.item_updated',2,['item' => trans("general.category")]));

            return redirect()->route('tenant.categories.index');
        }
    }

    public function delete(Request $request)
    {
       
        $category = Category::find($request->category_id);
       
        if($category->delete())
        {
            sendNotify(trans('alerts.success').'!','success' , trans_choice('alerts.item_deleted',2,['item' => trans('general.category')]));
            return redirect()->route('tenant.categories.index');
        }
    }

    private function getCategoryArray(Request $request)
    {
        $data = $request->all();
        $data['enabled'] = (empty($data['enabled']) ? 0 : 1);
        return $data;
    }

}