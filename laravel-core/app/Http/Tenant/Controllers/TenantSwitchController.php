<?php

namespace Octoplus\Http\Tenant\Controllers;

use Illuminate\Http\RedirectResponse;
use Octoplus\App\Controllers\Controller;
use Octoplus\Domain\Company\Events\CompanyUserLogin;
use Octoplus\Domain\Company\Models\Company;

class TenantSwitchController extends Controller
{
    /**
     * Switch tenant.
     *
     * @param Company $company
     * @return RedirectResponse
     */
    public function switch(Company $company)
    {
        event(new CompanyUserLogin(
            request()->user(),
            $company
        ));

        return redirect()->route('tenant.dashboard');
    }
}
