<?php

namespace Octoplus\Http\Api\Controllers\Account;

use Illuminate\Http\JsonResponse;
use Octoplus\App\Controllers\Controller;
use Octoplus\Domain\Account\Requests\DeactivateAccountRequest;

class DeactivateController extends Controller
{

    /**
     * Handle account deactivation.
     *
     * @param DeactivateAccountRequest $request
     * @return JsonResponse
     */
    public function store(DeactivateAccountRequest $request)
    {
        $user = $request->user();

        if ($user->subscribed('main')) {
            $user->subscription('main')->cancel();
        }

        $user->delete();

        return response()->json(null, 204);
    }
}
