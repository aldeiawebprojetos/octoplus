<?php

namespace Octoplus\Http\Api\Controllers\Auth;

use Illuminate\Http\Request;
use Octoplus\App\Controllers\Controller;
use Octoplus\Domain\Auth\Events\UserRequestedActivationEmail;
use Octoplus\Domain\Auth\Requests\ActivateResendRequest;
use Octoplus\Domain\Users\Models\User;
use Illuminate\Http\JsonResponse;

class ActivationResendController extends Controller
{

    /**
     * Resend activation link.
     *
     * @param ActivateResendRequest|Request $request
     * @return JsonResponse
     */
    public function store(ActivateResendRequest $request)
    {
        //find user
        $user = User::where('email', $request->email)->first();

        //send activation email
        event(new UserRequestedActivationEmail($user));

        return response()->json(null, 204);
    }
}
