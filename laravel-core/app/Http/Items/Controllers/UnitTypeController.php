<?php

namespace Octoplus\Http\Items\Controllers;

use Illuminate\Http\Request;
use Octoplus\App\Controllers\Controller;
use Octoplus\Domain\Item\Models\UnitType;

class UnitTypeController extends Controller
{
    public function index()
    {
        $unit_types = UnitType::paginate();
        

        return view('unitTypes.index', compact('unit_types'));
    }

    public function create()
    {
        return view('unitTypes.create');
    }

    public function store(Request $request)
    {
        if(UnitType::create($request->all()))
        {
            sendNotify(trans("alert.success"), 'success', trans_choice('alerts.item_created',2,['item' => trans("item.unit_type")]));
            return redirect()->route('tenant.items.unitTypes.index');
        }
    }
}
