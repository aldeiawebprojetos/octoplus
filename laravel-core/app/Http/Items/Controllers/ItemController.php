<?php

namespace Octoplus\Http\Items\Controllers;

use Illuminate\Http\Request;
use Octoplus\App\Controllers\Controller;
use Octoplus\Domain\Category\Models\Category;
use Octoplus\Domain\Item\Models\Item;
use Modules\SaleRent\Models\Invoice;
use Octoplus\Http\Items\Requests\ItemRequest;
use Illuminate\Support\Facades\DB;
use Octoplus\Domain\Item\Models\UnitType;
use Octoplus\Traits\Upload;
use Spatie\Activitylog\Models\Activity;
use Carbon\Carbon;

class ItemController extends Controller
{
    use Upload;
    
    public function __construct()
    {
        $this->middleware('can:octoplus.items_create')->only(['create','store']);
        $this->middleware('can:octoplus.items_update')->only(['edit','update']);
        $this->middleware('can:octoplus.items_show')->only(['index']);
        $this->middleware('can:octoplus.items_delete')->only(['delete']);
    }

    public function index(Request $request)
    {
        if($request->all() != null){
            $query = Item::orderBy('name');
            if(isset($request->category))
            {
                
                $query->whereHas('categories', function($cat) use($request){
                    $cat->where('categories.id', $request->category);
                });
            }

               $query->where('enabled',(isset($request->status) ? $request->status : true));

    
            if(isset($request->s))
            {
               $query->where('name',$request->s)->orWhere([['name','LIKE','%'.$request->s.'%']]);
            }

            if(isset($request->product_type))
            {
                $query->where('item_type',$request->product_type);
            }
    
            $items = $query->paginate()->withQueryString();
                  
        }else{
            $items = null;
        }
        $categories = Category::where('type','item')->pluck('name', 'id');
        return view('items.index', compact('items','categories'));
    }

    public function create(Request $request)
    {
        $item_type = (isset($request->item_type) ? $request->item_type : null);
        $categories = Category::where([['type','item'],['enabled',1]])->pluck('name', 'id');
        $unitTypes = settings('unitTypes',[]);

        
        return view('items.create', compact('categories','unitTypes','item_type'));
    }

    public function store(ItemRequest $request)
    {
        $data = $this->getItemArray($request);
        $data['code'] =  str_pad(settings('sequence.item',1) , 5 , '0' , STR_PAD_LEFT);
        $data['rent_status']  = true;
        
        $item = Item::create($data);
    
        if($item)
        {
            if($request->hasFile('image')){
                $this->uploadFile($item, $request);
            }  
             //incrementa a sequencia do código
             settings(['sequence.item' => settings('sequence.item',1) +1])->save();
             
            $item->categories()->attach($request->categories);

            sendNotify(trans('alerts.success').'!','success',trans_choice('alerts.item_created',2,['item' => trans("general.item")]));
           
            if($item->item_type == 'rent')
            {
                return redirect()->route('sale-rent.items.rent');
            }else{
                return redirect()->route('tenant.items.index');
            }

        }
    }

    public function edit(Request $request)
    {
        $item = Item::findOrFail($request->item_id);
        
        $categories = Category::where('type','item')->pluck('name', 'id');
        $unitTypes = UnitType::pluck('name','code');

        return view('items.edit', compact('item','categories','unitTypes'));
    }

    public function show(Request $request)
    {
        $item = Item::findOrFail($request->item_id);
        
        $categories = Category::where('type','item')->pluck('name', 'id');
        $unitTypes = UnitType::pluck('name','code');

        return view('items.show', compact('item','categories','unitTypes'));
    }

    public function update(ItemRequest $request)
    {
        $item = Item::findOrFail($request->item_id);

        if($item->update($this->getItemArray($request)))
        {
            if($request->hasFile('image')){
                $this->uploadFile($item, $request);
            }  

            $item->categories()->sync($request->categories);
            sendNotify(trans('alerts.success').'!','success',trans_choice('alerts.item_updated',2,['item' => trans("general.item")]));
            return redirect()->route('tenant.items.index');
        }
    }

    public function logItem(Request $request)
    {
        $activities = Activity::where([['subject_type', Item::class],['subject_id', $request->item_id]])->get();
        
        return view('logs.item', compact('activities'));
    }

    public function changeInvoiceStatus( Request $request )
    {

        if( $request->item_status == "delivered" )
            $itemStatus = 0;
        else
            $itemStatus = 1;

        $item = Item::findOrFail( $request->item_id );
        $item->rent_status = $itemStatus;
        $item->item_status = $request->item_status;
        DB::table('item_status_history')->insert([
            'data' => Carbon::now()->startOfDay()->toDateString(),
            'item_id' => $item->id,
            'invoice_id' => $request->pedido_id,
            'status' => $request->item_status,
            'usuario' => auth()->user()->first_name . ' ' . auth()->user()->last_name
        ]);
        $item->update();
        

        $items_id = DB::table('invoice_items')->where('invoice_id', $request->pedido_id)->pluck('item_id')->toArray();
        $items = Item::whereIn('id', $items_id)->where('item_status', '!=', $request->item_status)->pluck('rent_status')->toArray();
        if(count($items) == 0) {
            $pedido = Invoice::findOrFail( $request->pedido_id );
            $pedido->shipping_status = $request->item_status;
            $pedido->update();
        }
        
        
        return redirect()->route('sale-rent.items.rent');
    }

    public function searchItems(Request $request)
    {
        $query = Item::orderBy('name')->where([['name','LIKE','%' .$request->q.'%'],['enabled',true]])->limit(10);
      
        if($request->type != null){
            $query->where([['item_type', $request->type]]);

            if($request->type == 'rent')
            {
                $query->where('rent_status', true);
            }

            if($request->type == 'sale')
            {
                $query->where('storage','>','0');
            }
    
        }


        $items = $query->get()->filter(function($item){
            $item->name = $item->name . ' - ' . (settings('contract.category_as_name', true) ? $item->categories->last()->name : '');
            return $item;
        });
        
        return response()->json($items);
    }

    private function getItemArray(ItemRequest $request)
    {

        $data = $request->all();
        $SKU = $this->getSKU($data['categories']);
      
        $data['purchase_price'] = empty_to_null(format_money($data['purchase_price']));
        $data['sale_price'] = empty_to_null(format_money($data['sale_price']));
        $data['enabled'] = (isset($data['enabled']) ? 1 : 0);
        $data['rent_status'] = (isset($data['rent_status']) ? 1 : 0);
        $data['name'] = ($data['name'] != null ? $data['name'] : str_pad(settings('sequence.item',1) , 5 , '0' , STR_PAD_LEFT));
        $data['sku'] =  ($data['sku'] != null ?  $data['sku'] : $SKU);
       
        return $data;
    }

    private function getSKU(array $categories)
    {
        $category = Category::find($categories[array_key_last($categories)]);

        $sku = str_slug($category->name .'-' .str_pad(settings('sequence.item',1) , 5 , '0' , STR_PAD_LEFT),'-');

        return $sku;
    }

    public function delete(Request $request)
    {
        $item = Item::findOrFail($request->item_id);

        if( $item->rent_status == 1 )
        {
            if($item->delete())
            {
                sendNotify(trans('alerts.success').'!','success' , trans_choice('alerts.item_deleted',2,['item' => trans('general.item')]));
            }
        }
        else
            sendNotify( 'Item não deletado, pois esta com faturas em aberto' ,'error');

        return redirect()->route('tenant.items.index');

    }

    private function uploadFile($item, $request)
    {
        $media = $this->getMedia($request->file('image'), 'items');
        $item->syncMedia($media, 'product');
    }

}
