<?php

namespace Octoplus\Http\Items\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku',
            'description',
            'sale_price',
            'purchase_price',
            'rent_price',
            'quantity',
            'categories' => 'required',
            'enabled',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans_choice('validation.required',2,['general.name']),
        ];
    }
}
