<?php

namespace Octoplus\Http\Middleware;

use Closure;
use Octoplus\Events\Menu\ModuleSettingsCreated;

class SettingsMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if logged in
        if (!auth()->check()) {
            return $next($request);
        }
      
        menu()->create('settings', function ($menu) {
           
            $menu->style('neon-settings');

            event(new ModuleSettingsCreated($menu));
        });

        return $next($request);
    }
}
