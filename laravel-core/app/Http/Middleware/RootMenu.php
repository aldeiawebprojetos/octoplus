<?php

namespace Octoplus\Http\Middleware;

use Closure;
use Octoplus\Events\Menu\RootCreated;

class RootMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if logged in
        if (!auth()->check()) {
            return $next($request);
        }
      
        menu()->create('root', function ($menu) {
           
            $menu->style('neon');

            event(new RootCreated($menu));

        });

        return $next($request);
    }
}
