<?php

namespace Octoplus\Http\Middleware;

use Closure;
use Octoplus\Events\Menu\AdminCreated;

class AdminMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if logged in
        if (!auth()->check()) {
            return $next($request);
        }
      
        menu()->create('admin', function ($menu) {
           
            $menu->style('neon');

            event(new AdminCreated($menu));
        });

        return $next($request);
    }
}
