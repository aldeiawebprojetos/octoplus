<?php

namespace Octoplus\Http\Account\Controllers;

use Octoplus\App\Controllers\Controller;
use Octoplus\Domain\Account\Requests\ProfileStoreRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Show the user profile view.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        return view('account.profile.index');
    }

    /**
     * Store user's profile details in storage.
     *
     * @param ProfileStoreRequest|Request $request
     * @return RedirectResponse
     */
    public function store(ProfileStoreRequest $request)
    {
        //update user
        $request->user()->update($request->only(['first_name', 'last_name', 'username', 'email', 'phone']));

        //redirect with success
        return back()->with('success', 'Profile updated successfully.');
    }
}
