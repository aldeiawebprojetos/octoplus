<?php

namespace Octoplus\Http\Account\Controllers;

use Dotenv\Result\Success;
use Octoplus\App\Controllers\Controller;
use Illuminate\Http\Request;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Users\Models\Permission;
use Octoplus\Domain\Users\Models\RegraDeAcesso;
use Octoplus\Domain\Users\Models\User;

class PermissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:octoplus.assign_roles');
    
    }

    public function userPermission(Request $request)
    {
        $regra_de_acesso = RegraDeAcesso::all();
        $users = Company::find(session('tenant'))->users;
        $user = $users->where('id',$request->user_id)->first();


        $permissions = getCompanyPermissions();
       
        return view('permissions.userPermissions', compact('user', 'users','permissions','regra_de_acesso'));
    }

    public function setUserPermission(Request $request)
    {
        $user =  Company::find(session('tenant'))->users->where('id',$request->user_id)->first();

            if($request->rule_save)
            {
               $this->salvarRegraDeAcesso($request);
            }
            if($request->regra_acesso_id)
            {
                $user->roles()->sync([]);
                $this->definePermissaoDeRegraDeAcesso($user,$request);
            }elseif($request->flush_admin){
                $this->defineAdminPermissions($user);
            }else{
                $user->roles()->sync([]);
                $user->permissions()->sync($request->permissions);
            }
        

        sendNotify(trans('permissions.permissios_designed'), 'success',trans('alerts.success'));
        return redirect()->back();
    }

    private function salvarRegraDeAcesso(Request $request)
    {
        $request->validate(['rule_name' => 'required'],['rule_name.required' => trans('permissions.rule_name_required')]);

        $regra_de_acesso = RegraDeAcesso::create(['name' => $request->rule_name, 'company_id' => session('tenant')]);

        if($regra_de_acesso){
            $regra_de_acesso->permissions()->sync($request->permissions);
        }

        sendNotify(trans('permissions.rule_saved'), 'success',trans('alerts.success'));
    }

    private function definePermissaoDeRegraDeAcesso(User $user, Request $request)
    {
        $permissions = RegraDeAcesso::find($request->regra_acesso_id)->permissions->pluck('id');
        
        $user->permissions()->sync($permissions);
    }

    private function defineAdminPermissions(User $user)
    {
        $permissions = getCompanyPermissions(null,null)->pluck('id');
        
        $user->roles()->sync([1]);

        $user->permissions()->sync($permissions);
    }
    
}
