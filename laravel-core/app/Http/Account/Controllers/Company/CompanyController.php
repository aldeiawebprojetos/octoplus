<?php

namespace Octoplus\Http\Account\Controllers\Company;

use Illuminate\Contracts\Foundation\Application;
use Octoplus\Domain\Company\Models\Company;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Octoplus\Traits\Upload;
use Octoplus\App\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CompanyController extends Controller
{
    use Upload;
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|Response|View
     */
    public function index(Request $request)
    {
        $companies = $request->user()->companies;

        return view('account.companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        return view('account.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $company = new Company;
        $data = $request->only('name', 'email', 'phone', 'document', 'address');
        $data['document'] = str_replace('/','',str_replace('-','',str_replace('.','',$data['document'])));
        $company->fill($data);
        $company->save();

        if($request->hasFile('image'))
        {
            $this->uploadFile($company, $request);
        }

        $request->user()->companies()->syncWithoutDetaching($company->id);

        session()->put('company_id', $company->id);
        session()->put('tenant', session('company_id'));  
        
        settings(['perfix' => ['sale' => 'PV', 'rent'=> 'PA'],'sequence' => ['sale' => 1,'rent' => 1], 'unitTypes' => ['un' => trans('general.unitTypes.un')]])->save();
        
        Artisan::call('db:seed', ['--class' => 'CurrencySeeder']);

        return redirect()->route('tenant.switch', $company)
            ->with('success','Company created successfully.');
    }

    public function edit(Request $request)
    {
        $company = Company::find($request->company);

        return view('account.companies.edit', compact('company'));
    }

    public function update(Request $request)
    {
        $company = Company::find($request->company);
        $data = $request->all();
        $data['document'] = str_replace('/','',str_replace('-','',str_replace('.','',$data['document'])));
        
        if($request->hasFile('image'))
        {
            $this->uploadFile($company, $request);
        }
        if($company->update($data))
        {
            sendNotify(trans('general.company'),'success',trans_choice('alerts.item_updated',2,['item' => trans('general.company')]));
        }else{
            return redirect()->back();
        }

        return redirect()->route('account.companies.index');
    }

    private function uploadFile($item, $request)
    {
        $media = $this->getMedia($request->file('image'), 'logo');
        $item->syncMedia($media, 'logo');
    }
}
