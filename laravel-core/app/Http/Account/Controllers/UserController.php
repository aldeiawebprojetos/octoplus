<?php

namespace Octoplus\Http\Account\Controllers;

use Octoplus\Domain\Company\Models\Company;
use Octoplus\App\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Octoplus\Domain\Users\Models\RegraDeAcesso;
use Octoplus\Domain\Users\Models\User;
use Octoplus\Traits\Upload;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Str;
use Svg\Tag\Path;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class UserController extends Controller
{
    use Upload, SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('can:octoplus.users_create')->only(['create','store']);
        $this->middleware('can:octoplus.users_update')->only(['edit','update']);
        $this->middleware('can:octoplus.users_show')->only(['index']);
        $this->middleware('can:octoplus.users_delete')->only(['delete']);
    }

    public function index(Request $request): View
    {
        $company = Company::find(session('tenant'));
        $query = $company->users();
            if(isset($request->s)){
                $query
                    ->where('first_name', $request->s)
                    ->orWhere('first_name','LIKE','%'. $request->s.'%')
                    ->orWhere('last_name','LIKE','%'. $request->s.'%');
            }
            if(isset($request->status))
            {
               $query->where('enabled',$request->status);
            }
        $users = $query->paginate();

       
        return view('account.users.index', compact('users'));
    }

    public function create(): View
    {
        $regra_de_acesso = RegraDeAcesso::orderBy('name')->get();
        return view('account.users.create', compact('regra_de_acesso'));
    }

    public function store(Request $request)
    {
       $data = $this->executeFormValidation($request);

        $data['password'] = isset($data['password']) ? $data['password'] : bcrypt(Str::random(40)); 
       //Cadastra o usuário
        $user = User::create($data);

        if(isset($data['admin']) && $data['admin'])
        {
            $user->roles()->sync([1]);
        }else{
            $user->roles()->sync([]);
            if($request->regra_acesso_id)
            {
                $this->definePermissaoDeRegraDeAcesso($user,$request);
            }
        }
            //define o status do usuário
            $user->activated = isset($data['activated']);
            $user->save();

        settings(['company.userDescription.'.$user->id => isset($data['description']) ? $data['description'] : ''])->save();

       //Inclui o Usuário nas empresas selecionadas
       $user->companies()->attach($data['companies']);

       if(isset($data['send_link_email']))
       {
           $response = $this->broker()->sendResetLink($request->only('email'));

           $this->sendResetLinkResponse($request, $response);
          
       }

       if($request->hasFile('user_image')){
            $this->uploadFile($user, $request);
        }  


       sendNotify(trans_choice('alerts.item_created',2,['item' => trans("user.user")]), 'success', trans("alerts.success"));

       return redirect()->route('account.users.index');

    }

    public function edit(Request $request): View
    {
        $user = User::find($request->user_id);
        $regra_de_acesso = RegraDeAcesso::orderBy('name')->get();
       
        return view('account.users.edit', compact('user','regra_de_acesso'));
    }

    public function update(Request $request)
    {
        $user = User::find($request->user_id);
        $data = $this->executeFormValidation($request, $user);

        if($request->hasFile('user_image')){
            $this->uploadFile($user, $request);
        } 

        if($request->hasFile('assinatura_image')){
            $user->assinatura = base64_encode(file_get_contents($request->file('assinatura_image')));
            $user->save();
        } 
    
        if($user->update($data))
        {
            //define o status do usuário
            $user->activated = isset($data['activated']);
            $user->save();

                if(isset($data['admin']) && $data['admin'])
                {
                    $user->roles()->sync([1]);
                }else{
                    $user->roles()->sync([]);

                    if($request->regra_acesso_id)
                    {
                        $this->definePermissaoDeRegraDeAcesso($user,$request);
                    }
                }


                settings(['company.userDescription.'.$user->id => isset($data['description']) ? $data['description'] : ''])->save();

                //Inclui o Usuário nas empresas selecionadas
                $user->companies()->sync($data['companies']);

                if(isset($data['send_link_email']))
                {
                    $response = $this->broker()->sendResetLink($request->only('email'));

                    $this->sendResetLinkResponse($request, $response);
                   
                }

                sendNotify(trans_choice('alerts.item_updated',2,['item' => trans("user.user")]), 'success', trans("alerts.success"));
        }

        return redirect()->back();

    }

    public function logs(Request $request): View
    {
        $company = Company::find(session('tenant'));
        //prepara os dados para pesquisa
        $query = Activity::where([['causer_type', User::class],
                                ['causer_id', $request->user_id]])
                            ->orderBy('created_at','DESC');
                         
        if(isset($request->log_date))
        {
            $query->whereBetween('created_at',$this->formataRangeDate($request));
        }
        //Pega as atividades do usuário de acordo com o filtro aplicado, se aplicado
        $activities = $query->paginate();

        $users = $company->users;

        //Seleciona o usuário para exibição
        $user = $company
                ->users
                ->where('id', $request->user_id)
                ->first();
        
       
       return view('account.users.logs.index', compact('activities','user','users'));
    }

    public function delete(Request $request)
    {
        $user = User::find($request->user_id);
        $company = Company::find(session('tenant'));
        $user_company = $company->users->where('id', $user->id);

       if(!$user->hasRole('admin'))
       {
         if($user->delete())
         {
            sendNotify(trans('alerts.item_deleted', ['item' => trans('general.user')]), 'success',  trans("alerts.success"));

            return redirect()->back();
         }
         
         
       }else{
           sendNotify(trans('alerts.forbidden_action'), 'error',  trans("alerts.error"));
           return redirect()->back();
       }


    }

    private function executeFormValidation(Request $request, $user = null): array
    {
        if($request->send_link_email == null)
        {
            $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users,email,'.(isset($user) ? $user->id : null),
                'companies' => 'required',
                'password' => (isset($user) ? 'confirmed' : 'required|confirmed'),
                'phone' =>  ($request->phone == null ? '' : 'unique:users,phone,'.$request->user_id),
                'document_id' => ($request->document_id == null ?'' : 'unique:users,document_id,'.$request->user_id),
            ],
            [
               'password.confirmed' => trans_choice('validation.confirmed',2,['attribute' => trans('auth.password')]),
            ]);
            
        }else{
            $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users,email,'.$request->user_id,
                'phone' =>  ($request->phone == null ? '' : 'unique:users,phone,'.$request->user_id),
                'document_id' => ($request->document_id == null ?'' : 'unique:users,document_id,'.$request->user_id),
            ]);
        }
        return $this->getUserArray($request)->filter()->toArray();
    }

    private function formataRangeDate(Request $request): array
    {
        
        $dateFormated = array();
        $rangedateArray = explode(' - ', $request->log_date);

        $rangedateArray = array_map(function($value){
            
            return date('Y-m-d',strtotime(str_replace('/','-',$value)));
        }, $rangedateArray);

       //Formata a data para colocar a hora para o SQL ter precisão da busca

        $dateFormated = [$rangedateArray[0]." 00:00:00",
                         $rangedateArray[1]." 23:59:59"];

        return $dateFormated;
    }

    private function getUserArray(Request $request): Collection
    {
        $data = $request->all();

        $data['password'] = ($data['password'] == null ? null : bcrypt($data['password']));
        $data['activated'] = (isset($data['activated']) ? true : false);
        $data['admin'] = (isset($data['admin']) ? true : false);

        return collect($data);
    }

    private function definePermissaoDeRegraDeAcesso(User $user, Request $request)
    {
        $permissions = RegraDeAcesso::find($request->regra_acesso_id)->permissions->pluck('id');
        
        $user->permissions()->sync($permissions);
    }

    private function uploadFile($user, $request)
    {
        $media = $this->getMedia($request->file('user_image'), 'users',false);
        $user->syncMedia($media, 'users');
    }
}