<?php

namespace Octoplus\Http\Currencies\Controllers;

use Illuminate\Http\Request;
use Octoplus\App\Controllers\Controller;
use Octoplus\Domain\Currency\Tax;

class TaxController extends Controller
{
   public function store(Request $request)
   {
       $data = $this->getTaxArray($request);

       $tax = Tax::create($data);
       if($request->wantsJson())
       {

            return response()->json($tax);

       }else{
       
        }
   }

   public function searchTaxes(Request $request)
   {
       $taxes = Tax::where([['name','LIKE','%'.$request->q.'%'],['enabled', true]])->orderBy('name')->get();

        return response()->json($taxes);
   }

   private function getTaxArray(Request $request)
   {
       $data = $request->all();

       return $data;
   }
}
