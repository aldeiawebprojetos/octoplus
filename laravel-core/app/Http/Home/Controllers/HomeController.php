<?php

namespace Octoplus\Http\Home\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Octoplus\App\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Octoplus\Domain\Company\Models\Company;

class HomeController extends Controller
{
    /**
     * Show the application home page.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
      
        
        return view('home.index');
    }
}
