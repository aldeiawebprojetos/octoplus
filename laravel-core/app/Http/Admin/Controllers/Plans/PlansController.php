<?php

namespace Octoplus\Http\Admin\Controllers\Plans;

use Octoplus\Domain\Subscriptions\Models\Plan;
use Octoplus\App\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\View\View;


class PlansController extends Controller
{

    public function index()
    {
        $plans = Plan::paginate();
        
        return view('admin.plans.index', compact('plans'));
    }

    public function create()
    {
        return view('admin.plans.create');
    }

    public function store(Request $request)
    {
        if(Plan::create($request->merge(['gateway_id'=> str_slug($request->name, '_').$request->valor])->all()))
        {
            return redirect()->route('admin.plans.index');
        }
    }


    public function edit(Request $request){
        $plan = Plan::find($request->plan_id);

        return view('admin.plans.edit', compact('plan'));
    }

    public function update(Request $request)
    {
        $plan = Plan::find($request->plan_id);

        if($plan->update($request->merge(['gateway_id'=> str_slug($request->name, '_').$request->valor])->all()))
        {
            return redirect()->route('admin.plans.index');
        }
    }

}