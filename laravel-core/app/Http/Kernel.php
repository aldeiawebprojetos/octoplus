<?php

namespace Octoplus\Http;

use Octoplus\Http\Middleware\AbortIfHasNoPermission;
use Octoplus\Http\Middleware\AbortIfHasNoRole;
use Octoplus\Http\Middleware\Admin\Impersonate;
use Octoplus\Http\Middleware\AuthenticateRegister;
use Octoplus\Http\Middleware\ChecksExpiredConfirmationTokens;
use Octoplus\Http\Middleware\EncryptCookies;
use Octoplus\Http\Middleware\RedirectIfAuthenticated;
use Octoplus\Http\Middleware\Subscription\RedirectIfCancelled;
use Octoplus\Http\Middleware\Subscription\RedirectIfNotActive;
use Octoplus\Http\Middleware\Subscription\RedirectIfNotCancelled;
use Octoplus\Http\Middleware\Subscription\RedirectIfNotCustomer;
use Octoplus\Http\Middleware\Subscription\RedirectIfNoTeamPlan;
use Octoplus\Http\Middleware\Subscription\RedirectIfNotInactive;
use Octoplus\Http\Middleware\Subscription\RedirectIfNotSubscriptionOwner;
use Octoplus\Http\Middleware\Tenant\TenantConfigMiddleware;
use Octoplus\Http\Middleware\Tenant\TenantMiddleware;
use Octoplus\Http\Middleware\TrimStrings;
use Octoplus\Http\Middleware\TrustProxies;
use Octoplus\Http\Middleware\VerifyCsrfToken;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Auth\Middleware\Authorize;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use Illuminate\Foundation\Http\Middleware\ValidatePostSize;
use Illuminate\Http\Middleware\SetCacheHeaders;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Octoplus\Http\Middleware\AdminMenu;
use Octoplus\Http\Middleware\RootMenu;
use Octoplus\Http\Middleware\SettingsMenu;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        CheckForMaintenanceMode::class,
        ValidatePostSize::class,
        TrimStrings::class,
        ConvertEmptyStringsToNull::class,
        TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            EncryptCookies::class,
            AddQueuedCookiesToResponse::class,
            StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            ShareErrorsFromSession::class,
            VerifyCsrfToken::class,
            Impersonate::class,
            SettingsMenu::class,
           
        ],
        'admin' => [
            AdminMenu::class,
        ],
        'root' => [
            RootMenu::class,
        ],
        'tenant' => [
            TenantMiddleware::class,
            TenantConfigMiddleware::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => Authenticate::class,
        'auth.basic' => AuthenticateWithBasicAuth::class,
        'bindings' => SubstituteBindings::class,
        'cache.headers' => SetCacheHeaders::class,
        'can' => Authorize::class,
        'guest' => RedirectIfAuthenticated::class,
        'throttle' => ThrottleRequests::class,
        'confirmation_token.expired' => ChecksExpiredConfirmationTokens::class,
        'role' => AbortIfHasNoRole::class,
        'permission' => AbortIfHasNoPermission::class,
        'auth.register' => AuthenticateRegister::class,
        'subscription.active' => RedirectIfNotActive::class,
        'subscription.notcancelled' => RedirectIfCancelled::class,
        'subscription.cancelled' => RedirectIfNotCancelled::class,
        'subscription.customer' => RedirectIfNotCustomer::class,
        'subscription.inactive' => RedirectIfNotInactive::class,
        'subscription.team' => RedirectIfNoTeamPlan::class,
        'subscription.owner' => RedirectIfNotSubscriptionOwner::class,

        
    ];
}
