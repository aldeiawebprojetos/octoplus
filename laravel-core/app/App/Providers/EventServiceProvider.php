<?php

namespace Octoplus\App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Octoplus\Domain\Auth\Events\UserRequestedActivationEmail;
use Octoplus\Domain\Auth\Events\UserSignedUp;
use Octoplus\Domain\Auth\Listeners\CreateDefaultTeam;
use Octoplus\Domain\Auth\Listeners\CreateFirstCompany;
use Octoplus\Domain\Auth\Listeners\SendActivationEmail;
use Octoplus\Domain\Company\Listeners\CompanyUserEventSubscriber;
use Octoplus\Events\Menu\AdminCreated;
use Octoplus\Events\Menu\ModuleSettingsCreated;
use Octoplus\Events\Menu\RootCreated;
use Octoplus\Listeners\Menu\AddAdminItems;
use Octoplus\Listeners\Menu\AddRootItems;
use Octoplus\Events\Module\Installed;
use Octoplus\Listeners\Menu\AddSettingsItems;
use Octoplus\Listeners\Module\FinishInstallation;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Illuminate\Auth\Events\Login' => [
            'Octoplus\Listeners\Auth\Login',
        ],
        UserSignedUp::class => [
            CreateDefaultTeam::class,
            SendActivationEmail::class,
            CreateFirstCompany::class,
        ],
        UserRequestedActivationEmail::class => [
            SendActivationEmail::class,
        ],
        ModuleSettingsCreated::class => [
            AddSettingsItems::class,
        ],
        AdminCreated::class => [
            AddAdminItems::class,
        ],
        RootCreated::class => [ 
            AddRootItems::class, 
        ],
        Installed::class => [
            FinishInstallation::class,
        ],
        
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        CompanyUserEventSubscriber::class
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
