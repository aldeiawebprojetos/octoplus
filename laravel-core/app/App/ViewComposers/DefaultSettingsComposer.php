<?php
namespace Octoplus\App\ViewComposers;

use Illuminate\View\View;
use Octoplus\Domain\Account;
use Octoplus\Domain\Currency\Currency;

class DefaultSettingsComposer
{
   

    public function compose(View $view)
    {
        $settings = collect();
       
        $currency = Currency::where('code', 'BRL')->first();
        $account = Account::where('default', true)->first();
        $companySettings = [
            'dateFormat' => 'd/m/Y',
        ];
        $accountDefault = (!empty($account) ? $account : false);

        $view->with('currency', $currency);
        $view->with('accountDefault', $accountDefault);
        $view->with('companySettings',$companySettings);
    }
}