<?php

namespace Octoplus\App\Console\Commands;
use Modules\SaleRent\Models\Invoice as ModelsInvoice;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Modules\Financial\Models\Fatura;

class FaturaRecorrenteCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fatura_recorrente:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command FaturaRecorrente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $faturas = Fatura::where('recurrent', 1)
        ->where('data_vencimento', '=', Carbon::today()->toDateTimeString())
        ->where('payment_quantity', null)
        ->get();

        foreach($faturas as $fatura){
            $aux = clone $fatura;
            unset($aux->id);
            $aux->created_at = Carbon::today()->toDateTimeString();
            $aux->updated_at = $aux->created_at;
            $aux->data_competencia = $aux->created_at;
            switch ($aux->payment_frequency) {
                case "monthly":
                  $aux->data_vencimento = Carbon::today()->addMonths(1)->toDateTimeString();
                  break;
                case "weekly":
                  $aux->data_vencimento = Carbon::today()->addDays(7)->toDateTimeString();
                break;
                case "bimonthly":
                  $aux->data_vencimento = Carbon::today()->addMonths(2)->toDateTimeString();
                  break;
                case "quarterly":
                  $aux->data_vencimento = Carbon::today()->addMonths(3)->toDateTimeString();
                  break;
                case "semiannual":
                  $aux->data_vencimento = Carbon::today()->addMonths(6)->toDateTimeString();
                  break;
                case "yearly":
                  $aux->data_vencimento = Carbon::today()->addYear()->toDateTimeString();
                  break;
            }
            $aux->code = (explode('-', $aux->code)[0]) . '-' . strval(intval(explode('-', $aux->code)[1]) + 1);
            Fatura::create($aux->toArray());
        }
        return 0;
    }
}
