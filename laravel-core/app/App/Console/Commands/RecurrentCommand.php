<?php

namespace Octoplus\App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Financial\Models\Fatura;

class RecurrentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recurrent:verify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica se existe cadastro recorrente com prazo indefinido';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      
        $faturas = Fatura::where([['recurrent',true],
                                  ['status','!=','canceled'],
                                  ['payment_quantity',null],
                                  ['parent_id', null]
                                  ])->get();
      
        foreach($faturas as $fatura){
            $payment_frequencies = getPaymentFreqencyFormat($fatura->data_vencimento);

           if($fatura->faturas->count() >= 1)
           {
               $OldFatura = $fatura->faturas->last();
               if($OldFatura->data_vencimento->format('Y-m-d') == date('Y-m-d'))
               {
                $newFatura = $OldFatura->replicate();
                $code = explode('-', $newFatura->code);
               
                $newFatura->code = $code[0].'-'.($code[1]+1);
                $newFatura->data_vencimento = $payment_frequencies[$fatura->payment_frequency]($code[1]+1); 

               $newFatura->push();
               }
           }else{
               
             if($fatura->data_vencimento->format('Y-m-d') == date('Y-m-d'))
             {
                 $newFatura = $fatura->replicate();
                 $code = explode('-', $newFatura->code);
                
                 $newFatura->code = $code[0].'-'.($code[1]+1);
                 $newFatura->parent_id = $fatura->id;
                 $newFatura->data_vencimento = $payment_frequencies[$fatura->payment_frequency](2); 

                $newFatura->push();
                
             }
           }
        }
        return 0;
    }
}
