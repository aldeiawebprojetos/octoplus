<?php

use Illuminate\Support\Collection;
use Modules\Customers\Models\CustomerType;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Users\Models\Permission;

if (!function_exists('on_page')) {
    /**
     * Check's whether request url/route matches passed link
     *
     * @param $link
     * @param string $type
     * @return null
     */
    function on_page($link, $type = "name")
    {
        switch ($type) {
            case "url":
                $result = ($link == request()->fullUrl());
                break;

            default:
                $result = ($link == request()->route()->getName());
        }

        return $result;
    }
}

if (!function_exists('exists_in_filter_key')) {
    /**
     * Appends passed value if condition is true
     *
     * @param $key
     * @param $value
     * @return null
     */
    function exists_in_filter_key($key, $value = null)
    {
        return collect(explode("&", $key))->contains($value);
    }
}

if (!function_exists('join_in_filter_key')) {
    /**
     * Appends passed value if condition is true
     *
     * @param $value
     * @return null
     */
    function join_in_filter_key(...$value)
    {
        //remove empty values
        $value = array_filter($value, function ($item) {
            return isset($item);
        });

        return collect($value)->implode('&');
    }
}

if (!function_exists('remove_from_filter_key')) {
    /**
     * Appends passed value if condition is true
     *
     * @param $key
     * @param $oldValues
     * @param $value
     * @return null
     */
    function remove_from_filter_key($key, $oldValues, $value)
    {
        $newValues = array_diff(
            array_values(
                explode("&", $oldValues)
            ), [
            $value, 'page'
        ]);

        if (count($newValues) == 0) {
            array_except(request()->query(), [$key, 'page']);

            return null;
        }

        return collect($newValues)->implode('&');
    }
}

if (!function_exists('return_if')) {
    /**
     * Appends passed value if condition is true
     *
     * @param $condition
     * @param $value
     * @return null
     */
    function return_if($condition, $value)
    {

        if ($condition) {
            return $value;
        }
    }
}

if(!function_exists('module_enabled'))
{
    function module_enabled($alias): bool
    {
       
        $company = get_company();
       
        if(!$company){
            return false;
        }
      

        $module_installed = $company->modules()->where('alias', $alias)->first();
       
        if($company &&  $module_installed != null){    
           
            return  $module_installed->status;
        }else{
            return false;
        }

    }
}

if(!function_exists('categories_types'))
{
    function categories_types($forget = []): Collection
    {
        return collect([
            'expense'  => trans('general.expenses'),
            'income'   => trans('financial::general.incomes'),
            'item'     => trans('general.items'),
            'other'    => trans('general.others'),
        ])->forget($forget);
    }
}

if(!function_exists('frete_types'))
{
    function frete_types($keys_to_forget = []): Collection
    {
        return collect([
            'withdrawal'                 => trans('sale-rent::general.shipping_withdrawal'),
            'delivery'                   => trans('sale-rent::general.shipping_delivery'),
            'delivery_home'              => trans('sale-rent::general.shipping_delivery_home'),
        ])->forget($keys_to_forget);
    }
}


if(!function_exists('item_types'))
{
    function item_types(): Collection
    {
        return collect([
            'sale'      => trans('item.sale'),
            'rent'      => trans('item.rent'),
            'service'   => trans('item.service')
        ]);
    }
}

if(!function_exists('payemntMethod_types'))
{
    function payemntMethod_types($keys_to_forget = []): Collection
    {
        return collect([
            'in_cash'    => trans('general.in_cash'),
            'part'       => trans("general.part"),
            'recurrent'  => trans("general.recurrent")
        ])->forget($keys_to_forget);
    }
}

if(!function_exists('frequency_recurrent'))
{
    function frequency_recurrent(): Collection
    {
        return collect([
            'daily'        => trans('calendar.daily'),
            'weekly'       => trans('calendar.weekly'),
            'monthly'      => trans('calendar.monthly'),
            'bimonthly'    => trans('calendar.bimonthly'),
            'quarterly'    => trans('calendar.quarterly'),
            'semiannual'   => trans('calendar.semiannual'),
            'yearly'       => trans('calendar.yearly')
        ]);
    }
}

if(!function_exists('settings'))
{
    function settings($key = null, $default = null)
    {
        $setting = app('setting');
        $setting->setExtraColumns(['company_id' => session('tenant')]);

        if (is_null($key)) {
            return $setting;
        }

        if (is_array($key)) {
            $setting->set($key);

            return $setting;
        }

        return $setting->get($key, $default);

    }
}

if(!function_exists('get_company'))
{
    function get_company()
    {
        if(session()->has('tenant'))
        {
            return Company::find(session('tenant'));
        }else{
            return false;
        }
    }
}

if(!function_exists('typeOfPeople'))
{
    function typeOfPeople(): Collection
    {
        return collect([
            'f' => trans('general.pessoa.fisica'),
            'j' => trans("general.pessoa.juridica")
        ]);
    }
}

if(!function_exists('getCompanyPermissions'))
{
    function getCompanyPermissions($company = null, $callback = 'formatPermissionsArray'){
            $permissions = Permission::where('name','LIKE', 'octoplus%')->get();
            $company = Company::find((!empty($company) ? $company : session('tenant')));
            $modules = $company->modules->where("status",1);
            

           foreach($modules as $module)
           {
               $permissions = $permissions->merge(Permission::where('name','LIKE', $module->alias.'%')->get());
           } 
           
            return ($callback !== null ? $callback($permissions) : $permissions);
            
    }
}

if(!function_exists('typeOfCustomer'))
{
    function typeOfCustomer(): Collection
    {
        $types = collect();

        $typeCustomers = CustomerType::all();

        foreach($typeCustomers as $typeCustomer)
        {
            $types->push([
                'id' => $typeCustomer->id,
                'name' => trans($typeCustomer->name)
            ]);
        }

        return $types->pluck('name','id');
       
    }
}

if(!function_exists('formatPermissionsArray'))
{
    function formatPermissionsArray($permissions): Collection
    {
        $data = collect();
        $formatedPermissions = collect();

       foreach($permissions as $permission)
       {
           $module = explode('.',$permission->name);
           $params = explode('_', $module[1]);

           $data->push(['module' => $module[0], 'action' => $params[1], 'permission' => $params[0],'id' => $permission->id]);
       }
       
       foreach($data->groupBy('module') as $key => $formated)
       {
        $formatedPermissions = $formatedPermissions->merge([$key => $formated->groupBy('permission')]);
       }

        return $formatedPermissions;
    }
}

if(!function_exists('sendNotify'))
{
    function sendNotify($message, $type = null,$title = null)
{
    //Cria a session da Notificação
    session()->flash('notify', [
        'title' => ($title ? $title : trans('alerts.sistem_info')),
        'type' => ($type ? $type : 'info'),
        'message' => $message,
        ]);

}
}

if(!function_exists('version'))
{
    function version(): string
    {
        
        return config('version.major').'.'. config('version.minor').'.'.config('version.patch') . (config('version.build') != 'stable' ? '-'.config('version.build') : null);
    }
}

if(!function_exists('format_money'))
{
    function format_money($valor)
    {
        return str_replace(['.', ','], ['', '.'], $valor);
    }
}

if(!function_exists('empty_to_null'))
{
    function empty_to_null($value)
    {
        return is_string($value) && $value === '' ? null : $value;
    }
}

if(!function_exists('formatar_cpf_cnpj'))
{
    	
    function formatar_cpf_cnpj($doc) {
	
 
	
        $doc = preg_replace("/[^0-9]/", "", $doc);
	
        $qtd = strlen($doc);
	
 
	
        if($qtd >= 11) {
	
 
	
            if($qtd === 11 ) {
	
 
	
                $docFormatado = substr($doc, 0, 3) . '.' .
	
                                substr($doc, 3, 3) . '.' .
	
                                substr($doc, 6, 3) . '-' .
	
                                substr($doc, 9, 2);
	
            } else {
	
                $docFormatado = substr($doc, 0, 2) . '.' .
	
                                substr($doc, 2, 3) . '.' .
	
                                substr($doc, 5, 3) . '/' .
	
                                substr($doc, 8, 4) . '-' .
	
                                substr($doc, -2);
	
            }
	
 
	
            return $docFormatado;
	
 
	
        } else {
	
            return $doc;
	
        }
	
    }
}

if(!function_exists('masc_tel'))
{
    function masc_tel($TEL) {
        $tam = strlen(preg_replace("/[^0-9]/", "", $TEL));
          if ($tam == 13) { // COM CÓDIGO DE ÁREA NACIONAL E DO PAIS e 9 dígitos
          return "+".substr($TEL,0,$tam-11)."(".substr($TEL,$tam-11,2).")".substr($TEL,$tam-9,5)."-".substr($TEL,-4);
          }
          if ($tam == 12) { // COM CÓDIGO DE ÁREA NACIONAL E DO PAIS
          return "+".substr($TEL,0,$tam-10)."(".substr($TEL,$tam-10,2).")".substr($TEL,$tam-8,4)."-".substr($TEL,-4);
          }
          if ($tam == 11) { // COM CÓDIGO DE ÁREA NACIONAL e 9 dígitos
          return "(".substr($TEL,0,2).")".substr($TEL,2,5)."-".substr($TEL,7,11);
          }
          if ($tam == 10) { // COM CÓDIGO DE ÁREA NACIONAL
          return "(".substr($TEL,0,2).")".substr($TEL,2,4)."-".substr($TEL,6,10);
          }
          if ($tam <= 9) { // SEM CÓDIGO DE ÁREA
          return substr($TEL,0,$tam-4)."-".substr($TEL,-4);
          }
      }
}