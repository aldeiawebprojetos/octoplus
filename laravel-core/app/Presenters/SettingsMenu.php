<?php

namespace Octoplus\Presenters;

use Akaunting\Menu\Presenters\Presenter;
use Illuminate\Support\Str;

class SettingsMenu extends Presenter{
    public function getOpenTagWrapper()
    {
        return '';
    }

    public function getCloseTagWrapper()
    {
        return '';
    }

    public function getMenuWithoutDropdownWrapper($item)
    {
        return '<li class=" ">
			<a href="' . $item->getUrl() . '" ' . $item->getAttributes() . '>'
        . $item->getIcon() .  $item->title . '</a></li>' . PHP_EOL;
    }

    public function getActiveState($item, $state = ' active')
    {
        return $item->isActive() ? $state : null;
    }

    public function getActiveStateOnChild($item, $state = 'active opened')
    {
        return $item->hasActiveOnChild() ? $state : null;
    }

    public function getActiveStateChild($item, $state = 'visible')
    {
        return $item->hasActiveOnChild() ? $state : null;
    }

    public function getDividerWrapper()
    {
        return '<li class="divider"></li>';
    }

    public function getHeaderWrapper($item)
    {
        return '<li class="dropdown-header">' . $item->title . '</li>';
    }

    public function getMenuWithDropDownWrapper($item)
    {
        $id = Str::random();

        return '
		<li class="' . $this->getActiveState($item) . ' has-sub ">
			<a  href="#' . $id . '">
				' . $item->getIcon() . '<span class="title"> ' . $item->title . ' </span>
			</a>
			<ul id="' . $id . '" class=" ' . $this->getActiveStateChild($item) . '">
				' . $this->getChildMenuItems($item) . '
			</ul>
		</li>
		' . PHP_EOL;
    }
    public function getMultiLevelDropdownWrapper($item)
    {
        return $this->getMenuWithDropDownWrapper($item);
    }

}