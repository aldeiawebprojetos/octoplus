<?php

namespace Octoplus\Listeners\Module;

use Octoplus\Events\Module\Installed as Event;
use Octoplus\Traits\Permissions;
use Artisan;

class FinishInstallation
{
    use Permissions;

    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $module = module($event->alias);

        Artisan::call('migrate', ['--force' => true]);

        $this->attachDefaultModulePermissions($module);
    }
}
