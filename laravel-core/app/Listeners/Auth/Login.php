<?php

namespace Octoplus\Listeners\Auth;

use Illuminate\Auth\Events\Login as Event;
use Octoplus\Domain\Company\Events\CompanyUserLogin;

class Login
{
    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $user = auth()->user();
        $company = ($user->lastAccessedCompany != null ? $user->lastAccessedCompany :  $user->companies->first());
      
            if($company != null)
            {
                event(new CompanyUserLogin(
                    $user,
                     $company
                 ));

                 session()->put('tenant',  $company->id);
            }
        
    }
       
    
}
