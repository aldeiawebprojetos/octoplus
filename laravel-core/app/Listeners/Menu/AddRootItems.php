<?php

namespace Octoplus\Listeners\Menu;

use Octoplus\Events\Menu\RootCreated as Event;

class AddRootItems
{
    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $menu = $event->menu;

        $user = auth()->user();

       

        $menu->dropdown(trim(trans_choice('user.users', 2)), function ($sub) use ($user) {

            $sub->route('admin.users.create', trans_choice('general.add', 2,['item'=> trans('user.user')]), [], 20, [ 'icon'=>'fas fa-user-plus']);

            if($user->hasRole('admin-root'))
            {
                $sub->route('admin.users.impersonate.index', trans('admin.impersonate'), [], 20, ['icon' => 'fas fa-theater-masks']);
            }

            $sub->route('admin.users.index', trans('user.users'),[],20,[ 'icon' => 'fas fa-users']);

        }, 30, [
            'title' => trans_choice('user.users', 2),
            'icon' => 'fas fa-users',
        ]);


        $menu->dropdown(trim(trans_choice('admin.permissions', 2)), function ($sub) use ($user) {
            $sub->route('admin.permissions.index', trans('admin.permissions'),[],20,['icon' => 'fas fa-flag']);
            $sub->route('admin.permissions.create', trans_choice('general.add', 2, ['item' => trans('admin.permissions')]), [],20, ['icon' => 'fas fa-plus-circle']);
        }, 30, [
            'title' => trans_choice('admin.permissions', 2),
            'icon' => 'fas fa-flag',
        ]);

        $menu->dropdown(trim(trans_choice('admin.roles', 2)), function ($sub) use ($user) {

            $sub->route('admin.roles.index', trans('admin.roles'),[],20,['icon' => 'fas fa-user-lock']);
            $sub->route('admin.roles.create', trans_choice('general.add', 2, ['item' => trans('admin.roles')]), [],20, ['icon' => 'fas fa-plus-circle']);

        }, 30, [
            'title' => trans_choice('admin.roles', 2),
            'icon' => 'fas fa-user-lock',
        ]);

        $menu->dropdown(trim(trans_choice('general.plan', 2)), function ($sub) use ($user) {

            $sub->route('admin.plans.index', trans('general.plan'),[],20,['icon' => 'fas fa-hand-holding-usd']);
            $sub->route('admin.plans.create', trans_choice('general.add', 2, ['item' => trans('general.plan')]), [],20, ['icon' => 'fas fa-plus-circle']);

        }, 30, [
            'title' => trans_choice('admin.roles', 2),
            'icon' => 'fas fa-hand-holding-usd',
        ]);
        
    }
       
    
}
