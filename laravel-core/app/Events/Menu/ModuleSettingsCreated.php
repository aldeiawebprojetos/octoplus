<?php

namespace Octoplus\Events\Menu;

use Illuminate\Queue\SerializesModels;

class ModuleSettingsCreated
{
    use SerializesModels;

    public $menu;

    /**
     * Create a new event instance.
     *
     * @param $menu
     */
    public function __construct($menu)
    {
        $this->menu = $menu;
    }
}
