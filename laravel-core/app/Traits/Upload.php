<?php

namespace Octoplus\Traits;

use MediaUploader;

trait Upload{

    public function getUploadedFilePath($file, $folder = 'settings', $company_id = null)
    {
        $path = '';

        if (!$file || !$file->isValid()) {
            return $path;
        }

        if (!$company_id) {
            $company_id = session('tenant');
        }

        $file_name = $file->getClientOriginalName();

        // Upload file
        $file->storeAs($company_id . '/' . $folder, $file_name);

        // Prepare db path
        $path = $folder . '/' . $file_name;

        return $path;
    }

    public function getMedia($file, $folder = 'settings', $is_tenantable = true ,$company_id = null)
    {
        $path = '';

        if (!$file || !$file->isValid()) {
            return $path;
        }

        if (!$company_id) {
            $company_id = session('tenant');
        }

        $path = ($is_tenantable ? 'companies/' . $company_id . '/' . $folder : $folder);

        return MediaUploader::fromSource($file)->toDirectory($path)->upload();
    }

    public function importMedia($file, $folder = 'settings', $company_id = null, $disk = null)
    {
        $path = '';

        if (!$disk) {
            $disk = config('mediable.default_disk');
        }

        if (!$company_id) {
            $company_id = session('tenant');
        }

        $path = $company_id . '/' . $folder . '/' . basename($file);

        return MediaUploader::importPath($disk, $path);
    }
}