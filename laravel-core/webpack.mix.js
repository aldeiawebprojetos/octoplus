let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

    mix.setPublicPath('../public_html/').js('resources/js/app.js', '../public_html/js')
    .sass('resources/sass/app.scss', '../public_html/css');


 mix.setPublicPath('../public_html/').js('resources/js/admin.js', '../public_html/js')
    .js('resources/js/items.js', '../public_html/js')
    .sass('resources/sass/admin/admin.scss', '../public_html/css')

    .js('modules/SaleRent/Resources/assets/js/invoices.js', 'js/sale-rent')
    .js('modules/SaleRent/Resources/assets/js/itens.js', 'js/sale-rent')
    .js('modules/SaleRent/Resources/assets/js/rent.js', 'js/sale-rent')

    .js('modules/Financial/Resources/assets/js/incomes.js', 'js/financial')
    .js('modules/Financial/Resources/assets/js/accounts.js', 'js/financial')
    
    .js('modules/Customers/Resources/assets/js/customers.js', 'js/customers');

var LiveReloadPlugin = require('webpack-livereload-plugin');

mix.setPublicPath('../public_html/').webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});