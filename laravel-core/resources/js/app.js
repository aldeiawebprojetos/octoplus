
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import money from 'v-money';

import VueTimeago from 'vue-timeago';
import Autocomplete from 'vuejs-auto-complete'
import VueTheMask from 'vue-the-mask'


Vue.use(VueTimeago, {
    name: 'timeago', // component name, `timeago` by default
    locale: 'en-US',
    locales: {
        // you will need json-loader in webpack 1
        'en-US': require('vue-timeago/locales/en-US.json')
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.use(money, {precision: 2});

Vue.use(VueTheMask)

Vue.filter('maskMoney', function (value, thousand = '.', decimal = ',',simbol = 'R$', simbol_first = true) {

    let val = value.toFixed(2).replace('.', decimal);

   return (simbol_first ? simbol + '' : '') + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousand) + (!simbol_first ? '' + simbol : '');
    
  });

  
    
  Vue.component('tax-select',
  require('./components/typeahead/tax.vue')
  );

  Vue.component('customer-select',
    require('./components/typeahead/customers.vue')
    );


Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component('Autocomplete',Autocomplete);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

Vue.component('item-typeahead',
require('./components/typeahead/item.vue')
);

window.goToEdit = async (url) => {
        await sleep(10);
 
       window.location.href = url;

    }

window.confirmDelete = (item_name, item_route) => {
    var item_tag = document.getElementById('item-name'), 
    form_delete = document.getElementById('form-delete');

    form_delete.removeAttribute("action");
    form_delete.setAttribute('action', item_route);

    item_tag.innerHTML = item_name;
    
        function fallbackDelete(item_name, form_delete)
        {
            console.error('Erro, jQuery Não instanciado');
            var confirm = window.confirm(`Você realmente deseja apagar ${item_name}`);

            if(confirm)
            {
                form_delete.submit();
            }
        }

        if(typeof jQuery == 'function')
        {
            jQuery.noConflict(true);
           
            if(typeof jQuery == 'function')
            {
                jQuery('#delete-modal').modal('show');
            }else{
                fallbackDelete(item_name, form_delete)
            }
            
        }else{
            fallbackDelete(item_name, form_delete)
        }
}

window.getConsulta = (path,query,element) =>
{
    const formatQuery = (query,element) => {
        const queryArray = query.split('&');
            for(var i in queryArray)
            {
                if(queryArray[i].includes(element.name) > 0)
                {
                   
                    queryArray.splice(i, 1);
                }
            }
           
            return queryArray.join('&');
    };
    const queryFormated = formatQuery(query,element);
   
    const searchUrl = `${baseurl}/${path}?${queryFormated}&${element.name}=${element.value}`;
    window.location.href = searchUrl;
};

window.sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
  };

window.chkAll = (class_item) => {
    $(`.chkbx_${class_item}`).prop('checked', true); 
};

window.unchkAll = (class_item) => {
    $(`.chkbx_${class_item}`).prop('checked', false); 
};

window.changeChkboxes = (element, class_items) =>
{
    const replaced = $('.checkbox-replace');

    
    if(element.checked)
    {
        replaced.addClass('checked');
        chkAll(class_items);
    }else{
        replaced.removeClass('checked');
        unchkAll(class_items);
    }
};

window.changeIdUrl = (path,element) => {
  const url = baseurl +'/'+ path.replace(/\d+/g, element.value)
  
  window.location.href = url;
};



