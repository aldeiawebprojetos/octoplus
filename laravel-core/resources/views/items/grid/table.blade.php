<table class="table table-bordered table-striped datatable" id="table-4">
    <thead>
        <tr>
            <th >{{ trans('general.actions') }}</th>
            <th width="2%">{{trans('general.code')}}</th>
            <th>{{trans("general.name")}}</th>
            <th>{{ trans('item.storage') }}</th>
            <th>{{ trans('general.status') }}</th>
            <th width="1%" class="text-center"> 
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_item" onchange="changeChkboxes(this,'item')"/>
                </div>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($items as $item)
        <tr class="odd gradeX">
            <td width="2%"  class="text-center">
                <!-- Actoins -->
                @include('common.tableActions',['options' => [
                    ['route' => ['tenant.items.edit', true],'permission' => 'octoplus.items_update','text'=> trans('general.edit'),'icon' => 'entypo-pencil']
                ],'name' => $item->name,'id' => $item->id, 'route_delete'=>'tenant.items.delete','delete_permission' => 'octoplus.items_delete'])
            </td>
           
            <td onclick="goToEdit('{{ route('tenant.items.show',$item->id) }}')" class="center"> {{$item->id}} </td>
            <td onclick="goToEdit('{{ route('tenant.items.show',$item->id) }}')"> {{$item->name}} </td>
            <td onclick="goToEdit('{{ route('tenant.items.show',$item->id) }}')">{{$item->storage}}</td>
            <td onclick="goToEdit('{{ route('tenant.items.show',$item->id) }}')" class="center">
                {{ $item->enabled == 1 ? trans('general.active') : trans('general.inactive') }}</td>
            
                <td class="text-center">
                    <div class="checkbox checkbox-replace">
                        <input type="checkbox" class="chkbx_item" value="{{$item->id}}"/>
                    </div>
                </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th >{{ trans('general.actions') }}</th>
            <th width="2%">{{trans('general.code')}}</th>
            <th>{{trans("general.name")}}</th>
            <th>{{ trans('item.storage') }}</th>
            <th>{{ trans('general.status') }}</th>
           
            <th width="1%" class="text-center"> 
                <div class="checkbox checkbox-replace">
                    <input class="chkbx_item" type="checkbox"  onchange="changeChkboxes(this,'item')"/>
                </div>
            </th>
        </tr>
    </tfoot>
</table>

