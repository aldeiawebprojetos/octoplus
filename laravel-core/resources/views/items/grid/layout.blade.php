<div class="gallery-env">
    <div class="row">
        @foreach ($items as $item)
        <div class="col-sm-3 col-xs-6">
            <article class="album">
                <section class="album-info text-center">
                    <h3> <a href="{{route('tenant.items.edit',$item->id)}}">{{$item->name}} {{(settings('contract.category_as_name', true) ? '- '.$item->categories->last()->name : '')}} </a> </h3>
                    <h3> #{{$item->id}} </h3>
                </section>
                
                <header>
                    <a href="{{route('tenant.items.edit',$item->id)}}">
                        <img class="item-disponivel"  src="{{($item->firstMedia('product') != null ? $item->firstMedia('product')->getUrl() : asset('img/success-item.svg'))}}" alt="">
                    </a>
                    
                    <a href="{{route('tenant.items.edit',$item->id)}}" class="album-options">
                        <i class="entypo-pencil"></i>
                        {{trans('general.edit')}}
                    </a>
                </header>
                
                <footer>
                    
                                                        
                    <div class="album-options">
                        <a class="btn btn-lg" href="#" title="Marcar Item como Entregue: Quando o item locado já foi enviado e entregue">
                            <i class="fa fa-truck"></i>
                        </a>
                        
                        <a class="btn btn-lg" href="#" title="Marcar Item como Disponível: Quando o contrato vence, e o produto fica novamente disponível.">
                            <i class="fa fa-check-square"></i>
                        </a>
                    </div>
                    
                </footer>
                
            </article>
        </div>
        @endforeach
    </div>
</div>