@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('general.items')}}</a> </li>

<li class="active"> <strong>{{trans('general.listing')}}</strong> </li>	
@endsection

@section('title',trans('general.item'))
    

@section('content')
<div class="mail-env">
    <div class="mail-body">
				
        <div class="mail-header">
            <!-- Search  form -->
            
            <form method="get" role="form" class="search-form-full">
                <div class="form-group">
                    
                    <input type="text" class="form-control input-lg" value="{{ Request::query('s') }}" name="s" id="search-input" placeholder="Pesquise pelo Nome, código..." />
                    <i class="entypo-search"></i>
                </div>
                
            </form>
        </div>

        <div class="mail-header">
            <!-- title -->
            <h1 class="mail-title">
                {{ trans('general.items') }}
                <span class="count text-info">({{isset($items) ? $items->count() : ''}})</span>

                <div class="btn-group" role="group" aria-label="Escolha o formato de ver os dados">
                      <a class="btn btn-secondary {{((Request::query('grid_type') == 'default' || Request::query('grid_type') == null) ? 'active' : null)}}" href=''> <i class="entypo-list"></i> </a>

                      <a class="btn btn-secondary {{(Request::query('grid_type') == 'layout' ? 'active' : null)}}" href='?'> <i class="entypo-layout"></i> </a>
                </div>

            </h1>
                      

            <!-- Filtrar por Categoria Produto -->					
            <div class="mail-search">
                {{ Form::select('category', $categories, null,['class' => 'form-control','onchange'=>'getConsulta("'. Request::path() .'","'.http_build_query(Request::query()).'",this)','placeholder'=>trans('general.select').'...']) }}
            </div>
            <!-- botão apagar para bulkactions ainda não implementado -->					
            {{-- <div class="pull-left">				
                <a href="#" class="btn btn-danger pull-left">
                    <i class="entypo-trash"></i>
                </a>
            </div> --}}

        </div>
        

        @if(isset($items))
            @include('items.grid.table')

            {{ $items->links() }}
        @endif
    </div>

    <div class="mail-sidebar">
				
        <!-- compose new email button -->
        <div class="mail-sidebar-row hidden-xs">
            @can('octoplus.items_create')
                <a href="{{route('tenant.items.create')}}" class="btn btn-blue btn-lg btn-icon btn-block">
                    {{trans_choice('general.add',2,['item' => trans('general.item')])}}
                    <i class="entypo-plus"></i>
                </a>
            @endcan
        </div>
       

        <!-- menu -->
        <ul class="mail-menu">
            @foreach (item_types() as $key => $type)                   
            <li class="">
                <a href="?{{ http_build_query(collect(['product_type' => $key])->merge(collect(Request::query())->forget(['product_type']))->toArray()) }}">
                 
                    {{ item_types()[$key] }} <span class="badge badge-gray pull-right">{{isset($items) ? $items->where('item_type',$key)->count() : ''}}</span>
                </a>
            </li>
            @endforeach
        
        </ul>
        
        <div class="mail-distancer"></div>
        
        <h4>{{ trans('general.status') }}</h4>
       
        <!-- menu -->
        <ul class="mail-menu">
            <li class="">
                <a href="?{{ http_build_query(collect(['status' => 0])->merge(collect(Request::query())->forget(['status']))->toArray()) }}">
                    <span class="badge badge-danger badge-roundless pull-right"></span>
                    {{ trans('general.inactive') }}
                </a>
            </li>

            <li class="">
                <a href="?{{ http_build_query(collect(['status' => 1])->merge(collect(Request::query())->forget(['status']))->toArray()) }}">
                    <span class="badge badge-success badge-roundless pull-right"></span>
                    {{ trans('general.active') }}
                </a>
            </li>
        </ul>
        
    </div>
    
</div>

@endsection

@section('modals')
@include('common.modals.delete',['type' => trans('general.items')])
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>

<script type="text/javascript">

    jQuery( document ).ready( function( $ ) {
        var $table4 = $("#table-4");
        
        $table4.DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "paging":   false
        } );

        // Highlighted rows
        $table4.find( "tbody input[type=checkbox]" ).each(function(i, el) {
            var $this = $(el),
                $p = $this.closest('tr');
            
            $( el ).on( 'change', function() {
                var is_checked = $this.is(':checked');
                
                $p[is_checked ? 'addClass' : 'removeClass']( 'highlight' );
            } );
        } );
        
        // Replace Checboxes
        $table4.find( ".pagination a" ).click( function( ev ) {
            replaceCheckboxes();
        } );


    } );	
    
</script>
@endsection

