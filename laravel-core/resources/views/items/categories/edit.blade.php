@extends('layouts.app')

@section('title',  trans('general.categories') )

@section('breadcrumb')
<li> <a href="{{route('tenant.categories.index')}}">{{trans('general.categories')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.edit',2,['item' => ''])}}</strong> </li>	
@endsection

@section('content')
{{ Form::model($category,['method'=>'PUT', 'route' => [(isset($routeCategoryUpdate) ? $routeCategoryUpdate : 'tenant.categories.update'),$category->id]]) }}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('general.categories')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

   @include('common.buttonsForm',['route_return' => 'tenant.categories.index'])
</div>

@include('categories.form',['type' => 'item'])

<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'tenant.items.index'])
</div>
{{ Form::close() }}
@endsection