@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('general.items')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.edit',2,['item' => trans('general.item')])}}</strong> </li>	
@endsection

@section('title', trans('general.items')  .' / '.  trans('general.products') )

@section('content')
{{ Form::model($item, ['method'=>'PUT', 'route' => ['tenant.items.update',$item->id],'enctype'=>'multipart/form-data']) }}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('general.items')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

   @include('common.buttonsForm',['route_return' => 'tenant.items.index', 'route_delete' => 'tenant.items.delete','id' =>$item->id, 'name'=> $item->name])
</div>

@include('items.form')


<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'tenant.items.index', 'route_delete' => 'tenant.items.delete','id' =>$item->id, 'name'=> $item->name])
</div>
{{ Form::close() }}
@endsection

@section('modals')
 @include('common.modals.delete', ['type' => trans('general.item')])
 @include('common.modals.addcategory', ['type' => trans('general.item')])
@endsection
