@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('general.items')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.edit',2,['item' => trans('general.item')])}}</strong> </li>	
@endsection

@section('title', trans('general.items')  .' / '.  trans('general.products') )

@section('content')
{{ Form::model($item, []) }}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('general.items')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

   
</div>

@include('items.form', ['readOnly' => true])


<div class="row">
    <div class="col-md-6"></div>
  
</div>
{{ Form::close() }}
@endsection

