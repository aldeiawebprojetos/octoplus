<div class="row">
    <div class="form-group col-md-2 col-xs-4">
      {{ Form::label('cod_product', trans('general.code')) }}
      {{ Form::text('cod_product', (isset($item->id) ? '#'. $item->id : null), ['class' => 'form-control input-lg', 'readonly']) }}
    </div>

    <div class="form-group col-md-6 col-xs-8">

      {{ Form::label('name', trans('general.name') ) }} 
      {{ Form::text('name', null, ['class'=> 'form-control input-lg', 'placeholder'=> 'Digite o nome do Item',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
    
    </div>

    <div class="form-group col-md-4">
      {{ Form::label('item_type', trans("item.type"))}} <strong class="text-danger">*</strong>
	  {{ Form::select('item_type', item_types(), ( isset( $item_type ) ? $item_type : null ), ['class' => 'selectboxit input-lg','placeholder' => trans('general.select').'...','onchange'=>'display_details(this.value)',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
	  <span class="description"> <i class="fa fa-cogs"></i> {{ trans('item.type_details') }}. </span>
    </div>


  <!-- ;) -->


  <div class="form-group col-md-6 col-xs-5">

      {{ Form::label('categories', trans('general.category')) }} <strong class="text-danger">*</strong>
      {{ Form::select('categories[]', $categories, null,['class' => 'select2 form-control input-lg','multiple','id'=>'categories',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}

      <div class="btn-group">
          <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
          <i class="fa fa-plus-square"></i>	 <span class="caret"></span>
          </button>
          <ul class="dropdown-menu dropdown-blue" role="menu">
              <li>
                  <a href="javascript:void(0);" onclick="$('#categoryAdd').modal('show');" class="">{{trans_choice('general.add',2,['item'=>trans('general.category')])}}</a>
              </li>
            
          </ul>
      </div>
  </div>


  <div class="form-group col-md-4 col-xs-4">
        
		<label for="sale_price">{{trans('general.price.value')}} {{trans('general.of')}} <span id="label_sale_price">{{trans('item.sale')}}</span></label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fas fa-money-bill"></i></span>
		  <money
		  decimal="{{$currency->decimal_mark}}"
		  thousands="{{$currency->thousands_separator}}"
		  name="sale_price"
		  {{ (isset($readOnly) && $readOnly ? 'readonly' : '') }}
		  id="sale_price"
		  value="{{(isset($item->sale_price) ? number_format($item->sale_price,2) :null)}}"
		  class="form-control input-lg"
			></money>  
         
      </div>
  </div>


    <div class="form-group col-md-2 col-xs-3">
      <label>{{ trans("general.active") }}</label>

             <label class="switch switch-left-right">
				{{ Form::checkbox('enabled', 1, isset($item) ? null : true, ['class'=> 'switch-input',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
				<span class="switch-label switch-label-3" data-on="{{ trans('general.yes') }}" data-off="{{ trans('general.not') }}"></span> <span class="switch-handle"></span> 
          	 </label>  
  </div>
  <div class="form-group col-md-12">

	{{ Form::label('sku', trans('item.sku')) }}
	{{ Form::text('sku', null,['class'=>'form-control input-lg', 'placeholder'=>trans('placeholders.sku'),(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
	
	<span class="description ">   
	<button class="btn btn-default popover-default btn-xs" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{trans('item.sku_details')}}" data-original-title="SKU"><i class="entypo-info"></i></button>
	</span>
  </div>
</div>



			 <!-- Metaboxes -->
        <div class="row">
				
				<!-- Painel que aparecerá apenas para produtos, serviços não -->
				<div class="col-sm-6 {{ (isset($item->item_type) and $item->item_type == 'service'?'hidden':'') }}" id="products">
					
					<div class="panel panel-primary" data-collapsed="0">
				
						<div class="panel-heading">
							<div class="panel-title">
								{{trans('item.details')}}
							</div>
							
							<div class="panel-options">
							
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>

							</div>
						</div>
						
						<div class="panel-body">

						    <div class="form-group">
						      	{{ Form::label('purchase_price',trans('general.price.purchase')) }}

						      	<div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money-bill"></i></span>
									<money
									decimal="{{$currency->decimal_mark}}"
									thousands="{{$currency->thousands_separator}}"
									name="purchase_price"
									{{ (isset($readOnly) && $readOnly ? 'readonly' : '') }}
									id="purchase_price"
									value="{{ (isset($item->sale_price) ? number_format($item->purchase_price,2) : null) }}"
									class="form-control input-lg"
									  ></money>  
								</div>
						    </div>

						    <hr>
						


							<div class="form-group">
						      	{{ form::label('storage', trans('item.storage')) }}

						      	<div class="input-spinner">
                                    <button type="button" class="btn btn-default" data-step="-1">-</button>
                                    {{ Form::text('storage', null,['class' => 'form-control size-2',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
									<button type="number" class="btn btn-default" data-step="1">+</button>
								</div>

						    </div>


							<div class="form-group">
                                {{ form::label('storage_min', trans('item.storage_min')) }}

                                <div class="input-spinner">
                                  <button type="button" class="btn btn-default" data-step="-1">-</button>
                                  {{ Form::text('storage_min', null,['class' => 'form-control size-2',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
                                  <button type="number" class="btn btn-default" data-step="1">+</button>
                              </div>

                          </div>

						    <hr>


					  		<div class="form-group">
					    		{{Form::label('barcode', trans('item.barcode'))}}

					    		<div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    {{ Form::text('barcode', null, ['class' => 'form-control input-lg', 'placeholder' =>  'Digite ou faça a leitura do código de barras',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
								</div>
					  		</div>

					  		<hr>

					  		<div class="form-group">
						      {{ Form::label('unit_type', trans("item.unit_type")) }}

						      	<div class="input-group">
                                    <span class="input-group-addon"><i class="entypo-doc-text"></i></span>
                                    {{ Form::select('unit_type', $unitTypes,( isset($item) ? null : 'un'), ['class' => 'selectboxit input-lg','placeholder' => trans('general.select').'...',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
									
								</div>
						    </div>

						    <hr>

						</div>
					
					</div>
					
				</div>
      
        				<!-- Metabox :: Tags -->
				<div class="col-sm-6">
					
					<div class="panel panel-primary" data-collapsed="0">
				
						<div class="panel-heading">
							<div class="panel-title">
							{{ trans('item.additional_info') }}
							</div>
							
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
							@if(isset($item)) 
							
								@if(!getPedidoCodeByItemId($item->id))
									<div class="form-group">
										<label>{{ trans("sale-rent::general.item_rented") }}</label>

										<label class="switch switch-left-right">
										{{ Form::checkbox('rent_status', 1, null, ['class'=> 'switch-input',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
										<span class="switch-label switch-label-3" data-on="{{ trans('general.yes') }}" data-off="{{ trans('general.not') }}"></span> <span class="switch-handle"></span> 
										</label>  
									</div>
								@endif
							@endif
							<div class="form-group">
							{{Form::label('description',trans('general.description'))}}
							{{ Form::textarea('description', null,['class'=>'form-control', 'placeholder' => trans('placeholders.descriptions'),(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
							</div>

							<hr>

							<p>{{ trans('item.image') }}:</p>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="max-width: 310px; height: 160px;" data-trigger="fileinput">
									<img src="{{(isset($item) && $item->firstMedia('product') != null ? $item->firstMedia('product')->getUrl() : 'http://placehold.it/320x160')}}" alt="image">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 320px; max-height: 160px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">{{ trans('general.choose_image') }}</span>
										<span class="fileinput-exists">{{ trans('general.change') }}</span>
										{{ Form::file('image', null, ['accept'=>'image/*']) }}
										
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">{{ trans('general.remove') }}</a>
								</div>
							</div>
							
							
						</div>
					
					</div>
					
				</div>
      </div>

@section('scripts')

<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">
<script src="{{asset('assets/js/fileinput.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script src="{{asset('js/items.js')}}?v={{ filemtime('js/items.js') }}"></script>
<link rel="stylesheet" href="{{asset('assets/js/selectboxit/jquery.selectBoxIt.css')}}">

<script src="{{asset('assets/js/selectboxit/jquery.selectBoxIt.min.js')}}"></script>
<script>
function display_details(value)
{
	var panel = document.getElementById('products');

	if(value == 'service')
	{
		panel.classList.add("hidden");
	}else{
		panel.classList.remove("hidden");
	}

	var select = document.getElementById('item_type'), label = document.getElementById('label_sale_price'),text;

	for(var i=0; i < select.options.length; i++)
	{
	
		if(select.options[i].value === value)
		{
			text = select.options[i].text;
		}
	}

	label.innerHTML = text;
}


</script>

@endsection