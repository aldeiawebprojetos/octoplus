@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('general.items')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.add',2,['item' => ''])}}</strong> </li>	
@endsection

@section('title', trans('general.items')  .' / '.  trans('general.products') )

@section('content')

{{ Form::open(['method'=>'POST', 'route' => 'tenant.items.store','enctype'=>'multipart/form-data']) }}

<div class="row">
     <!-- title -->
     <div class="col-md-6">
        <h1 class="pull-left">
            {{trans('general.items')}} <i class="entypo-pencil"></i>
        </h1>
    </div>

    @include('common.buttonsForm',['route_return' => 'tenant.items.index'])
</div>

@include('items.form')

<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'tenant.items.index'])
</div>

{{ Form::close() }}
@endsection

@section('modals')
    @include('common.modals.addcategory', ['type' => 'item'])
    
@endsection