<div class="btn-group ">
								
	<button type="button" class="btn {{ isset($classbtn) ? $classbtn : 'btn-gold' }} dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		<i class="entypo-dot-3"></i>
	</button>
	
	<ul class="dropdown-menu" role="menu">
		@foreach($options as $option)
		
			@if(isset($option['permission']))
				@can($option['permission'])
					<li><a href="{{isset($option['routejs']) ? "javascript:".$option['routejs'] : route($option['route'][0],$option['route'][1] ? $id : null)}}" class="text-defaut">
						<i class="{{$option['icon']}}"></i> {{ $option['text'] }} </a> </li>
					<li class="divider"></li>
				@endcan
			@else
			   @if(!empty($option))
					<li><a href="{{isset($option['routejs']) ? "javascript:".$option['routejs'] : route($option['route'][0],$option['route'][1] ? $id : null)}}" class="text-defaut">
						<i class="{{$option['icon']}}"></i> {{ $option['text'] }} </a> </li>
					<li class="divider"></li>
			   @endif
			@endif
		@endforeach
		@if(isset($route_delete))
			@can($delete_permission)
				<li>@include('common.delete',['route' => $route_delete]) </li>
			@endcan
		@endif
	</ul>
</div>	