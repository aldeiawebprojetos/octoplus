	<!-- Modal 6 (Long Modal)-->
	<div class="modal fade" id="taxAdd">
		<div class="modal-dialog" style="margin-top: 80px;">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title"> <i class="entypo-folder"></i> {{ trans_choice('general.add',2,['item' => trans('general.tax')]) }} </h3>
				</div>
				
				<div class="modal-body">
				{{ Form::open(['method' => 'POST', 'route' => 'tenant.taxes.store' ,'id' =>'postTax', 'onsubmit'=>'return false;']) }}
				<div class="row">
					<div class="col-md-12">
						
						<div class="form-group">
							
							{{Form::label('name_tax', trans('general.name'))}}
							{{Form::text('name_tax', null, ['class' => 'form-control input-lg'])}}

						</div>	
						
					</div>
				</div>

					<div class="row">
						<div class="col-md-12">
							{{Form::label('aliquot',trans('general.aliquot'))}}
							{{Form::text('aliquot', null,['class' => 'form-control input-lg'])}}
						</div>
					</div>
					
					
				
				
				{{ Form::close() }}
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">{{ trans('general.close') }}</button>
					<a class="btn btn-green btn-lg btn-icon pull-right" href="javascript:saveTax()">{{trans('general.save')}}<i class="entypo-floppy"></i></a>
				</div>
			</div>
		</div>
	</div>

	<script>
	function saveTax(){
            var name = document.getElementById('name_tax').value,
			
			aliquot = document.getElementById('aliquot').value,
			form = document.getElementById('postTax');
			
			
			 axios.post(form.action,{
				 name: name,
				 rate: aliquot,
				 enabled: 1,
			 }).then(function (response) {
                var data = response.data;
                
              
                
                name = '';
                aliquot = '';
				$('#taxAdd').modal('hide');
             });
        }
	</script>