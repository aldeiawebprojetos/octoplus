	<!-- Modal 6 (Long Modal)-->
	<div class="modal fade" id="categoryAdd">
		<div class="modal-dialog" style="margin-top: 80px;">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title"> <i class="entypo-folder"></i> {{ trans_choice('general.add',2,['item' => trans('general.category')]) }} </h3>
				</div>
				
				<div class="modal-body">
				{{ Form::open(['method' => 'POST', 'route' => 'tenant.categories.store' ,'id' =>'postCategory', 'onsubmit'=>'return false;']) }}
				<div class="row">
					<div class="col-md-12">
						
						<div class="form-group">
							
							{{Form::label('name_category',trans('general.name'))}}
							{{Form::text('name_category', null, ['class' => 'form-control'])}}

						</div>	
						
					</div>
				</div>
				
					{{-- <div class="row">
						<div class="col-md-12">
							{{Form::label('category_id', trans_choice('general.parent',2,['item'=> trans('general.category')]))}}
							{{Form::select('category_id',  $categories,null,['class' => 'form-control input-lg','placeholder'=>trans('general.select').'...'])}}
						</div>
					</div> --}}
					
					
				
				{{ Form::hidden('type',$type,['id' => 'type']) }}
				{{ Form::close() }}
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">{{ trans('general.close') }}</button>
					<a class="btn btn-green btn-lg btn-icon pull-right" href="javascript:saveCategory()" >{{trans('general.save')}}<i class="entypo-floppy"></i></a>
				</div>
			</div>
		</div>
	</div>

	<script>
		function saveCategory(selectName = 'categories')
		{
			var name = document.getElementById('name_category'),
			type = document.getElementById('type').value,
			//category_select = document.getElementById('category_id'),
			form = document.getElementById('postCategory'),
			select = document.getElementById(selectName);
			
			
			 axios.post(form.action,{
				 name: name.value,
				 type: type,
				 //parent_id: category_select.value,
				 enabled: 1,
			 }).then(function (response) {
				
				var category = response.data;

				// var option = document.createElement("option");
				// option.text = category.name;
				// option.value = category.id;

				name.value = '';

				// category_select.add(option);
				// category_select.value = '';

				var option_select = document.createElement("option");
				option_select.text = category.name;
				option_select.value = category.id;
				
				select.add(option_select);
				select.value = '';

				$('#categoryAdd').modal('hide');
			 });
		}
	</script>
