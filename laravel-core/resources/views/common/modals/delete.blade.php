	<!-- Confirm Delete -->
	<div class="modal fade" id="delete-modal" style="margin-top: 40px;">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{$type}}</h4>
				</div>
				
				<div class="modal-body">
					{!! trans('general.msg_delete') !!}
				</div>
				
            {!! Form::open(['method'=>'DELETE','id'=>'form-delete' ]) !!}
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('general.close')}}</button>
                <button type="submit" class="btn btn-danger">{{trans('general.delete')}}</button>
            </div>
            {!! Form::close() !!}
			</div>
		</div>
	</div>