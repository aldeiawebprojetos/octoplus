<!-- Botoes -->
<div class="clear"><br><br></div>
<div class="col-md-12 pull-right">
	<div class="align-middle  pull-right">

		&nbsp;
		@if(isset($close) && $close == 'delivered')
			<label for="submit-close" tabindex="0" class="btn-danger" id="label-close-contract"> 
				Encerrar locação
			</label>
		@endif
		&nbsp;
		
		&nbsp;
		@if(isset($route_include))
			<a class="btn btn-blue btn-icon" href="{{ route($route_include) }}">
				{{ trans('general.add') }}
				<i class="entypo-plus"></i>
			</a>
		@endif
		&nbsp;
		
		@if(isset($route_delete))
		&nbsp;
			<a class="btn btn-danger" href="javascript:confirmDelete('{{$name}}', '{{route($route_delete,$id)}}')">
				{{trans('general.delete')}}
				<i class="fa fa-trash"></i>
			</a>
		&nbsp;
		@endif

	
		@if(isset($route_return))
		&nbsp;
			<a href="{{route($route_return)}}" class="btn btn-white">
				  {{trans("general.cancel_return")}}
				  <i class="entypo-block"></i>
			</a>
		 &nbsp;
		@endif

		&nbsp;
		
		<button {{ isset($edit) && !$edit  ? 'disabled' : ''}} type="submit" class="btn btn-green btn-icon pull-right">
			{{trans("general.save")}}
			<i class="fa fa-save"></i>
		</button>
		&nbsp;
	</div>
</div>
<br>
<div class="clear"><br><br><br></div>