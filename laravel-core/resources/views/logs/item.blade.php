@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{!!route('tenant.items.index')!!}">{!!trans('general.items')!!}</a> </li>

<li class="active"> <strong>{!!trans('general.log')!!}</strong> </li>	
@endsection

@section('title','Itens / Log')


@section('content')
    <ul class="cbp_tmtimeline">
       @foreach ($activities as $activity)
            <li>
                <time class="cbp_tmtime" datetime="{!!$activity->created_at!!}"><span>{!!$activity->created_at->format('d/m/Y')!!}</span> <span>{!!$activity->created_at->format('H:i')!!}</span></time>
                
                <div class="cbp_tmicon bg-{{($activity->description == 'created' ? "success" : ($activity->description == 'updated' ? "warning": "danger"))}}">
                   {!! ($activity->description == 'created' ? "<i class='entypo-plus'></i>" : ($activity->description == 'updated' ? "<i class='entypo-feather'></i>": "<i class='entypo-trash'></i>"))!!}
                </div>
                
                <div class="cbp_tmlabel">
                    <h2><a href="#">{!!$activity->causer->name!!} </a> <span>{!! trans_choice('logs.'.$activity->description, 2,['item' => trans('general.item')])!!}</span></h2>
                    @if($activity->description == 'created')
                        <p>
                            @foreach ($activity->properties['attributes'] as $key => $item)
                                    @if ($item != null && $key != 'company_id')
                                        @if ($key == 'name')
                                            <strong> {!! trans('general.name') !!}: {!! $item !!} </strong><br/>
                                        @endif
                                        @if ($key == 'sku')
                                            <strong> {!! trans('item.sku') !!}: {!! $item !!} </strong><br/>
                                        @endif
                                        @if ($key == 'enabled')
                                            <strong> {!! trans('general.status') !!}: {!! ($item == 1 ? trans("general.enabled") : trans('general.disabled')) !!} </strong><br/>
                                        @endif
                                        @if ($key == 'storage')
                                            <strong> {!! trans('item.storage') !!}: {!! $item !!} </strong><br/>
                                        @endif
                                        @if ($key == 'item_type')
                                            <strong> {!! trans('item.type') !!}: {!! $item !!} </strong><br/>
                                        @endif
                                        @if ($key == 'sale_price')
                                            <strong> {!! trans('general.price.sale') !!}: {!! $currency->symbol !!} {!! number_format($item, 2,$currency->decimal_mark, $currency->thousands_separator) !!} </strong><br/>
                                        @endif
                                        @if ($key == 'purchase_price')
                                            <strong> {!! trans('general.price.purchase') !!}:  {!! $currency->symbol !!} {!! number_format($item, 2,$currency->decimal_mark, $currency->thousands_separator) !!} </strong><br/>
                                        @endif
                                    @endif
                            @endforeach
                        </p>
                    @elseif($activity->description == 'updated')
                    <p>
                        @foreach ($activity->properties['attributes'] as $key => $item)
                                @if ($item != null && $key != 'company_id')
                                    @if ($key == 'name')
                                        <strong> {!! trans('general.name') !!}: {!! $item == $activity->properties['old'][$key] ? $item : "<s>{$activity->properties['old'][$key]}</s> $item" !!} </strong><br/>
                                    @endif
                                    @if ($key == 'sku')
                                        <strong> {!! trans('item.sku') !!}: {!! $item == $activity->properties['old'][$key] ? $item : "<s>{$activity->properties['old'][$key]}</s> $item" !!}</strong><br/>
                                    @endif
                                    @if ($key == 'enabled')
                                        <strong> {!! trans('general.status') !!}: {!! $item == $activity->properties['old'][$key] ? ($item == 1 ? trans("general.enabled") : trans('general.disabled')) : "<s>". ($activity->properties['old'][$key] == 1 ? trans("general.enabled") : trans('general.disabled')) ."</s> $item" !!}  </strong><br/>
                                    @endif
                                    @if ($key == 'storage')
                                        <strong> {!! trans('item.storage') !!}: {!! $item == $activity->properties['old'][$key] ? $item : "<s>{$activity->properties['old'][$key]}</s> $item" !!} </strong><br/>
                                    @endif
                                    @if ($key == 'item_type')
                                          <strong> {!! trans('item.type') !!}: {!!  $item == $activity->properties['old'][$key] ? $item : "<s>".item_types()[$activity->properties['old'][$key]]."</s> " . item_types()[$item] !!} </strong><br/>
                                     @endif
                                    @if ($key == 'sale_price')
                                        <strong> {!! trans('general.price.sale') !!}:  {!! $currency->symbol !!} {!! $item == $activity->properties['old'][$key] ? number_format($item, 2,$currency->decimal_mark, $currency->thousands_separator) : "<s>".  number_format($activity->properties['old'][$key], 2,$currency->decimal_mark, $currency->thousands_separator) ."</s> ".  number_format($item, 2,$currency->decimal_mark, $currency->thousands_separator) !!} </strong><br/>
                                    @endif
                                    @if ($key == 'purchase_price')
                                        <strong> {!! trans('general.price.purchase') !!}:  {!! $currency->symbol !!} {!! $item == $activity->properties['old'][$key] ? number_format($item, 2,$currency->decimal_mark, $currency->thousands_separator) : "<s>".  number_format($activity->properties['old'][$key], 2,$currency->decimal_mark, $currency->thousands_separator) ."</s> ".  number_format($item, 2,$currency->decimal_mark, $currency->thousands_separator) !!} </strong><br/>
                                    @endif
                                @endif
                        @endforeach
                    </p>
                    @else
                    <p></p>
                    @endif
                </div>
            </li>
       @endforeach
    </ul>
@endsection


@section('scripts')
<link rel="stylesheet" href="{{asset("assets/js/vertical-timeline/css/component.css")}}">
@endsection