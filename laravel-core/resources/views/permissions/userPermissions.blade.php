@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('account.users.index')}}">{{trans('user.users')}}</a> </li>

<li> <a href="javascript:void(0)">{{trans('general.settings')}}</a> </li>

<li class="active"> <strong>{{trans('auth.user_permission')}}</strong> </li>	
@endsection

@section('title', trans('user.users')  .' / '.  trans('auth.user_permission') )

@section('content')
<hr />
{{ Form::model($user,['method' => 'POST', 'route' =>  ['account.users.save.permission','user_id' => $user->id]]) }}
<div class="profile-env">
    
    <header class="row">
        
        <div class="col-sm-2">
            
            <a href="#" class="profile-picture">
                <img src="{{(isset($user) && $user->firstMedia('users') != null ? $user->firstMedia('users')->getUrl() : asset('assets/images/profile-picture.png'))}}" class="img-responsive img-circle" />
            </a>
            
        </div>
        
        <div class="col-sm-7">
            
            <ul class="profile-info-sections">
                <li>
                    <div class="profile-name">
                        <strong>
                            <a href="#">{{$user->name}}</a>
                            <a href="#" class="user-status is-online tooltip-primary" data-toggle="tooltip" data-placement="top" data-original-title="Online"></a>
                            <!-- User statuses available classes "is-online", "is-offline", "is-idle", "is-busy" -->						</strong>
                        <span><a href="#">{{ trans('general.code') }}: #{{$user->id}}</a></span>
                    </div>
                </li>
               
                <li>
                    <div class="profile-stat">
                        @if(auth()->user()->hasRole('admin'))
                            <div class="">
                                {{ trans('auth.admin') }} ?  <button class="btn btn-default popover-default btn-xs" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{ trans("permissions.admin_description") }}" data-original-title="Admin"><i class="fa fa-star"></i></button>
                                
                               
                                <div class="">
                                    <label class="switch switch-left-right">
                                    <input name='flush_admin' value="true" class="switch-input" {{ $user->isAdmin() ? 'checked' : '' }}  type="checkbox" id="isAdmin" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                    <span class="switch-label switch-label-2" data-on="SIM" data-off="NÃO"></span> <span class="switch-handle"></span> </label>
                                </div>
                            </div>
                        @endif
                    </div>
                </li>
                
                
            </ul>
            
        </div>

        <div class="col-sm-3">
            
            <div class="form-group collapse in" id="collapseExample">
                @if ($regra_de_acesso->count() > 0)
                    <label for=""><i class="fa fa-unlock-alt"></i> {{trans('auth.rules_access')}}  </label>
                    @foreach ($regra_de_acesso as $regra)
                    <div class="form-check">
                        {!! Form::radio('regra_acesso_id', $regra->id, null, ['class'=>"form-check-input",'id'=>'regra_acesso_id']) !!}
                        <label class="form-check-label" for="regra_acesso_id">
                        {{$regra->name}}
                        </label>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
        
        
        
    </header>
    
    <section class="profile-info-tabs">
        
        <div class="row">
            
            <div class="col-sm-offset-2 col-sm-10">
                
                <ul class="user-details">
                    <li>
                        <a href="#">
                            <i class="fa fa-envelope"></i>
                            {{$user->email}}
                        </a>
                    </li>
                    
                    <li>
                        <a href="#">
                            <i class="fa fa-phone"></i>
                            {{$user->phone}}
                        </a>
                    </li>
                    
                    <li>
                        <a href="#">
                            <i class="entypo-calendar"></i>
                            {{trans('auth.last_access')}}: <span>{{$user->companies()->withPivot('last_login_at')->first()->toArray()['pivot']['last_login_at']}}</span>
                        </a>
                    </li>
                </ul>
                
                
                <!-- tabs for the profile links -->
                <ul class="nav nav-tabs">
                   
                         <li class="active"><a href="#">{{trans('auth.user_permission')}}</a></li>
                   
                   @can('octoplus.logs_show')
                        <li><a href="{{ route('account.users.log',$user->id) }}">{{trans('general.log')}}</a></li>
                   @endcan
                   @can('octoplus.users_update')
                         <li><a href="{{ route('account.users.edit', $user->id) }}">{{trans('general.edit')}}</a></li>
                   @endcan
                </ul>
                
            </div>
            
        </div>
        
    </section>
    
</div>

<hr>

{{-- <div class="row ">
    <div class="col-sm-12 text-center">
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-user"></i></span>
                {{Form::select('change_user', $users->pluck('name',"id"), null,['class' => 'select2 form-control input-lg','onchange' => 'changeIdUrl("'.Request::path().'",this)','placeholder' => trans("general.select").'...'])}}
            </div>
            <span class="description"> {{trans("permissions.select_to_change")}} </span>
        </div>

        
    </div>
    
</div> --}}

  
@foreach($permissions as $module => $permission)
    <div class="row">
        <div class="col-sm-12 text-center">
            <h1 class="text-default">
                @if($module == 'octoplus')
                    {{ trans('general.system_permission') }}
                @else 
                    {{ trans_choice('general.module', 2, ['module' => trans($module.'::general.name')]) }}
                @endif
            </h1>
            
            <div class="">
                <label class="switch switch-left-right">
                    <input class="switch-input" type="checkbox" id="module_{{$module}}" {{($user->permissions->filter(function($item) use ($module){ return preg_match('/'.$module.'/',$item->name) ; })->count() > 0 ? 'checked' : '')}} data-toggle="collapse" data-target="#module_collapse_{{$module}}" aria-controls="module_collapse_{{$module}}" onchange="unchkAll('{{$module}}')">
                    <span class="switch-label" data-on=" ON" data-off="OFF"></span> <span class="switch-handle"></span>
                </label>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-sm-12">
            
            <div class="panel panel-primary collapse {{($user->permissions->filter(function($item) use ($module){ return preg_match('/'.$module.'/',$item->name) ; })->count() > 0 ? 'in' : '')}}" data-collapsed="0" id="module_collapse_{{$module}}" aria-expanded="{{($user->permissions->filter(function($item) use ($module){ return preg_match('/'.$module.'/',$item->name) ; })->count() > 0 ? 'true' : 'false')}}">
            
                <div class="panel-heading">
                    <div class="panel-title">
                        {{($module =='octoplus' ? trans('permissions.screen_system_permission') : trans_choice('permissions.screen_modules_permission',2,['module' => trans( $module.'::general.name')]))}}
                    </div>
                    
                    <div class="panel-options">
                        <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                    </div>
                </div>
                
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-12"> 


                            <a class="btn btn-white" onclick="chkAll('{{$module}}')">
                                <i class="far fa-check-circle"></i>
                                {{ trans('permissions.checkAll') }}
                            </a>

                            <a class="btn btn-white"  onclick="unchkAll('{{$module}}')">
                                <i class="far fa-circle"></i>
                                {{ trans('permissions.uncheckAll') }}
                            </a>
                        </div>
                    </div>
                    @foreach($permission as $key => $item)
                    <h2 class=" text-center"> {{ trans(($module == 'octoplus' ? 'general.' : $module.'::permissions.').$key) }} </h2> 
                    <br>
                    
                    <div class="row  text-center">
                        @foreach ($item as $switch)
                        <div class="col-sm-2 ">
                            {{ trans(($module == 'octoplus' ? 'general.' : $module.'::permissions.').$switch['action']) }}
                            <div class="form-group">
                                <div class="">
                                      <label class="switch switch-left-right">
                                          {{ Form::checkbox('permissions[]',$switch['id'],null, ['class' => "switch-input chkbx_{$module}"]) }}
                                    <span class="switch-label switch-label-3" data-on="{{trans("general.yes")}}" data-off="{{trans('general.not')}}"></span> <span class="switch-handle"></span> </label>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    
                    <hr />
                    @endforeach
                </div>
            </div>
        </div>
    </div>
        
    
    <hr /> <!-- / -->
    @endforeach
    <div class="row">
        <div class="col-xs-12">

            <div class="checkbox">
                <label>
                    <input name="rule_save" value='true' type="checkbox" data-toggle="collapse" data-target="#collapseExample22" aria-expanded="true" aria-controls="collapseExample22">
                    {{ trans("permissions.save_rule") }}?
                </label>
            </div>
            {{ Form::text('rule_name', null, ['class' => 'form-control collapse input-lg','id'=>'collapseExample22','placeholder'=> trans("permissions.rule_name")]) }}
           

            
        </div>
    </div>
   
    <div class="row">
        <!-- title -->
        <div class="col-md-6">
          
       </div>
   
       @include('common.buttonsForm',['route_return' => 'account.users.index'])
   </div>
{{Form::close()}}
@endsection

@section('scripts')

<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
@endsection