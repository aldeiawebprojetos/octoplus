@extends('layouts.app')

@section('title',  trans('general.categories') )

@section('content')
{{ Form::model($category, ['method'=>'PUT', 'route' => ['tenant.categories.update','category_id' => $category->id]]) }}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('general.categories')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

   @include('common.buttonsForm',['route_return' => 'tenant.categories.index','route_delete'=>'tenant.categories.delete','id'=>$category->id,'name'=>$category->name])
</div>

@include('categories.form')

<div class="row">
    <!-- title -->
    <div class="col-md-6">
      
   </div>

   @include('common.buttonsForm',['route_return' => 'tenant.categories.index','route_delete'=>'tenant.categories.delete','id'=>$category->id,'name'=>$category->name])
</div>
{{ Form::close() }}
@endsection

@section('modals')
    @include('common.modals.delete',['type'=>trans("general.categories")])
@endsection