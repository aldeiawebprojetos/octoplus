@extends('layouts.app')

@section('title',  trans('general.categories') )

@section('content')
{{ Form::model($category, []) }}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('general.categories')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

 
</div>

@include('categories.form', ['readOnly' => true])

<div class="row">
    <!-- title -->
    <div class="col-md-6">
      
   </div>
</div>
{{ Form::close() }}
@endsection

@section('modals')
    @include('common.modals.delete',['type'=>trans("general.categories")])
@endsection