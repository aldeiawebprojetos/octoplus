@extends('layouts.app')

@section('title',  trans('general.categories') )

@section('breadcrumb')
@if(isset($type))
<li> <a href="{{route('tenant.categories.index')}}">{{trans('general.'.$type)}}</a> </li>
@endif
<li> <a href="{{route('tenant.categories.index')}}">{{trans('general.categories')}}</a> </li>

<li class="active"> <strong>{{trans('general.listing')}}</strong> </li>	
@endsection

@section('content')
<div class="mail-env">
    <div class="mail-body">
				
        <div class="mail-header">
            <!-- Search  form -->
            
          
        </div>

        <div class="mail-header">
            <!-- title -->
            <h1 class="mail-title">
                {{ trans('general.categories') }}
                <span class="count text-info">({{$categories->count()}})</span>

               
            </h1>
                      

            <!-- Filtrar por Categoria Produto -->	
            <!--				
            <div class="mail-search">

                {{ Form::select('category', $categories_select->pluck('name','id'), null,['class' => 'form-control','onchange'=>'getConsulta("'. Request::path() .'","'.http_build_query(Request::query()).'",this)','placeholder'=>trans('general.select').'...']) }}

            </div>
            -->
            
            <!-- search -->	
            <!--				
            <div class="pull-left">				
                <a href="#" class="btn btn-danger pull-left">
                    <i class="entypo-trash"></i>
                </a>
            </div>
            -->     

        </div>
        

        <table class="table table-bordered table-striped datatable" id="table-1">
            <thead>
                <tr>
                    <th>{{trans('general.actions')}}</th>
                    <th>{{trans('general.categories')}}</th>
                    {{-- <th>{{trans_choice('general.parent',2,['item'=> trans('general.category')])}}</th> --}}
                    <th class="{{ (isset($type) ? 'hidden' : '') }}">{{trans('general.type')}}</th>
                    <th>{{trans('general.status')}}</th>
                    <th width="5%">
                        <div class="checkbox checkbox-replace">
                            <input type="checkbox" class="chkbx_category" onchange="changeChkboxes(this,'category')"/>
                        </div>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td>
                            @include('common.tableActions',['options' => [
                                ['route' => [(isset($routeCategoryEdit) ? $routeCategoryEdit : 'tenant.categories.edit'), true],'permission' => 'octoplus.categories_update','text'=> trans('general.edit'),'icon' => 'entypo-pencil']
                            ],'id'=>$category->id,'name'=>$category->name, 'route_delete'=>'tenant.categories.delete','delete_permission' => 'octoplus.categories_delete'])
                            
                         </td>
                        <td onclick="goToEdit('{{ route(isset($routeCategoryShow) ? $routeCategoryShow : 'tenant.categories.show',$category->id) }}')">{{ $category->name }}</td>
                        {{-- <td onclick="goToEdit('{{ route(isset($routeCategoryShow) ? $routeCategoryShow : 'tenant.categories.show',$category->id) }}')">{{ $category->parent->name ?? '' }}</td> --}}
                        <td onclick="goToEdit('{{ route(isset($routeCategoryShow) ? $routeCategoryShow : 'tenant.categories.show',$category->id) }}')" class="{{ (isset($type) ? 'hidden' : '') }}">{{ $types[$category->type] }}</td>
                        <td onclick="goToEdit('{{ route(isset($routeCategoryShow) ? $routeCategoryShow : 'tenant.categories.show',$category->id) }}')">{{ $category->enabled == 1 ? 'Ativo' : 'Inativo' }}</td>
                        <td width="5%">
                            <div class="checkbox checkbox-replace">
                                <input type="checkbox" value="{{$category->id}}" class="chkbx_category"/>
                            </div>
                        </td>
                    </tr>    
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>{{trans('general.actions')}}</th>
                    <th>{{trans('general.categories')}}</th>
                    {{-- <th>{{trans_choice('general.parent',2,['item'=> trans('general.category')])}}</th> --}}
                    <th class="{{ (isset($type) ? 'hidden' : '') }}">{{trans('general.type')}}</th>
                    <th>{{trans('general.status')}}</th>
                    <th width="5%">
                        <div class="checkbox checkbox-replace">
                            <input type="checkbox" class="chkbx_category" onchange="changeChkboxes(this,'category')"/>
                        </div>
                    </th>
                </tr>
            </tfoot>
        </table>
        
        {{ $categories->links() }}
    </div>

    <div class="mail-sidebar">
				
        <!-- compose new email button -->
        <div class="mail-sidebar-row hidden-xs">
            <a href="{{route((isset($routeCategoryCreate) ? $routeCategoryCreate : 'tenant.categories.create'))}}" class="btn btn-blue btn-lg btn-icon btn-block">
                {{trans_choice('general.add',2,['item' => trans('general.category')])}}
                <i class="entypo-plus"></i>
            </a>
        </div>
       

        <!-- menu -->
        <ul class="mail-menu">
           
        
        </ul>
        
        <div class="mail-distancer"></div>
        
        <h4>{{ trans('general.status') }}</h4>
       
        <!-- menu -->
        <ul class="mail-menu">
            @foreach (categories_types(['item','other']) as $key => $typeCat)
          
                <li class="">
                    <a href="?{{ http_build_query(collect(['type' => $key])->merge(collect(Request::query())->forget(['type']))->toArray()) }}">
                        <span class="badge badge-{{ ($key == 'expense' ? 'danger' : 'primary') }} badge-roundless pull-right"></span>
                        {{ $typeCat }}
                    </a>
                </li>
            @endforeach
            <li class="{{ Request::query('status') == '0' ? 'active' : null }}">
                <a href="?{{ http_build_query(Request::merge(['status' => 0])->input()) }}">
                    <span class="badge badge-warning badge-roundless pull-right"></span>
                    {{ trans('general.inactive') }}
                </a>
            </li>

            <li class="{{ Request::query('status') == 1 ? 'active' : null }}">
                <a href="?{{ http_build_query(Request::merge(['status' => 1])->input()) }}">
                    <span class="badge badge-success badge-roundless pull-right"></span>
                    {{ trans('general.active') }}
                </a>
            </li>
        </ul>
        
    </div>
    
</div>

    
    
@endsection

@section('modals')
    @include('common.modals.delete',['type'=>trans("general.categories")])
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
    <script>
    jQuery( document ).ready( function( $ ) {
        var $table3 = jQuery("#table-1");
        var table3 = $table3.DataTable( {
        "sPaginationType": "full_numbers",
        "bPaginate": false,
        "sDom": 't'

        } );
    
    } );
    </script>
@endsection