@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('general.items')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.add',2,['item' => trans('general.item')])}}</strong> </li>	
@endsection

@section('title', trans('general.items')  .' / '.  trans('item.unit_type') )

@section('content')

{{ Form::open(['method'=>'POST', 'route' => 'tenant.items.unitTypes.store']) }}

<div class="row">
     <!-- title -->
     <div class="col-md-6">
        <h1 class="pull-left">
            {{trans('item.unit_type')}} <i class="entypo-pencil"></i>
        </h1>
    </div>

  
</div>

@include('unitTypes.form')

<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => route('tenant.items.index')])
</div>

{{ Form::close() }}
@endsection

