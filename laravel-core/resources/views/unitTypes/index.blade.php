@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('general.items')}}</a> </li>

<li class="active"> <strong>{{trans('general.listing')}}</strong> </li>	
@endsection

@section('title','Itens / Produtos')
    
@section('content')
<a href="{{route('tenant.items.unitTypes.create')}}" class="btn btn-blue btn-lg btn-icon btn-block">
    {{trans_choice('general.add',2,['item' => trans('item.unit_type')])}}
    <i class="entypo-plus"></i>
</a>
<table class="table table-bordered table-striped datatable" id="table-4">
    <thead>
        <tr>
            <th width="1%" class="text-center"> 
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" />
                </div>
            </th>
            <th>{{trans("general.name")}}</th>
            <th>{{ trans('general.code') }}</th>
            <th>{{ trans('general.actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($unit_types as $item)
        <tr class="odd gradeX">
            <td class="text-center">
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" />
                </div>
            </td>
            <td> <a href="newedit.itens.php" class=""> {{$item->name}} </a> </td>
            
            <td>{{$item->code}}</td>           
            <td class="center">
                
                <!-- Actoins -->
                @include('common.tableActions',['route_edit'=>'tenant.items.edit','route_details'=>'tenant.items.edit','route_delete'=>'tenant.items.delete', 'id' => $item->id])

            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th width="1%" class="text-center">
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" />
                </div>
            </th>
            <th>{{trans("general.name")}}</th>
            <th>{{ trans('general.code') }}</th>
            <th>{{ trans('general.actions') }}</th>
        </tr>
    </tfoot>
</table>
@endsection