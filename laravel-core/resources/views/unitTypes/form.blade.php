<div class="row">
    <div class="form-group col-md-8 col-xs-8">

        {{ Form::label('name', trans('general.name') ) }} <strong class="text-danger">*</strong>
        {{ Form::text('name', null, ['class'=> 'form-control input-lg', 'required', 'placeholder'=> trans_choice('placeholders.input',2,['attribute'=> trans('general.name'),'of'=>trans('general.of'), 'description'=>trans('item.unit_type')])]) }}
      </div>
      <div class="form-group col-md-4 col-xs-4">

        {{ Form::label('code', trans('general.code') ) }} <strong class="text-danger">*</strong>
        {{ Form::text('code', null, ['class'=> 'form-control input-lg', 'required', 'placeholder'=> trans_choice('placeholders.input',2,['attribute'=> trans('general.code'),'of'=>'', 'description'=>''])]) }}
      
      </div>
</div>