@extends('layouts.login')

@section('form')
<form method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon">
                <i class="entypo-mail"></i>
            </div>
            <input id="email" type="email"
            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
            name="email"
            value="{{ old('email') }}" placeholder="{{trans('auth.email')}}" required autofocus>

            @if ($errors->has('email'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
        </div>
    </div>
<div class="form-group">		
    <div class="input-group">
        <div class="input-group-addon">
            <i class="entypo-key"></i>
        </div>
        
        <input id="password" type="password"
        class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
        name="password" required placeholder="{{trans('auth.password')}}">
    </div>
</div>


<div class="form-group">

        <div class="checkbox">
            <label>
                <input type="checkbox"
                           name="remember" {{ old('remember') ? 'checked' : '' }} style="position:sticky"> {{trans('auth.remember')}}
            </label>
        </div>

</div>


				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						{{trans('auth.login')}}
					</button>
				</div>

</form>
<div class="form-group">
    <em>- {{trans('general.or')}} -</em>
</div>

<div class="form-group">
				
    <a style="color: #fff;" class="btn btn-block  btn-login btn-success" href="{{ route('register') }}">
        {{trans("auth.register")}}
        <i class="entypo-user"></i>
     </a>  
    
</div>
@endsection

@section('bottom-links')
<a class="btn btn-link" href="{{ route('password.request') }}">
   {{trans('auth.forgot')}}
</a>

<a class="btn btn-link" href="{{ route('activation.resend') }}">
    {{trans("auth.resend.activate_email")}}
 </a>  
@endsection