@extends('layouts.login')

@section('form')
  

<form method="POST" action="{{ route('activation.resend.store') }}">
    {{ csrf_field() }}

    <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">{{trans('auth.email')}}</label>

        <div class="col-md-6">
            <input id="email" type="email"
                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email" value="{{ old('email') }}" required autofocus>

            @if ($errors->has('email'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{trans('auth.resend.email')}}
            </button>
        </div>
    </div>
</form>

@endsection
