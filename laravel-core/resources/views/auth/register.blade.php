@extends('layouts.login')

@section('form')
   

<form method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
      
        <div class="input-group">
            <div class="input-group-addon">
                <i class="entypo-key"></i>
            </div>
            <input id="first_name" type="text"
                    class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                    name="first_name"
                    value="{{ old('first_name') }}" placeholder="{{trans('auth.first_name')}}" required autofocus>

            @if ($errors->has('first_name'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
      

        <div class="input-group">
            <div class="input-group-addon">
                <i class="entypo-key"></i>
            </div>
            <input id="last_name" type="text"
                    class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                    name="last_name"
                    value="{{ old('last_name') }}" placeholder="{{trans('auth.last_name')}}" required autofocus>

            @if ($errors->has('last_name'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
        

        <div class="input-group">
            <div class="input-group-addon">
                <i class="entypo-user"></i>
            </div>
            <input id="username" type="text"
                    class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
                    name="username"
                    value="{{ old('username') }}" placeholder="{{trans('user.user')}}" required autofocus>

            @if ($errors->has('username'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('username') }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
      

        <div class="input-group">
            <div class="input-group-addon">
                <i class="entypo-mail"></i>
            </div>
            <input id="email" type="email"
                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email"
                    value="{{ old('email') }}" placeholder="{{trans('auth.email')}}" required>

            @if ($errors->has('email'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
     
        <div class="input-group">
            <div class="input-group-addon">
                <i class="entypo-key"></i>
            </div>
            <input id="password" type="password"
                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                    name="password" placeholder="{{trans('auth.password')}}" required>

            @if ($errors->has('password'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
            @endif
        </div>
    </div>

    <div class="form-group ">
      
        <div class="input-group">
            <div class="input-group-addon">
                <i class="entypo-key"></i>
            </div>
            <input id="password-confirm" type="password" class="form-control"
                    name="password_confirmation" placeholder="{{trans_choice('general.confirm',2,['item' => trans('auth.password')])}}" required>
        </div>
    </div>

    <div class="form-group {{ $errors->has('terms') ? ' has-error' : '' }}">
        <div class="">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" name="terms"
                        class="custom-control-input{{ $errors->has('terms') ? ' is-invalid' : '' }}"
                        id="terms">
                <label class="custom-control-label" for="terms">
                   {{trans('auth.accept_terms')}} <a href="#" target="_blank">{{trans('auth.service_terms')}}</a>
                </label>

                @if ($errors->has('terms'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('terms') }}</strong>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group ">
      
            <button type="submit" class="btn btn-success btn-block btn-login">
                {{ trans('auth.register') }}
                <i class="entypo-user"></i>
            </button>
       
    </div>
</form>

@endsection
