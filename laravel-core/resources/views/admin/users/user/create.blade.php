@extends('admin.layouts.default')

@section('admin.content')
{{ Form::open(['method'=>'POST','route'=>'admin.users.store']) }}

    @include('admin.users.user.forms')

    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">{{trans('general.save')}}</button>
    </div>
{{ Form::close() }}
@endsection