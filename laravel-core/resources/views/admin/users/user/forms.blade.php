<div class="form-group">
    {{ Form::label('first_name', trans('auth.first_name')) }}
    {{ Form::text('first_name', null, ['class'=>'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('last_name', trans('auth.last_name')) }}
    {{ Form::text('last_name', null, ['class'=>'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('username', trans('auth.username')) }}
    {{ Form::text('username', null, ['class'=>'form-control']) }}
</div>
<div class="form-group">
    {{ Form::label('email', trans('auth.email')) }}
    {{ Form::text('email', null, ['class'=>'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('password', trans('auth.password')) }}
    {{ Form::password('password',  ['class'=>'form-control']) }}
</div>