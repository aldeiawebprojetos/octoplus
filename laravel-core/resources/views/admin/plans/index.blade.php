@extends('admin.layouts.default')

@section('admin.content')
    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <strong>{{trans('general.plan')}}</strong>
                <a class="pull-right" href="{{ route('admin.plans.create') }}">{{trans_choice('general.add',2,['item'=>trans('general.plan')])}}</a>
            </div>
            @if($plans->total())
            <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="selectAll">
                            <label class="custom-control-label" for="selectAll"></label>
                        </label>
                    </th>
                    <th>Name</th>
                    <th>Valor</th>
                    <th>Multiplos</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($plans as $plan)
                     <tr>
                        <td>
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="user{{ $plan->id }}">
                                <label class="custom-control-label" for="user{{ $plan->id }}"></label>
                            </label>
                        </td>
                        <td>{{ $plan->name }}</td>
                        <td>
                            {{ $plan->price }}
                        </td>
                        <td>{{ $plan->teams_enabled ? trans('general.yes') : trans('general.not') }}</td>
                        <td>
                            <a href="{{ route('admin.plans.edit',$plan->id) }}">{{ trans_choice('general.edit',2,['item'=>trans('general.plan')]) }}</a>
                        </td>
                    </tr> 
                @endforeach
                </tbody>
            </table>
            <div class="card-body">
                {{ $plans->links() }}
            </div>

        </div>

        @else
        <div class="card-body">
            <div class="card-text">{{trans_choice('general.not_found',2,['item' => trans('general.plan')])}}.</div>
        </div>
        @endif

    </div>
@endsection

