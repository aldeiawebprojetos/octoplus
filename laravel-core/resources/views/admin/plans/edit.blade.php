@extends('admin.layouts.default')

@section('admin.content')
{{ Form::model($plan,['method'=>'PUT', 'route'=> ['admin.plans.update', $plan->id]]) }}

    @include('admin.plans.forms')

    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">{{trans('general.save')}}</button>
    </div>
{{ Form::close() }}
@endsection