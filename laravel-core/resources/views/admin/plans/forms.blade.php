<div class="input-group">
    {{ Form::label('name',trans('general.name')) }}
    {{ Form::text('name', null, ['class'=>'form-control']) }}
</div>

<div class="input-group">
    {{ Form::label('price',trans('general.price')) }}
    {{ Form::text('price', null, ['class'=>'form-control']) }}
</div>

<div class="input-group">
    {{ Form::label('teams_enabled',trans('admin.teams')) }}
    {{ Form::checkbox('teams_enabled', 1, ['class'=>'form-control']) }}
</div>
<div class="input-group">
    {{ Form::label('teams_limit',trans('user.limit')) }}
    {{ Form::number('teams_limit', null, ['class'=>'form-control']) }}
</div>