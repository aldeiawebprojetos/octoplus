@extends('admin.layouts.default')

@section('admin.content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{ trans_choice('general.add',2,['item'=>trans('general.plan')]) }}</h4>

        <form action="{{ route('admin.plans.store') }}" method="post">
            {{ csrf_field() }}
                @include('admin.plans.forms')
            <div class="form-group row">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">{{trans('general.save')}}</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection