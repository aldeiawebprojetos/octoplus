@extends('account.layouts.default')

@section('title',  trans_choice('general.edit',2,['item' => trans('user.overview')]) )

@section('breadcrumb')
<li> <a href="{{route('account.index')}}">{{trans('user.account')}}</a> </li>

<li class="active"> <strong>{{ trans('user.overview') }}</strong> </li>	
@endsection

@section('account.content')
    <div class="card">
        <div class="card-body">
            <h1 class="card-title">{{trans('user.overview')}}</h1>
        </div>
        <div class="list-group list-group-flush">
            <div class="list-group-item">
                <h4>{{trans('general.name')}}</h4>
                <p>{{ auth()->user()->name }}</p>
            </div>
            <div class="list-group-item">
                <h4>{{trans('auth.email')}}</h4>
                <p>{{ auth()->user()->email }}</p>
            </div>
            @subscribed
                @notpiggybacksubscription
                    <div class="list-group-item">
                        <h4>{{trans('geneal.plan')}}</h4>
                        <p>{{  auth()->user()->plan->name }}</p>
                    </div>
                @endnotpiggybacksubscription
            @endsubscribed
            <div class="list-group-item">
                <h4>{{trans('general.joined')}}</h4>
                <p>{{ auth()->user()->created_at->diffForHumans() }}</p>
            </div>
        </div>
    </div>
@endsection