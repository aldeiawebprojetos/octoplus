{{ Form::open(['id' =>'searchForm','method'=>"GET"]) }}
<div class="filtering">
		
    <div class="row">
        <div class="col-sm-4">
            {{-- <div class="daterange daterange-inline add-ranges" data-format="MMMM D, YYYY" data-start-date="October 22, 2020" data-end-date="November 19, 2020">
                <i class="entypo-calendar"></i>
                <span>October 22, 2020 - November 19, 2020</span>
            </div> --}}
           <div class="form-group">
               <div class="input-group">
                <span class="input-group-addon"> <i class="entypo-calendar"></i> </span>
                {{form::text('log_date',isset( Request::query()['log_date']) ? Request::query()['log_date'] : null,['class' => 'daterange daterange-inline add-ranges input-lg form-control','onchange' =>'$("#searchForm").submit()', 'data-format'=>"DD/M/YYYY"])}}
               </div>
           </div>
        </div>
        <div class="col-sm-4 search-and-pagination">
            <!-- Search Form -->
            <div class="search-form-contaner">
    
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="entypo-user"></i></span>
                        {{ Form::select('user_filter', $users->pluck('name','id'), null, ['class' =>'input-lg form-control select2','placeholder' => trans('general.select').'...','onchange' => 'changeIdUrl("'.Request::path().'",this)']) }}
                    </div>
                    <span class="description"> {{ trans('logs.filter_by_user') }} </span>
                </div>
                
            </div>
        </div>
        <div class="col-sm-4 search-and-pagination">
            <!-- Pagination -->
            <div class="pagination-container">
                {{ $activities->links() }}                
            </div> 
        </div>
    </div>
    
</div>
{{ Form::close() }}

