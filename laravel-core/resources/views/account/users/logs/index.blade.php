@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('account.users.index')}}">{{trans('user.users')}}</a> </li>

<li class="active"> <strong>{{trans('general.logs')}}</strong> </li>	
@endsection

@section('title',trans('general.logs'))

@section('content')
@include('account.users.logs.filtersPagination')

<ul class="cbp_tmtimeline">
    <li>
        <time class="cbp_tmtime" datetime="{{date('Y-m-d')}}"><span class="hidden">{{date('d/m/Y')}}</span> <span class="large">{{trans("general.now")}}</span></time>
        
        <div class="cbp_tmicon">
            <i class="entypo-user"></i>
        </div>
        
        <div class="cbp_tmlabel empty">
            <span>{{trans('logs.no_logs')}}</span>
        </div>
    </li>
    @foreach ($activities as $activity)
 
    <li>
        <time class="cbp_tmtime" datetime="{{$activity->created_at}}"><span>{{$activity->created_at->format('d/m/Y')}}</span> <span>{!!$activity->created_at->format('H:i')!!}</span></time>
        
        <div class="cbp_tmicon bg-{{($activity->description == 'created' ? "success" : ($activity->description == 'updated' ? "warning": "danger"))}}">
            <i class="fa fa-dollar"></i>
        </div>
       
        <div class="cbp_tmlabel">
           
            <h2>
                <a href="#">
                    <img src="{{  ($user->firstMedia('users') != null ? $user->firstMedia('users')->getUrl() : asset('assets/images/profile-picture.png')) }}" alt="" class="img-circle" width="30" />
                    {{$user->name}}</a> 
                <span class=""> {{ trans_choice("logs.".$activity->description,2,['item' => trans($activity->log_name)]) }} #{{$activity->subject_id}}</span>
            </h2>

            <blockquote>
                @if($activity->description == 'created')
                    @foreach ($activity->properties['attributes'] as $key => $item)
                        @if ($item != null && $key != 'company_id')
                        <strong>{{__($key)}}: </strong> {{ $item }}<br>
                        @endif

                    @endforeach
                @endif

                @if($activity->description == 'updated')
                    @foreach ($activity->properties['attributes'] as $key => $item)
                       
                        @if (!is_array($item) && $item != null && $key != 'company_id')
                         <strong>{{__($key)}}: </strong> {!! $item == $activity->properties['old'][$key] ? $item : "<s>{$activity->properties['old'][$key]}</s> $item" !!}<br>
                        @endif
                    @endforeach
                @endif
            </blockquote>
 
            <p>
               @if(preg_match('/::/',$activity->log_name))
                    <i class="fa fa-desktop"></i> {{trans(explode('::',$activity->log_name)[0]."::general.name")}} > {{trans($activity->log_name)}} 
               @else
               <i class="fa fa-desktop"></i> {{trans('permissions.system')}} > {{trans($activity->log_name)}} 
               @endif
            </p>

        </div>
    </li>
   
    @endforeach

    
</ul>
    @include('account.users.logs.filtersPagination')
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset("assets/js/vertical-timeline/css/component.css")}}">
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">


<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker/daterangepicker.js')}}"></script>
@endsection