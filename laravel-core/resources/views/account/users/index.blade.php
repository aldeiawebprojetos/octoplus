@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('account.users.index')}}">{{trans('user.users')}}</a> </li>

<li class="active"> <strong>{{trans('general.listing')}}</strong> </li>	
@endsection

@section('title',trans('user.users'))

@section('content')
<div class="mail-env">
    <div class="mail-body">
				
        <div class="mail-header">
            <!-- Search  form -->
            
            <form method="get" role="form" class="search-form-full">
                <div class="form-group">
                    
                    <input type="text" class="form-control input-lg" value="{{ Request::query('s') }}" name="s" id="search-input" placeholder="{{trans_choice('placeholders.searchBy',2,['item'=> trans('general.name')])}}" />
                    <i class="entypo-search"></i>
                </div>
                
            </form>
        </div>

        <div class="mail-header">
            <!-- title -->
            <h1 class="mail-title">
                {{ trans('user.users') }}
                <span class="count text-info">({{$users->count()}})</span>
            </h1>
                      

            <!-- Filtrar por Categoria Produto -->					
            <div class="mail-search">
               
            </div>
            <!-- Delete para bulkactions ainda não implementado -->					
            {{-- <div class="pull-left">				
                <a href="#" class="btn btn-danger pull-left">
                    <i class="entypo-trash"></i>
                </a>
            </div> --}}

        </div>
      
       @include('account.users.table')
        
        {{ $users->links() }}
    </div>

    <div class="mail-sidebar">
				
        <!-- compose new email button -->
       @can('octoplus.users_create')
        <div class="mail-sidebar-row hidden-xs">
            <a href="{{route('account.users.create')}}" class="btn btn-blue btn-lg btn-icon btn-block">
                {{trans_choice('general.add',2,['item' => trans('user.user')])}}
                <i class="entypo-plus"></i>
            </a>
        </div>
       @endcan
       

        <!-- menu -->
        <ul class="mail-menu">
           
        
        </ul>
        
        <div class="mail-distancer"></div>
        
        <h4>{{ trans('general.status') }}</h4>
       
        {{-- <!-- menu -->
        <ul class="mail-menu">
            <li class="{{ Request::query('status') == '0' ? 'active' : null }}">
                <a href="?{{ http_build_query(Request::merge(['status' => 0])->query()) }}">
                    <span class="badge badge-danger badge-roundless pull-right"></span>
                    {{ trans('general.inactive') }}
                </a>
            </li>

            <li class="{{ Request::query('status') == 1 ? 'active' : null }}">
                <a href="?{{ http_build_query(Request::merge(['status' => 1])->query()) }}">
                    <span class="badge badge-success badge-roundless pull-right"></span>
                    {{ trans('general.active') }}
                </a>
            </li>
        </ul>
         --}}
    </div>
    
</div>
@endsection

@section('modals')
@include('common.modals.delete',['type' => trans('general.users')])
@endsection