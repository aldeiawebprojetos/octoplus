<div class="row">
    <div class="form-group col-md-2 col-xs-2">
        {{Form::label('cod', trans("general.code"))}}
        {{Form::text('cod', isset($user) ? $user->id : null, ['class' => 'form-control input-lg','readonly'])}}
      </div>

      <div class="form-group col-md-5 col-xs-10">
        {{Form::label('first_name',trans('auth.first_name'))}} <strong class="text-danger">*</strong>
        {{Form::text('first_name', null, ['class' => 'form-control input-lg'])}}
      </div>
      <div class="form-group col-md-5 col-xs-10">
        {{Form::label('last_name',trans('auth.last_name'))}} <strong class="text-danger">*</strong>
        {{Form::text('last_name', null, ['class' => 'form-control input-lg'])}}
      </div>
</div>

<div class="row">
  <div class="form-group col-md-4 col-xs-12">
    {{Form::label('email', trans("auth.email"))}}  <strong class="text-danger">*</strong> 
  
  <div class="input-group">
    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
    {{ Form::email('email', null, ['class'=>'form-control input-lg', 'placeholder' => trans_choice("placeholders.input",2,  ['attribute' => trans("auth.email"),'of' => '','description'=>'']),'required']) }}
    
</div>
<span class="description"> {{trans('auth.used_to_login')}}</span>
</div>
  <div class="form-group col-md-4 col-xs-12">
    {{ Form::label('phone', trans("user.phone")) }}
    {{ Form::number('phone', null, ['class' =>'form-control input-lg', 'placeholder' => '(999) 99999-9999']) }}
    <span class="description">  {{trans('auth.used_to_login')}} </span>
  </div>

  <div class="form-group col-md-4 col-xs-12"> 
   {{Form::label('document_id', trans('user.document_id'))}} 
    {{Form::text('document_id', null,['class' => 'form-control input-lg'])}}
    <span class="description">  </span>
  </div>
</div>

<div class="row">
  <div class="form-group col-md-2 col-xs-12">
    <label for="radio1">{{ trans('general.enabled') }} ?</label>
    
        <label class="switch switch-left-right">
            {{ Form::checkbox('activated', 1, null, ['class'=> 'switch-input']) }}
        <span class="switch-label switch-label-3" data-on="{{trans('general.yes')}}" data-off="{{trans('general.not')}}"></span> <span class="switch-handle"></span> </label>        
</div>

<div class="form-group col-md-10 col-xs-12">
    {{ Form::label('companies',trans("general.companies")) }} <strong class="text-danger">*</strong>
    {{ Form::select('companies[]', $user_companies->pluck('name','id'), null, ['class' => 'select2 form-control input-lg','multiple']) }}
   
</div>
</div>

<div class="row">
    <div class="form-group col-md-12 col-xs-12">
        <hr>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4 col-xs-12">
        <label for="">{{ trans('auth.send_link_instructions') }}</label>
    
        <div class="">
              <label class="switch switch-left-right">
                  {{ Form::checkbox('send_link_email', 1, null, ['class' => 'switch-input', 'data-toggle'=> 'collapse', 'data-target' => '#setPassword', 'aria-controls' => 'setPassword', 'aria-expanded' => 'false']) }}

            <span class="switch-label switch-label-3" data-on="{{trans('general.yes')}}" data-off="{{trans("general.not")}}"></span> <span class="switch-handle"></span> </label>
        </div>
            
    </div>
    
    
    
    <div class="form-group col-md-8 col-xs-12 collapse in" id="setPassword">
        <div class="form-group col-md-6 col-xs-6 " >
              {{form::label("password", trans('auth.password'))}}<strong class="text-danger">*</strong> 
    
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                {{ Form::password('password',['class' => 'form-control input-lg', 'placeholder' => trans('placeholders.input_password')]) }}
               
            </div>
            <span class="description"> {{ trans('auth.min_password') }} </span>
        </div>
    
        <div class="form-group col-md-6 col-xs-6">
              {{Form::label('password_confirmation', trans_choice("general.confirm",2,['item' =>  trans('auth.password')]))}} <strong class="text-danger">*</strong>
    
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                {{ Form::password('password_confirmation',['class' => 'form-control input-lg','placeholder' => trans("placeholders.repeat_password")]) }}
               
            </div>
        </div>
    </div>
</div>

<div class="form-group col-md-12 col-xs-12">
    <hr>
</div>


  <!--<div class="form-group col-md-12 col-xs-12">
    <p> {{ trans('item.image') }}: </p>
    <div class="fileinput fileinput-new" data-provides="fileinput">
      <div class="fileinput-new thumbnail" style="max-width: 310px; height: 160px;" data-trigger="fileinput">
        <img src="{{(isset($user) && $user->firstMedia('users') != null ? $user->firstMedia('users')->getUrl() : 'http://placehold.it/320x160')}}" alt="image">
      </div>
      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 320px; max-height: 160px"></div>
      <div>
        <span class="btn btn-white btn-file">
          <span class="fileinput-new">{{ trans('general.choose_image') }}</span>
          <span class="fileinput-exists">{{ trans_choice('general.change',2,['item'=>'']) }}</span>
          {{ Form::file('user_image', null, ['accept'=>'image/*']) }}
          
        </span>
        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">{{ trans('general.remove') }}</a>
      </div>
    </div>
</div>-->

  <div class="form-group col-md-12 col-xs-12">
    <p> Assinatura </p>
    <div class="fileinput fileinput-new" data-provides="fileinput">
      <div class="fileinput-new thumbnail" style="max-width: 310px" data-trigger="fileinput">
        <img src="{{(isset($user) && $user->assinatura != null ? 'data:image/gif;base64,' . $user->assinatura : 'http://placehold.it/320x160')}}" alt="image">
      </div>
      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 320px"></div>
      <div>
        <span class="btn btn-white btn-file">
          <span class="fileinput-new">{{ trans('general.choose_image') }}</span>
          <span class="fileinput-exists">{{ trans_choice('general.change',2,['item'=>'']) }}</span>
          {{ Form::file('assinatura_image', null, ['accept'=>'image/*']) }}
          
        </span>
        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">{{ trans('general.remove') }}</a>
      </div>
    </div>
  </div>


  <div class="form-group col-md-12 col-xs-12">
   {{ Form::label("description", trans('item.additional_info') )  }}
   {!! Form::textarea('description', isset($user) ? settings('company.userDescription.'.$user->id,null) : null, ['class'=>'form-control','rows' => 5]) !!}
  </div>


<div class="form-group col-md-12 col-xs-12">
    <hr>
</div>


<div class="form-group col-md-6 col-xs-6">
    <div class="">
        {{ trans('user.admin') }}  <button class="btn btn-default popover-default btn-xs" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Admin tem permissão de acesso total a todos os módulos, assim como controle de usuários e empresas." data-original-title="Admin"><i class="fa fa-star"></i></button>
        
        
        <div class="">
              <label class="switch switch-left-right">
                {{ Form::checkbox('admin', true, isset($user) ? $user->hasRole('admin') : null, ['class' => 'switch-input', 'data-toggle' => 'collapse', 'data-target' => '#rulesPermission', 'aria-expanded' => 'false', 'aria-controls' => 'rulesPermission']) }}

            <span class="switch-label switch-label-2" data-on="{{trans('general.yes')}}" data-off="{{ trans('general.not') }}"></span> <span class="switch-handle"></span> </label>
        </div>
        <span class="description">{{ trans('auth.admin_description') }} </span>
    </div>
</div>





  <div class="form-group col-md-6 col-xs-6 collapse in" id="collapseExample">
    @if ($regra_de_acesso->count() > 0)
      <label for=""><i class="fa fa-unlock-alt"></i> {{trans('auth.rules_access')}}  </label>
      @foreach ($regra_de_acesso as $regra)
        <div class="form-check">
            {!! Form::radio('regra_acesso_id', $regra->id, null, ['class'=>"form-check-input",'id'=>'regra_acesso_id']) !!}
            <label class="form-check-label" for="regra_acesso_id">
            {{$regra->name}}
            </label>
        </div>
      @endforeach
    @endif
</div>

<div class="form-group col-md-12 col-xs-12">
    <hr>
</div>

</div>

<hr>

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/js/fileinput.js')}}"></script>
@endsection