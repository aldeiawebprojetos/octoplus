@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('account.users.index')}}">{{trans('user.users')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.add',2,['item' => trans('user.user')])}}</strong> </li>	
@endsection

@section('title',trans('user.users'))

@section('content')

{{ Form::model($user,['method'=>'PUT', 'route' => ['account.users.update', $user->id],'enctype'=>'multipart/form-data']) }}

<div class="row">
     <!-- title -->
     <div class="col-md-6">
        <h1 class="pull-left">
            {{trans('user.users')}} <i class="entypo-pencil"></i>
        </h1>
    </div>

    @include('common.buttonsForm',['route_return' => 'account.users.index'])
</div>

@include('account.users.form')

<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'account.users.index'])
</div>

{{ Form::close() }}
@endsection