<table class="table table-bordered table-striped datatable" id="table-4">
    <thead>
        <tr>
            <th width="2%">{{ trans('general.actions') }}</th>
            <th width="2%">{{trans('general.code')}}</th>
            <th>{{trans("general.name")}}</th>
            <th>{{ trans('auth.email') }}</th>
            <th>{{ trans('general.status') }}</th>
            
            <th width="1%" class="text-center"> 
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_user" onchange="changeChkboxes(this,'user')" />
                </div>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)

        <tr class="odd gradeX">
            <td>
                @include('common.tableActions',['options' => [
                     ['route' => ['account.users.permissions',true], 'text' => trans('general.permissions'),'permission' => 'octoplus.assign_roles','icon' => 'fa fa-unlock-alt'],
                     ['route' => ['account.users.edit',true],'text' => trans('general.edit'),'icon' => 'entypo-pencil'],
                     ['route' => ['account.users.log',true],'text' => trans('general.log'),'icon' => 'fas fa-clipboard-list']
                    ],'id' => $user->id, 'route_delete'=>'account.users.delete','delete_permission' => 'octoplus.users_delete', 'name' => $user->name])
            </td>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ ($user->activated ? trans('general.active') : trans("general.inactive")) }}</td>
            <td>
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_user" name="users" value="{{$user->id}}" />
                </div>
            </td>
            
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th width="2%">{{ trans('general.actions') }}</th>
            <th width="2%">{{trans('general.code')}}</th>
            <th>{{trans("general.name")}}</th>
            <th>{{ trans('auth.email') }}</th>
            <th>{{ trans('general.status') }}</th>
            
            <th width="1%" class="text-center"> 
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_user" onchange="changeChkboxes(this,'user')" />
                </div>
            </th>
        </tr>
    </tfoot>
</table>