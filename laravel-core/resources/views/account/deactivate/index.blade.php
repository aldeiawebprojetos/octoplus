@extends('account.layouts.default')

@section('account.content')
    <div class="card">
        <div class="card-body">
            

            <form method="POST" action="{{ route('account.deactivate.store') }}">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="pull-left">
                            {{trans('auth.deactivate')}} <i class="entypo-pencil"></i>
                        </h1>
                    </div>
                   
                </div>
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                    <label for="current_password" class="control-label">{{ trans_choice('general.current_item',2,['item'=>trans('auth.password')]) }}</label>

                    <input id="current_password" type="password"
                           class="form-control col-sm-8{{ $errors->has('current_password') ? ' is-invalid' : '' }}  input-lg"
                           name="current_password"
                           required autofocus>

                    @if ($errors->has('current_password'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('current_password') }}</strong>
                        </div>
                    @endif
                </div>

                @subscriptionnotcancelled
                <div class="form-group">
                    <p class="form-text">
                        {{ trans('auth.cancel_subscribe') }}
                    </p>
                </div>
                @endsubscriptionnotcancelled

                <div class="form-group">
                    <button type="submit" class="btn btn-danger">
                        {{trans('auth.deactivate')}}
                    </button>

                    <p class="form-text">
                       {{ trans('auth.read_deactive') }}
                        <a href="#">{{ trans('auth.provacy_police') }}</a>
                    </p>
                </div>
                
            </form>
        </div>
    </div>
@endsection
