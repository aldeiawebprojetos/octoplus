@extends('account.layouts.default')

@section('title',  trans_choice('general.change',2,['item' => trans('auth.password')]) )

@section('breadcrumb')
<li> <a href="{{route('account.index')}}">{{trans('user.account')}}</a> </li>

<li class="active"> <strong>  {{trans_choice('general.change',2,['item' => trans('auth.password')])}}</strong> </li>	
@endsection


@section('account.content')
    <div class="card">
        <div class="card-body">
          

            <form method="POST" action="{{ route('account.password.store') }}">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="pull-left">
                            {{trans_choice('general.change',2,['item' => trans('auth.password')])}} <i class="entypo-pencil"></i>
                        </h1>
                    </div>
                    @include('common.buttonsForm')
                </div>
               
                {{ csrf_field() }}

                <div class="form-group row{{ $errors->has('current_password') ? ' has-error' : '' }}">
                    <label for="current_password" class="col-md-4 control-label">{{ trans_choice('general.current_item',2,['item'=>trans('auth.password')]) }}</label>

                    
                        <input id="current_password" type="password"
                               class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}  input-lg"
                               name="current_password"
                               required autofocus>

                        @if ($errors->has('current_password'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('current_password') }}</strong>
                            </div>
                        @endif
                   
                </div>

                <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">{{ trans('auth.new_password') }}</label>

                   
                        <input id="password" type="password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} input-lg" name="password"
                               required>

                        @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                    
                </div>


                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 control-label ">{{trans_choice('general.confirm',2,['item' => trans('auth.password')])}}</label>

                   
                        <input id="password-confirm" type="password" class="form-control  input-lg" name="password_confirmation"
                               required>
                    </div>
               

                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        @include('common.buttonsForm')
                    </div>
            </form>
        </div>
    </div>
@endsection
