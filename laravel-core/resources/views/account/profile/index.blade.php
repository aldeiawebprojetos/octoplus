@extends('account.layouts.default')

@section('title',  trans_choice('general.edit',2,['item' => trans('user.profile')]) )

@section('breadcrumb')
<li> <a href="{{route('account.index')}}">{{trans('user.account')}}</a> </li>

<li class="active"> <strong>{{ trans('user.profile') }}</strong> </li>	
@endsection

@section('account.content')
    <div class="card">

        <div class="card-body">
            <form method="POST" action="{{ route('account.profile.store') }}">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="pull-left">
                        {{trans('user.profile')}} <i class="entypo-pencil"></i>
                    </h1>
                </div>
                @include('common.buttonsForm')
            </div>
           
                {{ csrf_field() }}

                <div class="form-group row{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label for="first_name" class="col-md-4 control-label">{{ trans('auth.first_name') }}</label>

                    
                        <input id="first_name" type="text"
                               class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }} input-lg"
                               name="first_name"
                               value="{{ old('first_name', auth()->user()->first_name) }}" required autofocus>

                        @if ($errors->has('first_name'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </div>
                        @endif
                   
                </div>

                <div class="form-group row{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="last_name" class="col-md-4 control-label">{{ trans('auth.last_name') }}</label>
                        <input id="last_name" type="text"
                               class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }} input-lg" name="last_name"
                               value="{{ old('last_name', auth()->user()->last_name) }}" required autofocus>

                        @if ($errors->has('last_name'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </div>
                        @endif

                </div>

                <div class="form-group row{{ $errors->has('username') ? ' has-error' : '' }}">
                    <label for="username" class="col-md-4 control-label">{{ trans('user.user') }}</label>

                    
                        <input id="username" type="text"
                               class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }} input-lg" name="username"
                               value="{{ old('username', auth()->user()->username) }}" required autofocus>

                        @if ($errors->has('username'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('username') }}</strong>
                            </div>
                        @endif

                </div>

                <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">{{ trans('auth.email') }}</label>

                   
                        <input id="email" type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} input-lg" name="email"
                               value="{{ old('email', auth()->user()->email) }}" required>

                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                   
                </div>

                <div class="form-group row{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label for="phone" class="col-md-4 control-label">{{ trans('user.phone') }}</label>

                   
                        <input id="phone" type="text"
                               class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }} input-lg" name="phone"
                               value="{{ old('phone', auth()->user()->phone) }}" required autofocus>

                        @if ($errors->has('phone'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </div>
                        @endif
                    
                </div>

                <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">{{ trans('auth.password') }}</label>

                   
                        <input id="password" type="password"
                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} input-lg" name="password"
                               required>

                        @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                   
                </div>

                <div class="row">
                    <div class="col-md-6"></div>
                    @include('common.buttonsForm')
                </div>
            </form>
        </div>
    </div>
@endsection
