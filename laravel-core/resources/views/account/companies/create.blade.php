@extends('layouts.app')

@section('content')
    
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <h4 class="card-title">{{trans_choice('general.new',2,['item' => trans('general.company')])}}</h4>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
       
          {{Form::open(['method' => 'POST', 'route' => 'account.companies.store','enctype'=>'multipart/form-data'])}}
    
          <div class="row">
            <!-- title -->
            <div class="col-md-6">
               <h1 class="pull-left">
                   {{trans('general.company')}} <i class="entypo-pencil"></i>
               </h1>
           </div>
       
           @include('common.buttonsForm',['route_return' => 'account.companies.index'])
       </div>

            @include('account.companies.form')
    
          
    
        {{Form::close()}}
    </div>
</div>
@endsection
