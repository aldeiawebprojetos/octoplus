@extends('layouts.app')

@section('content')
   
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Empresas</h4>
                        <div class="card-subtitle">
                            Lista de Empresas da qual você faz parte.
                        </div>
                    </div>
                    <div class="list-group list-group-flush">
                        @foreach($companies as $company)
                            <div class="list-group-item d-flex justify-content-between align-items-center">

                                <h2>
                                    {{ $company->name }}
                                </h2>

                                <aside>
                                    <a href="{{ route('tenant.switch', $company) }}" class="btn btn-default btn-xs">
                                        <i class="fa fa-info"></i>
                                    {{ trans('general.view') }}</a>

                                    &nbsp;&nbsp;&nbsp;
                                    
                                    <a href="{{route('account.companies.edit', $company)}}" class="btn btn-info btn-xs">
                                       {{ trans('general.edit') }}
                                       <i class="fa fa-edit"></i>
                                    </a>
                                </aside>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

@endsection