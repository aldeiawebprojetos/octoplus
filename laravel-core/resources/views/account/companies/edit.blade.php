@extends('layouts.app')

@section('content')
    
<div class="row">
    <div class="col-md-12">
       
        {{Form::model($company,['method' => 'PUT', 'route' => ['account.companies.update',$company->id],'enctype'=>'multipart/form-data'])}}

        <div class="row">
            <!-- title -->
            <div class="col-md-6">
               <h1 class="pull-left">
                   {{trans('general.company')}} <i class="entypo-pencil"></i>
               </h1>
           </div>
       
           @include('common.buttonsForm',['route_return' => 'account.companies.index'])
       </div>

            @include('account.companies.form')
    
          
    
        {{Form::close()}}
    </div>
</div>
@endsection
