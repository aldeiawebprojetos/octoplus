<div class="row">
    
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-6 col-xs-12">
        {{ Form::label('name', trans('general.name')) }} <strong class="text-danger">*</strong>
        {{ Form::text('name', null,['required','autofocus','class' => 'form-control  input-lg' . ($errors->has('name') ? ' is-invalid' : '')]) }}
        @if ($errors->has('name'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </div>
        @endif
    </div>
    

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-6 col-xs-12">
        {{ Form::label('email', trans('auth.email')) }} <strong class="text-danger">*</strong>
        {{ Form::text('email', null,['required','autofocus','class' => 'form-control  input-lg' . ($errors->has('email') ? ' is-invalid' : '')]) }}
        @if ($errors->has('email'))
            <div class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
    </div>

</div>

<div class="row">

    <div class="form-group col-md-6 col-xs-12">
        {{ Form::label("document", trans('customers::general.cpf')."/".trans('customers::general.cnpj'))}}
        {{ Form::text('document', (isset($company) ? formatar_cpf_cnpj($company->document) : null),['class' => "form-control input-lg"]) }}
        
    </div>
    
    <div class="form-group col-md-6 col-xs-12">
        {{ Form::label("phone", trans('user.phone'))}}
        {{ Form::text('phone', null,['class' => "form-control input-lg"]) }}
    </div>

</div>


<div class="row">

    <div class="form-group col-md-6 col-xs-12">
        {{ Form::label("address", trans('customers::general.address'))}}
        {{ Form::textarea('address', null,['class' => "form-control"]) }}
    </div>

    <div class="form-group col-md-6 col-xs-12">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail" style="max-width: 310px; height: 160px;" data-trigger="fileinput">
                <img src="{{(isset($company) && $company->firstMedia('logo') != null ? $company->firstMedia('logo')->getUrl() : 'http://placehold.it/320x160')}}" alt="logo">
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 320px; max-height: 160px"></div>
            <div>
                <span class="btn btn-white btn-file">
                    <span class="fileinput-new">Escolha a Imagem</span>
                    <span class="fileinput-exists">Alterar</span>
                    <input type="file" name="image" accept="image/*">
                </span>
                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover</a>
            </div>
        </div>
    </div>

</div>

@section('scripts')
<script src="{{asset('assets/js/fileinput.js')}}"></script>
@endsection