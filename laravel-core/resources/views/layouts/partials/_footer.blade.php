


		<!-- Footer -->
		<footer class="main hidden-print">
			<div class="row">
				<!--
				<div class="col-md-6 clearfix">
					<label class="control-label">  {{trans("general.noturn_mode")}} </label>

					<div class="make-switch" data-text-label="<i class='entypo-moon'></i>">
						<input type="checkbox" checked />
					</div>

				</div>
			-->

				<div class="col-md-6 clearfix">
					&copy; 2020-{{date('Y')}}
					<!--
					<strong>8plus.me</strong> 
					--> 
				</div>
				
			</div>

			<div class="row">
				<div class="col-md-6">
					
				</div>

				<div class="col-md-6">
					<i class="fa fa-spinner"></i> {{trans('general.version')}} <strong>{{version()}}</strong>  
				</div>
				
			</div>
							
			
			
		
		</footer>