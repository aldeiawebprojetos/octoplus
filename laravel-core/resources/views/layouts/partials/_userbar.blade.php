
		<!-- Profile Info and Notifications -->
			<div class="col-md-6 col-sm-8 clearfix">
				
				<ul class="user-info pull-left pull-right-xs pull-none-xsm">			
			<!-- Task Notifications -->
			<li class="notifications dropdown">
		
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="fas fa-building"></i>
					{{ (session('tenant') != null && $user_companies->where('id', session('tenant'))->first() != null ? $user_companies->where('id', session('tenant'))->first()->name : null )}}
				</a>

				<ul class="dropdown-menu">
					<li class="top">
						<p><!-- Create New Company Link -->
							<a class="dropdown-item" href="{{ route('account.companies.create') }}">
								{{trans_choice('general.new',2,['item' => trans('general.company')])}}
							</a></p>
					</li>
					
					<li>
						<ul class="dropdown-menu-list scroller">
							@foreach($user_companies as $company)
							<li>
								<a class="" href="{{ route('tenant.switch', $company) }}">
								<img src="" alt="">
								<span>{{ $company->name }}</span>
								</a>
							</li>
							@endforeach
						</ul>
					</li>
					
					<li class="external">
						<a class="dropdown-item" href="{{ route('account.companies.index') }}">
							{{trans('general.viewall')}}
						</a>
					</li>
				</ul>

			</li>
				
					<!-- Raw Notifications -->
					<li class="dropdown notifications">
		
						<button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="fas fa-cogs"></i>
							<span class="caret"></span>
						</button>
		
						<ul class="dropdown-menu">
						<!-- Impersonating -->
						@impersonating
							<li>

								<a  href="#"
								   onclick="event.preventDefault(); document.getElementById('impersonate-form').submit();">
								   {{trans('admin.stop.impersonating')}}
								</a>
								<form id="impersonate-form" action="{{ route('admin.users.impersonate.destroy') }}"
									  method="POST" style="display: none;">
									@csrf
									@method('DELETE')
								</form>
								
						  </li>
						@endimpersonating
						 <!-- Admin Panel Link -->
						 @role('admin-root')
						  <li>
							  
							   <a  href="{{ route('admin.dashboard') }}">
								<i class="fas fa-users-cog"></i>
								   {{trans('admin.panel')}}
							   </a>
							  
						  </li>
						  @endrole
						  <li>
							<!-- Developer Link -->
							<a  href="{{ route('developer.index') }}">
								<i class="fas fa-code"></i>
								{{trans('user.developer')}}
							</a>
						</li>
						{!! menu('settings') !!}
						</ul>
		
					</li>
		
				</ul>
		
			</div>
		
		
			<!-- Raw Links -->
			<div class="col-md-6 col-sm-4 clearfix hidden-xs">
		
				<ul class="list-inline links-list pull-right">
	
					<li class="">
						<a href="{{ route('logout') }}"
								onclick="event.preventDefault();
													  document.getElementById('logout-form').submit();">
								<i class="fa fa-power-off right"></i>
								{{trans('user.logout')}}
							 </a>
 
							 @include('layouts.partials.forms._logout')
					</li>
				</ul>
		
			</div>
