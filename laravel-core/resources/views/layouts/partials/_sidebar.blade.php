<div class="sidebar-menu">
		<div class="sidebar-menu-inner">
			
			<header class="logo-env">
			
				<!-- logo -->
				<div class="logo">
					<a href="#">
						<img src="{{ asset('assets/images/logo@2x.png') }}" width="120" alt="" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon" style="padding: 3px 5px"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-switch"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-switch"></i>
					</a>
				</div>

			</header>
			
			<div class="sidebar-user-info">

				<div class="sui-normal">
					<a href="#" class="user-link">
						<img src="{{ (auth()->user()->firstMedia('users') == null ? asset('assets/images/thumb-1@2x.png') : auth()->user()->firstMedia('users')->getUrl()) }}" width="55" alt="" class="img-circle" />
						<span>{{trans('general.welcome')}},</span>
						<strong>{{ Auth::user()->short_name }} </strong>
						<strong > <i class="fa fa-unlock-alt"></i> 
						 <i class="fa fa-caret-down"></i> </strong>
					</a>
				</div>

				<div class="sui-hover inline-links animate-in"><!-- You can remove "inline-links" class to make links appear vertically, class "animate-in" will make A elements animateable when click on user profile -->
						

						<!-- User Account Link -->
						<a  href="{{ route('account.index') }}">
							<i class="fas fa-user"></i>
							{{trans('user.account')}}
						</a>
					
					@can('octoplus.users_show')
						<!-- User Manage Link -->
						<a  href="{{ route('account.users.index') }}">
							<i class="fas fa-users"></i>
							{{trans('user.users')}}
						</a>
					@endcan
					<span class="close-sui-popup">&times;</span><!-- this is mandatory -->				</div>
			</div>				

                {!! menu('admin') !!}

			
		</div>

	</div>