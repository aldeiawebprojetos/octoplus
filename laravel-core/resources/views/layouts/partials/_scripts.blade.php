<script>jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"><\/script>')</script>


<!-- Scripts Comuns para toda a aplicação -->
<script src="{{asset('assets/js/gsap/TweenMax.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/joinable.js')}}"></script>
<script src="{{asset('assets/js/resizeable.js')}}"></script>
<script src="{{asset('assets/js/neon-api.js')}}"></script>
<script src="{{asset('assets/js/toastr.js')}}"></script>
<script src="{{asset('js/app.js')}}?v={{ filemtime('js/app.js') }}"></script>
<script src="{{asset('js/notify.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-switch.min.js')}}"></script>

<script> 
   
    var daterange_translateData = {
                                today: "{{ trans('calendar.today') }}",
                                yesterday: "{{ trans('calendar.yesterday') }}",
                                last_7_days: "{{ trans('calendar.last_7_days') }}",
                                last_30_days: "{{ trans('calendar.last_30_days') }}",
                                this_month: "{{ trans('calendar.this_month') }}",
                                last_month: "{{ trans('calendar.last_month') }}",   
                                month_names: [
                                    "{{ trans('calendar.months.january') }}",
                                    "{{ trans('calendar.months.february') }}",
                                    "{{ trans('calendar.months.march') }}",
                                    "{{ trans('calendar.months.april') }}",
                                    "{{ trans('calendar.months.may') }}",
                                    "{{ trans('calendar.months.june') }}",
                                    "{{ trans('calendar.months.july') }}",
                                    "{{ trans('calendar.months.august') }}",
                                    "{{ trans('calendar.months.september') }}",
                                    "{{ trans('calendar.months.october') }}",
                                    "{{ trans('calendar.months.november') }}",
                                    "{{ trans('calendar.months.december') }}",
                                ], 
                                apply: "{{ trans('calendar.apply') }}",
                                cancel: "{{ trans('calendar.cancel') }}",
                                from: "{{ trans('calendar.from') }}",
                                to: "{{ trans('calendar.to') }}",
                                custom: "{{ trans('calendar.custom') }}",
                                days_of_week: [
                                    "{{ trans('calendar.days_of_week.su') }}",
                                    "{{ trans('calendar.days_of_week.mo') }}",
                                    "{{ trans('calendar.days_of_week.tu') }}",
                                    "{{ trans('calendar.days_of_week.we') }}",
                                    "{{ trans('calendar.days_of_week.th') }}",
                                    "{{ trans('calendar.days_of_week.fr') }}",
                                    "{{ trans('calendar.days_of_week.sa') }}",
                                ]
                                  }; 
    </script>
<!-- Custom Scripts -->
@yield('scripts')


@if($errors->any())
<script> 
 
@foreach($errors->all() as $error)
    sendNotify('warning','Não foi possivel salvar',"{{ $error }}");
@endforeach


</script>
@endif

@if(session('notify'))
 <script type="text/javascript">sendNotify("{{ session('notify')['type'] }}","{{ session('notify')['title'] }}","{{ session('notify')['message'] }}");</script>
@endif

<!-- JavaScripts initializations and stuff -->
<script src="{{asset('assets/js/neon-custom.js')}}"></script>



