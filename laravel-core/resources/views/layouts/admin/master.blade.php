<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.partials._head')
<link rel="stylesheet" href="{{asset('assets/css/font-icons/entypo/css/entypo.css')}}">
</head>
<body class="page-body page-fade" data-url="{{$_ENV['APP_URL']}}">
    <div id="app" class="page-container">

        @yield('sidebar')
       
{{-- <div class="main-content">

    <div class="row">
       
            @include('layouts.partials._userbar')
        
    </div>
<hr> --}}

   <div class="row">
    <div class="col-md-12">
        @yield('content')
    </div>
   </div>

    @include('layouts.partials._footer')
</div>

    </div>

    @include('layouts.partials._scripts')
</body>
</html>
