<ol class="breadcrumb 2 hidden-print" >
    <li> <a href="{{route('home')}}"><i class="entypo-cd"></i>{{trans('general.home')}}</a> </li>

    @yield('breadcrumb')
</ol>
