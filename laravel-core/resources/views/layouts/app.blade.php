<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.partials._head')
    <link rel="stylesheet" href="{{asset('assets/css/font-icons/entypo/css/entypo.css')}}">
            <!-- This is needed when you send requests via Ajax -->
            <script type="text/javascript">
                var baseurl ='{{ getenv('APP_URL') }}';
            </script>
</head>
<body class="page-body gray page-left-in" data-url="{{ getenv('APP_URL') }}">
    <div id="app" class="page-container">

        @include('layouts.partials._sidebar')

       
<div class="main-content">

    <div class="row hidden-print">
       
            @include('layouts.partials._userbar')
        
    </div>
<hr>
    @include('layouts.admin.partials._breadcrumb')
    
   <div class="row">
    <div class="col-md-12">
        @yield('content')
    </div>
   </div>
   <div class="clear"></div>
    @include('layouts.partials._footer')
</div>

    </div>
    @yield('modals')
    @include('layouts.partials._scripts')
</body>
</html>
