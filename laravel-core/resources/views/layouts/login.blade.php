<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Neon Admin Panel" />
        <meta name="author" content="" />
    
        <link rel="icon" href="assets/images/favicon.ico">
    
        <title>{{getenv('APP_NAME')}} | Login</title>
    
        <link rel="stylesheet" href="{{asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/font-icons/entypo/css/entypo.css')}}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/neon-core.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/neon-theme.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/neon-forms.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    
        <script src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
    
        <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    
    
    </head>

    <body class="page-body login-page login-form-fall" data-url="">


        <!-- This is needed when you send requests via Ajax -->
        <script type="text/javascript">
        var baseurl = '';
        </script>
        <div class="login-container">
	
            <div class="login-header login-caret">
                
                <div class="login-content">
                    <!--
                    <a href="" class="logo">
                        <img src="assets/images/logo@2x.png" width="120" alt="" />
                    </a>
                    -->
                    <p class="description">@yield('description')</p>
                    
                    <!-- progress bar indicator -->
                    <div class="login-progressbar-indicator">
                        <h3>43%</h3>
                        <span>logging in...</span>
                    </div>
                </div>
                
            </div>
            
            <div class="login-progressbar">
                <div></div>
            </div>
            
            <div class="login-form">
                
                <div class="login-content">
                    
                    <div class="form-login-error">
                        <h3>Login Inválido!</h3>
                        <p>
                        </p>
                    </div>
                    
                    @yield('form')
                    
                    
                    <div class="login-bottom-links">
                        @yield('bottom-links')
                        
                        <br />
                        <!--
                        <a href="#">&copy; Octoplus</a>  - <a href="#">Privacy Policy</a>
                        -->
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
        
            <!-- Bottom scripts (common) -->
            <script src="{{asset('assets/js/gsap/TweenMax.min.js')}}"></script>
            <script src="{{asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
            <script src="{{asset('assets/js/bootstrap.js')}}"></script>
            <script src="{{asset('assets/js/joinable.js')}}"></script>
            <script src="{{asset('assets/js/resizeable.js')}}"></script>
            <script src="{{asset('assets/js/neon-api.js')}}"></script>
            <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
            <script src="{{asset('assets/js/neon-login.js')}}"></script>
        
        
            <!-- JavaScripts initializations and stuff -->
            <script src="{{asset('assets/js/neon-custom.js')}}"></script>
            <script src="{{asset('assets/js/toastr.js')}}"></script>
            <script src="{{asset('js/notify.js')}}"></script>
            @yield('scripts')
            @if($errors->any())
            <script>
            @foreach($errors->all() as $error)
                sendNotify('warning','Não foi possivel salvar',"{{ $error }}");
            @endforeach
            </script>
            @endif
            
            @if(session('notify'))
             <script type="text/javascript">sendNotify("{{ session('notify')['type'] }}","{{ session('notify')['title'] }}","{{ session('notify')['message'] }}");</script>
            @endif
            

    </body>
</html>