<?php

return [
    'permissions_of_module'                        => 'Permissões do Módulo :module',
    'screen_modules_permission'                    => 'Permissão de telas do módulo :module',
    'screen_system_permission'                     => 'Permissão de telas do Sistema',
    'system'                                       => 'Sistema',
    'permissios_designed'                          => 'Permissões Designadas',
    'checkAll'                                     => 'Marcar Todos',
    'uncheckAll'                                   => 'Desmarcar Todos',
    'save_rule'                                    => 'Salvar regra',
    'rule_name'                                    => 'Nome da regra',
    'rule_name_required'                           => 'Para Salvar uma Regra, você precisa informar um nome',
    'rule_saved'                                   => 'Regra de Acesso Salva',
    'admin_description'                            => 'Admin tem permissão de acesso total a todos os módulos, assim como controle de usuários e empresas.',
    'select_to_change'                             => 'Selecione para Trocar de Usuário',

];