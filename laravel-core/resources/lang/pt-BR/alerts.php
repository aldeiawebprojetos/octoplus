<?php
return [
    'success'                           => 'Sucesso',
    'error'                             => 'Erro',
    'warning'                           => 'Alerta',
    'item_updated'                      => ':item Editado com Sucesso!',
    'item_updated_error'                => 'Não foi possível atualizar, tente novamente',
    'item_created'                      => ':item Criado com Sucesso',   
    'item_created_error'                => 'Não foi possível Cadastrar, tente novamente.',
    'item_deleted'                      => ':item Apagado com sucesso',
    'item_deleted_error'                => 'Não foi possível apagar, Favor Tente novamente',
    'sistem_info'                       => 'Informação do sistema',
    'forbidden_action'                  => 'Você não tem Permissão para Executar Esta ação',
];