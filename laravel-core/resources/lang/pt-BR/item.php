<?php

return [
   'storage'                             => 'Estoque',
   'type'                                => 'Tipo',
   'sale'                                => 'Venda',
   'rent'                                => 'Locação',
   'service'                             => 'Serviços',
   'details'                             => 'Características Produtos',
   'storage'                             => 'Estoque',
   'storage_min'                         => 'Estoque Mínimo',  
   'barcode'                             => 'Cód. de Barras',
   'sku'                                 => 'SKU',
   'unit_type'                           => 'Unidade de Medida',
   'additional_info'                     => 'Informações Adicionais',
   'image'                               => 'Imagem do Produto',
   'sku_details'                         => 'Unidade de Manutenção de Estoque é um código personalizado utilizado para auxiliar a gestão dos produtos. Esse item pode ser desativado em configurações',
   'type_details'                        => 'Os Tipo podem ser ativados ou desativados nas configurações',
];