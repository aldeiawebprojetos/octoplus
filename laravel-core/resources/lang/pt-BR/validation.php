<?php

return [
    'required'             => 'O campo :attribute é obrigatório.',
    'confirmed'            => 'O campo de confirmação de :attribute  não conincide.',
    'unique'               => 'O :attribute informado já está em uso.',
    'size'                 => [
        'numeric' => 'O campo :attribute Excede o limite máximo de :size.',
        'file'    => 'O campo  :attribute Excede o limite máximo de :size kilobytes.',
        'string'  => 'O campo  :attribute Excede o limite máximo de :size caracteres.',
        'array'   => 'O campo :attribute Excede o limite máximo de :size itens.',
    ],
];