<?php
return [
    'created'                                       => 'Cadastrou um :item',
    'updated'                                       => 'Editou um :item',  
    'deleted'                                       => 'Excluiu o :item',
    'no_logs'                                       => 'Nenhuma Atividade',
    'filter_by_user'                                => 'Filtrar por Usuário',
];