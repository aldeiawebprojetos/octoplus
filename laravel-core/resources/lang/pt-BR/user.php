<?php

return [
    'user'                              => 'Usuário',
    'users'                             => 'Usuários',

    'account'                           => 'Conta',
    'phone'                             => 'Telefone',
    'dial_code'                         => 'Código do País',
    'developer'                         => 'Desenvolvedor',
    'logout'                            => 'Sair',
    'overview'                          => 'Visão Geral da Conta',
    'profile'                           => 'Perfil',
    'limit'                             => 'Limite de usuários',
    'client'                            => 'Cliente',
    'document_id'                       => 'CPF',
    'admin'                             => 'Administrador'
    
   
];