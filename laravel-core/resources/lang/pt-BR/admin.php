<?php

return [

    'admin'                         => 'Administrador',
    'panel'                         => 'Painel Administrativo',

    'stop'                              => [
        'impersonating'                 => 'Parar Personificação'
    ],

    'impersonate'                     => 'Personificar',
    'permissions'                     => 'Permissões',
    'roles'                           => 'Papéis',
    'teams'                           => 'Multiplos Usuários'
];