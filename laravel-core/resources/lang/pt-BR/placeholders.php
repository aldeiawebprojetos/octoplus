<?php
return [
    'input'                               => 'Digite o :attribute :of :description',
    'descriptions'                        => 'Coloque informações',
    'sku'                                 => 'Unidade de Manutenção de Estoque',
    'searchBy'                            => 'Pesquise por :item',
    'input_password'                      => 'Digite a Senha',
    'repeat_password'                     => 'Repita a Senha'
];