<?php

return [
    'email'                             => 'Email',
    'password'                          => 'Senha',
    'login'                             => 'Login',
    'auth'                              => 'Autenticação',
    '2fauth'                            => 'Autenticação de dois fatores',
    'api'                               => 'Token de API',
    'remember'                          => 'Lembrar de mim',
    'forgot'                            => 'Esqueceu a senha?',
    'resend'                            => [
        'email'                         => 'Reenviar',
        'activate_email'                =>  'Reenviar Email de Ativação'
    ],
    'enable'                            => 'Ativar',
    'register'                          => 'Cadastre-se',
    'first_name'                        => 'Nome',
    'last_name'                         => 'Sobrenome',
    'accept_terms'                      => 'Eu aceito os',
    'service_terms'                     => 'Termos de Serviço',
    'provacy_police'                    => 'Política de Provacidade',
    'new_password'                      => 'Nova Senha',
    'deactivate'                        => 'Desativar Conta',
    'cancel_subscribe'                  => 'Isso também cancelará sua assinatura ativa.',
    'read_deactive'                     => 'Leia mais sobre desativação de contas em nossa',
    'used_to_login'                     => 'Pode ser usado para efetuar login no sistema. ',
    'min_password'                      => 'Mínimo 8 Caracteres.',    
    'send_link_instructions'            => 'Enviar Link com instruções de redefinição de senha',
    'admin_description'                 => ' O Administrador tem acesso máximo ao sistema. Somente o Administrador tem acesso a Usuário, Configurações, Log de Atividade.',
    'user_permission'                   => 'Permissões de Acesso',
    'rules_access'                      => 'Regras de Acesso',
    'admin'                             => 'Administrador',
    'last_access'                       => 'Último Acesso'
];