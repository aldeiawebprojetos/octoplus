<?php
/**
 * Created by PhpStorm.
 * User: Cuthbert Mirambo
 * Date: 4/28/2018
 * Time: 7:01 PM
 */

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here is where you can register 'tenant' routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "tenant" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'tenant.'], function () {

   
/**
 * Categories
 */

Route::group(['namespace' => 'Categories\Controllers', 'prefix' => 'categories', 'as' => 'categories.'], function () {

        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('create','CategoryController@create')->name('create');
        Route::post('store', 'CategoryController@store')->name("store");
        Route::get('{category_id}/edit', 'CategoryController@edit')->name('edit');
        Route::get('{category_id}/show', 'CategoryController@show')->name('show');
        Route::put('{category_id}/update', 'CategoryController@update')->name('update');
        Route::delete('{category_id}/delete', 'CategoryController@delete')->name('delete');

});

/**
 *  Items
 */

Route::group(['namespace' => 'Items\Controllers', 'prefix' => 'items', 'as' => 'items.'], function(){

    Route::get('/', 'ItemController@index')->name('index');
    Route::get('create', 'ItemController@create')->name('create');
    Route::get('{item_type}/create','ItemController@create')->name('type.create');
    Route::post('store', 'ItemController@store')->name("store");
    Route::post('changeInvoiceStatus', 'ItemController@changeInvoiceStatus')->name("changeInvoiceStatus");
    Route::get('{item_id}/edit', 'ItemController@edit')->name('edit');
    Route::get('{item_id}/show', 'ItemController@show')->name('show');
    Route::put('{item_id}/update', 'ItemController@update')->name('update');
    Route::delete('{item_id}/delete', 'ItemController@delete')->name('delete');

    Route::get('{item_id}/log','ItemController@logItem')->name('log');

    Route::get('getItem','ItemController@searchItems')->name('search');

    Route::group(['prefix' => 'unitTypes', 'as'=>'unitTypes.'], function () {
        Route::get('/','UnitTypeController@index')->name("index");
        Route::get('create','UnitTypeController@create')->name("create");
        Route::post('store','UnitTypeController@store')->name('store');
    });

    Route::group(['prefix'=>'categories', 'as' => 'categories.'], function(){
        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('create','CategoryController@create')->name('create');
        Route::post('store', 'CategoryController@store')->name("store");
        Route::get('{category_id}/edit', 'CategoryController@edit')->name('edit');
        Route::put('{category_id}/update', 'CategoryController@update')->name('update');
    });

});

Route::group(['prefix' => 'taxes', 'as' => 'taxes.','namespace' => 'Currencies\Controllers'], function () {
    
    Route::post('store','TaxController@store')->name('store');
    Route::get('getTaxes','TaxController@searchTaxes')->name('search');
   
});

Route::group(['prefix' => 'currencies', 'as' => 'currencies.', 'namespace' => 'Currencies\Controllers'], function () {
    // Route::get('/', 'CategoryController@index')->name('index');
    // Route::get('create', 'CategoryController@create')->name('create');
    // Route::post('store', 'CategoryController@store')->name("store");

    // Route::get('{currency_id}/edit', 'CategoryController@edit')->name('edit');
    // Route::put('{currency_id}/update', 'CategoryController@update')->name('update');
    // Route::delete('{currency_id}/delete', 'CategoryController@delete')->name('delete');
});

Route::group(['namespace' => 'Tenant\Controllers'], function () {
    /**
     * --------------------------------------------------------------------------
     * Dashboard
     *
     * Alternative tenant dashboard route
     * --------------------------------------------------------------------------
     *
     * All other tenant routes should go above this one
     */
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    /**
     * Alternative tenant switch route
     *
     * Switch Tenant Route
     */
     Route::get('/{company}', 'TenantSwitchController@switch')->name('switch');
    });

});

