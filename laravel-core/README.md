# Criação de Módulos
- Ao criar um módulo, é necessário que se edite o arquivo module.json a fim de incluir os provides do módulo a serem carregados.

# Versão 0.1.0
- Recurso de itens adicionado ao sistema
- Recurso de Categorias Adicionado

# Versão 0.2.0
- Recurso de Parceiros adicionado no sistema
- CRUD de Parceiros Adicionado

# Versão 0.3.0 
- Recurso de Permissão e usuários adicionado ao sistema

# Versão 0.4.0 
- Recurso de logs de ações adicionado no Sistema

# Versão 0.5.0 
- Módulo Financeiro <EM DESENVOLVIMENTO>

# Versão 0.6.0 - Não finalizado, Aguardando Financeiro
- Recurso de venda adicionado no sistema
- Recurso de Aluguel Aguardando Financeiro