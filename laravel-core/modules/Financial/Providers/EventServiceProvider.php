<?php

namespace Modules\Financial\Providers;


use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Financial\Events\CreateFatura;
use Modules\Financial\Listeners\AccountTranzaction;
use Modules\Financial\Events\TranzactionCreated;
use Modules\Financial\Listeners\GetCreateFatura;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        TranzactionCreated::class => [
            AccountTranzaction::class
        ],
        CreateFatura::class => [
            GetCreateFatura::class
        ]
    ];

    public function boot()
    {
        parent::boot();
    }
}
