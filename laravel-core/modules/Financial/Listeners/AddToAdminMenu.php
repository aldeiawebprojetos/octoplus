<?php

namespace Modules\Financial\Listeners;


use Octoplus\Events\Menu\AdminCreated as Event;

class AddToAdminMenu
{
    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
       
        $menu = $event->menu;
       
        if(module_enabled('financial'))
        {


            if(auth()->user()->can('financial.income_show') || auth()->user()->can('financial.expenses_show'))
            {
                $menu->dropdown(trans('financial::general.finances'), function($sub)
                {
                    if(auth()->user()->can('financial.income_show'))
                    {
                        $sub->route('financial.incomes.index', trans('financial::general.incomes'), [], 1,['icon'=>'fas fa-arrow-up']);
                    }

                    if(auth()->user()->can('financial.expenses_show'))
                    {
                        $sub->route('financial.expenses.index', trans('financial::general.expenses'), [], 1,['icon'=>'fas fa-arrow-down']);
                    }

                    if(auth()->user()->can('octoplus.categories_show')){
                    
                        $sub->route('financial.categories.index', trans("general.categories"),[],2,['icon'=>'entypo-flow-tree']);
                        }

                        if(auth()->user()->can('financial.account_show'))
                        {
                            $sub->dropdown(trans('financial::general.bank'), function($sub2)
                            {
                                if(auth()->user()->can('financial.account_show'))
                                {
                                    $sub2->route('financial.accounts.index', trans('financial::general.account'), [], 1,['icon'=>'fas fa-piggy-bank']);
                                }                   
            
                            },[
                                'icon'=>'fas fa-university'
                            ]);
                        }
                    

                },[
                    'icon'=>'fas fa-dollar-sign'
                ]);
            }

            // if(auth()->user()->can('financial.expenses_show'))
            // {
            //     $menu->dropdown(trans('financial::general.bank'), function($sub)
            //         {
            //             $sub->route('financial.expenses.index', trans('financial::general.accounts'), [], 1,['icon'=>'fa fa-arrow-up']);
            //         },[
            //             'icon'=>'fas fa-dollar-sign'
            //         ]);
            // }
        }
   
    }

    
}