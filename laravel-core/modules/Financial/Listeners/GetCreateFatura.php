<?php

namespace Modules\Financial\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Financial\Models\Fatura;
use Modules\SaleRent\Models\Invoice;
use Octoplus\Domain\Item\Models\Item;
use Octoplus\Domain\Account;

class GetCreateFatura
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $data = $event->FaturasData;
        $invoice = $event->Invoice;
        $items_id = $event->Items_id;
        
        if(isset($items_id)){
            $items = '   Iten(s):';
            foreach($items_id as $item_id){
                $items = $items . ' Cod.: ' . Item::find( $item_id )->code;
            }
        }

        if(isset($data['parts']) && count($data['parts']) > 0){
            
            foreach($data['parts'] as $key => $part){
                $part['index'] = $key + 1;
                $dataFatura = $this->getArrayParcelas($part,$invoice);

                if(isset($items_id)){
                    $dataFatura['description'] = $dataFatura['description'] . $items;
                }
                
                $modelFatura = Fatura::create($dataFatura);
                
                if($modelFatura){
                   
                    $invoice->faturas()->attach($modelFatura->id);
                    if(isset($data['categories']))
                    {
                        $modelFatura->categories()->sync($data['categories']);
                    }
                }
             
            }
        }else{
            
            $dataFatura = $this->getArrayFatura($data,$invoice);
            if(isset($items_id)){
                $dataFatura['description'] = $dataFatura['description'] . $items;
            }
            $modelFatura = Fatura::create($dataFatura);
            
            if($modelFatura){
                $invoice->faturas()->attach($modelFatura->id);
                if(isset($data['categories']))
                {
                    $modelFatura->categories()->sync($data['categories']);
                }
            }

        }
    }

    private function getArrayFatura(array $data, Invoice $invoice){
        $result = collect(); 
        $account = Account::where('default', true)->first();

            $code = $invoice->code . '/1' ;

        $result->push([
            'customer_id' => $invoice->customer_id,
            'account_id' =>  (isset($account->id) ? $account->id : null),
            'code' => $code,
            'payment_method' => $invoice->payment_method,
            'data_competencia' => $invoice->sale_date,
            'data_vencimento' => $invoice->sale_date,
            'description' => $invoice->description ."\n" . trans_choice('financial::general.fatura_invoice',2,['service' => trans('sale-rent::general.sale'),'code'=> $invoice->code]),
            'value' => $data['amount'],
            'type' => ($invoice->invoice_type == 'sale' || $invoice->invoice_type == 'rent' ? 'incomes' : 'expenses'),
            'status' => 'open',
        ]);
        
        return $result->first();
    }

    private function getArrayParcelas(array $part, Invoice $invoice){
        $result = collect(); 
        $account = Account::where('default', true)->first();
      
        if(isset($part['type']) && $part['type'] == 'shipping')
        {
            $code = $invoice->code . '/' .  $part['index'] . '-' . trans('sale-rent::general.shipping');

        }else{

            $code = $invoice->code . '/' . $part['index'];
        }
        
        $result->push([
            'customer_id' => $invoice->customer_id,
            'account_id' =>  (isset($account->id) ? $account->id : null),
            'code' => $code,
            'payment_method' => $invoice->payment_method,
            'data_competencia' => $invoice->sale_date,
            'data_vencimento' => date('Y-m-d', strtotime(str_replace('/','-',$part['date']))),
            'description' => isset($part['description']) ?
                                $part['description'] . "\n" .(isset($part['type']) && $part['type'] == 'shipping' ?
                                trans_choice('financial::general.fatura_invoice',2,['service' => trans('sale-rent::general.shipping'),'code'=> $invoice->code]) :
                                trans_choice('financial::general.fatura_invoice',2,['service' => ($invoice->invoice_type == 'sale' ?trans('sale-rent::general.sale') : trans('sale-rent::general.rent')),'code'=> $invoice->code])) :
                                (isset($part['type']) && $part['type'] == 'shipping' ? trans_choice('financial::general.fatura_invoice',2,['service' => trans('sale-rent::general.shipping'),'code'=> $invoice->code]) : trans_choice('financial::general.fatura_invoice',2,['service' => ($invoice->invoice_type == 'sale' ?trans('sale-rent::general.sale') : trans('sale-rent::general.rent')),'code'=> $invoice->code])),
            'value' => format_money($part['value']),
            'type' => ($invoice->invoice_type == 'sale' || $invoice->invoice_type == 'rent' ? 'incomes' : 'expenses'),
            'status' => 'open',
        ]);
          
        return $result->first();
    }
}
