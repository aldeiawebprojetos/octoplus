<?php

namespace Modules\Financial\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Financial\Events\TranzactionCreated;
use Modules\Financial\Models\AccountTransaction as DbTransaction;
use Modules\Financial\Models\Fatura;
use Octoplus\Domain\Account;

class AccountTranzaction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TranzactionCreated $event)
    {
        
        $account = $event->account;
        $fatura = $event->fatura;
        $method = $fatura->type;

        if(isset($fatura->transaction_type) && $fatura->transaction_type == 'reversal'){
           $this->reversal($account,$fatura);
        }else{

            if($fatura->status == 'partial' or $fatura->status == 'open')
            {
                if(isset($fatura->new_paid))
                {
                    $fatura->old_paid = $fatura->value_paid;
                    $fatura->value_paid = $fatura->new_paid;
                }

                $this->$method($account,$fatura); 
            }
        }
        $this->setAccountTransaction($fatura);
    }

    private function incomes(Account $account,Fatura $fatura)
    {
     
        $account->update([
            'balance' => $account->balance + $fatura->value_paid
            ]);

            $this->changeFaturaStatus($fatura);
    }

    private function expenses(Account $account,Fatura $fatura)
    {
        $account->update([
            'balance' => $account->balance - $fatura->value_paid
            ]);

            $this->changeFaturaStatus($fatura);
    }

    private function changeFaturaStatus(Fatura $fatura)
    {
        if(isset($fatura->old_paid))
        {
            $fatura->value_paid = $fatura->old_paid;
            unset($fatura->old_paid,$fatura->new_paid);
        }

        if($fatura->value_liquid - $fatura->value_paid == 0){
            $fatura->update(['status' => 'settled','paid' =>true]);
        }else{
            $fatura->update(['status' => 'partial','paid' => false]);
        }
    }

    private function setAccountTransaction(Fatura $fatura)
    {
        DbTransaction::create([
            'fatura_id' => $fatura->id,
            'account_id' => $fatura->account_id,
            "type" => (isset($fatura->transaction_type) ? $fatura->transaction_type : 'payoff'),
            'status' => $fatura->status,
            'description' => null,
            'total' => ($fatura->value_paid != null ? $fatura->value_paid : $fatura->value),
            'date' => date('Y-m-d'),
        ]);
    }

    private function reversal(Account $account,Fatura $fatura)
    {
        $account->update([
            'balance' => $account->balance - $fatura->value_paid
            ]);
    }
}
