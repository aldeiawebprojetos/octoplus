<?php

namespace Modules\Financial\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Financial\Events\TranzactionCreated;
use Modules\Financial\Models\Fatura;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Users\Models\User;
use Illuminate\Support\Facades\DB;
use PDF;

class FaturaController extends Controller
{
    public function payOff(Request $request)
    {
        
        $fatura = Fatura::find($request->fatura_id);
      
        $fatura->value_paid = $fatura->value;
        $fatura->data_recebimento = date('Y-m-d');
        $fatura->value_liquid = $fatura->value;
        $fatura->save();
       
        event(new TranzactionCreated($fatura->account, $fatura));

        sendNotify(trans_choice('financial::general.item_payedOff',2,['item' => trans('financial::general.title') . ' #' . $fatura->code]),'success',trans('alerts.success'));

        if($fatura->type == 'incomes'){
            return redirect()->route('financial.incomes.index', ['fatura_id' => $fatura->id]);
        } else {
            return redirect()->route('financial.expenses.index', ['fatura_id' => $fatura->id]);         
        }
    }

    public function reversal(Request $request)
    {
        $fatura = Fatura::find($request->fatura_id);
      
        $fatura->value_paid = null;
        $fatura->data_recebimento = null;
        $fatura->value_liquid = $fatura->value;
        $fatura->paid = false;
        $fatura->status = 'open';
        $fatura->save();

        $fatura->transaction_type = 'reversal';
        event(new TranzactionCreated($fatura->account, $fatura));

        sendNotify(trans_choice('financial::general.item_reversed',2,['item' => trans('financial::general.title') . ' #' . $fatura->code]),'success',trans('alerts.success'));

        return redirect()->route('financial.incomes.edit', $fatura->id);
    }

    public function cancel(Request $request)
    {
        $fatura = Fatura::find($request->fatura_id);
        $fatura->paid = false;
        $fatura->status = 'canceled';
        $fatura->save();

        sendNotify(trans_choice('financial::general.item_canceled',2,['item' => trans('financial::general.title') . ' #' . $fatura->code]),'alert',trans('alerts.success'));

        return redirect()->back();
    }

    public function print(Request $request)
    {

        $fatura = Fatura::find($request->fatura_id);
        $company = Company::find(session("tenant"));
        $invoice = $fatura->invoice->toArray();

        $end_entrega = '';
        $tipo_end = -1;
        if(isset($invoice) && !empty($invoice)){
            if($invoice[0]['shipping_address'] == 'delivery_home'){
                $end_aux = $fatura->customer->user['address_id'];
                if(isset($end_aux)){
                    $end_entrega = DB::table('addresses')->where('id', $end_aux)->get();
                    $tipo_end = 0;
                }
            }else{
                $end_entrega = $invoice[0]['address_complete'];
                $tipo_end = 1;
            }
        }

        $data = [
            'fatura' => $fatura,
            'company' => $company,
            'end_entrega' => $end_entrega,
            'tipo_end' => $tipo_end
        ];

		$pdf = PDF::loadView('financial::faturas.pdf', $data);
		return $pdf->stream('document.pdf');
      
       
    }
}
