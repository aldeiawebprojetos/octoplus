<?php

namespace Modules\Financial\Http\Controllers\Accounts;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Octoplus\Domain\Account;
use Octoplus\Domain\Currency\Currency;

class AccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:financial.account_create')->only(['create','store']);
        $this->middleware('can:financial.account_update')->only(['edit','update']);
        $this->middleware('can:financial.account_show')->only(['index']);
        $this->middleware('can:financial.account_delete')->only(['delete']);       
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $accounts = Account::paginate();

        return view('financial::accounts.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $currencies = Currency::pluck('name','id');


        return view('financial::accounts.create', compact("currencies"));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
       $data = $this->getAccountArray($request);

        if(Account::create($data)){
            sendNotify(trans_choice('alerts.item_created',2,['item' => trans('financial::general.account')]),'success',trans('alerts.success'));

            return redirect()->route('financial.accounts.index');
        }else{
            sendNotify(trans_choice('alerts.item_created_error',2,['item' => trans('financial::general.account')]),'error',trans('alerts.error'));

            return redirect()->back();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('financial::accounts.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $account = Account::find($request->account_id);
        $currencies = Currency::pluck('name','id');

        return view('financial::accounts.edit', compact('account','currencies'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $account = Account::find($request->account_id);
        $data = $request->all();
        
        if($account->update($data)){
            sendNotify(trans_choice('alerts.item_updated',2,['item' => trans('financial::general.account')]),'success',trans('alerts.success'));

            return redirect()->route('financial.accounts.index');
        }else{
            sendNotify(trans_choice('alerts.item_updated_error',2,['item' => trans('financial::general.account')]),'error',trans('alerts.error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    private function getAccountArray(Request $request, $edit = false)
    {
        $request->validate([
            'name' => 'required',
            'number' => 'required',
            'currency_id' => 'required',
            'balance' => 'required'
        ],[
            'name.required' => trans_choice('validation.required',2,['attribute' => trans('general.name')]),
            'number.required' => trans_choice('validation.required',2,['attribute' => trans('financial::general.account_number')]),
            'currency_id.required' => trans_choice('validation.required',2,['attribute' => trans('financial::general.currency')]),
            'balance.required' => trans_choice('validation.required',2,['attribute' => trans('financial::general.balance')])
        ]);

        $data = $request->all();

        $data['balance'] = format_money($data['balance']);

        return $data;
    }
}
