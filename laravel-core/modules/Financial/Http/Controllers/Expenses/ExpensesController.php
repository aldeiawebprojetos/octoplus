<?php

namespace Modules\Financial\Http\Controllers\Expenses;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Octoplus\Domain\Category\Models\Category;
use Modules\Financial\Events\TranzactionCreated;
use Modules\Financial\Models\Fatura;
use Octoplus\Domain\Account;
use Carbon\Carbon;

class ExpensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:financial.expenses_create')->only(['create','store']);
        $this->middleware('can:financial.expenses_update')->only(['edit','update']);
        $this->middleware('can:financial.expenses_show')->only(['index']);
        $this->middleware('can:financial.expenses_delete')->only(['delete']);       
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
      
        $query = Fatura::where('type', 'expenses')->orderBy('data_vencimento')
        ->when($request->range_date, function ($q) use ($request) {
            $data = 'data_vencimento';
            if(isset($request->status) && $request->status == 'settled')
                $data = 'data_recebimento';
            return $q->whereBetween($data, $this->formataRangeDate($request));
        });

        $faturasOpen = Fatura::where('type', 'expenses')->where('status',  'open')->whereDate('data_vencimento', '>', Carbon::now()->startOfDay()->toDateString())->count();
        $faturasPayable = Fatura::where('type', 'expenses')->where('status',  'open')->whereDate('data_vencimento', '<', Carbon::now()->startOfDay()->toDateString())->count();
        $faturasWinning  = Fatura::where('type', 'expenses')->where('status',  'open')
            ->whereDate('data_vencimento', '<=', Carbon::now()->startOfDay()->addDays(7)->toDateString())
            ->whereDate('data_vencimento', '>=', Carbon::now()->startOfDay()->toDateString())->count();
        $faturasSettled = Fatura::where('type', 'expenses')->where('status',  'settled')->count();
        $faturasCanceled = Fatura::where('type', 'expenses')->where('status',  'canceled')->count();
        $faturasPartial = Fatura::where('type', 'expenses')->where('status',  'partial')->count();
        
                
        $categories = Category::where([['enabled', true],['type','expense']])->pluck('name','id');

       if(isset($request->s))
        {
            $query->where(function($aux) use ($request){
                $aux->orwhereHas('customer',function($customer) use ($request){
                    $customer->whereHas('user', function($user) use ($request){
                        $user->where('first_name','LIKE', '%'.$request->s.'%');
                    });
                })
                ->orWhere('code', 'LIKE', '%'. $request->s .'%');
            });
        }

        if(isset($request->status)){
            $query->where('status',  $request->status);
            if(isset($request->vencendo)){
                $query->whereDate('data_vencimento', '<=', Carbon::now()->startOfDay()->addDays(7)->toDateString())
                ->whereDate('data_vencimento', '>=', Carbon::now()->startOfDay()->toDateString())->count();
            }
            if(isset($request->vencido)){
                $query->whereDate('data_vencimento', '<', Carbon::now()->startOfDay()->toDateString());
            }
        } else {
            $query->where('status',  'open')->whereDate('data_vencimento', '>=', Carbon::now()->startOfDay()->toDateString());
        }

        $faturasValue = $query->sum('value');
        $faturaCount = $query->count();
  
        $faturas = $query->paginate()->withQueryString();

        $query_id = null;
        if(isset($request->fatura_id)){
            $query_id = $request->fatura_id;
        }

        return view('financial::expenses.index', compact( 
            'faturas','categories','faturasOpen','faturasPayable','faturasWinning',
            'faturasSettled','faturasCanceled','faturasPartial','faturasValue','faturaCount', 'query_id'
        ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories = Category::where([['type','expense'],['enabled',true]])->orderBy('name')->pluck('name','id');
        $accounts = Account::where([['enabled', true]])->pluck('name', 'id');

        return view('financial::expenses.create', compact('categories','accounts'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        if(isset($request->recurrent) && $request->recurrent == 1 && $request->payment_quantity == null){
            $request->merge([
                'payment_quantity' => "50"
            ]);
        }
        $data = $this->getFaturaArray($request);
      
        $data['status'] = 'open';
        $data['code'] = settings('prefix.expenses','D'). str_pad(settings('sequence.expenses',1) , 5 , '0' , STR_PAD_LEFT);
        

        if($data['recurrent'] && $data['payment_quantity'] != null)
        {
           $faturas = $this->setRecurrentFaturas($data);

           foreach($faturas as $fatura)
           {
                 //Coloca as categorias
             $fatura->categories()->sync($data['categories']);
                //Verifica se a fatura já está paga e dispara o evento para moviemntar a conta
                if(isset($data['setTranzaction']))
                {
                    event(new TranzactionCreated($fatura->account, $fatura));
                }

           }

            //incrementa a sequencia do código
            settings(['sequence.expenses' => settings('sequence.expenses',1) +1]);
            //envia a notificação
            sendNotify(trans_choice('alerts.item_created',2,['item' => trans('financial::general.expenses')]),'success',trans('alerts.success'));

            return redirect()->route('financial.expenses.index');
        }else{
            if($data['recurrent'])
            {
                $data['code'] = $data['code']. '-1';
            }

            $fatura = Fatura::create($data);
        }

    
       if($fatura){
            //incrementa a sequencia do código
            settings(['sequence.expenses' => settings('sequence.expenses',1) +1]);

             //Verifica se a fatura foi paga e dispara o evento para moviemntar a conta
             if(isset($data['setTranzaction']))
             {
               
                if(isset($data['balance']))
                {
                     $fatura->new_paid = $data['new_paid'];
                }
                
                 event(new TranzactionCreated($fatura->account, $fatura));
             }

             if(count($data['categories']) > 0)
             {
                  //Coloca as categorias
                 $fatura->categories()->attach($data['categories']);
             }

            //incrementa a sequencia do código
            settings(['sequence.expenses' => settings('sequence.expenses',1) +1]);

            //envia a notificação
            sendNotify(trans_choice('alerts.item_created',2,['item' => trans('financial::general.expenses')]),'success',trans('alerts.success'));

            return redirect()->route('financial.expenses.index');
        }else{
            sendNotify(trans_choice('alerts.item_created_error',2,['item' => trans('financial::general.expenses')]),'error',trans('alerts.error'));

            return redirect()->back();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request)
    {
        $fatura = Fatura::find($request->fatura_id);
        $categories = Category::where([['type','expense'],['enabled',true]])->orderBy('name')->pluck('name','id');
        $accounts = Account::where([['enabled', true]])->pluck('name', 'id');

        return view('financial::expenses.show', compact('fatura','categories','accounts'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $fatura = Fatura::find($request->fatura_id);
        $categories = Category::where([['type','expense'],['enabled',true]])->orderBy('name')->pluck('name','id');
        $accounts = Account::where([['enabled', true]])->pluck('name', 'id');

        return view('financial::expenses.edit', compact('fatura','categories','accounts'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $data = $this->getFaturaArray($request);

        $fatura = Fatura::find($request->fatura_id);
        
        
      
        if($fatura->update($data)){
                //Verifica se a fatura foi paga e dispara o evento para moviemntar a conta
                if(isset($data['setTranzaction']))
                {
                   
                   if(isset($data['balance']))
                   {
                        $fatura->new_paid = $data['new_paid'];
                   }
                   
                    event(new TranzactionCreated($fatura->account, $fatura));
                }

                if(count($data['categories']) > 0){
                    $fatura->categories()->sync($data['categories']);  
                }
                
            sendNotify(trans_choice('alerts.item_updated',2,['item' => trans('financial::general.expenses')]),'success',trans('alerts.success'));

            return redirect()->route('financial.expenses.index');
        }else{
            sendNotify(trans_choice('alerts.item_updated_error',2,['item' => trans('financial::general.expenses')]),'error',trans('alerts.error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $fatura = Fatura::find($request->fatura_id);
        $fatura->categories()->sync([]);
        if($fatura->delete())
        {
            sendNotify(trans_choice('alerts.item_deleted',2,['item' => trans('financial::general.expenses')]),'success',trans('alerts.success'));

            return redirect()->route('financial.expenses.index');
        }else{
            sendNotify(trans_choice('alerts.item_deleted_error',2,['item' => trans('financial::general.expenses')]),'error',trans('alerts.error'));

            return redirect()->route('financial.expenses.index');
        }
    }

    private function getFaturaArray(Request $request)
    {
       
        $request->validate([
            'customer_id' => 'required',
            'value' => 'required',
            'data_vencimento' => 'required'
        ],[
            'data_vencimento.required' =>  trans_choice('validation.required',2,['attribute' => trans('financial::general.data_vencimento')]),
            'customer_id.required' => trans_choice('validation.required',2,['attribute' => trans('user.client')]),
            'value.required' => trans_choice('validation.required',2,['attribute' => trans('general.price.value')]),
        ]);

        $data = $request->all();
        $data['recurrent'] = (isset($data['recurrent']) ? true : false);
        $data['type'] = 'expenses';
        $data['categories'] = (isset($data['categories']) ? $data['categories'] : []);
        $data['value'] = format_money($data['value']);
        $data['data_vencimento'] = date('Y-m-d', strtotime(str_replace('/','-',$data['data_vencimento'])));
        $data['data_competencia'] = date('Y-m-d', strtotime(str_replace('/','-',$data['data_competencia'])));
        
            if(isset($data['paid']))
            {
                $data['setTranzaction'] = true;
            }
            if(!isset($data['status']))
            {
                $data['value_paid'] = format_money($data['value_paid']);
                $data['data_recebimento'] = date('Y-m-d', strtotime(str_replace('/','-',$data['data_recebimento'])));
                $data['discount'] = format_money($data['discount']);
                $data['juros_multas'] = format_money($data['juros_multas']);                
            }

            if(isset($data['balance']))
            {
                $data['new_paid'] = $data['value_paid'];
                $data['value_paid'] = $data['old_value_paid'] + $data['new_paid'];
            }

            
        return $data;
    }

    private function setRecurrentFaturas(array $data)
    {
       
        $payment_frequencies = getPaymentFreqencyFormat($data['data_vencimento']);
        $faturas = collect();
       for($i=0; $i<$data['payment_quantity'];$i++)
        {
            $faturaData = $data;
            $faturaData['data_vencimento'] = $payment_frequencies[$data['payment_frequency']]($i+1);
            $faturaData['code'] = $faturaData['code'].'-'.($i+1);
               $faturas->push(
                   Fatura::create($faturaData)
               );
        }  
       
       return $faturas;
    }


    private function formataRangeDate(Request $request): array
    {
        
        $dateFormated = array();
        $rangedateArray = explode(' - ', $request->range_date);

        $rangedateArray = array_map(function($value){
            
            return date('Y-m-d',strtotime(str_replace('/','-',$value)));
        }, $rangedateArray);

       //Formata a data para colocar a hora para o SQL ter precisão da busca

        $dateFormated = [$rangedateArray[0]." 00:00:00",
                         $rangedateArray[1]." 23:59:59"];

        return $dateFormated;
    }
}