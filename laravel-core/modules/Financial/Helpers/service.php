<?php

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Octoplus\Domain\Currency\Currency;

if(!function_exists('getFaturaStatus'))
{
    function getFaturaStatus()
    {
        return collect([
            'settled'   => trans('financial::general.settled'),
            'canceled'  => trans('financial::general.canceled'),
            'pending'   => trans('financial::general.pending'),
            'partial'   => trans('financial::general.partial'),
            'open'      => trans('financial::general.open'),
        ]);
    }
}

if(!function_exists('getPaymentFreqencyFormat'))
{
    
    function getPaymentFreqencyFormat($data_vencimento)
    {
        return collect([
            'daily'        => function($increment = 1) use($data_vencimento){
                $vencimento = new Carbon($data_vencimento);
                return $vencimento->addDays((1*$increment)); 
            },
            'weekly'       => function($increment = 1) use($data_vencimento){
                $vencimento = new Carbon($data_vencimento);
                return $vencimento->addDays((7*$increment)); 
            },
            'monthly'      => function($increment = 1) use($data_vencimento){
                $vencimento = new Carbon($data_vencimento);
                return $vencimento->addMonths((1*$increment)); 
            },
            'bimonthly'    => function($increment = 1) use($data_vencimento){
                $vencimento = new Carbon($data_vencimento);
                return $vencimento->addMonths((2*$increment)); 
            },
            'quarterly'    => function($increment = 1) use($data_vencimento){
                $vencimento = new Carbon($data_vencimento);
                return $vencimento->addMonths((3*$increment)); 
            },
            'semiannual'   => function($increment = 1) use($data_vencimento){
                $vencimento = new Carbon($data_vencimento);
                return $vencimento->addMonths((6*$increment)); 
            },
            'yearly'       => function($increment = 1) use($data_vencimento){
                $vencimento = new Carbon($data_vencimento);
                return $vencimento->addYears((1*$increment)); 
            }
        ]);
    }
}

if(!function_exists('formata_moeda'))
{
    function formata_moeda($valor)
    {
       $currency = Currency::where([['company_id', session('tenant')], ['enabled', true]])->first();
       
       return ($currency->symbol_first ? $currency->symbol . ' ' : null). number_format($valor, $currency->precision, $currency->decimal_mark, $currency->thousands_separator) .(!$currency->symbol_first ? $currency->symbol : null);
    }
}