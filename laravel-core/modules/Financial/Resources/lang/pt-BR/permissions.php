<?php

return [
    'create'                            => 'Cadastrar',
    'update'                            => 'Atualizar',
    'show'                              => 'Vizualizar',
    'delete'                            => 'Apagar',
    'account'                           => 'Contas',
    'expenses'                          => 'Despesas',
    'income'                            => 'Receitas',
    'transactions'                      => 'Transações',

    
];