import Form from './plugins/form';

var vm = new Vue({
    el: '#app',
    data: function(){
        return {
            form: new Form('accounts'),  
            edit: {},         
        };
    },
    mounted()
    {
        if(typeof editStatus !== 'undefined')
        {
            this.edit.status = editStatus;
        }else{
            this.edit.status = false;
        }

    }
});