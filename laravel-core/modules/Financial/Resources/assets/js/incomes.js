import Form from './plugins/form';

var vm = new Vue({
    el: '#app',
    data: function(){
        return {
            form: new Form('incomes'),  
            edit: {},         
        };
    },
    mounted()
    {
        if(typeof editStatus !== 'undefined')
        {
            this.edit.status  =  editStatus;
        }
        
       
    },
    methods:{
        getCalculo(){

            if(this.edit.status)
            {
                if(this.form.balance)
                {
                    this.form.value_liquid = this.form.value + this.form.juros_multas - this.form.discount;
                    this.form.value_paid = this.form.value_liquid - this.form.balance;
                } else{
                   
                    this.form.value_liquid = this.form.value + this.form.juros_multas - this.form.discount;
                    this.form.value_paid = this.form.value_liquid;
                  
                }
            }else{
                this.form.value_liquid = this.form.value + this.form.juros_multas - this.form.discount;
                this.form.value_paid = this.form.value_liquid;
            }
        },
        getShowLink(){
            const link = document.getElementById('showLink');

            link.removeAttribute('href');
            link.setAttribute('href', this.form.customer_route);

            link.classList.remove('hidden');
        },
        hideShowLink()
        {
            const link = document.getElementById('showLink');
            link.removeAttribute('href');
            link.classList.add('hidden');
           
        },
        getPartialPaid()
        {
            
           if(this.form.paid[0] || this.form.paid)
           {
               const data_recebimento = document.getElementById('data_recebimento');
            this.form.value_paid = this.form.balance ? this.form.balance : this.form.value;
            this.form.data_recebimento = this.formatDate(new Date(), 'yyyy-mm-dd');
            data_recebimento.value = this.formatDate(new Date(), 'yyyy-mm-dd');
           
           }else{
            this.form.value_paid = this.form.value_liquid - (this.form.balance ? this.form.balance : this.form.value);
           }
          
        },
        formatDate(date, formato = 'dd/mm/yyyy'){
            return formato.replace('dd', ('0' + date.getDate()).slice(-2)).replace('mm', ('0' + (date.getMonth() +1)).slice(-2)).replace('yyyy', date.getFullYear() );
        },
    }
});