<div>
    <div style="width: 185px; float: left; padding-right: 30px">
        <img src="{{isset($company->logo) ? $company->logo : ''}}" alt="company_logo">
    </div>
    <div style="float: right; padding-top: 40px">
        <h1>{{'Recibo Nº ' . $fatura->code}}</h1>
    </div>
</div>

<br><br>

<div class="label">Recebemos de</div>
<div class="dados">
    {{$fatura->customer->user['name']}}
</div>

<br>

<div class="label">A importância de</div>
<div>
    <div class="dados" style="width: 20%;float: left;">
        {{'R$ ' . number_format(($fatura->value - $fatura->discount),2,$currency->decimal_mark,$currency->thousands_separator)}}
        
    </div>
    <div class="dados" style="width: 70%;float: right;">
        {{settings('payment.methods',getDefaultPaymentsMethods())[$fatura->payment_method]}}
    </div>
</div>

<br>

<div>
    <div style="width: 48%;float: left;">
        <div class="label">Desconto</div>
        <div class="dados">
            {{'R$ ' . number_format($fatura->discount,2,$currency->decimal_mark,$currency->thousands_separator)}}
        </div>
    </div>
    <div style="width: 48%;float: right;">
        <div class="label">Juros/multas</div>
        <div class="dados">
            {{'R$ ' . number_format($fatura->juros_multas,2,$currency->decimal_mark,$currency->thousands_separator)}}
        </div>
    </div>
</div>


<br>

<div class="label">Referente</div>
<div class="dados">
    {{$fatura->description}}
</div>

@if($tipo_end != -1)
    <br>

    <div class="label">Endereço de entrega</div>
    <div class="dados">
        @if($tipo_end == 0)
            {{$end_entrega[0]->address}} - {{$end_entrega[0]->neighborhood}}, 
            {{$end_entrega[0]->city}} - {{$end_entrega[0]->uf}}, 
            {{substr_replace($end_entrega[0]->cep, '-', 5, 0)}}
        @else
            {{$end_entrega}}
        @endif
    </div>
@endif

<br>

<div>
    <div style="width: 20%;float: left;padding-right: 30px">
        <div class="label">Data de vencimento</div>
        <div class="dados">
            {{$fatura->data_vencimento->format($companySettings['dateFormat'])}}
        </div>
    </div>

    <div style="width: 20%;float: left;">
        <div class="label">Data de recebimento</div>
        <div class="dados">
            @php
                $recebimento = new DateTime($fatura->data_recebimento)
            @endphp
            {{$recebimento->format('d/m/Y')}}
        </div>
    </div>
</div>

<br><br><br>

<div class="info">
    Emitente: {{$company->name}}
</div>

<div class="info">
    Endereço: {{$company->address}}
</div>

<div class="info">
    CNPJ: {{formatar_cpf_cnpj($company->document)}}
</div>

<br>

@if(auth()->user()->assinatura)

    <img src="{{'data:image/gif;base64,' . auth()->user()->assinatura}}" alt="company_logo" width="300px">
@else
    __________________________________________________
    <div style="margin-top: 8px">
        {{auth()->user()->first_name}} {{auth()->user()->last_name}}
    </div>
@endif


<style>
    .dados{
        background-color: #eeeeee;
        padding: 8px 12px;
        font-size: 18px
    }
    .label{
        font-size: 14px;
        margin-bottom: 8px;
        color: #955a5a
    }
    .info{
        margin-bottom: 10px
    }
</style>