@extends('layouts.app')

@section('title', trans('financial::general.name') . ' / ' .  trans('financial::general.expenses') )

@section('breadcrumb')
<li> <a href="javascript:void(0)">{{ trans('financial::general.name') }}</a> </li>
<li> <a href="javascript:void(0)">  {{trans('financial::general.expenses') }} </a></li>
<li class="active"> <strong>{{trans_choice('general.edit',2,['item' => trans('financial::general.expenses')])}}</strong> </li>		
@endsection

@section('content')
<br />	
		
<hr />

        
{{Form::model($fatura,["route" => ['financial.expenses.update',$fatura->id], 'id'=>'incomes','method' => 'PUT'])}}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('financial::general.expenses')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

</div>
@include('financial::expenses.form', ['formEdit' => true,'readOnly' => true])
<div class="row">
    <div class="col-md-6"></div>
  
</div>
{{ Form::close() }}


<div class="clear"></div>
<!-- Footer -->
@endsection
