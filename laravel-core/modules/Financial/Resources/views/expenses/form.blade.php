<div class="row">

  <div class="col-sm-12">
      
      <div class="panel panel-danger" data-collapsed="0">
      
          <div class="panel-heading">
              <div class="panel-title">
                  {{trans('financial::general.expense_data')}}
              </div>
              
              <div class="panel-options">
                  <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                  <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
              </div>
          </div>
          
          <div class="panel-body">
              <div class="form-group row">
                  <div class="col-md-3">
                    {!! Form::label('customer_id', trans('general.provider'),['class'=>' control-label']) !!}
                    <strong class="text-danger">*</strong>
                  </div>
                  <div class="col-md-7">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="entypo-user"></i></span>
                      @if(isset($fatura))
                        {{ Form::text('customer_name', ( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' ), ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled']) }}
                      @else 
                      <customer-select
                      {{(isset($readOnly) && $readOnly ? 'readonly' : '')}}
                      v-model="form.customer_name"
                      value="{{isset($fatura) ? $fatura->customer->user->name : null}}"
                      client_type="2"
                      placeholder="{{trans('sale-rent::general.type_to_search')}}"
                      url="{{ route('customers.search') }}"
                      ></customer-select>
                      @endif

                      {{ Form::hidden('customer_id', null,['class'=>'form-control input-lg','v-model'=>'form.customer_id', ]) }}
                    </div>
                    <span class="description">  </span>
                  </div>

                 @if(!isset($readOnly))
                    <a href="{{ route('customers.create') }}" target="_blank" type="button" class="btn btn-white">
                      <i class="fa fa-plus-square"></i>
                    </a>
                 @endif

                 <a id="showLink" target="_blank" type="button" class="btn btn-white hidden">
                  <i class="fa fa-eye"></i>
                </a>
                @if(isset($fatura))
                    <a href="{{ route('customers.show', $fatura->customer_id) }}" target="_blank" type="button" class="btn btn-white">
                      <i class="fa fa-eye"></i>
                    </a>
                @endif
              {{ form::hidden('default_customer_route', route('customers.show','#customer_id#')) }}
                        
                  
                </div>

                <!-- Datas -->
                <div class="form-group row">
                  <div class="col-md-3">
                  {!! Form::label('data_competencia', trans('financial::general.data_competencia'),['class'=>' control-label']) !!}
                  </div>
                  
                  <div class="col-sm-7">
                    <div class="input-group">
                      <div class="input-group-addon"><a href="#"><i class="entypo-calendar"></i></a></div>
                      {{ Form::date('data_competencia',  isset($fatura->data_competencia) ? $fatura->data_competencia : \Carbon\Carbon::now(), ['class' => 'form-control input-lg', 'v-model' => 'form.data_competencia',(isset($fatura) && $fatura->paid ? 'readonly' : (isset($readOnly) && $readOnly ? 'readonly' : ''))]) }}
                    </div>
                  </div>
                </div>
  
                <div class="form-group row">
                  <div class="col-md-3">
                    {!! Form::label('data_vencimento', trans('financial::general.data_vencimento'),['class'=>' control-label']) !!}
                    <strong class="text-danger">*</strong>
                  </div>
                  
                  <div class="col-sm-7">
                    <div class="input-group">
                      <div class="input-group-addon"><a href="#"><i class="entypo-calendar"></i></a></div>
                      {{ Form::date('data_vencimento', isset($fatura) ? $fatura->data_vencimento : \Carbon\Carbon::now(), ['class' => 'form-control input-lg', 'v-model' => 'form.data_vencimento',(isset($fatura) && $fatura->paid ? 'readonly' : (isset($readOnly) && $readOnly ? 'readonly' : ''))]) }}
                    </div>
                  </div>
                </div>

                <!-- Datas -->
                <div class="form-group">
                    <div class="form-group row">
                      <div class="col-md-3">
                      {{ Form::label('categories', trans("general.categories"), ['class' => 'control-label']) }}
                      </div>
                      
                      <div class="col-md-7">
                        {{Form::select('categories[]',$categories, (isset($fatura) ? $fatura->categories : []), ['class' => 'select2 form-control input-lg','multiple'=>'true', 'id'=>'categories',(isset($fatura) && $fatura->paid ? 'disabled' : (isset($readOnly) && $readOnly ? 'disabled' : ''))])}}
                      </div>
                      
                      @if(!isset($readOnly))
                        <a href="javascript:void(0);" onclick="$('#categoryAdd').modal('show');"  type="button" class="btn btn-white">
                          <i class="fa fa-plus-square"></i>
                        </a>
                      @endif
                    </div>
                </div>

                <!-- Descrição -->
                <div class="form-group row">
                    <div class="col-md-3">
                    {{ Form::label('description',trans("general.description"),['class' => 'control-label']) }}
                    </div>
                    
                    <div class="col-md-7">
                      {{ Form::textarea('description', null,['class' => 'form-control','v-model' => 'form.description',(isset($fatura) && $fatura->paid ? 'readonly' : (isset($readOnly) && $readOnly ? 'readonly' : ''))]) }}
                    </div>
                 </div>

                 <div class="form-group row">
                  <div class="col-md-3">
                    <label class="control-label" for="value">{{trans('general.price.value')}} </label>
                    <strong class="text-danger">*</strong>
                  </div>
                  <div class="col-md-7">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fas fa-money-bill"></i></span>
                        @if(!isset($readOnly))
                        <money
                        decimal="{{$currency->decimal_mark}}"
                        thousands="{{$currency->thousands_separator}}"
                        name="value"
                        {{(isset($readOnly) && $readOnly ? 'readonly' : '')}}
                        id="value"
                        v-model="form.value"
                        value="{{ isset($fatura) ? number_format($fatura->value,2,'.','') : (!empty(old('value')) ? old('value'): null) }}"
                        class="form-control input-lg"
                        {{(isset($fatura) && $fatura->paid ? 'readonly' : '')}}
                        ></money>
                        @else
                             {{ Form::text("vlaue", number_format($fatura->value,2,$currency->decimal_mark,$currency->thousands_separator),['class' => 'form-control input-lg', 'readonly']) }}
                        @endif
                      </div>
                  </div>
                 </div>
                 
                 <div class="form-group row">
                  <div class="col-md-3">
                    {{ Form::label('account_id', trans('financial::general.account'), ['class' => 'control-label']) }}</div>
                  
                  <div class="col-sm-7">
                    {{ Form::select('account_id', $accounts, ($accountDefault ? $accountDefault->id : null), ['class' => 'selectboxit input-lg', 'v-model' => 'form.account_id','placeholder' => trans("general.select").'...',(isset($fatura) && $fatura->paid ? 'disabled' : (isset($readOnly) && $readOnly ? 'disabled' : ''))]) }}
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-md-3">
                    {{ Form::label('payment_method', trans('sale-rent::general.payment_method'), ['class' => 'control-label']) }}</div>
                  
                  <div class="col-sm-7">
                    {{ Form::select('payment_method', settings('payment.methods',getDefaultPaymentsMethods()), null, ['class' => 'selectboxit input-lg','placeholder' => trans("general.select").'...',(isset($fatura) && $fatura->paid ? 'disabled' : (isset($readOnly) && $readOnly ? 'disabled' : ''))]) }}
                  </div>
                </div>
               
                <hr>
                @if(isset($fatura) && $fatura->status == 'partial')
               <div class="form-group row">
                 <div class="col-md-3"></div>
                <div class="col-md-3">
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fas fa-money-bill-wave-alt"></i></span>
                      <money
                      decimal="{{$currency->decimal_mark}}"
                      thousands="{{$currency->thousands_separator}}"
                      name="balance"
                      id="balance"
                      readonly
                      v-model="form.balance"
                      value="{{(isset($fatura->total_balance) ? number_format($fatura->total_balance,2,'.','') :(!empty(old('total_balance')) ? old('total_balance'): null))}}"
                      class="form-control input-lg"
                      ></money>
                  </div>
                    {{ trans('financial::general.balance') }}
                    
                </div>
               </div>

              
                @endif
               
                <hr>
                @if(isset($fatura) && $fatura->status == 'settled')
                {{ Form::hidden("status", null) }}
                <div class="form-group row">
                  <div class="col-md-12 text-center">
                    <hgroup>
                     <h2>{{ trans('financial::general.title') }} #{{$fatura->code}} {{ trans('financial::general.settled') }} </h2> 
                    <h3>{{ number_format($fatura->value_paid, 2,$currency->decimal_mark,$currency->thousands_separator) }}</h3>
                    </hgroup>
                  </div>
                </div>
                @else
                <div class="form-group row">
                  <div class="col-md-3">
                    {{ Form::label('paid', trans("financial::general.paid"),['class' => 'control-label']) }}
                  </div>
                  <div class="col-sm-3">
                    <div class="anil_nepal">
                          <label class="switch switch-left-right">
                            {{ Form::checkbox('paid', 1, null,['class' => 'switch-input', 'data-target'=>"#incomePaid", 'aria-controls'=>"incomePaid", 'data-toggle'=>"collapse", 'v-model' => 'form.paid','@change' => 'getPartialPaid',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
                          <span class="switch-label" data-on="{{trans('general.yes')}}" data-off="{{trans('general.not')}}"></span> <span class="switch-handle"></span> </label>
                      </div>
                  </div>
                </div>

                <div id="incomePaid" class="collapse {{ (isset($fatura) && $fatura->paid ? 'in' : '') }}">
                  <div class="form-group row">
                    <div class="col-md-3">
                     
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <div class="input-group-addon"><a href="#"><i class="entypo-calendar"></i></a></div>
                        {{ Form::date('data_recebimento', isset($fatura) ? $fatura->data_vencimento : null, ['class' => 'form-control input-lg', 'data-format' => 'dd/mm/yyyy', 'v-model' => 'form.data_recebimento','id' => 'data_recebimento',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
                      </div>
                        {{ Form::label('data_recebimento', trans('financial::general.data_recebimento')) }}
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fas fa-money-bill-wave-alt"></i></span>
                          <money
                          decimal="{{$currency->decimal_mark}}"
                          thousands="{{$currency->thousands_separator}}"
                          name="value_paid"
                          id="value_paid"
                          
                          v-model="form.value_paid"
                          value="{{(isset($fatura->value_paid) ? number_format($fatura->value_paid,2,'.','') :(!empty(old('value_paid')) ? old('value_paid'): null))}}"
                          class="form-control input-lg"
                          ></money>
                      </div>
                        {{ trans('financial::general.value_paid') }}
                        
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-md-3">
                     
                    </div>
                    <div class="col-md-4">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fas fa-money-bill-wave-alt"></i></span>
                          <money
                          decimal="{{$currency->decimal_mark}}"
                          thousands="{{$currency->thousands_separator}}"
                          name="discount"
                          id="discount"
                         {{ (isset($readOnly) && $readOnly ? 'readonly' : '')}}
                          @input="getCalculo"
                          v-model="form.discount"
                          value="{{(isset($fatura->discount) ? number_format($fatura->discount,2) :(!empty(old('discount')) ? old('discount'): null))}}"
                          class="form-control input-lg"
                          ></money>
                      </div>
                        {{ trans('financial::general.discount') }}
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fas fa-money-bill-wave-alt"></i></span>
                          <money
                          decimal="{{$currency->decimal_mark}}"
                          thousands="{{$currency->thousands_separator}}"
                          name="juros_multas"
                          id="juros_multas"
                          {{(isset($readOnly) && $readOnly ? 'readonly' : '')}}
                          @input="getCalculo"
                          v-model="form.juros_multas"
                          value="{{(isset($fatura->juros_multas) ? number_format($fatura->juros_multas,2) :(!empty(old('juros_multas')) ? old('juros_multas'): null))}}"
                          class="form-control input-lg"
                          ></money>
                      </div>
                        {{ trans('financial::general.juros_multas') }}
                        
                    </div>
                  </div>
                </div>
                @endif

                <hr>

                <div class="form-group row">
                  <div class="col-md-3">
                    {{ Form::label('recurrent', trans("general.recurrent"),['class' => 'control-label']) }}
                  </div>
                  <div class="col-sm-3">
                    <div class="anil_nepal">
                          <label class="switch switch-left-right">
                            {{ Form::checkbox('recurrent', 1, null,['class' => 'switch-input', 'data-target'=>"#paymentRecurrent", 'aria-controls'=>"paymentRecurrent", 'data-toggle'=>"collapse", 'v-model'=>'form.recurrent',(isset($fatura) && $fatura->paid || isset($fatura) && $fatura->parent_id != null ? 'disabled' : (isset($readOnly) && $readOnly ? 'disabled' : ''))]) }}
                          <span class="switch-label" data-on="{{trans('general.yes')}}" data-off="{{trans('general.not')}}"></span> <span class="switch-handle"></span> </label>
                      </div>
                  </div>
                </div>
                <div id="paymentRecurrent" class="collapse {{ isset($fatura) && $fatura->recurrent ? 'in' : ''}}">
                  <div class="form-group">
                    <div class="col-md-3">
                       
                      </div>
                      <div class="col-md-3">
                          {{ Form::number('payment_quantity',null,['class' => 'form-control input-lg', 'v-model' => 'form.payment_quantity',(isset($readOnly) && $readOnly ? 'readonly' : (isset($fatura) && $fatura->recurrent ? 'disabled' : ''))])}}
                          <div>
                            {{ trans('sale-rent::general.quantity_invoices') }}
                            <button type="button" class="btn btn-default popover-default btn-xs" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{ trans('sale-rent::general.quantity_description') }}" data-original-title="{{ trans('sale-rent::general.quantity_invoices') }}"><i class="entypo-info"></i></button>
                          </div>
                      </div>
                      <div class="col-md-4">
                        {{ Form::hidden('payment_frequency', 'monthly')}}
                      </div>
                  </div>
                </div>
          </div>
      </div>
  </div>
</div>
{{ Form::hidden('value_liquid', null,['v-model' => 'form.value_liquid']) }}
{{ Form::hidden('old_value_paid', isset($fatura) ? $fatura->value_paid : null) }}
{{ Form::hidden("customer_name", isset($fatura) ?( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' ) : null,['v-model' => 'form.customer_name']) }}

@section("scripts")
<!-- Imported styles on this page -->
<link rel="stylesheet" href="{{asset("assets/css/font-icons/font-awesome/css/font-awesome.min.css")}}">
<link rel="stylesheet" href="{{asset("assets/js/select2/select2-bootstrap.css")}}">
<link rel="stylesheet" href="{{asset("assets/js/select2/select2.css")}}">
<link rel="stylesheet" href="{{asset("assets/js/selectboxit/jquery.selectBoxIt.css")}}">

<!-- Imported scripts on this page -->
<script src="{{asset("assets/js/select2/select2.min.js")}}"></script>
<script src="{{asset("assets/js/jquery.inputmask.bundle.js")}}"></script>
<script src="{{asset("assets/js/bootstrap-datepicker.js")}}"></script>
<script src="{{asset("assets/js/selectboxit/jquery.selectBoxIt.min.js")}}"></script>
  <script>
      
      @if(isset($formEdit) or !empty(old()))
      const editStatus = true;
      @else
      const editStatus = false;
      @endif
  </script>
@if(!isset($readOnly))
<script src="{{asset("js/financial/incomes.js")}}?v={{ filemtime('js/financial/incomes.js') }}"></script>
@endif
@endsection