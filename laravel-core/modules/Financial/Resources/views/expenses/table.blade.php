<table class="table table-bordered table-striped datatable" id="table-3">
    <thead>
        <tr>
            <th>{{ trans('general.actions') }}</th>
            <th>{{ trans('general.status') }}</th>
            <th>{{ trans('general.code') }}</th>
            <th> {{ trans('sale-rent::general.payment_method') }} </th>
            <th>{{ trans('financial::general.billet') }}</th>
            <th>{{ trans('financial::general.fiscal_note') }}</th>
            <th>{{ trans('financial::general.data_vencimento') }}</th>
            <th>{{ trans('user.client') }}</th>
            <th>{{ trans('financial::general.amount') }}</th>
            <th>
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_expenses" onchange="changeChkboxes(this,'chkbx_expenses')"/>
                </div>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($faturas as $fatura)
            @php
                $fatura_id = $fatura->id;  
                $status_color = 'default';
                $status_name = 'Aberto';
                $dia_limite = \Carbon\Carbon::now()->startOfDay()->addDays(7);
                $hoje = \Carbon\Carbon::now()->startOfDay(); 
                $fatura_vencimento = \Carbon\Carbon::parse($fatura->data_vencimento)->startOfDay(); 

                if($fatura->status == 'canceled'){
                    $status_color = 'primary';
                    $status_name = 'Cancelado';
                }elseif($fatura->status == 'settled'){
                    $status_color = 'success';
                    $status_name = 'Recebido';
                }elseif($fatura_vencimento->lt($hoje)){
                    $status_color = 'danger';
                    $status_name = 'Vencido';
                }elseif($fatura_vencimento->gte($hoje) && $fatura_vencimento->lte($dia_limite)){
                    $status_color = 'warning';
                    $status_name = 'Vencendo';
                }

            @endphp
            <tr >
                
                <td>
                @include('common.tableActions',['options' => [
                        (!$fatura->paid && $fatura->status != 'canceled' ? ['text' => trans('financial::general.pay_off'),'route' => ['financial.transaction.payOff',true],'permission' => 'financial.income_update','icon' => 'fas fa-check'] : ''),
                       (!$fatura->paid && $fatura->status != 'canceled' ? ['text' => trans('financial::general.cancel'),'routejs' => 'cancelFatura("'.route("financial.transaction.cancel", $fatura->id).'","'.$fatura->code.'")','permission' => 'financial.income_update','icon' => 'fas fa-ban'] : ''),
                        ($fatura->paid && $fatura->status != 'canceled' ? ['text' => trans('financial::general.reversal'),'route' => ['financial.transaction.reversal',true],'permission' => 'financial.income_update','icon' => 'fas fa-undo'] : ''),
                        ['text' => trans('general.edit'),'route' => ['financial.expenses.edit',true],'permission' => 'financial.expenses_update','icon' => 'entypo-pencil'],
                    ], 'name' => '#'.$fatura->code, 'id' => $fatura->id, 'delete_permission' => 'financial.expenses_delete','route_delete'=>'financial.expenses.delete','classbtn' => 'BTN_'.$fatura->id.' '. ($fatura->status == 'canceled' ? 'btn-primary' : ($fatura->status == 'settled' ? 'btn-success' : ($fatura->vencendo? 'btn-warning' : ($fatura->vencido ? 'btn-danger' : 'btn-default'))))])
                </td>
                <td onclick="goToEdit('{{ route('financial.expenses.show', [$fatura_id]) }}')">
                    <span class="label label-{{ $status_color }} ">
                        {{ $status_name }}
                    </span>
                </td>
                <td onclick="goToEdit('{{ route('financial.expenses.show', [$fatura_id]) }}')">#{{$fatura->code}}</td>
                <td onclick="goToEdit('{{ route('financial.expenses.show', [$fatura_id]) }}')"> {{$fatura->payment_method != null && $fatura->payment_method != '0' ? getDefaultPaymentsMethods()[$fatura->payment_method] : null }} </td>
                <td onclick="goToEdit('{{ route('financial.expenses.show', [$fatura_id]) }}')">{{$fatura->boleto ? 'S' : 'N' }}</td>
                <td onclick="goToEdit('{{ route('financial.expenses.show', [$fatura_id]) }}')">{{$fatura->nota_fiscal ? 'S' : 'N' }}</td>
                <td onclick="goToEdit('{{ route('financial.expenses.show', [$fatura_id]) }}')">{{$fatura->data_vencimento->format($companySettings['dateFormat'])}}</td>
                <td onclick="goToEdit('{{ route('financial.expenses.show', [$fatura_id]) }}')">
                {{ 
                    ( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' )
                }}</td>
                <td onclick="goToEdit('{{ route('financial.expenses.show', [$fatura_id]) }}')">{{number_format($fatura->value, 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
                <td>
                    <div class="checkbox checkbox-replace">
                        <input type="checkbox" class="chkbx_expenses" name="fatura"/>
                    </div>
                </td>
            </div>
            </tr>
           
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>{{ trans('general.actions') }}</th>
            <th>{{ trans('general.status') }}</th>
            <th>{{ trans('general.code') }}</th>
            <th> {{ trans('sale-rent::general.payment_method') }} </th>
            <th>{{ trans('financial::general.billet') }}</th>
            <th>{{ trans('financial::general.fiscal_note') }}</th>
            <th>{{ trans('financial::general.data_vencimento') }}</th>
            <th>{{ trans('user.client') }}</th>
            <th>{{ trans('financial::general.amount') }}</th>
            <th>
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_expenses" onchange="changeChkboxes(this,'chkbx_sale-invoice')"/>
                </div>
            </th>
        </tr>
    </tfoot>
</table>

@section('modals')
    @include('common.modals.delete',['type'=>trans('financial::general.expenses')])
    @include('financial::modals.cancel')
    @include('financial::modals.print')
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script>
    jQuery( document ).ready( function( $ ) {
    var $table3 = jQuery("#table-3");
    var table3 = $table3.DataTable( {
        "aLengthMenu": [[ 25, 50, -1], [ 25, 50, "All"]],
        "ordering": false
    } );
    // Initalize Select Dropdown after DataTables is created
    $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
    minimumResultsForSearch: -1
    });

    } );
    
</script>
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}" >
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker/daterangepicker.js')}}"></script>

    <script>

        $(function(){
            @if (isset($query_id))
                $('#modal-print').show()
            @endif
        })

        function closeModalPrint(){
            $('#modal-print').hide()
        }

        $(function() {
            $('.search-form-full .form-group').append('<input type="hidden" name="range_date" id="range_date_input">')

            $('#range_date_selec').change(function (){
                $('#range_date_input').val($('#range_date_selec').val())
                $('.search-form-full').submit()
            });

            $('.status-filter').click(function(){
                $('#status_input').val($(this).data("status"))
                $('#vencendo_input').val($(this).data("vencendo"))
                $('#vencido_input').val($(this).data("vencido"))
                $('.search-form-full').submit()
                
            })

            $(".search-form-full").submit(function(){
                $('#range_date_input').val($('#range_date_selec').val())
            });

        });

        function cancelFatura(url,code)
        {
           
           var form = document.getElementById('cancel'), title = document.getElementById("cancel_code");
          
            title.innerHTML = code;
            form.action = url;

            $.noConflict(true);
            $('#modalCancel').modal();
            
        }
    </script>
@endsection