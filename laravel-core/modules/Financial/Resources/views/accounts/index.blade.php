@extends('layouts.app')

@section('title', trans('financial::general.name') . ' / ' .  trans('financial::general.account') )

@section('breadcrumb')
<li> <a href="javascript:void(0)">{{ trans('financial::general.name') }}</a> </li>
<li> <a href="javascript:void(0)">  {{trans('financial::general.account') }} </a></li>
<li class="active"> <strong>{{trans('general.listing')}}</strong> </li>		
@endsection


@section('content')
<hr>
<div class="mail-env">
		
   

    <!-- Mail Body -->
    <div class="mail-body">
        
        <div class="mail-header">
            <!-- Search  form -->
            
            <form method="get" role="form" class="search-form-full">
                <div class="form-group">
                    <input type="text" class="form-control input-lg" name="s" id="search-input" placeholder="Pesquise pelo Nome, código..." />
                    <i class="entypo-search"></i>
                </div>
                
            </form>
        </div>

        <div class="mail-header">
            <!-- title -->
            <h1 class="mail-title">
                {{ trans('financial::general.account') }}
                <span class="count text-info">({{$accounts->count()}})</span>

            </h1>
            

            <!-- Filtrar por Categoria Produto -->					
            <div class="mail-search">				
                <select name="test" class="form-control selectboxit">
                    <option value="1"> Todos </option>
                    <option value="2"> Recebimentos </option>
                    <option value="3"> Pagamentos </option>
                </select>
            </div>

            <!-- search -->					
            <div class="pull-left">				
                <a href="#" class="btn btn-danger pull-left">
                    <i class="entypo-trash"></i>
                </a>
            </div>

        </div>
                
        @include('financial::accounts.table')
    </div>
    
    <!-- Sidebar -->
    <div class="mail-sidebar">
        <!-- compose new email button -->
       
           		<!-- compose new email button -->
		<div class="mail-sidebar-row hidden-xs">
			@can('financial.account_create')
				<a href="{{route('financial.accounts.create')}}" class="btn btn-blue btn-lg btn-icon btn-block">
					{{ trans_choice('general.add',2,['item' => trans('financial::general.account')]) }}
					<i class="entypo-plus"></i>
				</a>
			@endcan
		</div>


        
    </div>
    
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
    <script>
    jQuery( document ).ready( function( $ ) {
        var $table3 = jQuery("#table-4");
        var table3 = $table3.DataTable( {
        "sPaginationType": "full_numbers",
        "bPaginate": false,
        "sDom": 't'

        } );
    
    } );
    </script>
@endsection