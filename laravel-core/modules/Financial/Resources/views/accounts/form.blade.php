<div class="row">

    <div class="col-sm-12">
        
        <div class="panel panel-primary" data-collapsed="0">
        
            <div class="panel-heading">
                <div class="panel-title">
                    {{trans('financial::general.account_info')}}
                </div>
                
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                    <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                </div>
            </div>
            
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('name', trans('general.name'),['class'=>' control-label']) !!} <strong class="text-danger">*</strong>
                    </div>
                    
                    <div class="col-sm-7">
                      <div class="input-group">
                        <div class="input-group-addon"><a href="#"><i class="fas fa-university"></i></a></div>
                        {{ Form::text('name', null, ['class' => 'form-control input-lg', 'required', 'v-model' => 'form.name']) }}
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('number', trans('financial::general.account_number'),['class'=>' control-label']) !!} <strong class="text-danger">*</strong>
                    </div>
                    
                    <div class="col-sm-7">
                      <div class="input-group">
                        <div class="input-group-addon"><a href="#"><i class="fas fa-pencil-alt"></i></a></div>
                        {{ Form::text('number', null, ['class' => 'form-control input-lg', 'required', 'v-model' => 'form.number']) }}
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('currency_id', trans('financial::general.currency'),['class'=>' control-label']) !!} <strong class="text-danger">*</strong>
                    </div>
                    
                    <div class="col-sm-7">
                      <div class="input-group">
                        <div class="input-group-addon"><a href="#"><i class="fas fa-exchange-alt"></i></a></div>
                        {{ Form::select('currency_id', $currencies, null, ['class' => 'form-control input-lg',  'v-model' => 'form.currency_id', 'required', 'placeholder'=> trans("general.select").'...']) }}
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('balance', trans('financial::general.balance'),['class'=>' control-label']) !!} <strong class="text-danger">*</strong>
                    </div>
                    
                    <div class="col-sm-7">
                      <div class="input-group">
                        <div class="input-group-addon"><a href="#"><i class="fas fa-dollar-sign"></i></a></div>
                        <money
                        decimal="{{$currency->decimal_mark}}"
                        thousands="{{$currency->thousands_separator}}"
                        name="balance"
                        id="balance"
                        v-model="form.balance"
                        value="{{(isset($account->balance) ? number_format($account->balance,2) :null)}}"
                        class="form-control input-lg"
                        {{ (isset($formEdit) ? 'disabled' : null) }}
                        required
                        ></money>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-md-3">

                      </div>
                      <div class="col-md-3">
                          <div class="form-group">
                              {{ Form::label('default', trans('financial::general.default_account')) }}
                            <div class="anil_nepal">
                                <label class="switch switch-left-right">
                                  {{ Form::checkbox('default', 1, null,['class' => 'switch-input', 'v-model'=>'form.default']) }}
                                <span class="switch-label" data-on="{{trans('general.yes')}}" data-off="{{trans('general.not')}}"></span> <span class="switch-handle"></span> </label>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                            {{ Form::label('enabled', trans('general.enabled')) }}
                            <div class="anil_nepal">
                                <label class="switch switch-left-right">
                                {{ Form::checkbox('enabled', 1, true,['class' => 'switch-input',  'v-model'=>'form.enabled']) }}
                                <span class="switch-label" data-on="{{trans('general.yes')}}" data-off="{{trans('general.not')}}"></span> <span class="switch-handle"></span> </label>
                            </div>
                        </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</div>

<!-- Informações do banco -->
<div class="row">

    <div class="col-sm-12">
        
        <div class="panel panel-primary" data-collapsed="0">
        
            <div class="panel-heading">
                <div class="panel-title">
                    {{trans('financial::general.bank_info')}}
                </div>
                
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                    <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('bank_name', trans('financial::general.bank_name'),['class'=>' control-label']) !!}
                    </div>
                    
                    <div class="col-sm-7">
                      <div class="input-group">
                        <div class="input-group-addon"><a href="#"><i class="fas fa-university"></i></a></div>
                        {{ Form::text('bank_name', null, ['class' => 'form-control input-lg',  'v-model' => 'form.bank_name']) }}
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('bank_phone', trans('financial::general.bank_phone'),['class'=>' control-label']) !!}
                    </div>
                    
                    <div class="col-sm-7">
                      <div class="input-group">
                        <div class="input-group-addon"><a href="#"><i class="fas fa-phone"></i></a></div>
                        {{ Form::text('bank_phone', null, ['class' => 'form-control input-lg',  'v-model' => 'form.bank_phone']) }}
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>

@section("scripts")
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="{{asset("assets/js/select2/select2-bootstrap.css")}}">
	<link rel="stylesheet" href="{{asset("assets/js/select2/select2.css")}}">
	<link rel="stylesheet" href="{{asset("assets/js/selectboxit/jquery.selectBoxIt.css")}}">

	<!-- Imported scripts on this page -->
	<script src="{{asset("assets/js/select2/select2.min.js")}}"></script>
    <script src="{{asset("assets/js/selectboxit/jquery.selectBoxIt.min.js")}}"></script>
    <script>
     
        const editStatus = {{(isset($formEdit) ? $formEdit : false)}};
      
    </script>
    <script src="{{asset("js/financial/accounts.js")}}"></script>
@endsection