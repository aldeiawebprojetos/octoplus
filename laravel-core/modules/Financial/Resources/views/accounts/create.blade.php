@extends('layouts.app')

@section('title', trans('financial::general.name') . ' / ' .  trans('financial::general.account') )

@section('breadcrumb')
<li> <a href="javascript:void(0)">{{ trans('financial::general.name') }}</a> </li>
<li> <a href="javascript:void(0)">  {{trans('financial::general.incomes') }} </a></li>
<li class="active"> <strong>{{trans_choice('general.add',2,['item' => trans('financial::general.account')])}}</strong> </li>		
@endsection

@section('content')
<br />	
		
<hr />

        
{{Form::open(["route" => 'financial.accounts.store', 'id'=>'accounts'])}}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('financial::general.account')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

   @include('common.buttonsForm',['route_return' => 'financial.accounts.index'])
</div>
@include('financial::accounts.form')
<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'financial.accounts.index'])
</div>
{{ Form::close() }}

@endsection