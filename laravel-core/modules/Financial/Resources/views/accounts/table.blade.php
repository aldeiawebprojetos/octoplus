<table class="table table-bordered table-striped datatable" id="table-4">
    <thead>
        <tr>
            <th>{{ trans('general.actions') }}</th>
            <th>{{ trans('general.name') }}</th>
            <th>{{ trans('financial::general.account_number') }}</th>
            <th>{{ trans('financial::general.balance') }}</th>
        </tr>
    </thead>  
    <tbody>
        @foreach ($accounts as $account)
            <tr>
                <td>
                    @include('common.tableActions',['options' => [
                        ['text' => trans('general.edit'),'route' => ['financial.accounts.edit',true],'permission' => 'financial.account_update','icon' => 'entypo-pencil'],
                    ], 'name' => $account->name, 'id' => $account->id, 'delete_permission' => 'financial.account_delete','route_delete'=>'financial.accounts.delete'])
                </td>
                <td>{{ $account->name }}</td>
                <td>{{ $account->number }}</td>
                <td>{{ number_format($account->balance,2,$currency->decimal_mark,$currency->thousands_separator) }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>{{ trans('general.actions') }}</th>
            <th>{{ trans('general.name') }}</th>
            <th>{{ trans('financial::general.account_number') }}</th>
            <th>{{ trans('financial::general.balance') }}</th>
        </tr>
    </tfoot>
</table>