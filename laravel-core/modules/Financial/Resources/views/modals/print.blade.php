<div id="modal-print" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <button class="close" onClick="closeModalPrint()">&times;</button>
    <h3>Imprimir fatura</h3>
    <p>A fatura foi recebida com sucesso. Deseja imprimi-la?</p>
    <br>
    <div id="modal-actions">
      <button class="btn btn-default" onClick="closeModalPrint()">Cancelar</button>
      &nbsp;&nbsp;
      @if(isset($query_id))
        <button class="btn btn-blue" onclick="window.open('{{ route('financial.transaction.print', [$query_id]) }}'); closeModalPrint()">Imprimir</button>
      @endif
      </div>
  </div>

</div>

<style>
    .modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 100000; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 330px
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

#modal-actions{
  text-align: right;
}
</style>