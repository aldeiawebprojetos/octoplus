	<!-- Modal 6 (Long Modal)-->
	<div class="modal fade" id="modalCancel">
		<div class="modal-dialog" style="margin-top: 80px;">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title"> <i class="entypo-folder"></i> {{ trans('financial::general.cancel') }} #<span id='cancel_code'></span></h3>
				</div>
				{{ Form::open(['method' => 'PUT','id' =>'cancel', ]) }}
				<div class="modal-body">
				
				<div class="row">
					<div class="col-md-12">
						
						<div class="form-group">
							
							{{Form::label('description_cancel',trans('financial::general.description_cancel'))}}
							{{Form::textarea('description_cancel', null, ['class' => 'form-control'])}}

						</div>	
						
					</div>
				</div>
				
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">{{ trans('general.close') }}</button>
					<button type="submit" class="btn btn-danger btn-lg btn-icon pull-right">{{trans('general.save')}}<i class="entypo-floppy"></i></button>
				</div>
                {{ Form::close() }}
			</div>
		</div>
	</div>

	<script>
		function sendCancel()
		{
			
			
		}
	</script>
