@extends('layouts.app')

@section('title', trans('financial::general.name') . ' / ' .  trans('financial::general.incomes') )

@section('breadcrumb')
<li> <a href="javascript:void(0)">{{ trans('financial::general.name') }}</a> </li>
<li> <a href="javascript:void(0)">  {{trans('financial::general.incomes') }} </a></li>
<li class="active"> <strong>{{trans('general.listing')}}</strong> </li>		
@endsection

@section('content')
<hr>
<div class="mail-env">
		
   
   <!-- Mail Body -->
    <div class="mail-body">
        
        <div class="mail-header">
            <!-- Search  form -->
            
            <form method="get" role="form" class="search-form-full" id="searchForm">
                <div class="form-group">
                    <input type="text" class="form-control input-lg" name="s" id="search-input" placeholder="Pesquise pelo Código, nome do cliente..." value="{{ isset( Request::query()['s']) ? Request::query()['s'] : null }}"/>
                    <i class="entypo-search"></i>
                </div>

                <input type="hidden" name="status" id="status_input" value="{{ isset( Request::query()['status']) ? Request::query()['status'] : null }}">
                <input type="hidden" name="vencendo" id="vencendo_input" value="{{ isset( Request::query()['vencendo']) ? Request::query()['vencendo'] : null }}">
                <input type="hidden" name="vencido" id="vencido_input" value="{{ isset( Request::query()['vencido']) ? Request::query()['vencido'] : null }}">
            
        </div>

        <div class="mail-header">
            <!-- title -->
            <h3 class="mail-title">
                Faturas
                <span class="count text-info">({{ $faturaCount }})  {{ trans('financial::general.receivable') }}</span>
            </h3>

        </div>
    </form>
                
        @include('financial::incomes.table')

        {!! $faturas->links() !!}
    </div>
    
    <!-- Sidebar -->
    <div class="mail-sidebar">
        <!-- compose new email button -->
        <div class="mail-sidebar-row hidden-xs">
            
            <div class="btn-group">
                <button type="button" class="btn btn-blue btn-lg btn-icon btn-block dropdown-toggle" data-toggle="dropdown">
                    {{ trans('financial::general.launches') }} 
                    <i class="entypo-plus"></i>
                </button>               
                <ul class="dropdown-menu" role="menu">
                    @can('financial.income_create')
                    <li><a href="{{ route('financial.incomes.create') }}">{{trans_choice('general.add', 2, ['item' => trans("financial::general.income")])}}</a>
                    </li>
                    @endcan           
                    
                </ul>
            </div>

        </div>

        <!-- Filtros -->
    

        <h4>Período</h4>

        <div class="mail-menu">
            {{ Form::text('range_date',isset( Request::query()['range_date']) ? Request::query()['range_date'] : null,['class' => ' form-control daterange  daterange-inline add-ranges active', 'id' => 'range_date_selec', 'data-format'=>"DD/M/YYYY"])}}
        </div>
        
        <div class="mail-distancer"></div>
        <h4>Filtrar por Status</h4>
        
        <!-- menu -->
        <ul class="mail-menu">
            <li>
                <a class="status-filter" style="cursor: pointer">
                    <span class="badge badge-gray badge-roundless pull-right"> {{ $faturasOpen }} </span>

                    Em Aberto
                </a>
            </li>
            
                <li>
                    <a class="status-filter" data-status="open" data-vencido="true" style="cursor: pointer">
                        <span class="badge badge-danger badge-roundless pull-right"> {{ $faturasPayable }} </span>

                        <span class="text-danger">Vencido </span>
                    </a>
                </li>

           
           
            
            <li>
                <a class="status-filter" data-status="settled" style="cursor: pointer">
                    <span class="badge badge-success badge-roundless pull-right"> {{ $faturasSettled }} </span>

                    <span class="text-success">Recebido</span>
                </a>
            </li>
            

            <li>
                <a class="status-filter" data-status="open" data-vencendo="true" style="cursor: pointer">
                    <span class="badge badge-warning badge-roundless pull-right"> {{ $faturasWinning }} </span>

                     <span class="text-warning">Vencendo</span>
                </a>
            </li>
            <li>
                <a  class="status-filter" data-status="canceled" style="cursor: pointer">
                    <span class="badge badge-primary badge-roundless pull-right"> {{ $faturasCanceled }} </span>

                     <span class="text-primary">Cancelado</span>
                </a>
            </li>
            <!--<li>
                <a href="{{ route('financial.incomes.index', ['status' => 'partial'])}}">
                    <span class="badge badge-info badge-roundless pull-right"> {{ $faturasPartial }} </span>

                    <span class="text-info">Parcial</span>
                </a>
            </li>-->
        </ul>

        <div class="mail-distancer"></div>
        
        <h4>Dados</h4>
        
        <!-- menu -->
        <ul class="mail-menu">
            <li >
                <div class="text-center">
                     Qtd.:
                    <br>
                    <h3 class="text-success">{{ $faturaCount }}</h3>
                </div>
            </li>
            
            <li>
                <div class="text-center">
                    Valor Total:
                    <br>
                    <span class="label label-info pull-right">R$ 
                        {{
                            number_format( 
                                $faturasValue, 
                                2,$currency->decimal_mark,$currency->thousands_separator )
                        }} 
                        
                    </span>
                </div>
            </li>
            
        </ul>

    </div>
    
</div>
@endsection

