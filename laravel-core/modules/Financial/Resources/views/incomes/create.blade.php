@extends('layouts.app')

@section('title', trans('financial::general.name') . ' / ' .  trans('financial::general.incomes') )

@section('breadcrumb')
<li> <a href="javascript:void(0)">{{ trans('financial::general.name') }}</a> </li>
<li> <a href="javascript:void(0)">  {{trans('financial::general.incomes') }} </a></li>
<li class="active"> <strong>{{trans_choice('general.add',2,['item' => trans('financial::general.incomes')])}}</strong> </li>		
@endsection

@section('content')
<br />	
		
<hr />

        
{{Form::open(["route" => 'financial.incomes.store', 'id'=>'incomes'])}}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('financial::general.incomes')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

   @include('common.buttonsForm',['route_return' => 'financial.incomes.index', ])
</div>
@include('financial::incomes.form')
<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'financial.incomes.index'])
</div>
{{ Form::close() }}


<div class="clear"></div>
<!-- Footer -->
@endsection

@section('modals')
@include('common.modals.addcategory', ['type' => 'income'])
@endsection