@extends('layouts.app')

@section('title', trans('financial::general.name') . ' / ' .  trans('financial::general.incomes') )

@section('breadcrumb')
<li> <a href="javascript:void(0)">{{ trans('financial::general.name') }}</a> </li>
<li> <a href="javascript:void(0)">  {{trans('financial::general.incomes') }} </a></li>
<li class="active"> <strong>{{trans_choice('general.edit',2,['item' => trans('financial::general.incomes')])}}</strong> </li>		
@endsection

@section('content')
<br />	
		
<hr />

        
{{Form::model($fatura,["route" => ['financial.incomes.update',$fatura->id], 'id'=>'incomes','method' => 'PUT'])}}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('financial::general.income')}} <i class="entypo-pencil"></i>


           <span class="label label-default"> #{{$fatura->code}} </span>
         
            @if($fatura->invoice->count() > 0)
              <a href="{{ route('sale-rent.invoices.rent.show', $fatura->invoice->first()->id) }}" target="_blank" type="button" class="btn btn-white">
                <i class="fa fa-eye"></i>
              </a>
            @endif
       </h1>
   </div>

   @include('common.buttonsForm',['route_return' => 'financial.incomes.index','edit' => (isset($fatura) && $fatura->paid && $fatura->status == 'closed' ? false : true)])
</div>
@include('financial::incomes.form', ['formEdit' => true])
<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'financial.incomes.index','edit' => (isset($fatura) && $fatura->paid && $fatura->status == 'closed' ? false : true)])
</div>
{{ Form::close() }}


<div class="clear"></div>
<!-- Footer -->
@endsection

@section('modals')
@include('common.modals.addcategory', ['type' => 'income'])
@endsection