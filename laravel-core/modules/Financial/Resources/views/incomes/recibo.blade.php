@extends('layouts.app')


@section('content')
<div class="invoice">
		
    <div class="row">
    
        <div class="col-sm-6 invoice-left ">
        
            <a href="#">
                <img src="{{($company->firstMedia('logo') != null ? ($pdf ? $company->firstMedia('logo')->getAbsolutePath() : $company->firstMedia('logo')->getUrl()) : ($pdf ? public_path('/assets/images/logo@2x.png') : asset('/assets/images/logo@2x.png')))}}" alt="company_logo" width="185">
            </a>
        </div>
        
        <div class="col-sm-6 invoice-right">
            <h3>{{trans('financial::general.receipt')}} <strong style="color: red;"> Nº {{ $fatura->code }} </strong></h3>
            <br>
            <span>{{trans("financial::general.value")}}: {{ formata_moeda($fatura->value_liquid) }}</span> <small>{{ $fatura->payment_method != null && $fatura->payment_method != '0' ? settings('payment.methods',getDefaultPaymentsMethods())[$fatura->payment_method] : null }}</small>
            <br>
            <span>{{trans('financial::general.data_vencimento')}}: {{ $fatura->data_vencimento->format('d/m/Y') }}</span>
            <br>
            <span>{{ trans("financial::general.data_recebimento") }}: {{ date('d/m/Y', strtotime($fatura->data_recebimento)) }}</span>
        </div>
    </div>
    
    
    <hr class="margin">
    

    <div class="row">
    
        <div class="col-md-6 invoice-left">
            <h4>{{__('financial::general.received')}} {{  ( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' ) }}</h4>
            
            <br>
            {{trans("customers::general.address")}}: <strong>{{ ( isset( $fatura->customer->address_complete  ) ? $fatura->customer->address_complete   : '' )}}</strong>
            <br>
            
            {{__('financial::general.value_of')}}: <strong>{{ formata_moeda($fatura->value_liquid) }}</strong>
            <br>
            {{ __('financial::general.reference') }}: <strong>{{ $fatura->description }}</strong>
        </div>
    
        
        <div class="col-md-6 invoice-right">
        
            <h4>{{__("financial::general.payee")}}: {{$company->name}}</h4>
            <br>
             {{ __('customers::general.address') }}: <strong>{{ $company->address }}</strong>
            <br>

              {{ strlen($company->document) == 11 ? __('customers::general.cpf') : __('customers::general.cnpj') }} : <strong> {{ formatar_cpf_cnpj($company->document) }}</strong>
            <br>
            <br>

             {{__('financial::general.signature')}}: <strong class="text-center">_____________________ <br> {{ auth()->user()->name }} </strong>
            <br>

        </div>
        
    </div>
    
    

    <div class="row">
        
        <br>
        
        <a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">
            Imprimir Recibo
            <i class="entypo-doc-text"></i>
        </a>
        
        
    </div>


    <hr class="margin">

</div>
@endsection