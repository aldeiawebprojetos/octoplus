@extends('layouts.app')

@section('breadcrumb')
<li> <a href="">{{ trans('customers::general.name') }}</a> </li>
<li> <a href="{{route('tenant.categories.index')}}">{{trans('general.categories')}}</a> </li>
<li class="active"> <strong>{{trans('general.show')}}</strong> </li>		
@endsection

@section('content')
		<br />	
		<hr />	
		{{Form::model($category, ['class' => 'disabled' ])}}
		<div class="row">
			<!-- title -->
			<div class="col-md-6">
			   <h1 class="pull-left">
				   {{trans('general.categories')}} <i class="entypo-pencil"></i>
			   </h1>
		   </div>
       </div>
       
		@include('financial::categories.form', ['readOnly' => true])

		{{ Form::close() }}
		<div class="clear"></div>
		<!-- Footer -->
@endsection