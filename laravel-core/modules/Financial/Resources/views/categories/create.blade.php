@extends('layouts.app')

@section('title',  trans('general.categories') )

@section('breadcrumb')
<li> <a href="{{route('tenant.categories.index')}}">{{trans('general.categories')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.add',2,['item' => ''])}}</strong> </li>	
@endsection

@section('content')
{{ Form::open(['method'=>'POST', 'route' => 'financial.categories.store']) }}
<div class="row">
    <!-- title -->
    <div class="col-md-6">
       <h1 class="pull-left">
           {{trans('general.categories')}} <i class="entypo-pencil"></i>
       </h1>
   </div>

   @include('common.buttonsForm',['route_return' => 'financial.categories.index'])
</div>

@include('financial::categories.form')

<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'financial.categories.index'])
</div>
{{ Form::close() }}
@endsection