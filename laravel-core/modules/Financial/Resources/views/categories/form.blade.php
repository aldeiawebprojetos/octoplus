<div class="clear"> <br>  <br> </div>

<div class="row">
  <div class="form-group col-md-2 col-xs-4 col-md-offset-3">
    {{ Form::label('cod_product', trans('general.code')) }}
    {{ Form::text('cod_product', (isset($category->id) ? '#'. $category->id : null), ['class' => 'form-control input-lg', 'readonly']) }}
  </div>

  <div class="form-group col-md-3 col-xs-4">
    <label class=""> {{ trans("general.active") }}? </label>
    
      <div class="">
             <label class="switch switch-left-right">
          {{ Form::checkbox('enabled', 1, true, ['class'=> 'switch-input',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
          <span class="switch-label switch-label-3" data-on="{{ trans('general.yes') }}" data-off="{{ trans('general.not') }}"></span> <span class="switch-handle"></span> 
          </label>  
        </div>
    
  </div>

</div>


<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="form-group">
      {{Form::label('type', trans("general.type"))}} <strong class="text-danger">*</strong>
      {{ Form::select('type', categories_types(['item', 'other']) ,null,['class' => 'form-control  input-lg','placeholder'=>trans("general.select").'...',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
    </div>
  </div>
</div>


<!--
<div class="row">
    <div class="col-md-6 col-md-offset-3 ">
        <div class="form-group">
           {{Form::label('parent_id', trans("general.category"))}}
           {{ Form::select('parent_id', $categories ,null,['class' => 'form-control input-lg','placeholder'=>trans("general.select").'...']) }}
           
        </div>
    </div>
</div>

-->


<div class="row">   
  <div class="form-group col-md-6 col-md-offset-3">
       {{ Form::label('name',trans('general.name')) }} <strong class="text-danger">*</strong>
        {{ Form::text('name', null,['class' => 'form-control input-lg',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
  </div>
</div>

<!--
<div class="row">   
  <div class="form-group col-md-6 col-md-offset-3">
    <label>Imagem / Ícone</label>
          
     <select class="selectboxit">
        <option selected>Nenhuma</option>
        <option >Ícone do Sistema</option>
        <option >Escolher Imagem </option>
      </select>
  </div>
</div>


<div class="row">   
  <div class="form-group col-md-6 col-md-offset-3" >
    <label class=""> Ícone da Categorias </label>

    <select name="test" class="selectboxit">
          <option value="SelectBoxIt themes:" data-iconurl="{{ asset('assets/images/container-diponivel.svg') }}"> Container </option>

          <option value="Twitter Bootstrap" data-iconurl="{{ asset('assets/images/item-disponivel.svg') }}"> Item </option>
          
          <option value="Twitter Bootstrap" data-iconurl="{{ asset('assets/images/item2-disponivel.svg') }}"> Item 2 </option>
          
          <option value="jQuery UI" data-iconurl=" {{ asset('assets/images/item2.svg') }}"> Caixa </option>
         
          <option value="jQuery Mobile" data-iconurl=" {{ asset('assets/images/item3.svg') }}"> Embalagem </option>
      </select>

      <code>Só mostra essa parte se no select acima for selecionado opção de icone.</code>
  </div>


  <div class="form-group col-md-6 col-md-offset-3">  
    <img src="{{ asset('assets/images/container-diponivel.svg') }}" width="250" height="250">
    <br>
    <code>Mostrar o ícone maior aqui de acordo com o que for selecionando no select.</code>
  </div>

</div>

<div class="clear"></div>

<div class="row">   
  <div class="form-group">
      <label class="col-sm-3 control-label">Imagem Upload</label>
      
      <div class="col-sm-5">
        
        <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
            <img src="http://placehold.it/200x150" alt="...">
          </div>
          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
          <div>
            <span class="btn btn-white btn-file">
              <span class="fileinput-new">Escolha a Imagem</span>
              <span class="fileinput-exists">Change</span>
              <input type="file" name="..." accept="image/*">
            </span>
            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
          </div>
        </div>
        
      </div>
      <code>Só mostra o upload se escolher a opção de imagem no select </code>
    </div>
</div>
-->
<div class="clear"> <br>  <br> </div>