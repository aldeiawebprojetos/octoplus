<?php

namespace Modules\Financial\Database\Seeds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Octoplus\Domain\Module\Models\Module;

class FinancialDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Module::create(['alias' => 'financial','status' => true]);
         $this->call(Permissions::class);

        Model::reguard();
    }
}
