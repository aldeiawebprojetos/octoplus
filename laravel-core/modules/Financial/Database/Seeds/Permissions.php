<?php

namespace Modules\Financial\Database\Seeds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Octoplus\Domain\Users\Models\Permission;

class Permissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
             ['name' => 'financial.account_create'],
             ['name' => 'financial.account_update'],
             ['name' => 'financial.account_show'],
             ['name' => 'financial.account_delete'],

             ['name' => 'financial.expenses_create'],
             ['name' => 'financial.expenses_update'],
             ['name' => 'financial.expenses_show'],
             ['name' => 'financial.expenses_delete'],
             
             ['name' => 'financial.income_create'],
             ['name' => 'financial.income_update'],
             ['name' => 'financial.income_show'],
             ['name' => 'financial.income_delete'],

             ['name' => 'financial.transactions_create'],
             ['name' => 'financial.transactions_update'],
             ['name' => 'financial.transactions_show'],
             ['name' => 'financial.transactions_delete'],
         ];

         foreach($permissions as $permission)
         {
             Permission::create($permission);
         }
    }
}
