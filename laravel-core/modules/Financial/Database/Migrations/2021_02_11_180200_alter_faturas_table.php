<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('faturas', function(Blueprint $table){
           
            $table->boolean('boleto')->nullable()->default(false);
            $table->boolean('nota_fiscal')->nullable()->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faturas', function(Blueprint $table){
           
            $table->dropColumn('boleto');
            $table->dropColumn('nota_fiscal');

        });
    }
}
