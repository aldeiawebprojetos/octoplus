<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccountTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('fatura_id')->unsigned();
            $table->bigInteger('account_id')->unsigned();

            $table->string("type",8);
            $table->string('status',7);
            $table->string('description');

            $table->float('total');
            $table->date('date');

            $table->timestamps();

            $table->foreign('fatura_id')->references('id')->on('faturas')->onDelete('cascade');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_transactions');
    }
}
