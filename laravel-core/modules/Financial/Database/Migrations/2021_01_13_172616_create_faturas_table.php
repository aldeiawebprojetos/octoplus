<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faturas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('company_id')->unsigned();
            $table->date('data_competencia')->nullable();
            $table->date('data_vencimento')->nullable();
            $table->text('description')->nullable();
            $table->string('type', 8);
            $table->string('code');
            $table->string('status',8)->nullable()->default('open');

            $table->float('value')->nullable()->default(0.00);
            $table->float('value_paid')->nullable()->default(0.00);
            $table->float('discount')->nullable()->default(0.00);
            $table->float('juros_multas')->nullable()->default(0.00);
            $table->float('value_liquid')->nullable()->default(0.00);

            $table->boolean('recurrent')->nullable()->default(false);
            $table->string('payment_frequency',12)->nullable();
            $table->integer('payment_quantity')->nullable();
            $table->string('payment_method')->nullable();

            $table->boolean('paid')->nullable()->default(false);
            $table->date('data_recebimento')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faturas');
    }
}
