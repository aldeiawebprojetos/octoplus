<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('faturas', function(Blueprint $table){
           
            $table->bigInteger('parent_id')->nullable()->unsigned();

            $table->foreign('parent_id')->references('id')->on('faturas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faturas', function(Blueprint $table){
           
            $table->dropColumn('parent_id');
        });
    }
}
