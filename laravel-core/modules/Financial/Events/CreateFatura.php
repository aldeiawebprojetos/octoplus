<?php

namespace Modules\Financial\Events;

use Illuminate\Queue\SerializesModels;
use Modules\SaleRent\Models\Invoice;

class CreateFatura
{
    use SerializesModels;

    public $FaturasData;
    public $Invoice;
    public $Items_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $FaturasData, Invoice $Invoice, array $Items_id)
    {
       $this->FaturasData = $FaturasData;
       $this->Invoice = $Invoice;
       $this->Items_id = $Items_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
