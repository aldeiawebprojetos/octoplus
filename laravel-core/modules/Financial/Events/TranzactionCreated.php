<?php

namespace Modules\Financial\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Financial\Models\Fatura;
use Octoplus\Domain\Account;

class TranzactionCreated
{
    use SerializesModels;

    public $account;
    public $fatura;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Account $account, Fatura $fatura)
    {
       $this->account = $account;
       $this->fatura = $fatura;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
