<?php

namespace Modules\Financial\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Customers\Models\Customer;
use Modules\SaleRent\Models\Invoice;
use Octoplus\App\Tenant\Traits\ForTenants;
use Octoplus\Domain\Account;
use Octoplus\Domain\Category\Models\Category;
use Spatie\Activitylog\Traits\LogsActivity;

class Fatura extends Model
{
    
    use ForTenants, LogsActivity, SoftDeletes;

    protected $casts = [
        'data_competencia' => 'date',
        'data_vencimento' => 'date'
    ];

    protected $fillable = [
        'customer_id',
        'account_id',
        'code',
        'id',
        'boleto',
        'nota_fiscal',
        'company_id',
        'data_competencia',
        'data_vencimento',
        'description',
        'value',
        'type',
        'status',
        'value_paid',
        'discount',
        'juros_multas',
        'value_liquid',
        'recurrent',
        'payment_method',
        'payment_frequency',
        'payment_quantity',
        'paid',
        'data_recebimento',
    ];

    protected static $logFillable = true;
    protected static $logName = 'financial::general.invoice';

    public function getTotalBalanceAttribute()
    {
        
       return $this->value_liquid - $this->value_paid;
    }

    public function getVencidoAttribute()
    {
        $hoje = Carbon::now();
        return $hoje->diffInDays($this->data_vencimento,false) < 0;
    }

    public function getVencendoAttribute()
    {
        $hoje = Carbon::now();
        return $hoje->diffInDays($this->data_vencimento,false) == 0;
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function faturas()
    {
        return $this->hasMany(Fatura::class,'parent_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'fatura_categories');
    }

    public function invoice()
    {
        return $this->belongsToMany(Invoice::class,'invoice_faturas');
    }
}
