<?php

namespace Modules\Financial\Models;

use Illuminate\Database\Eloquent\Model;

class AccountTransaction extends Model
{
    protected $fillable = [
        'fatura_id',
        'account_id',
        "type",
        'status',
        'description',
        'total',
        'date',
    ];
}
