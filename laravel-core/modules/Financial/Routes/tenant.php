<?php

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "tenant" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web','tenant','admin','bindings'], 'prefix'=>'financial', 'as'=>'financial.', 'namespace'=>'Modules\Financial\Http\Controllers' ], function () {
    
    Route::group(['prefix' => 'incomes','namespace' => 'Incomes', 'as' => 'incomes.'], function(){
        Route::get('/', 'IncomesController@index')->name('index');
        Route::get('/create', 'IncomesController@create')->name('create');
        Route::post('/store', 'IncomesController@store')->name('store');
        Route::get('/{fatura_id}/edit', 'IncomesController@edit')->name('edit');
        Route::get('/{fatura_id}/show', 'IncomesController@show')->name('show');
        Route::get('/{fatura_id}/recibo', 'IncomesController@recibo')->name('recibo');
        Route::put('/{fatura_id}/update', 'IncomesController@update')->name('update');
        Route::delete('/{fatura_id}/delete','IncomesController@delete')->name('delete');
    });

    Route::group(['prefix'=>'categories', 'as' => 'categories.'], function(){
        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('create','CategoryController@create')->name('create');
        Route::post('store', 'CategoryController@store')->name("store");
        Route::get('{category_id}/edit', 'CategoryController@edit')->name('edit');
        Route::get('{category_id}/show', 'CategoryController@show')->name('show');
        Route::put('{category_id}/update', 'CategoryController@update')->name('update');
    });
    
   Route::group(['prefix' => 'transaction' , 'as' => 'transaction.', 'namespace' => 'Transaction'], function(){
        Route::get('/{fatura_id}/payOff', 'FaturaController@payOff')->name('payOff');
        Route::get('/{fatura_id}/print', 'FaturaController@print')->name('print');
        Route::get('/{fatura_id}/reversal', 'FaturaController@reversal')->name('reversal');
        Route::put('/{fatura_id}/cancel', 'FaturaController@cancel')->name('cancel');
   });

    Route::group(['prefix' => 'expenses','namespace' => 'Expenses', 'as' => 'expenses.'], function(){
        Route::get('/', 'ExpensesController@index')->name('index');
        Route::get('/create', 'ExpensesController@create')->name('create');
        Route::post('/store', 'ExpensesController@store')->name('store');
        Route::get('/{fatura_id}/edit', 'ExpensesController@edit')->name('edit');
        Route::get('/{fatura_id}/show', 'ExpensesController@show')->name('show');
        Route::put('/{fatura_id}/update', 'ExpensesController@update')->name('update');
        Route::delete('/{fatura_id}/delete','ExpensesController@delete')->name('delete');
    });

    Route::group(['prefix' => 'accounts','namespace' => 'Accounts', 'as' => 'accounts.'], function(){
        Route::get('/', 'AccountsController@index')->name('index');
        Route::get('/create', 'AccountsController@create')->name('create');
        Route::post('/store', 'AccountsController@store')->name('store');
        Route::get('/{account_id}/edit', 'AccountsController@edit')->name('edit');
        Route::put('/{account_id}/update', 'AccountsController@update')->name('update');
        Route::delete('/{account_id}/delete','AccountsController@delete')->name('delete');
    });
});