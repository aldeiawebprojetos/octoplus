<?php

namespace Modules\SaleRent\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class Settings extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('sale-rent::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sale-rent::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('sale-rent::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit()
    {

        return view('sale-rent::settings.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $data = $this->getSettingsArray($request);
        
        settings($data)->save();

        sendNotify(trans('sale-rent::warnings.settings.updated'), 'success');

       return redirect()->back();
    }


    private function getSettingsArray(Request $request)
    {
        $data = $request->all();
        unset($data['_wysihtml5_mode'],$data['_token']);
        $data['rent']['module'] = isset($data['rent']['module']) ? $data['rent']['module'] : false;
        $data['rent']['use_contract'] = isset($data['rent']['use_contract']) ? $data['rent']['use_contract'] : false;
        $data['contract']['rent'] = isset($data['contract']['rent']) ? $data['contract']['rent'] : '<p></p>';
        $data['contract']['new_number'] = isset($data['contract']['new_number']) ? $data['contract']['new_number'] : false;
        $data['contract']['category_as_name'] = isset($data['contract']['category_as_name']) ? $data['contract']['category_as_name'] : '<p></p>';
        $data['purchase']['module'] = isset($data['purchase']['module']) ? $data['purchase']['module'] : false;
        $data['sale']['module'] = isset($data['sale']['module']) ? $data['sale']['module'] : false;
        
        return $data;
    }
}
