<?php

namespace Modules\SaleRent\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Financial\Events\CreateFatura;
use Modules\SaleRent\Models\Invoice as ModelsInvoice;
use Illuminate\View\View;
use Octoplus\Domain\Item\Models\Item;
use Modules\SaleRent\Models\InvoiceItem;
use Modules\SaleRent\Models\Invoice;
use Octoplus\Domain\Category\Models\Category;
use Octoplus\Domain\Currency\Tax;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Modules\Financial\Models\Fatura;
use Octoplus\Domain\Company\Models\Company;
use PDF;
use Storage;

class Rent extends Controller
{
    public function __construct()
    {
        $this->middleware('can:sale-rent.rent-invoices_create')->only(['create','store']);
        $this->middleware('can:sale-rent.rent-invoices_update')->only(['edit','update']);
        $this->middleware('can:sale-rent.rent-invoices_show')->only(['index']);
        $this->middleware('can:sale-rent.rent-invoices_delete')->only(['delete']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request): View
    {
        if($request->all() != null){

            $query = ModelsInvoice::where('invoice_type','rent')->orderBy('sale_date','DESC')
            ->when($request->range_date, function ($q) use ($request) {
                return $q->whereBetween('sale_date',$this->formataRangeDate($request));
            });


            if(isset($request->s))
            {
                $query->where(function($aux) use ($request){
                    $aux->orwhere('code','LIKE', '%'.$request->s.'%')
                        ->orWhere('customer_name', 'LIKE', '%'.$request->s.'%')
                        ->orWhere('id', $request->s);
                });
            }

            $invoices = $query->paginate();
        }else{
            /*$my_file = 'file4.txt';
                $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
                $data = 'Test data to see if this works!';
                fwrite($handle, $data);

                //$storagePath = Storage::disk('s3')->put("uploads", $my_file);
                Storage::put('abc', $my_file);*/

            $invoices = null;
        }
       
        return view('sale-rent::rent.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        error_log('create');
        $categories = Category::where([['type','income'],['enabled',true]])->orderBy('name')->pluck('name','id');

        return view('sale-rent::rent.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        /*$request->merge([
            'rent_endDate' => "2021-10-01"
        ]);*/
        $data = $this->getInvoiceArray($request);
        $data['code'] = settings('prefix.rent','PA') . str_pad(settings('sequence.rent',1) , 5 , '0' , STR_PAD_LEFT);
        
        //Cadastra o Pedido
        $invoice = ModelsInvoice::create($data);
       
        if($invoice){
            //incrementa a sequencia do código
            settings(['sequence.rent' => settings('sequence.rent',1) +1])->save();
         }
        //Inclui os itens no pedido
        $invoice->invoice_items()->attach($this->getItemsArray($request));


        $invoice->categories()->attach($request->categories);

        //muda o estado do item alocador
        $items_id = $this->getItemsArray( $request )->pluck('item_id');
        foreach($items_id as $id){
            $item = Item::find( $id );
            $item->rent_status = 0;
            $item->update();
        }

        event(new CreateFatura($data, $invoice, $items_id->toArray()));

        sendNotify(trans('sale-rent::warnings.invoice.success'), 'success');


        return redirect()->route('sale-rent.invoices.rent.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request)
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        $categories = Category::where([['type','income'],['enabled',true]])->orderBy('name')->pluck('name','id');
        $taxes = Tax::all();

        return view('sale-rent::rent.show', compact('invoice','categories','taxes'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        //dd($invoice);
        $categories = Category::where([['type','income'],['enabled',true]])->orderBy('name')->pluck('name','id');
        $taxes = Tax::all();
        if ($invoice->active_contract) {
            return view('sale-rent::rent.edit', compact('invoice','categories','taxes'));
        } else {
            return view('sale-rent::rent.show', compact('invoice','categories','taxes'));
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request)
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        $data = $this->getInvoiceArray($request);

       $data['parts'] = collect($data['parts'])->map(function($part){
            if($part['id'] == null){
                return $part;
            }
        })->filter()->toArray();

        $invoice->update($data);

         $invoice->categories()->sync($request->categories);


        if(count($data['parts']) > 0){
           
            event(new CreateFatura($data, $invoice, $invoice->items->pluck('item_id')->toArray()));
        }

         sendNotify(trans('sale-rent::warnings.invoice.updated'), 'success');

         return redirect()->route('sale-rent.invoices.rent.edit',$invoice->id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function close (Request $request) {
        $invoice = ModelsInvoice::find($request->invoice_id);

        if ($invoice) {
            $invoice->active_contract = false;
            $invoice->save();
            sendNotify(trans('sale-rent::warnings.invoice.updated'), 'success');
        }

         return redirect()->route('sale-rent.items.rent');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    

    public function rentItems(Request $request)
    {
            if( $request->product_type != "")
                $typeItem = $request->product_type;
            else
                $typeItem = "rent";

            $query = Item::where('enabled', isset($request->enabled) ? $request->enabled : true)
                        ->where('item_type', $typeItem)
                        ->orderBy('name');

            if(isset($request->category))
            {
                $query->whereHas('categories', function($cat) use ($request){
                    $cat->where('categories.id',$request->category);
                });   
            }
            
            if(isset($request->status))
            {
               $query->where('enabled',$request->status);
            }

            if(isset($request->rent_status) && $request->rent_status == 0)
            {
                $query
                ->where('rent_status', 0)
                ->whereDoesntHave('invoices', function($invoice){
                    $invoice
                    ->where('shipping_status','!=', 'closed')
                    ->where('rent_endDate', '!=', null)
                    ->where('active_contract', true)
                    ->whereDate('rent_endDate', '<=', Carbon::now()->startOfDay()->addDays(7)->toDateString())
                    ->whereDate('rent_endDate', '>=', Carbon::now()->startOfDay()->toDateString());
                });
                //dd(getPedidoByItemId('90'));
               //dd($query->toSql());
               //dd(Carbon::now()->startOfDay()->toDateString());
                
            }

            if(isset($request->rent_status) && $request->rent_status == 1)
            {
                $query->where('rent_status', 1);
            }

            if(isset($request->rent_status) && $request->rent_status == 2)
            {
                $query
                ->where('rent_status', 0)
                ->whereHas('invoices', function($invoice){
                    $invoice
                    ->where('shipping_status','!=', 'closed')
                    ->where('rent_endDate', '!=', null)
                    ->where('active_contract', true)
                    ->whereDate('rent_endDate', '<=', Carbon::now()->startOfDay()->addDays(7)->toDateString())
                    ->whereDate('rent_endDate', '>=', Carbon::now()->startOfDay()->toDateString());
                });
               
            }
    
            if(isset($request->s))
            {
               $query->where('code','LIKE','%'.$request->s.'%')->orWhere([['name','LIKE','%'.$request->s.'%']]);
            }

    
            $items = $query->paginate()->withQueryString();
            $dateToday = date('Y-m-d');
            $faturasWinningCount = 0;
            $faturasCanceledCount = 0;
            $faturasAvailableCount = 0;

            $itensAll = Item::where([['item_type', 'rent'], ['enabled', true]])->get();

            //dd($itensAll->toArray());
                foreach(  $itensAll as $item ){
                    //verificar ser existe alguma fatura aberta com o item
                        if( $item->rent_status == 0 ){
                            
                            //caso exista vai pegar o codigo da farura    
                            $invoices_id =  DB::table('invoice_items')->where('item_id', $item->id)->pluck('invoice_id')->toArray();
                            $invoice = Invoice::whereIn('id', $invoices_id)->where('shipping_status','!=', 'closed')->where('active_contract', true)->orderBy('id', 'desc')->first();

                            if(isset($invoice->rent_endDate)){
                                $dateInvoice = Carbon::parse($invoice->rent_endDate)->startOfDay();
                                $difDays = $dateInvoice->diffInDays(Carbon::now()->startOfDay(), true);
                                
                                if($difDays >= 0 && $difDays <= 7 && $dateInvoice >= Carbon::now()->startOfDay()){
                                    $faturasWinningCount++;
                                } else{
                                    $faturasCanceledCount++;
                                }
                            }

                        }else{
                            $faturasAvailableCount++;
                        }
                }
        $categories = Category::where('type','item')->pluck('name', 'id');

        return view('sale-rent::items.index', 
        compact('items', 'categories', 'faturasWinningCount', 'faturasCanceledCount', 'faturasAvailableCount', 'itensAll', 'typeItem' ));
    }

    public function delete(Request $request)
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        $faturaModel = new Fatura();
        $faturas = $invoice->faturas->pluck('id');
        $items = $invoice->invoice_items;

        if($items->count() > 0)
        {
            foreach($items as $item)
            {
                $item->rent_status = true;
                $item->save();
            }
        }
       if($faturas->count() > 0)
       {
            $faturaModel->destroy($faturas);
       }
       
        $invoice->delete();

        sendNotify(trans('sale-rent::warnings.invoice.deleted'),'success');

        return redirect()->route('sale-rent.invoices.rent.index');
    }

    public function contrato(Request $request){
        $invoice = ModelsInvoice::find($request->invoice_id);
        $company = Company::find(session("tenant"));

        $data = [
            'invoice' => $invoice,
            'company' => $company,
            'dataHoje' => Carbon::now()
        ];

		$pdf = PDF::loadView('sale-rent::rent.contrato', $data);
		return $pdf->stream('document.pdf');
    }

    /*public function contract(Request $request)
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        $contractData = $this->prepareContractData($invoice);
        $company = Company::find(session("tenant"));
       

        return view('sale-rent::rent.contract', compact('invoice', 'contractData','company'));
    }

    public function contractPDF(Request $request)
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        $contractData = $this->prepareContractData($invoice);
        $company = Company::find(session("tenant"));
       
        $pdf = PDF::loadView('sale-rent::rent.contractPrint', compact('invoice', 'contractData','company'));

        return $pdf->download(trans('sale-rent::general.contract').'_' .(settings('contract.new_number', true) ? $invoice->code : $invoice->id));
       
    }*/

    public function changeStatus(Request $request)
    {
        $response = [];
        $pedido = ModelsInvoice::find($request->pedido_id);
        $item = Item::find($request->item_id);
        $status = $request->status;
        
       
       if($pedido->update(['shipping_status' => $status]))
       {
            if($item->update(['rent_status' => getItemStatus($status)]))
            {
                $response = [
                    'success' => true,
                    'msg' => trans('sale-rent::general.pedido.'.$status),
                    'item' => (getItemStatus($status) ? 'success-item' : 'danger-item'),
                ];
            }else{
                $response = [
                    'success' => false,
                    
                ];
            }
       }else{
            $response = [
                'success' => false,
                
            ];
        }
        return response()->json($response);
    }

    /*private function prepareContractData(ModelsInvoice $invoice)
    {
        $dataHoje = Carbon::now();
        
        $variaveis = [
            '{data-hoje}' => $dataHoje->formatLocalized('%d '.trans('general.of').' %B '.trans('general.of').' %Y'),
            '{cpf-responsavel}' => formatar_cpf_cnpj($invoice->customer->responsible_cpf),
            '{nome-responsavel}' => $invoice->customer->responsible,
            '{nome-user}' => auth()->user()->name,
            '{cpf-user}' => formatar_cpf_cnpj(auth()->user()->document_id)
        ];

        $contrato = settings('contract.rent');
        $contrato = substr_replace($contrato, '', strpos($contrato, "\n") + 1, strrpos($contrato, "\n"));
       

        foreach($variaveis as $variavel => $valor)
        {
            $contrato = str_replace($variavel, $valor, $contrato);
        }

        return $contrato;
    }*/

    private function getInvoiceArray(Request $request): array
    {

        $request->validate([
            'customer_id'       => 'required',
        ],[
            'customer_id.required' => trans_choice('validation.required',2,['attribute' => 'Cliente']),
        ]);

        $data = $request->all();
        $data['sale_date'] = ($data['sale_date'] !== null ? date('Y-m-d', strtotime(str_replace('/','-',$data['sale_date']))) : date('Y-m-d'));
        
        return $data;
    }

    private function getItemsArray(Request $request)
    {
        $request->validate([
            'item' => 'required|min:1'
        ]);
        $items = $request->all()['item'];
        $result = collect();

        foreach($items as $item)
        {
            $data = $item;
            $data['quantity'] = 1;
          
            $result->push(
                $data
            );
        }

        return $result;

    }

    private function formataRangeDate(Request $request): array
    {
        
        $dateFormated = array();
        $rangedateArray = explode(' - ', $request->range_date);

        $rangedateArray = array_map(function($value){
            
            return date('Y-m-d',strtotime(str_replace('/','-',$value)));
        }, $rangedateArray);



        $dateFormated = [$rangedateArray[0]." 00:00:00",
                         $rangedateArray[1]." 23:59:59"];

        return $dateFormated;
    }
}
