<?php

namespace Modules\SaleRent\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;

use Illuminate\View\View;
use Modules\Financial\Events\CreateFatura;
use Modules\SaleRent\Models\Invoice as ModelsInvoice;
use Octoplus\Domain\Category\Models\Category;
use Octoplus\Domain\Currency\Tax;
use Octoplus\Domain\Item\Models\Item;

class Invoice extends Controller
{
    public function __construct()
    {
        $this->middleware('can:sale-rent.sale-invoices_create')->only(['create','store']);
        $this->middleware('can:sale-rent.sale-invoices_update')->only(['edit','update']);
        $this->middleware('can:sale-rent.sale-invoices_show')->only(['index']);
        $this->middleware('can:sale-rent.sale-invoices_delete')->only(['delete']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(): View
    {
        $invoices = ModelsInvoice::where('invoice_type','sale')->paginate();
       
        return view('sale-rent::pedidos.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(): View
    {
       
        $categories = Category::where([['type','income'],['enabled',true]])->orderBy('name')->pluck('name','id');
      
        #teste
        return view('sale-rent::pedidos.create', compact('categories'));
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $this->getInvoiceArray($request);
        $data['code'] = settings('prefix.sales','PV'). str_pad(settings('sequence.sales',1) , 5 , '0' , STR_PAD_LEFT);
        //Cadastra o Pedido
        $invoice = ModelsInvoice::create($data);
        $items = $this->getItemsArray($request);
        $this->setItemStorage($items);
        //Inclui os itens no pedido
        $invoice->invoice_items()->attach($items);

        //Inclui as categorias no pedido
        $invoice->categories()->attach($request->categories);

        if($invoice){
             //incrementa a sequencia do código
             settings(['sequence.sales' => settings('sequence.sales',1) +1])->save();
        }

        event(new CreateFatura($data, $invoice));

        sendNotify(trans('sale-rent::warnings.invoice.success'), 'success');


        return redirect()->route('sale-rent.invoices.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request): View
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        $categories = Category::where([['type','income'],['enabled',true]])->orderBy('name')->pluck('name','id');
        $taxes = Tax::all();

        return view('sale-rent::pedidos.show', compact('invoice','categories','taxes'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request): View
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        $categories = Category::where([['type','income'],['enabled',true]])->orderBy('name')->pluck('name','id');
        $taxes = Tax::all();

        return view('sale-rent::pedidos.edit', compact('invoice','categories','taxes'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request): RedirectResponse
    {
        $invoice = ModelsInvoice::find($request->invoice_id);
        $invoice->update($this->getInvoiceArray($request));

         //Inclui as categorias no pedido
         $invoice->categories()->sync($request->categories);

         sendNotify(trans('sale-rent::warnings.invoice.updated'), 'success');

         return redirect()->route('sale-rent.invoices.edit',$invoice->id);
    }

    public function delete(Request $request): RedirectResponse
    {
        $invoice = ModelsInvoice::find($request->invoice_id);

        $invoice->delete();

        sendNotify(trans('sale-rent::warnings.invoice.deleted'),'success');

        return redirect()->route('sale-rent.invoices.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */

    public function destroy($id)
    {
        //para excluir mutiplos quando o bulkaction estiver implementado
    }


    private function getInvoiceArray(Request $request): array
    {
      
        $data = $request->all();
        $data['sale_date'] = ($data['sale_date'] !== null ? date('Y-m-d', strtotime(str_replace('/','-',$data['sale_date']))) : date('Y-m-d'));
        
        return $data;
    }

    private function setItemStorage(Collection $items): void
    {
        foreach($items as $item)
        {
        
            $modelItem = Item::find($item['item_id']);
            $modelItem->update(['storage' => $modelItem->storage - $item['quantity']]);
            
        }
    }

    private function getItemsArray(Request $request): Collection
    {
        $items = $request->all()['item'];
        $result = collect();

        foreach($items as $item)
        {
            $data = $item;
          
            $result->push(
                $data
            );
        }

        return $result;


    }
}
