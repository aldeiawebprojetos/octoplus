<?php

namespace Modules\SaleRent\Database\Seeds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Octoplus\Domain\Module\Models\Module;

class SaleRentDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Module::create(['alias' => 'sale-rent','status' => true]);
        $this->call(Permissions::class);

        Model::reguard();
    }
}
