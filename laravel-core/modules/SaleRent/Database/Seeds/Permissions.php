<?php

namespace Modules\SaleRent\Database\Seeds;

use Octoplus\Domain\Users\Models\Permission;
use Illuminate\Database\Seeder;

class Permissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'sale-rent.sale-invoices_show'
            ],
            [
                'name' => 'sale-rent.sale-invoices_create'
            ],
            [
                'name' => 'sale-rent.sale-invoices_update'
            ],
            [
                'name' => 'sale-rent.sale-invoices_delete'
            ],

            
            [
                'name' => 'sale-rent.rent-invoices_show'
            ],
            [
                'name' => 'sale-rent.rent-invoices_create'
            ],
            [
                'name' => 'sale-rent.rent-invoices_update'
            ],
            [
                'name' => 'sale-rent.rent-invoices_delete'
            ],

            [
                'name' => 'sale-rent.purchase-invoices_show'
            ],
            [
                'name' => 'sale-rent.purchase-invoices_create'
            ],
            [
                'name' => 'sale-rent.purchase-invoices_update'
            ],
            [
                'name' => 'sale-rent.purchase-invoices_delete'
            ],
        ];

        foreach($permissions as $permission)
        {
            Permission::create($permission);
        }
       
    }
}
