<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id');
            $table->text('description')->nullable();
            $table->string('invoice_type');
            $table->double('amount', 15, 4);
            $table->integer('parent_id')->default(0);
            $table->integer('discount_rate')->nullable()->default(0);
            $table->date('sale_date')->nullable();

                /** informações do comprador */
            $table->integer('customer_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('customer_tax_number')->nullable();
            $table->string('customer_phone')->nullable();
            
                /** informações de entrega */
            $table->string('shipping_address')->nullable();
            $table->string('shipping_value')->nullable();
            $table->text('address_complete')->nullable();

                /** informações de pagamento */
            $table->string('payment_conditions')->nullable();
            $table->string('payment_frequency')->nullable();
            $table->string('payment_day')->nullable();
            $table->string('payment_quantity')->nullable();
            $table->string('quantity_parts')->nullable();

            $table->timestamps();
            $table->softDeletes();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
