<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function(Blueprint $table){
            $table->string('code')->nullable();
            $table->boolean('shipping_delivery')->nullable()->default(false);
            $table->boolean('shipping_withdrawal')->nullable()->default(false);
            $table->boolean('shippig_invoice')->nullable()->default(false);
            $table->integer('extra_days')->nullable();    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
