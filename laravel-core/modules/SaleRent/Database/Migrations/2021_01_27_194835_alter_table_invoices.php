<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function(Blueprint $table){
           
            $table->string('shipping_status',11)->nullable()->default('waiting');
            $table->string('payment_method')->nullable()->default('money');
            $table->string('rent_endDate')->nullable();
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function($table) {
            $table->dropColumn('shipping_status');
            $table->dropColumn('payment_method');
            $table->dropColumn('rent_endDate');
        });
    }
}
