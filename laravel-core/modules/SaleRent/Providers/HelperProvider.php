<?php

namespace Modules\SaleRent\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;

class HelperProvider extends ServiceProvider
{
    protected $helpers = [
        'service'
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->helpers as $helper) {
            $helper_path = module_path('sale-rent') . '/Helpers/' . $helper . '.php';
           
            if (File::isFile($helper_path)) {
                require_once $helper_path;
            }
        }
        
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
