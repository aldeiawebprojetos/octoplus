<?php

namespace Octoplus\Listeners\Menu;

use Octoplus\Events\Menu\ModuleSettingsCreated as Event;

class AddSettingsItems
{
    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $menu = $event->menu;

           if(auth()->user()->hasRole('admin'))
           {
             $menu->route('sale-rent.settings.edit',trans('sale-rent::general.invoice_settings'), [], 0, ['icon' => 'fas fa-tools']);  
           }

          
       
    }
       
    
}
