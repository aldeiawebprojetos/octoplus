<?php

namespace Modules\SaleRent\Listeners;


use Octoplus\Events\Menu\AdminCreated as Event;

class AddToAdminMenu
{
    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
       
        $menu = $event->menu;
       
       if(module_enabled('sale-rent'))
        {
            
         if(auth()->user()->can('octoplus.items_show') || auth()->user()->can('octoplus.categories_show')){        
            $menu->dropdown(trans('general.items'), function($sub)
            {
                if(auth()->user()->can('octoplus.items_show')){
                $sub->route('tenant.items.index', trans('general.items'), [], 1,['icon'=>'entypo-flow-line']);
                }
                if(auth()->user()->can('octoplus.categories_show')){
                    
                $sub->route('tenant.items.categories.index', trans("general.categories"),[],2,['icon'=>'entypo-flow-tree']);
                }
            },[
                'icon'=>'fas fa-barcode'
            ]);
        }
        if(settings('sale.module', false)){
            if(auth()->user()->can('sale-rent.sale-invoices_show') || auth()->user()->can('sale-rent.sale-invoices_create'))
            {
                $menu->dropdown(trans('sale-rent::general.sales'),function($sub){
                    if(auth()->user()->can('sale-rent.sale-invoices_show')){
                        $sub->route('sale-rent.invoices.index', trans('sale-rent::general.invoices'), [],3,['icon' => 'entypo-doc-text-inv']);
                    }

                    if(auth()->user()->can('sale-rent.sale-invoices_create')){
                        $sub->route('sale-rent.invoices.create', trans_choice('general.add',2,['item' => trans('sale-rent::general.invoices')]), [],3,['icon' => 'entypo-plus']);
                    }

                },[
                    'icon'=>'fas fa-rocket'  
                ]);
            }
        }

        if(settings('purchase.module', false))
        {
            if(auth()->user()->can('sale-rent.purchase-invoices_show') || auth()->user()->can('sale-rent.purchase-invoices_create'))
            {
                $menu->dropdown(trans('sale-rent::general.purchase'),function($sub)
                {
                    if(auth()->user()->can('sale-rent.purchase-invoices_show') ){
                        $sub->route('sale-rent.invoices.purchase.index', trans('sale-rent::general.purchase'), [],3,['icon' => 'entypo-doc-text-inv']);
                    }

                    if(auth()->user()->can('sale-rent.purchase-invoices_create') ){
                        $sub->route('sale-rent.invoices.purchase.create', trans_choice('general.add',2,['item' => trans('sale-rent::general.purchase')]), [],3,['icon' => 'entypo-plus']);
                    }
                    
                },[
                    'icon'=>'fas fa-shopping-cart'  
                ]);
            }
        }

        if(settings('rent.module', false)){
            if(auth()->user()->can('sale-rent.rent-invoices_show') || auth()->user()->can('sale-rent.rent-invoices_create'))
            {
                $menu->dropdown(trans('sale-rent::general.rent'),function($sub){
                    if(auth()->user()->can('sale-rent.rent-invoices_show')){
                    $sub->route('sale-rent.invoices.rent.index', trans('sale-rent::general.invoices'), [],3,['icon' => 'entypo-doc-text-inv']);
                    }

                    if(auth()->user()->can('sale-rent.rent-invoices_create')){
                        $sub->route('sale-rent.invoices.rent.create', trans_choice('general.add',2,['item' => trans('sale-rent::general.invoices')]), [],3,['icon' => 'entypo-plus']);
                    }
                    
                    if(auth()->user()->can('octoplus.items_show')){
                        $sub->route('sale-rent.items.rent', trans('sale-rent::general.items_rent'), [], 4,['icon'=>'fas fa-barcode'] );
                    }
                },[
                    'icon'=>'fas fa-sync-alt'  
                ]);
            }
        }

       }

    }
}