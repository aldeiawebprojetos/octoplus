<?php

namespace Modules\AdminControl\Listeners;

use Octoplus\Events\Module\Installed as Event;
use Octoplus\Traits\Permissions;
use Artisan;

class FinishInstallation
{
    use Permissions;

    public $alias = 'sale-rent';

    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        if ($event->alias != $this->alias) {
            return;
        }

        $this->updatePermissions();

        //$this->callSeeds();
    }

    protected function updatePermissions()
    {
        $this->attachPermissionsByRoleNames([
            'admin' => [$this->alias . '-main' => 'c,r,u,d'],
        ]);

        $this->attachPermissionsByRoleNames([
            'admin' => [$this->alias . '-plans' => 'c,r,u,d'],
        ]);
    }

    protected function callSeeds()
    {
        Artisan::call('company:seed', [
            'company' => session('company_id'),
            '--class' => 'Modules\SaleRent\Database\Seeds\AdminControlDatabaseSeeder',
        ]);
    }
}