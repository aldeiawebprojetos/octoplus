<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' =>  ['web','tenant','admin','bindings'], 'prefix'=>'sale-rent', 'as'=>'sale-rent.','namespace'=>'Modules\SaleRent\Http\Controllers'], function () {
    Route::group(['as' => 'invoices.','prefix'=>'invoices'], function(){
        
        Route::group(['prefix' => 'sale'], function () {
            Route::get('/', 'Invoice@index')->name('index');
            Route::get('/create', 'Invoice@create')->name('create');
            Route::post('/store', 'Invoice@store')->name('store');
            Route::get('{invoice_id}/edit', 'Invoice@edit')->name('edit');
            Route::get('{invoice_id}/show', 'Invoice@show')->name('show');
            Route::put('{invoice_id}/update', 'Invoice@update')->name('update');
            Route::delete('{invoice_id}/delete', 'Invoice@delete')->name('delete');
        });

        Route::group(['prefix' => 'rent', 'as'=>'rent.'], function () {
            Route::get('/', 'Rent@index')->name('index');
            Route::get('/create', 'Rent@create')->name('create');
            Route::post('/store', 'Rent@store')->name('store');
            Route::get('{invoice_id}/edit', 'Rent@edit')->name('edit');
            Route::get('{invoice_id}/show', 'Rent@show')->name('show');
            Route::put('{invoice_id}/update', 'Rent@update')->name('update');
            Route::post('{invoice_id}/close', 'Rent@close')->name('close');
            Route::delete('{invoice_id}/delete', 'Rent@delete')->name('delete');

            Route::get('{invoice_id}/contract', 'Rent@contrato')->name('contract'); 
            //Route::get('{invoice_id}/contract/pdf', 'Rent@contractPDF')->name('contract.pdf'); 
        });

        Route::group(['prefix' => 'purchase', 'as'=>'purchase.'], function () {
            Route::get('/', 'Purchasing@index')->name('index');
            Route::get('/create', 'Purchasing@create')->name('create');
            Route::post('/store', 'Purchasing@store')->name('store');
            Route::get('{invoice_id}/edit', 'Purchasing@edit')->name('edit');
            Route::get('{invoice_id}/show', 'Purchasing@show')->name('show');
            Route::put('{invoice_id}/update', 'Purchasing@update')->name('update');
            Route::delete('{invoice_id}/delete', 'Purchasing@delete')->name('delete');

        });
    });
    Route::group(['prefix' => 'settings', 'as' => 'settings.'], function(){
            Route::get('/', 'Settings@edit')->name('edit');
            Route::post('/update', 'Settings@update')->name('update');
    });

});

Route::group(['middleware' =>  ['web','tenant','admin','bindings'],'namespace'=>'Modules\SaleRent\Http\Controllers'], function () {
    Route::get('items/rent','Rent@rentItems')->name('sale-rent.items.rent');
    Route::post('items/changeStatus/{pedido_id}/{item_id}/{status}', 'Rent@changeStatus')->name('sale-rent.items.changestatus');
});