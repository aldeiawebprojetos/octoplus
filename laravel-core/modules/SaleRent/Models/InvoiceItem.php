<?php

namespace Modules\SaleRent\Models;

use Illuminate\Database\Eloquent\Model;
use Octoplus\Domain\Currency\Tax;
use Octoplus\Domain\Item\Models\Item;

class InvoiceItem extends Model
{
    protected $fillable = [];

    public function getAliquotaAttribute()
    {
        return $this->tax->rate ?? 0;
    }

    public function GetCategoriesAttribute()
    {
        return Item::find($this->item_id)->categories;
    }

    public function tax()
    {
        return $this->belongsTo(Tax::class);
    }
}
