<?php

namespace Modules\SaleRent\Models;

use Octoplus\Domain\Category\Models\Category;
use Spatie\Activitylog\Traits\LogsActivity;
use Octoplus\App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Customers\Models\Customer;
use Octoplus\Domain\Item\Models\Item;
use Modules\Financial\Models\Fatura;

class Invoice extends Model
{
    use ForTenants,
    LogsActivity, SoftDeletes;


    protected $fillable = [
        'company_id',
        'description',
        'invoice_type',
        'sale_date',
        'amount',
        'code',
        'id',
        'parent_id',
        'customer_id',
        'customer_name',
        'customer_email',
        'customer_tax_number',
        'customer_phone',
        'shipping_address',
        'address_complete',
        'shipping_value',
        'discount_rate',
        'payment_method',
        'address_complete',
        'payment_conditions',
        'payment_frequency',
        'payment_day',
        'shipping_delivery',
        'shipping_withdrawal',
        'shippig_invoice',
        'extra_days',
        'shipping_status',
        'payment_quantity',
        'rent_endDate',
        'quantity_parts',
    ];

    protected $dates = [
        'sale_date','rent_endDate'
    ];

  

    protected static $logFillable = true;
    protected static $logName = 'sale-rent::general.invoice';
    

    public function categories()
    {
        return $this->belongsToMany(Category::class,'invoices_categories');
    }

    public function faturas()
    {
        return $this->belongsToMany(Fatura::class,'invoice_faturas');
    }

    public function invoice_items()
    {
        return $this->belongsToMany(Item::class,'invoice_items');
    }
    
    public function items()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
