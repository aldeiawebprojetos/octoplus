<?php
return [
    'invoice'                       => [
        'success'                   => 'Pedido Cadastrado',
        'error'                     => 'Pedido não Cadastrado',
        'updated'                   => 'Pedido Atualizado',
        'deleted'                   => 'Pedido Apagado',
    ],
    'settings'                       => [
        'success'                   => 'Configurações Cadastradas',
        'error'                     => 'Configurações não Cadastradas',
        'updated'                   => 'Configurações Atualizadas',
        'deleted'                   => 'Configurações Apagadas',
    ],
];