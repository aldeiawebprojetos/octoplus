<?php

return [
    'sale-invoices'                               => 'Pedidos de Venda',
    'rent-invoices'                               => 'Pedidos de Aluguel',
    'create'                                      => 'Cadastrar',
    'update'                                      => 'Atualizar',
    'show'                                        => 'Visualizar',
    'delete'                                      => 'Apagar',
    'purchase-invoices'                           => 'Pedidos de Compra',
];