import { param } from 'jquery';
import Form from './plugins/form';

var vm = new Vue({
    el: '#app',
    data: function(){
        return {
            form: new Form('invoice'),
            totals: {
                sub: 0,
                item_discount: '',
                discount: 0,
                discount_text: false,
                tax: 0,
                total: 0
            },
            parts: [],
            transaction: [],
            discount: 0,
            taxes: [],
            edit: {},
            items_bakcup: [],
            colspan: 6
           
        };
    },
   async mounted() {
    
        if ((document.getElementById('items') != null) && (document.getElementById('items').rows)) {
            this.colspan = document.getElementById("items").rows[0].cells.length - 1;
        }

        this.items_bakcup =this.form.item_backup;
        this.form.items = [];
        this.form.quantity_parts = 1;
        this.form.extra_days = 0;
       
        if(typeof invoice_data === 'undefined')
        {
            //this.form.payment_conditions = "monthly";
            this.edit.status = false;
            
            this.onAddItem();
        }else{
           for(var row in invoice_data)
           {
                if(row == 'sale_date')
                {
                    const data_sale = Date.UTC(this.form.sale_date);
                    this.form.sale_date = this.formatDate(data_sale,'yyyy-mm-dd');
                }else if(row == 'shipping_value'){
                    this.form.shipping_value = parseFloat(invoice_data['shipping_value']);
                }else{
                    this.form[row] = invoice_data[row];
                }
               
           }
            
            this.taxes = taxes_edit;
            this.edit.status = true;
            this.onSelectConditions();
            this.onSelectFrete();
            this.onAddDiscount();

            if(typeof invoice_parts !== 'undefined')
            {
                this.prepareFaturas();
            }
            
        }
        this.form.old_quantity_parts = this.form.quantity_parts;
        this.form.customer_adress_formated = null;
        if (typeof invoice_items !== 'undefined' && invoice_items) {
            await sleep(300);
            this.form.items = invoice_items;
            
        }
        this.form.old_rent_endDate = this.form.rent_endDate;
        this.form.old_extra_days = this.form.extra_days;
    },

    methods:{
        log: function(msg) {
            console.log(msg);
        },
        onAddItem() {
            this.form.items.push(Object.assign({}, this.items_bakcup[0]));           
        },
        getShowLink(){
            const link = document.getElementById('showLink');

            link.removeAttribute('href');
            link.setAttribute('href', this.form.customer_route);

            link.classList.remove('hidden');


        },
        hideShowLink()
        {
            const link = document.getElementById('showLink');
            link.removeAttribute('href');
            link.classList.add('hidden');
        },
        prepareFaturas()
        {
            for (var row in invoice_parts)
            {
               

                let data = new Date(invoice_parts[row]['data_vencimento']);
                let data_competencia = this.formatDate(new Date(invoice_parts[row].data_competencia),'yyyy-mm-dd');

                invoice_parts[row]['class'] = (invoice_parts[row]['status'] == 'canceled' ? 'primary' : (invoice_parts[row]['status'] == 'settled' ? 'success': (invoice_parts[row]['vencendo'] ? 'warning' : (invoice_parts[row]['vencido'] ? 'danger' : 'default')))) ;
                invoice_parts[row]['name'] = translate_data.part + ' ' + (invoice_parts[row].code.indexOf(translate_data.shipping) > -1 ? translate_data.shipping : (parseInt(row)+1) +'/'+ this.form.quantity_parts);
                invoice_parts[row]['block_frete'] = (invoice_parts[row].code.indexOf(translate_data.shipping) > -1 || data_competencia < '2021-02-02');
                invoice_parts[row]['data_vencimento'] = this.formatDate(data,'yyyy-mm-dd');

                if(typeof this.form.default_url != 'undefined')
                {
                    invoice_parts[row]['link'] = this.form.default_url.replace('#row_id#',  invoice_parts[row]['id']);
                }

            }

            this.parts = invoice_parts;
        },
        onSelectFrete(){
          
            let freteDiv = document.getElementById('freteDetails');
            let arrayAddress = this.form.customer_address;

            if(typeof arrayAddress !== 'undefined')
            {
                if(this.form.shipping_address === "delivery_home")
                {
                    this.form.address_complete = arrayAddress.address + (arrayAddress.complement ? ', ' + arrayAddress.complement + ', ' : ', ') + arrayAddress.neighborhood + ', ' + arrayAddress.city + '-' + arrayAddress.uf + ', CEP: ' + arrayAddress.cep;
                }else{
                    this.form.address_complete = '';
                }
            }
           

        },
        onSelectConditions(){
          
            let paymentDetails = document.getElementById("paymentPartDetails");
            

            if(this.form.payment_conditions === 'in_cash')
            {
                paymentDetails.classList.remove('hidden');
            }else{
              
                paymentDetails.classList.remove('hidden'); 
               
            }

            this.onPartsQuantity()
          
        },
    //    async  onAddPart(){
    //         this.parts.push({
    //             type: 'part',               
    //             date:  this.formatDate(new Date(),'yyyy-mm-dd') ,
    //             value: 0.00,
    //         });
    //         await sleep(50);
    //         for(var row in this.parts)
    //         {
    //             if(typeof this.parts[row].id == "undefined")
    //             {                    
    //                 const data_input = document.getElementById(`date_${row}`),
    //                 value_input = document.getElementById(`value_${row}`),
    //                 code_input = document.getElementById(`code_${row}`);

    //                 data_input.removeAttribute('readonly');
    //                 value_input.removeAttribute('readonly');
    //                 code_input.classList.remove('col-md-2');
    //                 code_input.classList.add('col-md-3');

    //             }
    //         }
    //     },
       onRemovePart(index){
         this.parts.splice(index,1);
       },
       onPartsShipping: function () {
        var shipping_value = document.getElementById("form.shipping_value").value;

        if (this.form.shippig_invoice == true ) 
        {
            if( typeof this.form.shipping_delivery[0] != "undefined" ) 
            {
                var shipping_part_01 = document.getElementById("value_1");  
                
                if( shipping_part_01 != null )
                    shipping_part_01.value = shipping_value != null ? shipping_value : 0;

            }

            if( typeof this.form.shipping_withdrawal[0] != "undefined"  )
            {	
                var shipping_part_02 = document.getElementById("value_2");

                if( shipping_part_02 != null )
                    shipping_part_02.value = shipping_value != null ? shipping_value : 0;
            }
        }
      },
      async  onPartsQuantity()
        {
            this.setFreteAmount();

           if(this.edit.status){

               
               if(this.form.old_quantity_parts < this.form.quantity_parts)
               {
                  
                for(var index in this.parts)
                {
                    if(typeof this.parts[index].id == 'undefined')
                    {
                        this.parts.splice(index);
                    }
                }
               
                let data_vencimento = new Date(this.form.old_rent_endDate);
                
                for(i=0; i < this.form.quantity_parts-this.form.old_quantity_parts; i++)
                {
                    this.setFrequencyPayment(data_vencimento);
                    this.parts.push({
                        type: 'part',
                        data_vencimento:  this.formatDate(data_vencimento,'yyyy-mm-dd'),
                        value: this.totals.total,
                    });

                   
    
                }
                this.form.rent_endDate = this.formatDate(data_vencimento,'yyyy-mm-dd');
                for(var row in this.parts)
                {
                    if(typeof this.parts[row].id == 'undefined')
                    {
                        await sleep(50);
                        const data_input = document.getElementById(`date_${row}`),
                        value_input = document.getElementById(`value_${row}`);

                        data_input.removeAttribute('readonly');
                        value_input.removeAttribute('readonly');
                    }
                }

               }else if(this.form.old_extra_days < this.form.extra_days)
               {
                for(var index in this.parts)
                {
                    if(typeof this.parts[index].id == 'undefined')
                    {
                        this.parts.splice(index);
                    }
                }
                const extra_daysValue = (this.totals.total/30) * this.form.extra_days, data_vencimento = new Date(this.form.rent_endDate);
                
                data_vencimento.setDate(data_vencimento.getDate() + parseInt(this.form.extra_days));
                                
                this.parts.push({
                    type: 'part',
                    data_vencimento:  this.formatDate(data_vencimento,'yyyy-mm-dd'),
                    value: extra_daysValue,
                });

                for(var row in this.parts)
                {
                    if(typeof this.parts[row].id == 'undefined')
                    {
                        await sleep(50);
                        const data_input = document.getElementById(`date_${row}`),
                        value_input = document.getElementById(`value_${row}`);

                        data_input.removeAttribute('readonly');
                        value_input.removeAttribute('readonly');
                    }
                }
                this.form.rent_endDate = this.formatDate(data_vencimento,'yyyy-mm-dd');
               }else{
                    for(var index in this.parts)
                    {
                        if(typeof this.parts[index].id == 'undefined')
                        {
                            this.parts.splice(index);
                        }
                    }
               }

               let block_delivery = {}, block_withdrawal = {};
            let data = new Date(new Date().getFullYear() , new Date().getMonth() , (this.form.payment_day !== '' ? this.form.payment_day : new Date().getDate()) );
                  
                        
            for(var index in this.parts)
            {
                if(this.parts[index].edit == 'delivery')
                {
                    this.parts.splice(index,1);
                }
                if(typeof this.parts[index] != 'undefined' && this.parts[index].block_frete)
                {
                    block_delivery = {
                        status: true,
                        id: this.parts[index].id,
                    };
                }else{
                    if(typeof block_delivery.id == 'undefined')
                    {
                        block_delivery = {
                            status: false,
                            id: this.parts[index].id,
                        };
                    }
                }

            }
           
            for(var index in this.parts)
            {

            
                if(this.parts[index].edit == 'withdrawal')
                {
                    this.parts.splice(index,1);
                }

                if(typeof this.parts[index] != 'undefined' && this.parts[index].block_frete)
                {
                   if(block_delivery.id != this.parts[index].id)
                   {
                        block_withdrawal = {
                        status: true,
                        id: this.parts[index].id,
                    };
                   }else{
                       if(typeof block_withdrawal.id == 'undefined')
                       {
                            block_withdrawal = {
                                status: false,
                                id: this.parts[index].id,
                            };
                       }
                   }
                }else{
                       if(typeof block_withdrawal.id == 'undefined')
                       {
                            block_withdrawal = {
                                status: false,
                                id: this.parts[index].id,
                            };
                       }
                }

            }

           
            if(this.form.shipping_delivery && !block_delivery.status)
            {

                this.parts.push({
                    type: 'shipping',
                    edit: 'delivery',
                    name: translate_data.shipping,
                    date:  this.formatDate(data,'yyyy-mm-dd') ,
                    value: this.form.shipping_value,
                });
                await sleep(300);
                for(var index in this.parts)
                {
                    if(this.parts[index].edit == 'delivery')
                    {
                        const data_input = document.getElementById(`date_${index}`),
                            value_input = document.getElementById(`value_${index}`);

                            data_input.removeAttribute('readonly');
                            value_input.removeAttribute('readonly');
 
                    }
                }
            }

            if(this.form.shipping_withdrawal && !block_withdrawal.status)
            {
               
                this.parts.push({
                    type: 'shipping',
                    edit: 'withdrawal',
                   
                    name: translate_data.shipping,
                    date:   this.formatDate(data,'yyyy-mm-dd') ,
                    value: this.form.shipping_value,
                });
                await sleep(300);
                for(var index in this.parts)
                {
                    if(this.parts[index].edit == 'withdrawal')
                    {
                        const data_input = document.getElementById(`date_${index}`),
                            value_input = document.getElementById(`value_${index}`);

                            data_input.removeAttribute('readonly');
                            value_input.removeAttribute('readonly');
                            
                           
                       
                    }
                }

            }


           }else{
            
            this.parts = [];

            let paramDate;
            if (this.form.sale_date && this.form.sale_date.length === 10) {
                paramDate = new Date(this.form.sale_date.substring(0, 4), parseInt(this.form.sale_date.substring(5, 7)) - 1, this.form.sale_date.substring(8, 10));
            } else {
                paramDate = new Date();
            }
            
            let data = new Date(paramDate.getFullYear() , paramDate.getMonth() , (this.form.payment_day !== '' ? this.form.payment_day : paramDate.getDate()) );
            const first_data = new Date(data.getTime()),
            frete_value = (typeof this.form.shippig_invoice == 'undefined' ? (!this.form.shipping_entrega_retirada ? ( typeof this.form.shipping_delivery[0] != "undefined" || typeof this.form.shipping_withdrawal[0] != "undefined"  ? this.form.shipping_value : 0) : this.form.shipping_value * 2) / (this.form.quantity_parts > 0 ? this.form.quantity_parts : 1) : 0);
            var i; 

                const extra_daysValue = (this.totals.total/30) * this.form.extra_days;
              
           
           this.form.amount = (!this.form.shipping_entrega_retirada ? ( typeof this.form.shipping_delivery[0] != "undefined" || typeof this.form.shipping_withdrawal[0] != "undefined"  ? this.form.shipping_value : 0) : this.form.shipping_value * 2) + (this.totals.total*this.form.quantity_parts);
            
            if(this.form.payment_conditions == 'in_cash'){
                this.parts.push({
                    type: 'part',
                    date:    this.formatDate(data,'yyyy-mm-dd'),
                    value: (this.totals.total*this.form.quantity_parts) + frete_value + parseFloat(extra_daysValue.toFixed(2)),
                });
            }else{
            
            for(i=0; i < this.form.quantity_parts; i++)
            {
               
                
               this.setFrequencyPayment(data);
               
                this.parts.push({
                    type: 'part',
                    date:    this.formatDate(data,'yyyy-mm-dd'),
                    value: this.totals.total + frete_value + extra_daysValue,
                });

            }

        }
            let endDate = data;
            endDate.setDate(endDate.getDate() + parseInt(this.form.extra_days));
            this.form.rent_endDate = this.formatDate(endDate,'yyyy-mm-dd');

            if(typeof this.form.shippig_invoice !== 'undefined')
            {
                if(typeof this.form.shipping_delivery[0] !== 'undefined')
                {
                    this.parts.push({
                        type: 'shipping',
                        date:  this.formatDate(first_data,'yyyy-mm-dd') ,
                        value: this.form.shipping_value,
                    });
                } 

                if(typeof this.form.shipping_withdrawal[0] !== 'undefined')
                {
                    this.parts.push({
                        type: 'shipping',
                        date:   this.formatDate(first_data,'yyyy-mm-dd') ,
                        value: this.form.shipping_value,
                    });
                } 
            }

           }   
           const div_paymentDetails = document.getElementById('paymentPartDetails');

           if(this.form.payment_conditions == 'in_cash')
            {
                div_paymentDetails.classList.add('hidden');
            }else{
                div_paymentDetails.classList.remove('hidden');
            }
        },

        setFreteAmount(){
            
            if(typeof this.form.shipping_delivery[0] !== 'undefined' && typeof this.form.shipping_withdrawal[0] !== 'undefined'){
                this.form.shipping_entrega_retirada = true;
              
            }else{
                this.form.shipping_entrega_retirada = false;
            }

        },
        setFrequencyPayment(data){
            const vencimentos = {
                daily: () => { data.setDate(data.getDate() +1) },
                weekly: () => { data.setDate(data.getDate() +7) },
                monthly: () => { data.setMonth(data.getMonth() + 1) },
                bimonthly: () => { data.setMonth(data.getMonth() + 2) },
                quarterly: () => { data.setMonth(data.getMonth() + 3) },
                semiannual: () => { data.setMonth(data.getMonth() + 6) },
                yearly: () => { data.setFullYear(data.getFullYear() + 1) },
            };

            vencimentos[this.form.payment_conditions]();
        },
        
        formatDate(date, formato = 'dd/mm/yyyy'){
            return formato.replace('dd', ('0' + date.getDate()).slice(-2)).replace('mm', ('0' + (date.getMonth() +1)).slice(-2)).replace('yyyy', date.getFullYear() );
        },
        onAddDiscount() {
            let discount = document.getElementById('discount_rate').value;

            if (discount < 0) {
                discount = 0;
            } else if (discount > 100) {
                discount = 100;
            }

            document.getElementById('discount_rate').value = discount;

            this.form.discount = discount;
            this.discount = false;

            $('#desconto').modal('hide');
            this.onCalculateTotal();
        },
        onDeleteItem(index) {
            this.form.items.splice(index, 1);
        },
        onCalculateTotal(){
            let sub_total = 0;
            let discount_total = 0;
            let line_item_discount_total = 0;
            let tax_total = 0;
            let grand_total = 0;
            let items = this.form.items;
            let discount_in_totals = this.form.discount;
            let old_total = this.totals.total;

            if (items.length) {
                let index = 0;

                // get all items.
                for (index = 0; index < items.length; index++) {
                    let discount = 0;
                    // get row item and set item variable.
                    let item = items[index];

                    // item sub total calcute.
                    let item_total = item.price * item.quantity;

                    // item discount calculate.
                    let line_discount_amount = 0;

                    if (item.discount) {
                        line_discount_amount = item_total * (item.discount / 100);

                        item_discounted_total = item_total -= line_discount_amount;
                        discount = item.discount;
                    }

                    let item_discounted_total = item_total;

                    if (discount_in_totals) {
                        item_discounted_total = item_total - (item_total * (discount_in_totals / 100));
                        discount = discount_in_totals;
                    }

                    // item tax calculate.
                    let item_tax_total = 0;

                    if (item.tax_id) {
                        if(this.edit.status && typeof item.tax_id == 'number')
                        {
                            item.tax_id = [item.tax_id];
                        }
                        let inclusives = [];
                        let compounds = [];
                        let index_taxes = 0;
                        let taxes = this.taxes;

                           
                                item.tax_id.forEach(function(item_tax_id) {
                                    for (index_taxes = 0; index_taxes < taxes.length; index_taxes++) {
                                        let tax = taxes[index_taxes];
                                        
                                        if (item_tax_id != tax.id) {
                                            continue;
                                        }
        
                                        switch (tax.type) {
                                            case 'inclusive':
                                                inclusives.push(tax);
                                                break;
                                            case 'compound':
                                                compounds.push(tax);
                                                break;
                                            case 'fixed':
                                                item_tax_total += tax.rate * item.quantity;
                                                break;
                                            case 'withholding':
                                                item_tax_total += 0 - item_discounted_total * (tax.rate / 100);
                                                break;
                                            default:
                                                item_tax_total += item_discounted_total * (tax.rate / 100);
                                                break;
                                        }
                                    }
                                });
                            

                        if (inclusives.length > 0) {
                            let item_sub_and_tax_total = item_discounted_total + item_tax_total;

                            let inclusive_total = 0;

                            inclusives.forEach(function(inclusive) {
                                inclusive_total += inclusive.rate;
                            });

                            let item_base_rate = item_sub_and_tax_total / (1 + inclusive_total / 100);

                            item_tax_total = item_sub_and_tax_total - item_base_rate;

                            item_total = item_base_rate + discount;
                        }

                        if (compounds.length > 0) {
                            compounds.forEach(function(compound) {
                                item_tax_total += ((item_discounted_total + item_tax_total) / 100) * compound.rate;
                            });
                        }
                    }

                    // set item total
                    if (item.discount) {
                        items[index].total = item_discounted_total;
                    } else {
                        items[index].total = item_total;
                    }

                    // calculate sub, tax, discount all items.
                    line_item_discount_total += line_discount_amount;
                    sub_total += item_total;
                    tax_total += item_tax_total;
                }
            }
           
            // set global total variable.
            this.totals.sub = sub_total;
            this.totals.tax = Math.abs(tax_total);
            this.totals.item_discount = line_item_discount_total;

            // Apply discount to total
            if (discount_in_totals) {
                discount_total = sub_total * (discount_in_totals / 100);

                this.totals.discount = discount_total;

                sub_total = sub_total - (sub_total * (discount_in_totals / 100));
            }

            // set all item grand total.
            grand_total = sub_total + tax_total;
               
            this.totals.total = grand_total;
            if(old_total !== this.totals.total)
            {
                this.onPartsQuantity();
            }
        },
        
        onSubmit(){
            const form = document.getElementById('invoice');

           
             form.submit();
        },
    }
});


