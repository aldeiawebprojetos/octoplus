

var vm = new Vue({
    el: '#app',
    methods:{
        changeInvoiceStatus(pedido_id,item_id,status,element_id){
            const url = baseurl + `/items/changeStatus/${pedido_id}/${item_id}/${status}`;
            const element = document.getElementById(element_id),
                    button = document.getElementById(`${element_id}_button`);
           
            axios.post(url).then(({data}) => {
                if(data.success)
                {
                    const item_url = baseurl + `/img/${data.item}.svg`;
                    element.src = item_url;
                    button.classList.add('hidden');
                    sendNotify('success','Sucesso',data.msg);

                }else{
                    sendNotify('warning','Não foi possivel salvar',"Ocorreu um erro");
                }
            });
        },
    },
});