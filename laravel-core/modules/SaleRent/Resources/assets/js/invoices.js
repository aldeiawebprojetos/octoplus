import Form from './plugins/form';

var vm = new Vue({
    el: '#app',
    data: function(){
        return {
            form: new Form('invoice'),
            totals: {
                sub: 0,
                item_discount: '',
                discount: 0,
                discount_text: false,
                tax: 0,
                total: 0
            },
            parts: [],
            transaction: [],
            discount: 0,
            taxes: [],
            edit: {},
            items_bakcup: [],
            colspan: 6
           
        };
    },
   async mounted() {
        if ((document.getElementById('items') != null) && (document.getElementById('items').rows)) {
            this.colspan = document.getElementById("items").rows[0].cells.length - 1;
        }

        this.items_bakcup =this.form.item_backup;
        this.form.items = [];
       
        if(typeof invoice_data === 'undefined')
        {
            ////////////this.form.payment_conditions = "in_cash";
            this.edit.status = false;
            
            this.onAddItem();
        }else{
           for(var row in invoice_data)
           {
                if(row == 'sale_date')
                {
                    const data_sale = Date.UTC(this.form.sale_date);
                    this.form.sale_date = this.formatDate(data_sale,'yyyy-mm-dd');
                }else if(row == 'shipping_value'){
                    this.form.shipping_value = parseFloat(invoice_data['shipping_value']);
                }else{
                    this.form[row] = invoice_data[row];
                }
               
           }
            
            this.taxes = taxes_edit;
            this.edit.status = true;
            this.onSelectConditions();
            this.onSelectFrete();
            this.onAddDiscount();

            if(typeof invoice_parts !== 'undefined')
            {
                this.prepareFaturas();
            }
        }
       
        if (typeof invoice_items !== 'undefined' && invoice_items) {
            await sleep(300);
            this.form.items = invoice_items;
            
        }
    },

    methods:{
        onAddItem() {
            this.form.items.push(Object.assign({}, this.items_bakcup[0]));           
        },
        getShowLink(){
            const link = document.getElementById('showLink');

            link.removeAttribute('href');
            link.setAttribute('href', this.form.customer_route);

            link.classList.remove('hidden');
        },
        hideShowLink()
        {
            const link = document.getElementById('showLink');
            link.removeAttribute('href');
            link.classList.add('hidden');
           
        },
        prepareFaturas()
        {
            for (var row in invoice_parts)
            {
               

                let data = new Date(invoice_parts[row]['data_vencimento']);
                let data_competencia = this.formatDate(new Date(invoice_parts[row].data_competencia),'yyyy-mm-dd');

                invoice_parts[row]['class'] = (invoice_parts[row]['status'] == 'canceled' ? 'primary' : (invoice_parts[row]['status'] == 'settled' ? 'success': (invoice_parts[row]['vencendo'] ? 'warning' : (invoice_parts[row]['vencido'] ? 'danger' : 'default')))) ;
                invoice_parts[row]['name'] = translate_data.part + ' ' + (invoice_parts[row].code.indexOf(translate_data.shipping) > -1 ? translate_data.shipping : (parseInt(row)+1) +'/'+ this.form.quantity_parts);
                invoice_parts[row]['block_frete'] = (invoice_parts[row].code.indexOf(translate_data.shipping) > -1 || data_competencia < '2021-02-02');
                invoice_parts[row]['data_vencimento'] = this.formatDate(data,'yyyy-mm-dd');

                if(typeof this.form.default_url != 'undefined')
                {
                    invoice_parts[row]['link'] = this.form.default_url.replace('#row_id#',  invoice_parts[row]['id']);
                }

            }

            this.parts = invoice_parts;
        },
        onSelectFrete(){
          
            let freteDiv = document.getElementById('freteDetails');
            let arrayAddress = this.form.customer_address;

            if(typeof arrayAddress !== 'undefined')
            {
                if(this.form.shipping_address === "delivery_home")
                {
                    

                    this.form.address_complete = arrayAddress.address + (arrayAddress.complement ? ', ' + arrayAddress.complement + ', ' : ', ') + arrayAddress.neighborhood + ', ' + arrayAddress.city + '-' + arrayAddress.uf + ', CEP: ' + arrayAddress.cep;
                }
            }
           
            if(this.form.shipping_address === 'withdrawal')
            {
                this.form.shipping_value = 0;
                this.onCalculateTotal()
                freteDiv.classList.add('hidden');
            }else{
               
                freteDiv.classList.remove('hidden');
            }
        },
        onSelectConditions(){
            let paymentPart = document.getElementById("paymentPart");
            let paymentRecurrent = document.getElementById("paymentRecurrent");

            if(this.form.payment_conditions === 'part')
            {
                this.form.payment_frequency = '';
                paymentRecurrent.classList.add('hidden');
                paymentPart.classList.remove('hidden');
            }

            if(this.form.payment_conditions === "recurrent")
            {
                this.form.payment_frequency = 'monthly';
                paymentRecurrent.classList.remove('hidden');
                paymentPart.classList.add('hidden');
            }

            if(this.form.payment_conditions === 'in_cash')
            {
                this.form.payment_frequency = '';
                this.form.quantity_parts = 0;
                paymentRecurrent.classList.add('hidden');
                paymentPart.classList.add('hidden');
            }
          
        },
        onPartsQuantity()
        {
           
           if(this.edit.status){

           }else{
            this.parts = [];
            let data = new Date();
            var i, parcela = 0;
            if(this.totals.total > 0)
            {
                parcela = this.totals.total / this.form.quantity_parts;
            }    

            for(i=0; i < this.form.quantity_parts; i++)
            {
               
                
                data.setMonth(data.getMonth() + 1);
                this.parts.push({
                    date:    this.formatDate(data,this.form.date_format) ,
                    value: parcela,
                });
            }
           }   
        },
        formatDate(date, formato = 'dd/mm/yyyy'){
            return formato.replace('dd', ('0' + date.getDate()).slice(-2)).replace('mm', ('0' + (date.getMonth() +1)).slice(-2)).replace('yyyy', date.getFullYear() );
        },
        onAddDiscount() {
            let discount = document.getElementById('discount_rate').value;

            if (discount < 0) {
                discount = 0;
            } else if (discount > 100) {
                discount = 100;
            }

            document.getElementById('discount_rate').value = discount;

            this.form.discount = discount;
            this.discount = false;

            $('#desconto').modal('hide');
            this.onCalculateTotal();
        },
        onDeleteItem(index) {
            this.form.items.splice(index, 1);
        },
        onCalculateTotal(){
            let sub_total = 0;
            let discount_total = 0;
            let line_item_discount_total = 0;
            let tax_total = 0;
            let grand_total = 0;
            let items = this.form.items;
            let discount_in_totals = this.form.discount;
            let old_total = this.totals.total;

            if (items.length) {
                let index = 0;

                // get all items.
                for (index = 0; index < items.length; index++) {
                    let discount = 0;
                    // get row item and set item variable.
                    let item = items[index];

                    // item sub total calcute.
                    let item_total = item.price * item.quantity;

                    // item discount calculate.
                    let line_discount_amount = 0;

                    if (item.discount) {
                        line_discount_amount = item_total * (item.discount / 100);

                        item_discounted_total = item_total -= line_discount_amount;
                        discount = item.discount;
                    }

                    let item_discounted_total = item_total;

                    if (discount_in_totals) {
                        item_discounted_total = item_total - (item_total * (discount_in_totals / 100));
                        discount = discount_in_totals;
                    }

                    // item tax calculate.
                    let item_tax_total = 0;

                    if (item.tax_id) {
                        if(this.edit.status && typeof item.tax_id == 'number')
                        {
                            item.tax_id = [item.tax_id];
                        }
                        let inclusives = [];
                        let compounds = [];
                        let index_taxes = 0;
                        let taxes = this.taxes;

                           
                                item.tax_id.forEach(function(item_tax_id) {
                                    for (index_taxes = 0; index_taxes < taxes.length; index_taxes++) {
                                        let tax = taxes[index_taxes];
                                        
                                        if (item_tax_id != tax.id) {
                                            continue;
                                        }
        
                                        switch (tax.type) {
                                            case 'inclusive':
                                                inclusives.push(tax);
                                                break;
                                            case 'compound':
                                                compounds.push(tax);
                                                break;
                                            case 'fixed':
                                                item_tax_total += tax.rate * item.quantity;
                                                break;
                                            case 'withholding':
                                                item_tax_total += 0 - item_discounted_total * (tax.rate / 100);
                                                break;
                                            default:
                                                item_tax_total += item_discounted_total * (tax.rate / 100);
                                                break;
                                        }
                                    }
                                });
                            

                        if (inclusives.length > 0) {
                            let item_sub_and_tax_total = item_discounted_total + item_tax_total;

                            let inclusive_total = 0;

                            inclusives.forEach(function(inclusive) {
                                inclusive_total += inclusive.rate;
                            });

                            let item_base_rate = item_sub_and_tax_total / (1 + inclusive_total / 100);

                            item_tax_total = item_sub_and_tax_total - item_base_rate;

                            item_total = item_base_rate + discount;
                        }

                        if (compounds.length > 0) {
                            compounds.forEach(function(compound) {
                                item_tax_total += ((item_discounted_total + item_tax_total) / 100) * compound.rate;
                            });
                        }
                    }

                    // set item total
                    if (item.discount) {
                        items[index].total = item_discounted_total;
                    } else {
                        items[index].total = item_total;
                    }

                    // calculate sub, tax, discount all items.
                    line_item_discount_total += line_discount_amount;
                    sub_total += item_total;
                    tax_total += item_tax_total;
                }
            }
           
            // set global total variable.
            this.totals.sub = sub_total;
            this.totals.tax = Math.abs(tax_total);
            this.totals.item_discount = line_item_discount_total;

            // Apply discount to total
            if (discount_in_totals) {
                discount_total = sub_total * (discount_in_totals / 100);

                this.totals.discount = discount_total;

                sub_total = sub_total - (sub_total * (discount_in_totals / 100));
            }

            // set all item grand total.
            grand_total = sub_total + tax_total + this.form.shipping_value;
               
            this.totals.total = grand_total;
            if(old_total !== this.totals.total)
            {
                this.onPartsQuantity();
            }
        },
        
        onSubmit(){
            const form = document.getElementById('invoice');

           
             form.submit();
        },
    }
});


