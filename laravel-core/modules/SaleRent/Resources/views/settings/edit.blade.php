@extends('layouts.app')

@section('breadcrumb')
<li> <a> {{ trans('sale-rent::general.name') }} </a> </li>

<li class="active"> <strong>{{trans('sale-rent::general.invoice_settings')}}</strong> </li>	
@endsection

@section('title', trans('sale-rent::general.invoices'))

@section('content')
{{ Form::open(['method'=>'POST', 'route' => ['sale-rent.settings.update'],'enctype'=>'multipart/form-data','@submit.prevent' => 'onSubmit',]) }}

<div class="row">
			
    <!-- title -->
    <div class="col-md-6">
        <h1 class="pull-left">
            {{trans("sale-rent::general.invoice_settings")}} <i class="fa fa-cogs"></i> 
        </h1>
    </div>
    

    <!-- Tipo Contrato -->
    <div class="col-md-6">
        <h2 class="pull-right"></h2>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p class="col-sm-12">
            {{ trans('sale-rent::general.settings_description') }}
            <br>
            <strong class="text-danger">
                {{ trans('sale-rent::general.tip_settings') }}
            </strong>
            
        </p>
    
    </div>
</div>
@include('sale-rent::settings.form',['form_edit' => true])

<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'sale-rent.invoices.index'])
</div>

{{ Form::close() }}
   
@endsection

@section('modals')
@endsection