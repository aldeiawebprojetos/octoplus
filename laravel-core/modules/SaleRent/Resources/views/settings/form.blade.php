<div class="row">
    <div class="col-md-12">
        
        <div class="panel panel-primary" data-collapsed="0">
        
            <div class="panel-heading">
                <div class="panel-title">
                    Módulo Pedidos: Compra | Venda | Locação
                </div>
                
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                    <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                </div>
            </div>
            
            <div class="panel-body">

                <div class="form-group">
                    {{ Form::label('sale_module', trans('sale-rent::general.module_sale'),['class'=> 'col-sm-5 control-label']) }}
                    
                    <div class="col-sm-5">
                        <div class="anil_nepal">
                              <label class="switch switch-left-right">
                                {{ Form::checkbox('sale[module]', true, settings('sale.module'), ['class' => "switch-input",'id'=> 'sale_module']) }}
                            <span class="switch-label" data-on="Sim" data-off="Não"></span> <span class="switch-handle"></span> </label>
                        </div>
                    </div>
                </div>

                    <!--  Checkbox do módulo de compra desativado até o recurso ser implementado -->
                <div class="form-group">
                    {{ Form::label('purchase_module', trans('sale-rent::general.module_purchase'),['class'=> 'col-sm-5 control-label']) }}
                    
                    <div class="col-sm-5">
                        <div class="anil_nepal">
                              <label class="switch switch-left-right">
                                {{ Form::checkbox('purchase[module]', true, settings('purchase.module'), ['class' => "switch-input", 'id' => 'purchase_module']) }}
                            <span class="switch-label" data-on="Sim" data-off="Não"></span> <span class="switch-handle"></span> </label>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                   {{Form::label('rent_module',trans('sale-rent::general.module_rent'),['class'=> 'col-sm-5 control-label'])}}
                    
                    <div class="col-sm-5">
                        <div class="anil_nepal">
                              <label class="switch switch-left-right">
                            
                            {{ Form::checkbox('rent[module]', true, settings('rent.module'), ['class' => "switch-input", 'data-toggle' => "collapse", 'data-target' => '#collapseExample1', 'aria-expanded' => 'false', 'aria-controls' => 'collapseExample1','id' => 'rent_module']) }}
                            <span class="switch-label" data-on="Sim" data-off="Não"></span> <span class="switch-handle"></span> </label>
                        </div>
                    </div>
                </div>

                <div class="form-group collapse in" data-collapsed="0" id="collapseExample1">
                    {{ Form::label('rent_aviso_vencimento',trans('sale-rent::general.aviso_vencimento'),['class'=> 'col-sm-5 control-label']) }}
                    
                    <div class="col-sm-5">
                        {{ Form::number('rent[aviso_vencimento]',settings('rent.aviso_vencimento', 8),['id'=>'rent_aviso_vencimento']) }}
                       
                        <span class="description ">   
                            {{trans('sale-rent::general.aviso_vencimento_description')}}
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('rent_use_contract', trans('sale-rent::general.rent_contract'),['class'=> 'col-sm-5 control-label']) }}
                   
                    
                    <div class="col-sm-5">
                        <div class="anil_nepal">
                              <label class="switch switch-left-right">
                           
                            {{ Form::checkbox('rent[use_contract]', true, settings('rent.use_contract'), ['class' => "switch-input", 'data-toggle' => "collapse", 'data-target' => '#collapseExample2', 'aria-expanded' => 'false', 'aria-controls' => 'collapseExample2','id' =>'rent_use_contract']) }}
                            <span class="switch-label" data-on="Sim" data-off="Não"></span> <span class="switch-handle"></span> </label>
                        </div>
                    </div>
                </div>
                

                <div class="form-group " data-collapsed="1" id="collapseExample2">
                    <div class="form-group">
                        {{ Form::label('contract_new_number', trans('sale-rent::general.new_number'),['class'=> 'col-sm-5 control-label']) }}
                       
                        
                        <div class="col-sm-5">
                            <div class="anil_nepal">
                                  <label class="switch switch-left-right">
                               
                                {{ Form::checkbox('contract[new_number]', true, settings('contract.new_number'), ['class' => "switch-input"]) }}
                                <span class="switch-label" data-on="Sim" data-off="Não"></span> <span class="switch-handle"></span> </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('contract_category_as_name', trans('sale-rent::general.category_as_name'),['class'=> 'col-sm-5 control-label']) }}
                       
                        
                        <div class="col-sm-5">
                            <div class="anil_nepal">
                                  <label class="switch switch-left-right">
                               
                                {{ Form::checkbox('contract[category_as_name]', true, settings('contract.new_number'), ['class' => "switch-input"]) }}
                                <span class="switch-label" data-on="Sim" data-off="Não"></span> <span class="switch-handle"></span> </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    
                    <div class="col-sm-12">
                        
                        {{ Form::label('contract_rent', trans('sale-rent::general.rent_contract_content'),['class'=> 'control-label']) }}
                        <br>
                        <div class="form-group">
                            {{ Form::textarea('contract[rent]', settings('contract.rent', null),['class' => 'form-control', 'id'=>'contract_rent', ]) }}
                        </div>
                        <span class="description ">   
                        <strong> Variáveis disponíveis: </strong> <code> {nome-responsavel}</code> - <code>{cpf-responsavel}</code> - <code>{data-hoje}</code> - <code>{nome-user}</code> - <code>{cpf-user}</code>
                        <br>
                        </span>
                    </div>
                </div>

            
            </div>
        </div>
    </div>
    
    
    
</div>

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.css')}}">
<script src="{{asset('assets/js/wysihtml5/wysihtml5-0.4.0pre.min.js')}}"></script>
<script src="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>
<script>
    $("#contract_rent").wysihtml5('deepExtend', {
  parserRules: {
    classes: {
      "middle": 1
    },
    tags: {
      strong: {},
      table:{},
      tr: {},
      td:{},
      em: {}
    }
  }
});
</script>
@endsection