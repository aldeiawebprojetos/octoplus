@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('general.items')}}</a> </li>

<li class="active"> <strong>{{trans('general.listing')}}</strong> </li>	
@endsection

@section('title',trans('general.item'))
    

@section('content')
<div class="mail-env">
    <div class="mail-body">
				
        <div class="mail-header">
            <!-- Search  form   -->
            
            <form method="get" role="form" class="search-form-full">
                <div class="form-group">
                    
                    <input type="text" class="form-control input-lg" value="{{ Request::query('s') }}" name="s" id="search-input" placeholder="Pesquise pelo Nome, código..." />
                    <i class="entypo-search"></i>
                </div>
                
            </form>
        </div>

        <div class="mail-header">
            <!-- title -->
            <h1 class="mail-title">
                {{ trans('general.items') }}
                <span class="count text-info">({{$items->count()}})</span>

                <div class="btn-group" role="group" aria-label="Escolha o formato de ver os dados">
                      <a class="btn btn-secondary {{((Request::query('grid_type') == 'default' || Request::query('grid_type') == null) ? 'active' : null)}}" href=''> <i class="entypo-list"></i> </a>

                      <a class="btn btn-secondary {{(Request::query('grid_type') == 'layout' ? 'active' : null)}}" href='?'> <i class="entypo-layout"></i> </a>
                </div>

            </h1>
                      

            <!-- Filtrar por Categoria Produto -->					
            <div class="mail-search">
                {{ Form::select('category', $categories, null,['class' => 'form-control','onchange'=>'getConsulta("'. Request::path() .'","'.http_build_query(Request::query()).'",this)','placeholder'=>trans('general.select').'...']) }}
            </div>
            <!-- botão apagar para bulkactions ainda não implementado -->					
            {{-- <div class="pull-left">				
                <a href="#" class="btn btn-danger pull-left">
                    <i class="entypo-trash"></i>
                </a>
            </div> --}}

        </div>
                
            @include('sale-rent::items.layout')

      
        
        {{ $items->links() }}
    </div>

    <div class="mail-sidebar">
				
        <!-- compose new email button -->
        <div class="mail-sidebar-row hidden-xs">
            @can('octoplus.items_create')
                <a href="{{route('tenant.items.type.create','rent')}}" class="btn btn-blue btn-lg btn-icon btn-block">
                    {{trans_choice('general.add',2,['item' => trans('general.item')])}}
                    <i class="entypo-plus"></i>
                </a>
            @endcan
        </div>
       

        <!-- menu -->
        <ul class="mail-menu">
            @foreach (item_types() as $key => $type)                  
                <li class="{{ Request::query('product_type') == $key ? 'active' : null }}">
                    <a href="?{{ http_build_query( collect(['product_type' => $key,'rent_status' => 1,'page' => 1])->merge(collect(Request::query())->forget(['status','product_type','rent_status','page']))->toArray()) }} ">
                        {{ item_types()[$key] }} <span class="badge badge-gray pull-right">{{$itensAll->where('item_type',$key)->count()}}</span>
                    </a>
                </li>
            @endforeach
            <li class="{{ $typeItem == "rent" ? 'active' : 'link_disabled' }}" >
                <a href="?{{ http_build_query(collect(['rent_status' => 0,'page' => 1])->merge(collect(Request::query())->forget(['status','rent_status','page']))->toArray()) }}">
                    <span class="badge badge-danger badge-roundless pull-right"> {{ $faturasCanceledCount }}</span>
                    {{ trans('sale-rent::general.rented') }}
                </a>
            </li>
            <li class="{{ $typeItem == "rent" ? 'active' : 'link_disabled' }}" >
                <a href="?{{ http_build_query(collect(['rent_status' => 1,'page' => 1])->merge(collect(Request::query())->forget(['status','rent_status','page']))->toArray()) }}">
                    <span class="badge badge-success badge-roundless pull-right"> {{ $faturasAvailableCount }}</span>
                    {{ trans('sale-rent::general.unattached') }}
                </a>
            </li>
            <li class="{{ $typeItem == "rent" ? 'active' : 'link_disabled' }}" >
                <a href="?{{ http_build_query(collect(['rent_status' => 2,'page' => 1])->merge(collect(Request::query())->forget(['status','rent_status','page']))->toArray()) }}">
                    <span class="badge badge-warning badge-roundless pull-right"> {{ $faturasWinningCount }}</span>
                    {{ trans('sale-rent::general.overcome') }}
                </a>
            </li>
        </ul>
        
        <div class="mail-distancer"></div>
        
        <h4>{{ trans('general.status') }}</h4>
       
        <!-- menu -->
        <ul class="mail-menu">
            <li class="{{ Request::query('status') == 0 ? 'active' : null }}">
                <a href="?{{ http_build_query(collect(['enabled' => 0])->merge(collect(Request::query())->forget(['enabled']))->toArray()) }}">
                    <span class="badge badge-roundless pull-right"></span>
                    {{ trans('general.inactive') }}
                </a>
            </li>

            <li class="{{ Request::query('status') == 1 ? 'active' : null }}">
                <a href="?{{ http_build_query(collect(['enabled' => 1])->merge(collect(Request::query())->forget(['enabled']))->toArray()) }}">
                    <span class="badge badge badge-roundless pull-right"></span>
                    {{ trans('general.active') }}
                </a>
            </li>
        </ul>
        
    </div>
    
</div>

@endsection

@section('modals')
@include('common.modals.delete',['type' => trans('general.items')])
@endsection

@section('scripts')

{{-- <link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">


<script src="{{asset('assets/js/select2/select2.min.js')}}"></script> --}}
<script src="{{asset('js/sale-rent/itens.js')}}" ></script>
<script type="text/javascript">
    
</script>
@endsection