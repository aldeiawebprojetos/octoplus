<div class="gallery-env">
    <div class="row">
        @foreach ($items as $item)
            @php
                $pedido = getPedidoByItemId($item->id);  
                if($pedido)
                {
                    $vencimento_pedido = verifyRentValidate($pedido);

                    $dateInvoice = \Carbon\Carbon::parse($pedido->rent_endDate)->startOfDay();
                    $difDays = $dateInvoice->diffInDays(\Carbon\Carbon::now()->startOfDay(), true);
                    if($difDays >= 0 && $difDays <= 7 && $dateInvoice >= \Carbon\Carbon::now()->startOfDay()){
                        $winning[ $item->id ] = true; 
                    }else{
                        $winning[ $item->id ] = false;
                    }
                }
            @endphp
        <div class="col-md-3 col-sm-3 col-xs-6 card-w25">
            <article class="album">
                <section class="album-info text-center">
                    <h3> {{$pedido ? $pedido->id . ' - ' : null}} </h3>
                    <h3> <a href="{{route('tenant.items.edit',$item->id)}}">{{$item->name}} {{(settings('contract.category_as_name', true) ? '- '.$item->categories->last()->name : '')}}</a> </h3>
                    <h3> #{{$item->code}} </h3>
                </section>
                
                <header>
                    
                    <a href="{{route('tenant.items.edit',$item->id)}}">
                        <img class="item-disponivel" id="item_{{ $item->code }}" src="{{($item->firstMedia('product') != null ? 
                        $item->firstMedia('product')->getUrl() : ($item->rent_status ? asset('img/success-item.svg') : 
                        (isset($vencimento_pedido) && $winning[ $item->id ] == true ? asset('img/alert-item.svg') : asset('img/danger-item.svg'))))}}" alt="">
                    </a>
                    <a href="{{route('tenant.items.edit',$item->id)}}" class="album-options">
                        <i class="entypo-pencil"></i>
                        {{trans('general.edit')}}
                    </a>
                </header>
                
                <footer>
                    
                                                        
                    <div class="album-options">
                        @if(getPedidoCodeByItemId($item->id))
                            @if ($pedido && $pedido->active_contract)
                                <a class="text-album-options d-block" href="{{ route('sale-rent.invoices.rent.edit', $pedido->id) }}" >
                                    <i class="entypo-doc-text-inv"></i> {{ getPedidoCodeByItemId($item->id) }}
                                </a>
                            @elseif ($item->item_status != 'closed')

                                <div class="contrato-encerrado" title="O contrato foi encerrado e o item está pronto para ser marcado como disponível.">
                                    Locação encerrada
                                </div>
                            @endif
                        @endif
                     @if($pedido && $pedido->shipping_status == 'waiting' && $item->item_status != 'delivered')
                       
                        <form action="{{ route('tenant.items.changeInvoiceStatus') }}" method="post">
                             @csrf
                             {{ Form::hidden( 'item_id', $item->id , [ 'class' => 'form-control input-lg' ] ) }}
                             {{ Form::hidden( 'pedido_id', $pedido->id , [ 'class' => 'form-control input-lg' ] ) }}
                             {{ Form::hidden( 'item_status',"delivered", [ 'class' => 'form-control input-lg' ]  )  }}
                             
                             <button type="submit" class="btn btn-lg inputChangeInvoiceStatus" id = "btnChangeInvoiceDelivered_{{ $item->id }}"
                                title = "Marcar Item como Entregue: Quando o item locado já foi enviado e entregue.">

                                Entregar
                                <i class="fa fa-truck"></i> 

                             </button> 

                        </form>
                        
                     @endif
                    
                     @if($pedido && !$pedido->active_contract && !$item->rent_status)
                    
                         <form action="{{ route('tenant.items.changeInvoiceStatus') }}" method="post">
                             @csrf
                             {{ Form::hidden( 'item_id', $item->id , [ 'class' => 'form-control input-lg' ] ) }}
                             {{ Form::hidden( 'pedido_id', $pedido->id , [ 'class' => 'form-control input-lg' ] ) }}
                             {{ Form::hidden( 'item_status',"closed", [ 'class' => 'form-control input-lg' ]  )  }}
                           
                             <button type="submit" class="btn btn-lg inputChangeInvoiceStatus" id = "btnChangeInvoiceClosed_{{ $item->id }}"
                                title = "Marcar Item como Disponível: Quando o contrato foi encerrado, e o produto fica novamente disponível." >

                                Liberar item
                                <i class="fa fa-check-square"></i>

                             </button>    

                        </form>
                        
                     @endif
                    </div>                         
                </footer>
                
            </article>
        </div>
        @endforeach
    </div>
</div>