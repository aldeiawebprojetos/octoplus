@extends('layouts.app')

@section('title',trans('sale-rent::general.contract').' - ' .(settings('contract.new_number', true) ? $invoice->code : $invoice->id))

@section('content')
    @include('sale-rent::rent.contractContent',['pdf' => false])
    <div class="row">
        
        <br>
        
        <a href="javascript:window.print()" class="btn btn-primary btn-icon icon-left hidden-print">
            Imprimir Contrato
            <i class="entypo-doc-text"></i>
        </a>
        
        &nbsp;
        
        {{-- <a href="mailbox-compose.html" class="btn btn-success btn-icon icon-left hidden-print">
            Enviar por Email
            <i class="entypo-mail"></i>
        </a> --}}
    </div>
@endsection