@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('sale-rent.items.rent')}}">{{trans('sale-rent::general.rent')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.add',2,['item' => trans('sale-rent::general.rent')])}}</strong> </li>	
@endsection

@section('title', trans('sale-rent::general.rent'))

@section('content')
{{ Form::open(['method'=>'POST', 'route' => 'sale-rent.invoices.rent.store','enctype'=>'multipart/form-data','id'=>'invoice','@submit.prevent' => 'onSubmit',]) }}

<div class="row">
     <!-- title -->
     <div class="col-md-6">
        <h1 class="pull-left">
            {{trans('sale-rent::general.rent')}} <i class="entypo-pencil"></i>
        </h1>
    </div>

    @include('common.buttonsForm',['route_return' => 'sale-rent.invoices.rent.index'])
</div>

@include('sale-rent::rent.form',['form_edit' => false])

<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'sale-rent.invoices.rent.index'])
</div>

{{ Form::close() }}
@endsection

@section('modals')
    @include('common.modals.addcategory', ['type' => 'income'])
    @include('common.modals.addtax')
@endsection