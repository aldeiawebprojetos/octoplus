<tbody id="items-list">
    @if ((isset($readOnly) && $readOnly) || $form_edit) 
        @foreach ($invoice->items as $item)
            <tr>

              {{-- Ações --}}
                <td class="text-center">
                  <button type="button"
                 
                  
                  {{($form_edit ? 'disabled' : '')}}
                  
                  class="btn btn-danger btn-lg">
                     <i class="fas fa-check"></i>
                  </button>
                </td>
        
                {{-- Pesquisa de item pelo nome --}}
                <td class="text-center">
                 {{$item->name}}
                  
                </td>
        
                {{-- Quantidade --}}
                <td class="text-center">
                  1
                </td>
        
                {{-- Valor unitário --}}
                <td class="text-center"  >
                  {{ formata_moeda($item->price) }}
                </td>
        
                {{-- Impostos --}}
                <td class="text-center">
                  {{ $item->tax->name ?? null }}
                </td>
        
                {{-- valor total --}}
                <td class="text-right total-column ">
                  {{ formata_moeda($item->price) }}
                </td>
            <tr>
        @endforeach
    @else
        <tr v-for="(row, index) in form.items" :index="index">
          {{-- Ações --}}
            <td class="text-center" v-if="index != 0">
              <button type="button"
              @click="onDeleteItem(index)"
              data-toggle="tooltip"
              {{($form_edit ? 'disabled' : '')}}
              title="{{ trans('general.delete') }}"
              class="btn btn-danger btn-lg btn-delete-item">
                <i class="fa fa-trash"></i>
              </button>
            </td>

            <td class="text-center" v-else>
              <div>-</div>
            </td>

            {{-- Pesquisa de item pelo nome --}}
            <td class="text-center item-check">
              <item-typeahead
              :item-index="index"
              type_item="{{$type_item}}"
              v-model="row.name"
              url="{{ route('tenant.items.search') }}"
              placeholder="{{ trans('general.search') }}"></item-typeahead>
              
              <input type="hidden"
                    data-item="name"
                    v-model="row.name"
                    @input="onCalculateTotal"
                    :name="'item['+index+'][name]'">
              <input type="hidden" :name="'item['+index+'][item_id]'" data-item="id" v-model="row.id">
            </td>

            {{-- Quantidade --}}
            <td class="text-center">
              <input type="text"
              class="form-control input-lg"
              :name="'item['+index+'][quantity]'"
              autocomplete="off"
              value="1"
              readonly
              data-item="quantity"
              v-model="row.quantity"
              @input="onCalculateTotal"
              @change="form.errors.clear('item['+index+'][quantity]')">
            </td>

            {{-- Valor unitário --}}
            <td class="text-center"  >
            <money decimal="{{$currency->decimal_mark}}" thousands="{{$currency->thousands_separator}}"
            data-item="price"
            class="form-control  input-lg"
            v-model="row.price"
            {{($form_edit ? 'readonly' : '')}}
            @input="onCalculateTotal"></money>
            <input type="hidden" v-model="row.price"
            :name="'item['+index+'][price]'" >
            </td>

            {{-- Impostos --}}
            <td class="text-center">
              <div class="input-group">
              <tax-select
              placeholder="{{ trans('general.search') }}"
              :name="'item['+index+'][tax_id]'"
              v-model="row.tax_id"
              {{($form_edit ? 'readonly' : '')}}
              url="{{ route('tenant.taxes.search') }}" 
              data-item="tax_id"
              v-model="row.tax_id"
              :item-index="index"></tax-select>
                <div class="input-group-btn">
                    <button class="btn btn-default" onclick="$('#taxAdd').modal('show');" style="height: 42px !important;" type="button"><i class="fa fa-plus-square"></i></button>
                  </div>
              </div>         
            </td>

            {{-- valor total --}}
            <td class="text-right total-column ">
              <span id="item-total" v-if="row.total" >{{ row.total |maskMoney('<?= $currency->thousands_separator ?>','<?= $currency->decimal_mark ?>','<?= $currency->symbol ?>','<?= $currency->symbol_first ?>') }}</span>
              <span v-else> {{ 0 | maskMoney('<?= $currency->thousands_separator ?>','<?= $currency->decimal_mark ?>','<?= $currency->symbol ?>','<?= $currency->symbol_first ?>') }}</span>
              <input type="hidden" :name="'item['+index+'][total]'" v-model="row.total">
            </td>
        <tr>
        
          <tr id="addItem">
            <td class="text-center border-right-0 border-bottom-0">
                <button type="button" {{($form_edit ? 'disabled' : '')}} @click="onAddItem" id="button-add-item"
                data-toggle="tooltip" title="{{ trans('general.add') }}" class="btn btn-success btn-lg"
                data-original-title="{{ trans('general.add') }}"><i class="fa fa-plus"></i>
                </button>
            </td>
            <td class="text-right border-bottom-0" colspan="5" :colspan="colspan"></td>
        </tr>
    @endif   
</tbody>