@extends('layouts.print')

@section('title' ,  trans('sale-rent::general.contract') . "_" .(settings('contract.new_number', true) ? $invoice->code : $invoice->id))
@php
$pdf = true;    
@endphp
@section('content')
<div class="table">
		
    <div class="table-row">
    
        <div class="table-cell">
        
          
                <img src="{{($company->firstMedia('logo') != null ? ($pdf ? $company->firstMedia('logo')->getAbsolutePath() : $company->firstMedia('logo')->getUrl()) : ($pdf ? public_path('/assets/images/logo@2x.png') : asset('/assets/images/logo@2x.png')))}}" alt="company_logo" width="185">
            
            <br>
            
            <h4 class="">
                <small>
                    {{ trans('customers::general.cnpj') }}: {{ formatar_cpf_cnpj($company->document) }}
                    <br>
                    {{ trans('customers::general.address') }}: {{ $company->address }}
                    <br>
                    {{ trans('user.phone') }}: {{ masc_tel($company->phone) }}
                </small>
            </h4>
            
        </div>
        
        <div class="table-cell" style="text-align: right">
        
                <h3>{{ trans('sale-rent::general.rent_contract') }}<strong style="color: red;"> Nº {{(settings('contract.new_number', true) ? $invoice->code : $invoice->id)}} </strong></h3>
                <span>{{ $invoice->sale_date->formatLocalized('%d '.trans('general.of').' %B '.trans('general.of').' %Y') }}</span>
        </div>
        
    </div>
</div>
    
    <hr class="margin">
    
<div class="table">
    <div class="table-row">
    
        <div class="table-cell">
        
            <h4>{{ trans('general.client') }}</h4>
            {{$invoice->customer_name}}
            <br>
            {{ !empty($invoice->customer_tax_number) ? $invoice->customer_tax_number : formatar_cpf_cnpj($invoice->customer->user->document) }}
            <br>

            
        </div>
    
        <div class="table-cell">
             
            <h4>{{ trans('customers::general.address') }}</h4>
            {{ $invoice->customer->address }}
            <br>
            {{ $invoice->customer->city }}-{{ $invoice->customer->uf }}
            <br>
            {{ masc_tel($invoice->customer->tel1) }}
        </div>
        
        <div class="table-cell" style="text-align: right">
        
            <h4>{{ trans('sale-rent::general.payment_details') }}:</h4>
             {{ trans('sale-rent::general.op_type') }}: <strong>{{trans('sale-rent::general.rent')}}</strong>
            <br>

             {{ trans('sale-rent::general.quantity_rent') }}: <strong> {{$invoice->quantity_parts}} {{ trans_choice('calendar.months_contract',$invoice->quantity_parts) }}</strong>
            <br>

             {{ trans('sale-rent::general.rent_end') }}: <strong>{{ $invoice->rent_endDate->format('d/m/Y') }}</strong>
            <br>

        </div>
        
    </div>
</div>
    
    <hr class="margin">
    
    <table class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th width="60%">{{trans('general.products')}}/{{trans('item.service')}}</th>
                <th class="text-center">{{trans('general.quantity')}}</th>
                <th>{{ trans('sale-rent::general.price') }}</th>
            </tr>
        </thead>
        
        <tbody>       
            @foreach($invoice->items()->get() as $key => $item)
                <tr>
                    <td class="text-center">{{ $key + 1 }}</td>
                    <td>{{ $item->name }} {{ (settings('contract.category_as_name', true) ? "- {$item->categories->last()->name}" : '') }}</td>
                    <td class="text-center">{{ $item->quantity }}</td>
                    <td>{{ $currency->symbol }} {{ $item->price }}</td>
                </tr>
            @endforeach
            @if($invoice->shipping_delivery || $invoice->shipping_withdrawal)
                <tr>
                    <td class="text-center">{{ $invoice->items->count()+1 }}</td>
                    <td>{{ trans('sale-rent::general.shipping') }}</td>
                    <td class="text-center">{{ ($invoice->shipping_delivery + $invoice->shipping_withdrawal) }}</td>
                    <td>{{ $currency->symbol }} {{ $invoice->shipping_value *  ($invoice->shipping_delivery + $invoice->shipping_withdrawal)}}</td>
                </tr>
            @endif
        </tbody>
    </table>
    
    <hr class="margin">
 <div class="table">
    <div class="table-row">
    
        <div class="table-cell" >
        
           

                <h4>{{ trans('sale-rent::general.shipping') }} {{ trans('general.and') }} {{ trans('sale-rent::general.delivery') }}</h4>

                {{ trans('sale-rent::general.shipping_type') }}: <strong>{{ getFreteType($invoice) }}</strong>
                <br>

                {{ trans('sale-rent::general.shipping_address') }}:
                <br>
                <strong>
               {{$invoice->address_complete}}
                <br>

                <br>
                {{ !empty($invoice->customer->responsible) ? $invoice->customer->responsible : $invoice->customer->user->name }}
                <br>
                {{ $invoice->customer->user->email }}

                </strong>
        
        
        </div>
        
        <div class="table-cell" style="text-align: right">
            
            
                
                <ul class="list-unstyled">
                    <li>
                        {{trans('sale-rent::general.sub_total')}} {{ trans('sale-rent::general.rent') }}: 
                    <strong>{{ $currency->symbol }} {{ $invoice->items->sum('price') * $invoice->quantity_parts}}</strong>
                    </li>

                    <li>
                        {{trans('sale-rent::general.sub_total')}} {{ trans('sale-rent::general.shipping') }}: 
                        <strong>{{ $currency->symbol }} {{ $invoice->items->sum('price') * $invoice->quantity_parts }}</strong>
                    </li>
                    

                    <li>
                        {{ trans('sale-rent::general.tax') }}: 
                        <strong>{{$invoice->items->sum('aliquota')}}%</strong>
                    </li>
                    <li>
                        {{ trans('sale-rent::general.discount') }}: 
                        <strong>{{$invoice->discount_rate ?? 0}}% </strong>
                    </li>
                    <li>
                        {{ trans('sale-rent::general.total') }}:
                        <strong>{{ $currency->symbol }} 

                            {{ 
    
                                ( $invoice->items->sum('price') * $invoice->quantity_parts ) +
                                
                                ( 
                                    $aliquotaValue = $invoice->items->sum('aliquota')  != 0 ? 
                                                     $invoice->items->sum('aliquota') * ( $invoice->items->sum('price') * $invoice->quantity_parts ) / 100 : 0 
                                ) -
    
                                ( 
                                    $discountValue = $invoice->discount_rate  != 0 ? 
                                                     $invoice->discount_rate * ( ( $invoice->items->sum('price') + $aliquotaValue  ) * $invoice->quantity_parts ) / 100 : 0 
                                ) +
    
                                (   $shippingValue = $invoice->shipping_value != 0 ? 
                                                     $invoice->shipping_value * ( $invoice->shipping_delivery + $invoice->shipping_withdrawal ) : 0  
                                ) 
    
                            }}
                        </strong>
                    </li>
                </ul>
                
        
            
        </div>
        
    </div>

    
    

    <div class="table-row">
        <div class="table-cell" style="text-align: justify">
          {!!$contractData!!}
             <br>

         </div>

    </div>

    <div>
        <img src="{{'data:image/gif;base64,' . auth()->user()->assinatura}}" alt="company_logo" width="300px">
    </div>

    {{-- <div class="row">
    
        <div class="col-sm-6 text-center">
             
             <br>
            _________________________________ 
            <br>
            Assinatura
            <br>

            {}
            <br>
            {}
            <br>

        </div>
        
        <div class="col-md-6 text-center">
        
            {}
            <br>
            {}
            <br>
            {}
            <br> <br>

        </div>
        
    </div> --}}


    

</div>


@endsection

@section('scripts')

@endsection