<div>
    <div style="width: 185px; float: left; padding-right: 20px">
        <img src="{{isset($company->logo) ? $company->logo : ''}}" alt="company_logo">
    </div>
    <div style="float: right; padding-top: 30px">
    <h4 class="">
                <small>
                    {{ trans('customers::general.cnpj') }}: {{ formatar_cpf_cnpj($company->document) }}
                    <br>
                    {{ trans('customers::general.address') }}: {{ $company->address }}
                    <br>
                    {{ trans('user.phone') }}: {{ masc_tel($company->phone) }}
                </small>
            </h4>
    </div>
</div>

<div style="text-align: center">
    <h1>{{'Contrato de Aluguel Nº ' . $invoice->code}}</h1>
</div>

<br>
<br>

<table style="width: 100%">
    <tr>
        <th style="text-align: left;">Cliente</th>
        <th style="text-align: left;">Endereço</th>
        <th style="text-align: right;">Detalhes de pagamento</th>
    </tr>

    <tr>
        <td style="width:40%">
            {{$invoice->customer_name}}
            <br>
            {{ !empty($invoice->customer_tax_number) ? $invoice->customer_tax_number : formatar_cpf_cnpj($invoice->customer->user->document) }}
            <br>
        </td>
        <td style="width:20%">
            {{ $invoice->customer->city }}-{{ $invoice->customer->uf }}
            <br>
            {{ masc_tel($invoice->customer->tel1) }}
        </td>
        <td style="text-align: right; width:40%">
            {{ trans('sale-rent::general.op_type') }}: <strong>{{trans('sale-rent::general.rent')}}</strong>
            <br>

            {{ trans('sale-rent::general.quantity_rent') }}: <strong> {{$invoice->quantity_parts}} {{ trans_choice('calendar.months_contract',$invoice->quantity_parts) }}</strong>
            <br>

            {{ trans('sale-rent::general.rent_end') }}: <strong>{{ $invoice->rent_endDate->format('d/m/Y') }}</strong>
            <br>
        </td>
    </tr>
        
</table>

<br>
<br>

<table style="width:100%;border-collapse: collapse;">
        <tr>
            <th  style="text-align: left;border: 1px solid black">#</th>
            <th  style="text-align: left;border: 1px solid black">{{trans('general.products')}}/{{trans('item.service')}}</th>
            <th  style="text-align: left;border: 1px solid black">{{trans('general.quantity')}}</th>
            <th style="text-align: left;border: 1px solid black">{{ trans('sale-rent::general.price') }}</th>
        </tr>
             
        @foreach($invoice->items()->get() as $key => $item)
            <tr>
                <td style="border: 1px solid black">{{ $key + 1 }}</td>
                <td style="border: 1px solid black">{{ $item->name }} {{ (settings('contract.category_as_name', true) ? "- {$item->categories->last()->name}" : '') }}</td>
                <td style="border: 1px solid black">{{ $item->quantity }}</td>
                <td style="border: 1px solid black">{{ $currency->symbol }} {{ $item->price }}</td>
            </tr>
        @endforeach
        @if($invoice->shipping_delivery || $invoice->shipping_withdrawal)
            <tr>
                <td style="border: 1px solid black">{{ $invoice->items->count()+1 }}</td>
                <td style="border: 1px solid black">{{ trans('sale-rent::general.shipping') }}</td>
                <td style="border: 1px solid black">{{ ($invoice->shipping_delivery + $invoice->shipping_withdrawal) }}</td>
                <td style="border: 1px solid black">{{ $currency->symbol }} {{ $invoice->shipping_value *  ($invoice->shipping_delivery + $invoice->shipping_withdrawal)}}</td>
            </tr>
        @endif
</table>

<br>

<table style="width:100%">
    <tr>
        <th  style="text-align: left">Frete e Entrega</th>
        <th  style="text-align: left">Valor final</th>
    </tr>
    <tr>
        <td  style="width:60%">
            <div>

                {{ trans('sale-rent::general.shipping_type') }}: <strong>{{ getFreteType($invoice) }}</strong>
                <br>

                {{ trans('sale-rent::general.shipping_address') }}:
                <br>
                <strong>
                {{$invoice->address_complete}}
                <br>

                <br>
                {{ !empty($invoice->customer->responsible) ? $invoice->customer->responsible : $invoice->customer->user->name }}
                <br>
                {{ $invoice->customer->user->email }}

                </strong>
            </div>
        </td>
        <td style="width:40%">

            <ul style="list-style-type: none;">
                <li>
                    {{trans('sale-rent::general.sub_total')}} {{ trans('sale-rent::general.rent') }}: 
                <strong>{{ $currency->symbol }} {{ $invoice->items->sum('price') * $invoice->quantity_parts }}</strong>
                </li>

                <li>
                    {{trans('sale-rent::general.sub_total')}} {{ trans('sale-rent::general.shipping') }}: 
                    <strong>{{ $currency->symbol }} {{$invoice->shipping_value * ($invoice->shipping_delivery + $invoice->shipping_withdrawal)}}</strong>
                </li>
                

                <li>
                    {{ trans('sale-rent::general.tax') }}: 
                    <strong>{{$invoice->items->sum('aliquota')}}%</strong>
                </li>
                <li>
                    {{ trans('sale-rent::general.discount') }}: 
                    <strong>{{$invoice->discount_rate ?? 0}}% </strong>
                </li>
                <li>
                    {{ trans('sale-rent::general.total') }}:
                    <strong>{{ $currency->symbol }} 

                    {{ 

                    /*
                        |--------------------------------------------------------------------------
                        | Calcular o valor final do contrato
                        |--------------------------------------------------------------------------
                        |
                        | Essa operação calculara o valor da locação vezes o tempo que o cliente ficará com o 
                        | produto depois vai adicionar o valor dos impostos, e logo depois disso ela removera 
                        | o desconto para chegar no valor final do item. E depois de chegar no valor 
                        | final ela adicionar o freet.
                        |
                    */

                        ( $invoice->items->sum('price') * $invoice->quantity_parts ) +
                        
                        ( 
                            $aliquotaValue = $invoice->items->sum('aliquota')  != 0 ? 
                                                $invoice->items->sum('aliquota') * ( $invoice->items->sum('price') * $invoice->quantity_parts ) / 100 : 0 
                        ) -

                        ( 
                            $discountValue = $invoice->discount_rate  != 0 ? 
                                                $invoice->discount_rate * ( ( $invoice->items->sum('price') + $aliquotaValue  ) * $invoice->quantity_parts ) / 100 : 0 
                        ) +

                        ( 
                            $shippingValue = $invoice->shipping_value != 0 ? 
                                                $invoice->shipping_value * ( $invoice->shipping_delivery + $invoice->shipping_withdrawal ) : 0 
                        ) 

                    }}</strong>
                </li>
            </ul>
        </td>
    </tr>
</table>

<br>
<hr>
<br>

<h4>Condições Gerais</h4>

<p>1. P01. Condições de Trabalho: NORMAIS.
De acordo as recomendações do fabricante e locadora: sendo estas feitas por meio de demonstração,aconselhamento verbal e/ou
escrito, tanto ao locatário ou pessoa designada para o manuseio do equipamento .Fica ao locatário aresponsabilidade de repassar
qualquer informação e também fiscalizar se esta sendo seguido as orientações colocadas a dispor sobreutilização do equipamento
a seus subordinados ;</p>


<p>2. O equipamento só poderá ser utilizado em conformidade com as normas do fabricante e para asfinalidades previstas, somente no local da obra ou no endereço indicado pelo locatário</p>

<p>
    3. Fica o locatário responsável legal e juridicamente, quanto ao uso do equipamento locado, cumprindotodas as normas e obrigações das legislações municipais, estaduais e federais em vigor
</p>

<p>
    4. O locatário fica inteiramente responsável por quaisquer danos físicos, mecânicos e elétricos,causados ao equipamento, sejam
    estes causados por:
    <ul>
        <li>Utilização incorreta do equipamento, não respeitando as normas e procedimentos de uso;</li>
        <li>Transporte do equipamento de forma incorreta;</li>
        <li>Falta de armazenamento proteção contra intempéries diversas;</li>
        <li>Ligações elétricas inapropriadas á naturezadas maquinas ou equipamentos;</li>
        <li>Extravio ou roubo de partes ou anexos da máquina ou equipamento</li>
    </ul>
</p>

<p>
5. O locatário fica inteiramente responsável por quaisquer danos causados a terceiros, quer pessoais, quermateriais, desde a
retirada do equipamento da locadora até a devolução do mesmo;
</p>

<p>
6. Transporte: as despesas de transporte do equipamento (entrega na obra até a devolução na locadora) etrocas de locais, são
por conta do locatário;
</p>

<p>
7. O locatário exclui a locadora de quaisquer responsabilidade cíveis, trabalhistas, tributarias indenizatórias,seja a que título for;
</p>

<p>
8. Fica a conhecimento de quem possa interessar que a locadora (DISK CONTAINERS), não executanenhum tipo de prestação de
Mão – de – obra para utilização de equipamento locado, fazendo apenas demonstrações do mesmo;
</p>

<p>
9. O locatário não poderá em hipótese alguma sublocar ,emprestar ou ceder o equipamento a terceiros sema previa autorização
por escrito da locadora;
</p>

<p>
10. O locador reserva seu direito de reclamar os valores gerados pelo acordo deste contrato mesmo que olocatário não faça a
utilização do equipamento. O prazo combinado na locação é por data corrida e não por dia útil.
</p>

<p>
11. Em caso de devolução do equipamento locado ou dispensa do mesmo antes do prazo contratado, olocatário terá de arcar com
os valores proporcionais ao tempo que o equipamento ficou a sua disposição e também com a multarescisória de 20% do restante
do valor da locação, bem como os fretes caso esteja assim acordado;
</p>

<p>
12. Retirada: será feita somente mediante o pagamento das faturas em aberto e no ato da retirada docontainer, o mesmo
deverá estar desocupado, ou seja, não contendo nenhum material ou equipamento em seu interior. Casonão seja possível a
retirada por responsabilidade do locatário, será cobrado valor do frete mais os dias proporcionais de uso docontainer.
</p>

<p>
13. No caso de dispensa antes do prazo e devolução do equipamento e também no caso de relocação domesmo, o locatário
deverá comunicar a locadora com antecedência de pelo menos 03 (um) dia úteis;
</p>

<p>
14. O locatário fica expressamente responsável no caso de extravio ou roubo de partes e anexos doequipamento, bem como pelo
interior do equipamento ficando responsável a reembolsar a locadora no prazo de 05 (cinco) dias úteis ovalor referente ao mesmo,
sendo que este valor será estipulado de acordo com o valor de mercado. Fica assim definido, pois olocatário é pleno fiel
depositário do equipamento;
</p>

<p>
15. O pagamento referente a presente locação será efetuado antecipadamente ou no final de cada período de 30 dias acordado
entre a locadora e o locatário que serão contados a partir da data de assinatura deste contrato, ou no ato dedevolução do
equipamento se o período de locação for inferior a 30 dias. Alteração contratual referente aos meses delocação é de
responsabilidade do locatário conforme sua necessidade e solicitação a locadora.
</p>

<p>
16. No caso do não pagamento da locação por parte do locatário, este mesmo terá uma carência de 05 diasapós o vencimento
para adimplir o débito, caso contrário será rescindido de pleno direito independente de interpelação judicialou extrajudicial;
</p>

<p>
17. Na rescisão de contrato, o locatário devera conceder ao locador amplos poderes para poder retirar oequipamento locado,
inclusive de solicitar a proteção de posse, juridicamente, com concordância expressa da locadora serreintegrada nos seus direitos
initio litis;
</p>

<p>
18. Em caso do não pagamento da locação do equipamento por parte do locatário, o locador utilizara osrecursos disponíveis,
sejam eles, extrajudiciais e judiciais, bem como também, o registro de inclusão no banco de dados doSPCB;
Fica assim ajustados e contratados, o locador e o locatário, a condição expressa neste contrato elegendo oforo de Anápolis ou 1°
Corte de conciliação e Arbitragem, para dirimir quaisquer inerentes a este documento, com base nalegislação em vigor e na lei n°
9.307 de 23/09/1996.
</p>

<br>

<div>
    Anápolis, {{$dataHoje->formatLocalized('%d '.trans('general.of').' %B '.trans('general.of').' %Y')}}
</div>

<br>

<div>
    Nome/CPF Responsável
</div>
<div>
    {{$invoice->customer->responsible}} - 
    {{formatar_cpf_cnpj($invoice->customer->responsible_cpf)}}
</div>

<br>
<br>

<div>
    Ass: __________________________________________________
</div>

<br>
<br>
<br>

@if(auth()->user()->assinatura)

    <img src="{{'data:image/gif;base64,' . auth()->user()->assinatura}}" alt="company_logo" width="300px">
@else
    __________________________________________________
    <div style="margin-top: 8px">
        {{auth()->user()->first_name}} {{auth()->user()->last_name}}
    </div>
@endif


