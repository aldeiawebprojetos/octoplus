@extends('layouts.app')

@section('breadcrumb')
<li> <a href="">{{ trans('sale-rent::general.rent') }}</a> </li>
<li class="active"> <strong>{{trans('general.show')}}</strong> </li>		
@endsection

@section('title', trans('sale-rent::general.rent'))

@section('content')				
		{{Form::model($invoice, ['class' => 'disabled' ])}}
    <!-- title -->
    <div class="col-md-6">
        <h1 class="pull-left">
            Pedido Nº <i class="fa fa-dot-circle-o"></i> <span class="label label-primary"> #{{$invoice->code}} </span>
        </h1>
    </div>
       
		@include('sale-rent::rent.form', ['readOnly' => true,'form_edit' => true])

		{{ Form::close() }}
				
		<div class="clear"></div>
		<!-- Footer -->
@endsection