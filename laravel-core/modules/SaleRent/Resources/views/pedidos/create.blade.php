@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('sale-rent::general.invoices')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.add',2,['item' => trans('sale-rent::general.invoices')])}}</strong> </li>	
@endsection

@section('title', trans('sale-rent::general.invoices'))

@section('content')
{{ Form::open(['method'=>'POST', 'route' => 'sale-rent.invoices.store','enctype'=>'multipart/form-data','id'=>'invoice','@submit.prevent' => 'onSubmit',]) }}

<div class="row">
     <!-- title -->
     <div class="col-md-6">
        <h1 class="pull-left">
            {{trans('sale-rent::general.invoices')}} <i class="entypo-pencil"></i>
        </h1>
    </div>

    @include('common.buttonsForm',['route_return' => 'sale-rent.invoices.index'])
</div>

@include('sale-rent::pedidos.form',['form_edit' => false])

<div class="row">
    <div class="col-md-6"></div>
    @include('common.buttonsForm',['route_return' => 'sale-rent.invoices.index'])
</div>

{{ Form::close() }}
   
@endsection

@section('modals')
    @include('common.modals.addcategory', ['type' => 'income'])
    @include('common.modals.addtax')
@endsection