@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('sale-rent::general.invoices')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.add',2,['item' => trans('sale-rent::general.invoices')])}}</strong> </li>	
@endsection

@section('title', trans('sale-rent::general.invoices'))

@section('content')
{{ Form::model($invoice,['method'=>'PUT', 'route' => ['sale-rent.invoices.update',$invoice->id],'enctype'=>'multipart/form-data','id'=>'invoice','@submit.prevent' => 'onSubmit',]) }}

<div class="row">
			
    <!-- title -->
    <div class="col-md-6">
        <h1 class="pull-left">
            {{ trans('sale-rent::general.invoice_number') }} <i class="fa fa-dot-circle-o"></i> <span class="label label-primary"> #{{$invoice->code}} </span>
        </h1>
    </div>
    

    <!-- Tipo Contrato -->
    <div class="col-md-6">
        <h2 class="pull-right">{{number_format($invoice->amount, 2,$currency->decimal_mark,$currency->thousands_separator)}}</h2>
    </div>
</div>

@include('sale-rent::pedidos.form',['form_edit' => true,'readOnly' => 'true'])

<div class="row">
    <div class="col-md-6"></div>
    
</div>

{{ Form::close() }}
   
@endsection

