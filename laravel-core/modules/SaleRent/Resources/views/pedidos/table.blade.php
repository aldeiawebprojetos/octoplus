<table class="table table-bordered table-striped datatable" id="table-4">
    <thead>
        <tr>
            <th width="1%" >{{ trans('general.actions') }}</th>
            <th width="2%">{{trans('general.code')}}</th>
            <th>{{trans("sale-rent::general.invoice_date")}}</th>
            <th>{{ trans('user.client') }}</th>
            <th>{{ trans('sale-rent::general.value') }}</th>
           
            <th width="1%" class="text-center"> 
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_sale-invoice" onchange="changeChkboxes(this,'chkbx_sale-invoice')"/>
                </div>
            </th>
        </tr>
    </thead>
    <tbody>
       @foreach ($invoices as $invoice)
           <tr>
            <td class="text-center">
                <!-- Actoins -->
                @include('common.tableActions',['options' => [
                    ['text' => trans('general.edit'),'route' => ['sale-rent.invoices.edit',true],'permission' => 'sale-rent.sale-invoices_update','icon' => 'entypo-pencil'],
                ], 'name' => trans('sale-rent::general.invoice'). ' #' . $invoice->id,'id' => $invoice->id, 'delete_permission' => 'sale-rent.sale-invoices_delete','route_delete'=>'sale-rent.invoices.delete'])

              </td>
               <td onclick="goToEdit('{{ route('sale-rent.invoices.show',$invoice->id) }}')">#{{ $invoice->code }}</td>
               <td onclick="goToEdit('{{ route('sale-rent.invoices.show',$invoice->id) }}')">{{ $invoice->sale_date->format('d/m/Y') }}</td>
               <td onclick="goToEdit('{{ route('sale-rent.invoices.show',$invoice->id) }}')">{{ $invoice->customer_name }}</td>
               <td onclick="goToEdit('{{ route('sale-rent.invoices.show',$invoice->id) }}')">{{ number_format($invoice->amount, 2,$currency->decimal_mark,$currency->thousands_separator) }}</td>
               <td>
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_sale-invoice" name="sale-invoice"/>
                </div>
               </td>
           </tr>
       @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th >{{ trans('general.actions') }}</th>
            <th width="2%">{{trans('general.code')}}</th>
            <th>{{trans("sale-rent::general.invoice_date")}}</th>
            <th>{{ trans('user.client') }}</th>
            <th>{{ trans('sale-rent::general.value') }}</th>
           
            <th width="1%" class="text-center"> 
                <div class="checkbox checkbox-replace">
                    <input type="checkbox" class="chkbx_sale-invoice" onchange="changeChkboxes(this,'chkbx_sale-invoice')"/>
                </div>
            </th>
        </tr>
    </tfoot>
</table>

@section('modals')
    @include('common.modals.delete',['type'=>trans('sale-rent::general.invoice')])
@endsection
