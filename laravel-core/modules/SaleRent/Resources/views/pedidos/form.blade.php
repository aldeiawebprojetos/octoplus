<div class="row" >
  <div class="col-md-12">
   
    <div class="panel panel-primary" data-collapsed="0">
					
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">{{ trans('sale-rent::general.main_data') }}</div>
        
        <div class="panel-options">
          <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
          <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
          <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
          <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
      </div>
      
      <!-- panel body -->
      <div class="panel-body">
          <div class="form-group row">
            <div class="col-md-3">
              {!! Form::label('customer_id', trans('user.client'),['class'=>' control-label']) !!}
              <strong class="text-danger">*</strong>
            </div>
            <div class="col-md-7">
              <div class="input-group">
                <span class="input-group-addon"><i class="entypo-user"></i></span>
                @if($form_edit)
                {{ Form::text('customer_name', null, ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
                @else 
                <customer-select
                v-model="form.customer_name"
                client_type="1"
                {{(isset($readOnly) && $readOnly ? 'readonly' : '')}}
                value='{{ isset($invoice) ? $invoice->customer_name : null}}'
                placeholder="{{trans('sale-rent::general.type_to_search')}}"
                url="{{ route('customers.search') }}"
                ></customer-select>
                @endif
                {{ Form::hidden('customer_id', null,['class'=>'form-control input-lg','v-model'=>'form.customer_id', ]) }}
              </div>
              <span class="description">  </span>
            </div>
            <a href="{{ route('customers.create') }}" target="_blank" type="button" class="btn btn-white">
              <i class="fa fa-plus-square"></i>
            </a>
            <a id="showLink" target="_blank" type="button" class="btn btn-white hidden">
              <i class="fa fa-eye"></i>
            </a>
            @if(isset($invoice))
                <a href="{{ route('customers.show', $invoice->customer_id) }}" target="_blank" type="button" class="btn btn-white">
                  <i class="fa fa-eye"></i>
                </a>
            @endif
          </div>  

          {{ form::hidden('default_customer_route', route('customers.show','#customer_id#')) }}
          <div class="form-group row">
           <div class="col-md-3">
            {{ Form::label('sale_date', trans('sale-rent::general.date_sale'),['class' => 'control-label'] ) }}
           </div>
            <div class="col-md-7">
              <div class="input-group">
                <div class="input-group-addon"><a href="#"><i class="entypo-calendar"></i></a></div>
                {{ Form::date('sale_date',isset($invoice) ? $invoice->sale_date->format('Y-m-d') : date('Y-m-d') , ['class' => 'form-control datepicker  input-lg', 'v-model'=>'form.sale_date',(isset($readOnly) && $readOnly ? 'readonly': '')]) }}
              </div>
            </div>
          </div>

          <div class="form-group row">
           <div class="col-md-3">
            {{ Form::label('categories', trans("general.categories"), ['class' => 'control-label']) }}
           </div>
           
            <div class="col-md-7">
              {{Form::select('categories[]',$categories, ($form_edit ? $invoice->categories : null), ['class' => 'select2 form-control input-lg','multiple', 'id'=>'categories',(isset($readOnly) && $readOnly ? 'disabled': '')])}}
            </div>
            
            <a href="javascript:void(0);" onclick="$('#categoryAdd').modal('show');"  type="button" class="btn btn-white">
              <i class="fa fa-plus-square"></i>
            </a>
          </div>

          <div class="form-group row">
           <div class="col-md-3">
            {{ Form::label('description',trans("general.description"),['class' => 'control-label']) }}
           </div>
           
            <div class="col-md-7">
              {{ Form::textarea('description', null,['class' => 'form-control',(isset($readOnly) && $readOnly ? 'readonly': '')]) }}
            </div>
          </div>
      </div>
      
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary" data-collapsed="0">
					
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">{{ trans('general.items') }}</div>
        
        <div class="panel-options">

          <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
          <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
          <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
      </div>
      
      <!-- panel body -->
      <div class="panel-body table-responsive">
          <table class="table table-condensed table-bordered table-hover table-striped" id="items">
            <thead>
              <tr>
                <th class="col-md-1">{{ trans('general.actions') }}</th>
                <th class="col-md-4">{{ trans('general.name') }}</th>
                <th class="col-md-1">{{ trans('general.quantity') }}</th>
                <th class="col-md-2">{{ trans('general.price.unit') }}</th>
                <th class="col-md-3">{{ trans('general.tax') }}</th>
                <th class="col-md-1">{{ trans('sale-rent::general.total') }}</th>
              </tr>
            </thead>
            @include('sale-rent::pedidos.items',['type_item' => 'sale'])
           <tfoot>
             <tr>
               <td colspan="5" class="text-right"><strong>{{trans('sale-rent::general.sub_total')}}</strong></td>
               <td class="text-right">{{ totals.sub | maskMoney('<?= $currency->thousands_separator ?>','<?= $currency->decimal_mark ?>','<?= $currency->symbol ?>','<?= $currency->symbol_first ?>') }}</td>
             </tr>
             <tr>
              <td colspan="5" class="text-right" ><a href="javascript:void(0)" {{ !isset($readOnly) ? 'onclick="$(\'#desconto\').modal(\'show\')"' : ''}}><strong>{{trans_choice('general.add', 2, ['item' => trans('sale-rent::general.discount')])}}</strong></a></td>
              <td class="text-right">{{ totals.discount | maskMoney('<?= $currency->thousands_separator ?>','<?= $currency->decimal_mark ?>','<?= $currency->symbol ?>','<?= $currency->symbol_first ?>') }}</td>
             </tr>
             <tr>
              <td colspan="5" class="text-right"><strong>{{trans('sale-rent::general.tax')}}</strong></td>
              <td class="text-right">{{ totals.tax | maskMoney('<?= $currency->thousands_separator ?>','<?= $currency->decimal_mark ?>','<?= $currency->symbol ?>','<?= $currency->symbol_first ?>') }}</td>
             </tr>
             <tr>
              <td colspan="5" class="text-right"><strong>{{trans('sale-rent::general.total')}}</strong></td>
              <td class="text-right">{{ totals.total | maskMoney('<?= $currency->thousands_separator ?>','<?= $currency->decimal_mark ?>','<?= $currency->symbol ?>','<?= $currency->symbol_first ?>') }}</td>
             </tr>
           </tfoot>
          </table>
      </div>
      <input type="hidden" name="amount" v-model="totals.total">
      <input type="hidden" name="discount" v-model="totals.discount">
      <input type="hidden" name="invoice_type" value="sale">

      <input type="hidden" name="customer_name" v-model="form.customer_name">
      <input type="hidden" name="customer_email" v-model="form.customer_email">
      <input type="hidden" name="customer_tax_number" v-model="form.customer_tax_number">
      <input type="hidden" name="customer_phone" v-model="form.customer_phone">
      
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary" data-collapsed="0">
					
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">{{ trans('sale-rent::general.shipping') }} {{ trans('general.and') }} {{ trans('sale-rent::general.delivery') }}</div>
        
        <div class="panel-options">
          <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
          <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
          <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
          <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
      </div>
      
      <!-- panel body -->
      <div class="panel-body">
        <div class="form-group row">
          <div class="col-md-3">
           {{ Form::label('shipping_address',trans('sale-rent::general.shipping_address'),['class' => 'control-label']) }}
          </div>
          
           <div class="col-md-7">
             {{ Form::select('shipping_address',settings('shipping.type',frete_types()) ,null,['class' => 'form-control input-lg', 'placeholder' => trans('general.select').'...','@change' => 'onSelectFrete','v-model' => 'form.shipping_address',(isset($readOnly) && $readOnly || isset($invoice) ? 'disabled' : '')]) }}
           </div>
         </div>
         <div id="freteDetails">
          <div class="form-group row">
            <div class="col-md-3">
            </div>
            
             <div class="col-md-7">
               {{ Form::textarea('address_complete', null,['class' => 'form-control','rows'=>"5",'v-model' => 'form.address_complete',(isset($readOnly) && $readOnly || isset($invoice) ? 'readonly' : '')]) }}
             </div>
           </div>
  
           <div class="form-group row">
            <div class="col-md-3">
             {{ Form::label('shipping_value',trans('sale-rent::general.shipping_value'),['class' => 'control-label']) }}
            </div>
            
             <div class="col-md-7">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fas fa-money-bill-alt"></i>
                </div>
                <money
                decimal="{{$currency->decimal_mark}}"
                thousands="{{$currency->thousands_separator}}"
                {{(isset($readOnly) && $readOnly || isset($invoice) ? 'readonly' : '')}}                  
                class="form-control  input-lg"
                v-model="form.shipping_value"
                 ></money> 

                 {{ form::hidden('shipping_value', null, ['v-model'=>"form.shipping_value"]) }}
              </div>
             </div>
           </div>
         </div>
      </div>
      
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary" data-collapsed="0">
					
      <!-- panel head -->
      <div class="panel-heading">
        <div class="panel-title">{{ trans('sale-rent::general.payment') }}</div>
        
        <div class="panel-options">
          <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
          <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
          <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
          <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
        </div>
      </div>
      
      <!-- panel body -->
      <div class="panel-body">
        <div class="form-group row">
          <div class="col-md-3">
           {{ Form::label('payment_method',trans('sale-rent::general.payment_method'),['class' => 'control-label']) }}
          </div>
          
           <div class="col-md-7">
             {{ Form::select('payment_method', settings('payment.methods',getDefaultPaymentsMethods()),(isset($invoice) ? null : 'money'),['class' => 'form-control input-lg','v-model' => 'form.payment_method',(isset($readOnly) && $readOnly || isset($invoice) ? 'disabled' : '')]) }}
           </div>
         </div>

         <div class="form-group row">
          <div class="col-md-3">
           {{ Form::label('payment_conditions',trans('sale-rent::general.payment_conditions'),['class' => 'selectboxit ']) }}
          </div>
          
           <div class="col-md-7">
             {{ Form::select('payment_conditions',settings('payment.conditions',payemntMethod_types('recurrent')) ,null,['class' => 'form-control  input-lg' ,'v-model' => 'form.payment_conditions','@change' => 'onSelectConditions',(isset($readOnly) && $readOnly || isset($invoice) ? 'disabled' : '')]) }}
           </div>
         </div>
        <div id="paymentRecurrent" class="hidden">
          <hr>
          <div class="form-group">
            <div class="col-md-3">
             
            </div>
            <div class="col-md-3">
              {{ Form::select('payment_frequency',frequency_recurrent(),null,['class' => 'form-control  input-lg','v-model'=>'form.payment_frequency'])}}
            </div>
            <div class="col-md-3">
              {{ Form::number('payment_day',null,['class' => 'form-control input-lg'])}}
              <span class="description"> 
                {{ trans('sale-rent::general.invoice_expiration') }} 
                <button type="button" class="btn btn-default popover-default btn-xs" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{ trans('sale-rent::general.expiration_description') }}" data-original-title="{{ trans('sale-rent::general.invoice_expiration') }}"><i class="entypo-info"></i></button> 
              </span>
            </div>
            <div class="col-md-3">
              {{ Form::number('payment_quantity',null,['class' => 'form-control input-lg'])}}
               
									{{ trans('sale-rent::general.quantity_invoices') }}
										<button type="button" class="btn btn-default popover-default btn-xs" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{ trans('sale-rent::general.quantity_description') }}" data-original-title="{{ trans('sale-rent::general.quantity_invoices') }}"><i class="entypo-info"></i></button>
									
            </div>
          </div>
        </div>

        <div id="paymentPart" class="hidden">
          <hr>
          <div class="form-group row">
            <div class="col-md-3">
              {!! Form::label('quantity_parts', trans('sale-rent::general.quantity_parts'),['class' => 'control-label']) !!}
            </div>
            <div class="col-md-4">
              {{ Form::number('quantity_parts', null,['class'=>'form-control input-lg','min' => '1','@change'=> 'onPartsQuantity','@keyup'=> 'onPartsQuantity','v-model'=>'form.quantity_parts',(isset($readOnly) && $readOnly || isset($invoice) ? 'readonly' : '')]) }}
            </div>
          </div>
         
          @if(isset($readOnly) && $readOnly)
          <div id="paymentParts" class="">
            {{Form::hidden('default_url',route('financial.incomes.edit', '#row_id#'))}}
            @foreach($invoice->faturas as $key => $fatura)
              <div class="form-group row">
                <div class="col-md-3">
                  <label >
                    @if (preg_match('/'. trans("sale-rent::general.shipping") .'/', $fatura->code))
                        {{ trans('sale-rent::general.shipping') }}
                    @else
                        {{ trans('sale-rent::general.part') }} {{$key+1}}/{{$invoice->quantity_parts}}
                    @endif
                  </label>
                 
                </div>
                <div class="col-md-3">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="entypo-calendar"></i></div>
                  <input type="date" readonly  class="form-control input-lg" value="{{ $fatura->data_vencimento->format('Y-m-d') }}"  />
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fas fa-money-bill-alt"></i>
                    </div>
                      <money
                        decimal="{{$currency->decimal_mark}}"
                        thousands="{{$currency->thousands_separator}}"
                       
                        readonly
                        value="{{ number_format($fatura->value,2) }}"
                        class="form-control input-lg "
                        ></money>  
                  </div>
                </div>
                <div class="col-md-2" >
                  <div  style="height: 42px; padding-top:15px">
                      <span class="label label-{{ ( $fatura->status == 'canceled' ? 'primary' : ($fatura->status == 'settled' ? 'success' : ($fatura->vencendo ? 'warning' : ($fatura->vencido ? 'danger': 'default')))) }} ">#{{$fatura->code}}</span>
                  </div>                     
                </div>
                               
              </div>
            @endforeach
          </div>
         @else 
            <div id="paymentParts" class="">
              <hr>
              
              {{ form::hidden('date_format','dd/mm/yyyy') }}
              @if(isset($invoice))
              {{Form::hidden('default_url',route('financial.incomes.edit', '#row_id#'))}}
                  
                  <div class="form-group row" v-for="(row, index) in parts" :index="index">
                    <input type='hidden' :id="'type_'+index" :name="'parts['+index+'][type]'" v-model="row.type"/>
                    <input type="hidden" readonly :id="'id_'+index" :name="'parts['+index+'][id]'" v-model="row.id"   />
                    <div class="col-md-3">
                      <label v-if="typeof row.name == 'undefined'" :for="'part_'+index">
                        {{ trans('sale-rent::general.part') }} @{{ index +1  }}/@{{form.quantity_parts}}
                        
                      </label>
                      <label v-else>@{{ row.name }}</label>
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <div class="input-group-addon"><i class="entypo-calendar"></i></div>
                      <input type="date" readonly :id="'date_'+index" :name="'parts['+index+'][date]'" v-model="row.data_vencimento" class="form-control input-lg"  />
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fas fa-money-bill-alt"></i>
                        </div>
                          <money
                            decimal="{{$currency->decimal_mark}}"
                            thousands="{{$currency->thousands_separator}}"
                            :id="'value_'+index"
                            :name="'parts['+index+'][value]'"
                            v-model="row.value" 
                            readonly
                            :id="'value_'+index"
                            class="form-control input-lg "
                            ></money>  
                      </div>
                    </div>
                    <div class="col-md-2" :id="'code_'+index">
                      <div v-if="typeof row.code != 'undefined'" style="height: 42px; padding-top:15px">
                        <span :class="'label label-' + row.class" :id="'code_label_'+row.id"> #@{{ row.code }}</span>
                      </div>

                        <div v-else class="input-group">
                          <input type="text" :id="'description_'+index" :name="'parts['+index+'][description]'" v-model="row.description" class="form-control input-lg"  placeholder="{{ trans('general.description') }}"/>
                        </div>
                      
                    </div>
                    <div class="col-md-1" v-if="typeof row.link != 'undefined'" :id="'link_'+index" style="height: 42px;">
                      @can('financial.income_update')
                        <a :class="'btn btn-'+row.class" :href="row.link"><i class="entypo-pencil"></i></a>
                      @endcan
                    </div>
                  
                  </div>
                
              @else 
              <div class="form-group row" v-for="(row, index) in parts" :index="index">
                <div class="col-md-3">
                  <label :for="'part_'+index" v-if="row.type == 'part'">{{ trans('sale-rent::general.invoice') }} @{{ index +1  }}/@{{form.quantity_parts}}</label>
                  <label :for="'part_'+index" v-else> {{__('sale-rent::general.shipping')}} </label>
                </div>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon"><i class="entypo-calendar"></i></div>
                    <input type="date" :id="'date_'+index" :name="'parts['+index+'][date]'" class="form-control  input-lg" data-format='D, dd MM yyyy'  v-model="row.date" />
                  </div>
                </div>
                <input type='hidden' :id="'type_'+index" :name="'parts['+index+'][type]'" v-model="row.type"/>
                <div class="col-md-4">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fas fa-money-bill-alt"></i>
                    </div>
                      <money decimal="{{$currency->decimal_mark}}"thousands="{{$currency->thousands_separator}}" :name="'parts['+index+'][value]'" class="form-control input-lg" :id="'value_'+index"  v-model="row.value" ></money>  
                  </div>
                </div>
              </div>
              @endif
            </div>
         @endif
        </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="desconto">
  <div class="modal-dialog" style="margin-top: 80px;">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title"> <i class="entypo-folder"></i> {{ trans_choice('general.add',2,['item' => trans('sale-rent::general.discount')]) }} </h3>
      </div>
      
      <div class="modal-body">
     
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="form-group">
            {{Form::label('discount_rate', trans('sale-rent::general.discount'))}}
            <div class="input-group">
              {{Form::number('discount_rate', null, ['class' => 'form-control input-lg',])}}
              <span class="input-group-addon"><i class="fas fa-percent"></i></span>
            </div>

          </div>	
        </div>
      </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">{{ trans('general.close') }}</button>
        <button type="button" class="btn btn-green btn-lg btn-icon pull-right" @click="onAddDiscount">{{trans('general.save')}}<i class="entypo-floppy"></i></button>
      </div>
    </div>
  </div>
</div>

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/selectboxit/jquery.selectBoxIt.css')}}">
<style>
.table-condensed > tfoot > tr > td {
    padding: 1em !important;
}
.total-column {
    padding-top: 1.75rem !important;
}
</style>
<script type="text/javascript">
 
  
   var invoice_items = {!! json_encode(old('items', ($form_edit ? $invoice->items()->get() : false))) !!};

   @if($form_edit)
  var taxes_edit = {!! $taxes !!},
  invoice_data = {!! $invoice !!},
  parts_data = [],
  invoice_parts = {!! $invoice->faturas()->orderBy('data_vencimento')->get() !!};
   @endif
   var   translate_data = {
     part: "{{ trans('sale-rent::general.part') }}",
     shipping: "{{ trans('sale-rent::general.shipping') }}"
   };
 </script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script src="{{asset('js/sale-rent/invoices.js')}}" ></script>
<script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/js/selectboxit/jquery.selectBoxIt.min.js')}}"></script>



@endsection





