@extends('layouts.app')

@section('breadcrumb')
<li> <a href="{{route('tenant.items.index')}}">{{trans('sale-rent::general.invoices')}}</a> </li>

<li class="active"> <strong>{{trans_choice('general.list',2,['item' => trans('sale-rent::general.invoices')])}}</strong> </li>	
@endsection

@section('title', trans('sale-rent::general.invoices'))

@section('content')
<div class="mail-env">
    <div class="mail-body">
				
        <div class="mail-header">
            <!-- Search  form -->
            
            <form method="get" role="form" class="search-form-full">
                <div class="form-group">
                    
                    <input type="text" class="form-control input-lg" value="{{ Request::query('s') }}" name="s" id="search-input" placeholder="Pesquise pelo Nome, código..." />
                    <i class="entypo-search"></i>
                </div>
                
            </form>
        </div>

        <div class="mail-header">
            <!-- title -->
            <h1 class="mail-title">
                {{ trans('sale-rent::general.invoices') }}
                <span class="count text-info"></span>


            </h1>
                      

            <!-- Filtrar por Categoria Produto -->					
            <div class="mail-search">
              
            </div>
            <!-- search -->					
            <div class="pull-left">				
                <a href="#" class="btn btn-danger pull-left">
                    <i class="entypo-trash"></i>
                </a>
            </div>

        </div>
        

     @include('sale-rent::pedidos.table')
        
      {{$invoices->links()}}
    </div>

    <div class="mail-sidebar">
				
        <!-- compose new email button -->
        @can('sale-rent.sale-invoices_create')
            <div class="mail-sidebar-row hidden-xs">
                <a href="{{route('sale-rent.invoices.create')}}" class="btn btn-blue btn-lg btn-icon btn-block">
                    {{trans_choice('general.add',2,['item' => trans('sale-rent::general.invoice')])}}
                    <i class="entypo-plus"></i>
                </a>
            </div>
        @endcan
       

        <!-- menu -->
        <ul class="mail-menu">
           
        
        </ul>
        
        <div class="mail-distancer"></div>
        
        <h4>{{ trans('sale-rent::general.invoices') }}</h4>
       
        <!-- menu -->
        <ul class="mail-menu">
           
        </ul>
        
    </div>
    
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
    <script>
            jQuery( document ).ready( function( $ ) {
        var $table3 = jQuery("#table-4");
        var table3 = $table3.DataTable( {
        "sPaginationType": "full_numbers",
        "bPaginate": false,
        "sDom": 't'

        } );
    
    } );
    </script>
@endsection