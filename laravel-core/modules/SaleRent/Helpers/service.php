<?php

use Octoplus\Domain\Item\Models\Item;
use Modules\SaleRent\Models\Invoice;

if(!function_exists('getPedidoCodeByItemId'))
{
    function getPedidoCodeByItemId(int $item_id): string
    {
        $item = Item::find($item_id);

            if(empty($item->invoices->toArray()))
             {
                return false;
                
             }else{

                $invoice = $item->invoices->where('shipping_status','!=', 'closed')->last();

                if(isset($invoice->code))
                {
                    return $invoice->code;
                }else{
                    return false;
                }
               
             }
      
    }
}

if(!function_exists('getPedidoByItemId'))
{
    function getPedidoByItemId(int $item_id)
    {
        $item = Item::find($item_id);

            if(empty($item->invoices->toArray()))
             {
                return false;
                
             }else{

                $invoice = $item->invoices->where('shipping_status','!=', 'closed')->last();
                
                if(!empty($invoice))
                {
                    return $invoice;
                }else{
                    return false;
                }
               
             }
    }
}

if(!function_exists('getItemStatus'))
{
    function getItemStatus(string $status)
    {
        $shipping_status = [
            'waiting' => true,
            'delivered' => false,
            'closed' => true,
        ];

        return $shipping_status[$status];
    }
}

if(!function_exists('getDefaultPaymentsMethods'))
{
    function getDefaultPaymentsMethods()
    {
        return collect([
            'money' => trans('sale-rent::general.money'),
            'billet' => trans('sale-rent::general.billet'),
            'transfer' => trans('sale-rent::general.transfer'),
            'bank_check' => trans('sale-rent::general.bank_check'),
            'pix' => trans('sale-rent::general.pix'),
            'debit card' => trans('sale-rent::general.debit card'),
        ]);     
    }
}


if(!function_exists('verifyRentValidate'))
{
    function verifyRentValidate(Invoice $pedido)
    {
       
        $data_vencimento = $pedido->rent_endDate;
        $hoje = Carbon\Carbon::now();

      
        return $hoje->diffInDays($data_vencimento, false) <= settings('rent.aviso_vencimento',8);
    }
}

if(!function_exists('getFreteType'))
{
    function getFreteType($invoice)
    {
        $tipos = [
            'delivery_withdrawal' => trans('sale-rent::general.delivery_withdrawal'),
            'delivery' => trans('sale-rent::general.delivery'),
            'withdrawal' => trans('sale-rent::general.withdrawal'),
            'tenant_delivery' => trans('sale-rent::general.tenant_delivery')
        ];

        if($invoice->shipping_delivery && $invoice->shipping_withdrawal)
        {
            $invoice_tipo = 'delivery_withdrawal';
        }elseif (!$invoice->shipping_delivery && $invoice->shipping_withdrawal){
            $invoice_tipo = 'withdrawal';
        }
        elseif ($invoice->shipping_delivery && !$invoice->shipping_withdrawal){
            $invoice_tipo = 'delivery';
        }else{
            $invoice_tipo = 'tenant_delivery';
        }

        return $tipos[$invoice_tipo];
    }
}