const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix
    .js(__dirname + '/Resources/assets/js/invoices.js', 'js/sale-rent')
    ;

if (mix.inProduction()) {
    mix.version();
}