<?php

return [
    'name'                                        => 'Relatórios',
    'sale-invoices'                               => 'Pedidos de Venda',
    'rent-invoices'                               => 'Pedidos de Aluguel',
    'create'                                      => 'Cadastrar',
    'update'                                      => 'Atualizar',
    'show'                                        => 'Visualizar',
    'delete'                                      => 'Apagar',
    'purchase-invoices'                           => 'Pedidos de Compra',
    'Report'                                     => 'Relatórios',
    'report'                                     => 'Relatórios',
    'statement'                                   => 'Extrato',
    'history-items'                               => 'Histórico de Itens',
    'clientes-inadimplentes'                      => 'Clientes inadimplentes',
    'financial'                                   => 'Receitas/Despesas',
    'rent-items'                                  => 'Status Itens de locação'
];