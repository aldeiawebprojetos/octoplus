<?php

return [

    'name'                               => 'Relatórios',
    'reports'                            => 'Relatórios',
    'statement'                          => 'Extrato',
    'history_items_rent'                 => 'Histórico de Itens de Locação',
    'income/expenses'                    => 'Receitas/Despesas',
    'rent_items'                         => 'Status Itens de Locação',

];