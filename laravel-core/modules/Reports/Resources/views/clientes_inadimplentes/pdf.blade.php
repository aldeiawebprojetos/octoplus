<!DOCTYPE html>
<html>
    <style>
        table{
            width: 100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;    
        }
        .valor-total{
            text-align: right;
        }
    </style>
<body>

    <div>
        <div style="width: 20%;float: left;padding-right: 30px">
        <img src="{{$company->firstMedia('logo') != null ? $company->firstMedia('logo')->getUrl() : asset('/assets/images/logo.png')}}" alt="company_logo">
        </div>
        <div style="float: left">
            <h2>
                Relatório de clientes inadimplentes referente ao período de {{$periodo}}
            </h2>
        </div>
    </div>

    <br>

    <div class="valor-total">
        <h4 style="color: #ff5252">
            Valor total: 
        </h4>
        <h3>
            <span style="color: #ff5252">
                {{'R$ ' . number_format($faturasValue, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div> 

    <br>

    <table class="table table-bordered table-striped datatable" id="table-3">
        <thead>
            <tr>
                <th>Cliente</th>
                <th>Montante</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($faturas as $fatura)
                <tr>
                    <td>{{( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' )}}</td>
                    <td>{{'R$ ' . number_format($fatura->sum, 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
                </div>
                </tr>
            
            @endforeach
        </tbody>
    </table>

    <br>
    
    <div class="valor-total">
        <h4 style="color: #ff5252">
            Valor total: 
        </h4>
        <h3>
            <span style="color: #ff5252">
                {{'R$ ' . number_format($faturasValue, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div> 


</body>
</html>