@if($filter) 

    <div class="text-right">
        <h4 style="color: #ff5252">
            Valor total: 
        </h4>
        <h3>
            <span style="color: #ff5252">
                {{'R$ ' . number_format($faturasValue, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div> 

    <table class="table table-bordered table-striped datatable" id="table-3">
        <thead>
            <tr>
                <th>Cliente</th>
                <th>Montante</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($faturas as $fatura)
                <tr>
                    <td>{{( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' )}}</td>
                    <td>{{'R$ ' . number_format($fatura->sum, 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
                </div>
                </tr>
            
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Cliente</th>
                <th>Montante</th>
            </tr>
        </tfoot>
    </table>

    <div class="text-right">
        <h4 style="color: #ff5252">
            Valor total: 
        </h4>
        <h3>
            <span style="color: #ff5252">
                {{'R$ ' . number_format($faturasValue, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div> 
    
    @if($faturasValue != 0)
        <div class="text-center">
            <button class="btn btn-primary btn-icon icon-left hidden-print" onclick="filter('{{ route('reports.clientes-inadimplentes.generate-pdf') }}', true)">
                Imprimir Relatório
                <i class="entypo-doc-text"></i>
            </button>
        </div>
    @endif
@endif



<form action="{{ route('reports.clientes-inadimplentes.filter') }}" method="get" id="form-fatura">
    <input type="hidden" name="range_date" id="aux-date">
</form>

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script>
    jQuery( document ).ready( function( $ ) {


    var $table3 = jQuery("#table-3");

    var table3 = $table3.DataTable( {
        "aLengthMenu": [[ 25, 50, -1], [ 25, 50, "All"]],
        "ordering": false
    } );
    // Initalize Select Dropdown after DataTables is created
    $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
    minimumResultsForSearch: -1
    });

    } );
    
</script>
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}" >
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker/daterangepicker.js')}}"></script>

    <script>

        function filter (url, novaAba) {
                $('#aux-date').val($('#range_date_selec').val())
                $('#form-fatura').attr('action', url);
                if (novaAba){
                    $("#form-fatura").attr("target", "_blank");
                } else {
                    $("#form-fatura").attr("target", "");
                }
                $( "#form-fatura").submit();
        }

        $(function() {

            $('#range_date_selec').change(function (){
                filter('{{ route('reports.clientes-inadimplentes.filter') }}', false)
            });
        });

    </script>
@endsection