@extends('layouts.app')

@section('title', trans('reports::general.rent_items') )

@section('breadcrumb')
<li> {{ trans('reports::general.reports') }} </li>
<li class="active"> <strong>{{trans('reports::general.rent_items')}}</strong> </li>
@endsection

@section('content')
<hr>
<div class="mail-env">
		
   
   <!-- Mail Body -->
    <div class="mail-body">
        
        

        @include('reports::rent_items.table')
 

    </div>

    <!-- Sidebar -->
    <div class="mail-sidebar">

    <img src="{{isset($company->logo) ? $company->logo : ''}}" alt="company_logo" width="90%">

        <!-- Filtros -->
    

        <h4>Itens de locação</h4>

        <div class="mail-search">				
            {{ Form::select('item_status', $item_status, isset( Request::query()['item_status']) ? Request::query()['item_status'] : null, ['class' => 'form-control select2 select-item','onchange' =>'$("#searchForm").submit()','placeholder' => trans('general.select').'...']) }}
        </div>
        
    </div>
    
    
</div>
@endsection

