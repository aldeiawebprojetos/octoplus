<!DOCTYPE html>
<html>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;    
        }
        #valor-total{
            text-align: right;
        }
    </style>
<body>

    <h2>
        Relatório de Itens {{ $status }}
    </h2>

    <br>

    <table style="width:100%">
        <thead>
            <tr>
                <th>Nº</th>
                <th>Categoria</th>
                @if(Request::query()['item_status'] != 0)
                    <th>Nº do contrato</th>
                    <th>Contrato ativo</th>
                    <th>Início do contrato</th>
                    <th>Entregue</th>
                @else
                    <th>Data Retirada</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($items as $index => $item)
                @php
                    $pedido = getPedidoByItemId($item->id);
                    $data_retirada = null;
                    foreach($items_status_history as $its){
                        if($its->item_id == $item->id){
                            $data_retirada = $its->data;
                            break;
                        }
                    }
                @endphp
                <tr>
                    <td>{{ $item->code }}</td>
                    <td>{{ $item->categories->last()->name }}</td>
                    @if(Request::query()['item_status'] != 0)
                        <td>{{ $pedido ? $pedido->id : '' }}</td>
                        <td>{{ $pedido->active_contract ? 'Sim' : 'Não' }}</td>
                        <td>{{ $pedido ? $pedido->sale_date->format('d/m/Y') : '' }}</td>
                        <td>{{ $item->item_status == 'delivered' ? 'Sim' : 'Não' }}</td>
                    @else
                        <td>{{ isset($data_retirada) ? \Carbon\Carbon::parse($data_retirada)->startOfDay()->format('d/m/Y') : '' }}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
</body>
</html>