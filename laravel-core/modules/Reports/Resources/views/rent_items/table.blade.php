@if($filter && sizeof($items) != 0)
    <table class="table table-bordered table-striped datatable" id="table-3">
        <thead>
            <tr>
                <th>Nº</th>
                <th>Categoria</th>
                @if(Request::query()['item_status'] != 0)
                    <th>Nº do contrato</th>
                    <th>Contrato ativo</th>
                    <th>Início do contrato</th>
                    <th>Entregue</th>
                @else
                    <th>Data Retirada</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($items as $index => $item)
                @php
                    $pedido = getPedidoByItemId($item->id);
                    $data_retirada = null;
                    foreach($items_status_history as $its){
                        if($its->item_id == $item->id){
                            $data_retirada = $its->data;
                            break;
                        }
                    }
                @endphp
                <tr>
                    <td>{{ $item->code }}</td>
                    <td>{{ $item->categories->last()->name }}</td>
                    @if(Request::query()['item_status'] != 0)
                        <td>{{ $pedido ? $pedido->id : '' }}</td>
                        <td>{{ $pedido->active_contract ? 'Sim' : 'Não' }}</td>
                        <td>{{ $pedido ? $pedido->sale_date->format('d/m/Y') : '' }}</td>
                        <td>{{ $item->item_status == 'delivered' ? 'Sim' : 'Não' }}</td>
                    @else
                        <td>{{ isset($data_retirada) ? \Carbon\Carbon::parse($data_retirada)->startOfDay()->format('d/m/Y') : '' }}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Nº</th>
                <th>Categoria</th>
                @if(Request::query()['item_status'] != 0)
                    <th>Nº do contrato</th>
                    <th>Contrato ativo</th>
                    <th>Início do contrato</th>
                    <th>Entregue</th>
                @else
                    <th>Data Retirada</th>
                @endif
            </tr>
        </tfoot>
    </table>
    
    @if(sizeof($items) != 0)
        <div class="text-center">
            <button class="btn btn-primary btn-icon icon-left hidden-print" onclick="filter('{{ route('reports.rent-items.generate-pdf') }}', true)">
                Imprimir Relatório
                <i class="entypo-doc-text"></i>
            </button>
        </div>
    @endif
@endif



<form action="{{ route('reports.rent-items.filter') }}" method="get" id="form-item">
    <input type="hidden" name="item_status" id="aux_item_status">
</form>

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script>
    jQuery( document ).ready( function( $ ) {


    var $table3 = jQuery("#table-3");

    var table3 = $table3.DataTable( {
        "aLengthMenu": [[ 25, 50, -1], [ 25, 50, "All"]],
        "ordering": false
    } );
    // Initalize Select Dropdown after DataTables is created
    $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
    minimumResultsForSearch: -1
    });

    } );
    
</script>
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}" >
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker/daterangepicker.js')}}"></script>

    <script>

        function filter (url, novaAba) {
            $('#aux_item_status').val($('.select-item option:selected').val())
            $('#form-item').attr('action', url);
            if (novaAba){
                $("#form-item").attr("target", "_blank");
            } else {
                $("#form-item").attr("target", "");
            }
            $( "#form-item").submit();
        }

        $(function() {
            $('.select-item').change(function (){
                if ($('.select-item option:selected').val()) {
                    filter('{{ route('reports.rent-items.filter') }}', false)
                }
                
            });


        });

    </script>
@endsection