@extends('layouts.app')

@section('title', trans('reports::general.history_items_rent') )

@section('breadcrumb')
<li> {{ trans('reports::general.reports') }} </li>
<li class="active"> <strong>{{trans('reports::general.history_items_rent')}}</strong> </li>
@endsection

@section('content')
<hr>
<div class="mail-env">
		
   
   <!-- Mail Body -->
    <div class="mail-body">
        
        

        @include('reports::history_items_rent.table')
 

    </div>

    <!-- Sidebar -->
    <div class="mail-sidebar">

    <img src="{{isset($company->logo) ? $company->logo : ''}}" alt="company_logo" width="90%">

        <!-- Filtros -->
    

        <h4>Item</h4>

        <div class="mail-search">				
            {{ Form::select('items_id', $items, isset( Request::query()['item_id']) ? Request::query()['item_id'] : null, ['class' => 'form-control select2 select-item','onchange' =>'$("#searchForm").submit()','placeholder' => trans('general.select').'...']) }}
        </div>
        
    </div>
    
    
</div>
@endsection

