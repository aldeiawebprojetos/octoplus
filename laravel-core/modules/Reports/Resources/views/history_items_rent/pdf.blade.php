<!DOCTYPE html>
<html>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;    
        }
        #valor-total{
            text-align: right;
        }
    </style>
<body>

    <h2>
        Relatório de locação do item {{$item[0]}}
    </h2>

    <br>

    <div id="valor-total" style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
        <h4>
            Total: 
        </h4>
        <h3>
            <span>
                {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div>

    <br>

    <table style="width:100%">
        <tr>
            <th>Cliente</th>
            <th>Período</th>
            <th>Montante</th>
        </tr>
        @foreach($dados as $d)
            <tr>
                <td>{{ isset($d->customer->user) ? $d->customer->user->name : $d->customer_name}}</td>
                <td>
                {{$d->sale_date->format($companySettings['dateFormat'])}} - {{$d->rent_endDate->format($companySettings['dateFormat'])}}
                </td>
                <td>{{'R$ ' . number_format($d->faturas->sum('value'), 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
            </tr>
        @endforeach
    </table>

    <br>

    <div id="valor-total">
        <h4 style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
            Total: 
        </h4>
        <h3>
            <span style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
                {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div>


</body>
</html>