@if($filter) 

    <div class="text-right">
        <h4 style="color: #329932">
            Valor total: 
        </h4>
        <h3>
            <span style="color: #329932">
                {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div> 

    <table class="table table-bordered table-striped datatable" id="table-3">
        <thead>
            <tr>
                <th>{{ trans('user.client') }}</th>
                <th>Período</th>
                <th>{{ trans('financial::general.amount') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($invoices as $invoice)
                <tr>
                    <td>
                        {{ 
                            isset($invoice->customer->user) ? $invoice->customer->user->name : $invoice->customer_name
                        }}
                    </td>
                    <td>{{$invoice->sale_date->format($companySettings['dateFormat'])}} - {{$invoice->rent_endDate->format($companySettings['dateFormat'])}}</td>
                    <td>{{'R$ ' . number_format($invoice->faturas->sum('value'), 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
                </div>
                </tr>
            
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>{{ trans('user.client') }}</th>
                <th>Período</th>
                <th>{{ trans('financial::general.amount') }}</th>
            </tr>
        </tfoot>
    </table>

    <div class="text-right">
        <h4 style="color: #329932">
            Valor total: 
        </h4>
        <h3>
            <span style="color: #329932">
                {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div> 
    
    @if($total != 0)
        <div class="text-center">
            <button class="btn btn-primary btn-icon icon-left hidden-print" onclick="filter('{{ route('reports.history-items.generate-pdf') }}', true)">
                Imprimir Relatório
                <i class="entypo-doc-text"></i>
            </button>
        </div>
    @endif
@endif



<form action="{{ route('reports.history-items.filter') }}" method="get" id="form-item">
    <input type="hidden" name="item_id" id="aux_item_id">
</form>

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script>
    jQuery( document ).ready( function( $ ) {


    var $table3 = jQuery("#table-3");

    var table3 = $table3.DataTable( {
        "aLengthMenu": [[ 25, 50, -1], [ 25, 50, "All"]],
        "ordering": false
    } );
    // Initalize Select Dropdown after DataTables is created
    $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
    minimumResultsForSearch: -1
    });

    } );
    
</script>
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}" >
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker/daterangepicker.js')}}"></script>

    <script>

        function filter (url, novaAba) {
            $('#aux_item_id').val($('.select-item option:selected').val())
            $('#form-item').attr('action', url);
            if (novaAba){
                $("#form-item").attr("target", "_blank");
            } else {
                $("#form-item").attr("target", "");
            }
            $( "#form-item").submit();
        }

        $(function() {
            $('.select-item').change(function (){
                if ($('.select-item option:selected').val()) {
                    filter('{{ route('reports.history-items.filter') }}', false)
                }
                
            });


        });

    </script>
@endsection