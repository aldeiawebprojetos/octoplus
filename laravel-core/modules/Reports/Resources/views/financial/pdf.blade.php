<!DOCTYPE html>
<html>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;    
        }
        #valor-total{
            text-align: right;
        }
    </style>
<body>

    <h2>
        Relatório {{$tipo == 'incomes' ? 'de Receitas' : 'de Despesas'}} {{isset($periodo) ? 'do período de ' . $periodo : ''}} 
        {{isset($categoria) ? 'da categoria ' . $categoria : ''}}
    </h2>

    <br>

    <div id="valor-total">
        <h4 style="color: {{ $tipo == 'expenses' ? '#ff5252' : '#329932'}}">
            {{$tipo == 'expenses' ? 'Despesa total do período: ' : 'Receita total do período: '}}
        </h4>
        <h3>
            <span style="color: {{ $tipo == 'expenses' ? '#ff5252' : '#329932'}}">
                {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div>

    <br>

    <table style="width:100%">
        <tr>
            <th>Cliente</th>
            @if (!isset($categoria))
                <th>Categoria</th>
            @endif
            <th>Data de vencimento</th>
            <th>Montante</th>
        </tr>
        @foreach($dados as $d)
            <tr>
                <td>{{$d->customer->user['name']}}</td>
                @if (!isset($categoria))
                    <td>
                        @php
                            $aux = '-';
                            foreach($faturascateg as $fatcat){
                                if ($d->id == $fatcat->fatura_id) {
                                    $aux = $categorias[$fatcat->category_id];
                                    break;
                                }
                            }
                        @endphp
                        {{$aux}}
                    </td>
                @endif
                <td>
                    {{$d->data_vencimento->format($companySettings['dateFormat'])}}
                </td>
                <td>{{'R$ ' . number_format($d->value, 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
            </tr>
        @endforeach
    </table>

    <br>

    <div id="valor-total">
        <h4 style="color: {{ $tipo == 'expenses' ? '#ff5252' : '#329932'}}">
            {{$tipo == 'expenses' ? 'Despesa total do período: ' : 'Receita total do período: '}}
        </h4>
        <h3>
            <span style="color: {{ $tipo == 'expenses' ? '#ff5252' : '#329932'}}">
                {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div>


</body>
</html>