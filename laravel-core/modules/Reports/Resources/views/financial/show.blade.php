@extends('layouts.app')

@section('title', trans('reports::general.reports'))

@section('breadcrumb')
<li> <a href="">{{ trans('financial::general.name') }}</a> </li>
<li class="active"> <strong>{{trans('general.show')}}</strong> </li>		
@endsection

@section('content')				
		{{Form::model($fatura, ['class' => 'disabled' ])}}
		<div class="row">
			<!-- title -->
			<div class="col-md-6">
			   <h1 class="pull-left">
				  Relatório de {{$fatura->type == 'incomes' ? 'receita' : 'despesa'}}
			   </h1>
		   </div>
       </div>
       
		@include('reports::financial.form', ['readOnly' => true])

		{{ Form::close() }}
				
		<div class="clear"></div>
		<!-- Footer -->
@endsection