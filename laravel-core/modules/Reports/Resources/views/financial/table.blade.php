@if($filter)
    <div class="text-right">
        <h4 style="color: {{ $type == 'expenses' ? '#ff5252' : '#329932'}}">
            {{$type == 'expenses' ? 'Despesa total do período: ' : 'Receita total do período: '}}
        </h4>
        <h3>
            <span style="color: {{ $type == 'expenses' ? '#ff5252' : '#329932'}}">
                {{'R$ ' . number_format($faturasValue, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div>  



    <table class="table table-bordered table-striped datatable" id="table-3">
        <thead>
            <tr>
                <th>{{ trans('user.client') }}</th>
                @if(!isset($category))
                    <th>Categoria</th>
                @endif
                <th>{{ trans('financial::general.data_vencimento') }}</th>
                <th>{{ trans('financial::general.amount') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($faturas as $fatura)
            @php
                $fatura_id = $fatura->id;    
            @endphp
                <tr >
                    <td >
                        {{ 
                            ( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' )
                        }}
                    </td>
                    @if(!isset($category))
                        <td>
                            @php
                                $aux = '-';
                                foreach($faturas_categ as $fatcat){
                                    if ($fatura->id == $fatcat->fatura_id) {
                                        $aux = $categories[$fatcat->category_id];
                                        break;
                                    }
                                }
                            @endphp
                            {{$aux}}
                        </td>
                    @endif
                    <td >{{$fatura->data_vencimento->format($companySettings['dateFormat'])}}</td>
                    <td >{{'R$ ' . number_format($fatura->value, 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
                </div>
                </tr>
            
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>{{ trans('user.client') }}</th>
                @if(!isset($category))
                    <th>Categoria</th>
                @endif
                <th>{{ trans('financial::general.data_vencimento') }}</th>
                <th>{{ trans('financial::general.amount') }}</th>
            </tr>
        </tfoot>
    </table>



    <div class="text-right">
        <h4 style="color: {{ $type == 'expenses' ? '#ff5252' : '#329932'}}">
            {{$type == 'expenses' ? 'Despesa total do período: ' : 'Receita total do período: '}}
        </h4>
        <h3>
            <span style="color: {{ $type == 'expenses' ? '#ff5252' : '#329932'}}">
                {{'R$ ' . number_format($faturasValue, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div>

    <br>
    @if($faturasValue != 0)
        <div class="text-center">
            <button class="btn btn-primary btn-icon icon-left hidden-print" onclick="filter('{{ route('reports.financial.generate-pdf') }}', true)">
                Imprimir Relatório
                <i class="entypo-doc-text"></i>
            </button>
        </div>
    @endif
@endif


<form action="{{ route('reports.financial.filter') }}" method="get" id="form-category">
    <input type="hidden" name="category_id" id="category_id">
    <input type="hidden" name="type" id="type">
    <input type="hidden" name="range_date" id="aux-date">
</form>

@section('modals')
    @include('common.modals.delete',['type'=>trans('financial::general.incomes')])
    @include('financial::modals.cancel')
@endsection

@section('scripts')

<style>
    @media print{
        #table-3{
            display: none;
        }
    }
</style>

<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script>
    jQuery( document ).ready( function( $ ) {
    var $table3 = jQuery("#table-3");
    var table3 = $table3.DataTable( {
        "aLengthMenu": [[ 25, 50, -1], [ 25, 50, "All"]],
        "ordering": false
    } );
    // Initalize Select Dropdown after DataTables is created
    $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
    minimumResultsForSearch: -1
    });

    } );
    
</script>
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}" >
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker/daterangepicker.js')}}"></script>

    <script>
        if(performance.navigation.type == 2){
            location.reload(true);
        }

        function filter (url, novaAba) {
            $('#category_id').val($('.select-category option:selected').val())
            $('#aux-date').val($('#range_date_selec').val())
            $('#type').val($('input[name="type-option"]:checked').val())
            $('#form-category').attr('action', url);
            if (novaAba){
                $("#form-category").attr("target", "_blank");
            } else {
                $("#form-category").attr("target", "");
            }
            $( "#form-category").submit();
        }

        $(function() {
            $('.select-category').change(function (){
                filter('{{ route('reports.financial.filter') }}', false)
            });

            $('#range_date_selec').change(function (){
                filter('{{ route('reports.financial.filter') }}', false)
            });

        });


        function cancelFatura(url,code)
        {
           
           var form = document.getElementById('cancel'), title = document.getElementById("cancel_code");
          
            title.innerHTML = code;
            form.action = url;

            $.noConflict(true);
            $('#modalCancel').modal();
            
        }
    </script>
@endsection