@extends('layouts.app')

@section('title', trans('reports::general.income/expenses') )

@section('breadcrumb')
<li> {{ trans('reports::general.reports') }} </li>
<li class="active"> <strong>{{trans('reports::general.income/expenses')}}</strong> </li>
@endsection

@section('content')
<h3 class="mail-title">
            <fieldset>
                <input type="radio" id="income-option" name="type-option" value="incomes" {{$type == 'incomes' ? 'checked' : 'aaaa'}}
                    onclick="location.href='{{ route('reports.financial.index') }}'">
                <label for="income-option">Receitas</label>
                &nbsp;&nbsp;

                <input type="radio" id="expense-option" name="type-option" value="expenses" {{$type == 'expenses' ? 'checked' : 'aaaa'}}
                    onclick="location.href='{{ route('reports.financial.index', ['type' => 'expenses']) }}'">
                <label for="expense-option">Despesas</label>
            </fieldset>
    </h3>

<div class="mail-env">
    <!-- title -->

		
   
   <!-- Mail Body -->
    <div class="mail-body">
        
        {{ Form::hidden('range_date', isset( Request::query()['range_date']) ? Request::query()['range_date'] : null,['id' => 'range_date']) }}


        @include('reports::financial.table')

    </div>
    
    <!-- Sidebar -->
    <div class="mail-sidebar">

    <img src="{{isset($company->logo) ? $company->logo : ''}}" alt="company_logo" width="90%">

        <!-- Filtros -->
    

        <!-- Filtrar por Categoria Financeiro -->	

        <h4>Categoria</h4>

        <div class="mail-search">				
            {{ Form::select('category_id', $categories,isset( Request::query()['category_id']) ? Request::query()['category_id'] : null,['class' => 'form-control select2 select-category','onchange' =>'$("#searchForm").submit()','placeholder' => trans('general.select').'...']) }}
        </div>



        <div class="mail-distancer"></div>



        <h4>Período</h4>

        <div class="mail-menu">
                {{ Form::text('range_date',isset( Request::query()['range_date']) ? Request::query()['range_date'] : null,['class' => ' form-control daterange  daterange-inline add-ranges active', 'id' => 'range_date_selec', 'data-format'=>"DD/M/YYYY"])}}
        </div>
        
    </div>
    
</div>
@endsection

