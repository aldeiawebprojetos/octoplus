<div class="row">

    <div class="col-sm-12">
        
        <div class="panel panel-info" data-collapsed="0">
            
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-md-3">
                      {!! Form::label('customer_id', trans('user.client'),['class'=>' control-label']) !!}
                    </div>
                    <div class="col-md-7">

                          {{ Form::text('customer_name',( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' ), ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled']) }}
                        
                    </div>

                            
                  </div>

                  <!-- Datas -->
                  <div class="form-group row">
                    <div class="col-md-3">
                    {!! Form::label('data_competencia', trans('financial::general.data_competencia'),['class'=>' control-label']) !!}
                    </div>
                    
                    <div class="col-sm-7">
                        {{ Form::date('data_competencia',  isset($fatura->data_competencia) ? $fatura->data_competencia : \Carbon\Carbon::now(), ['class' => 'form-control input-lg', 'v-model' => 'form.data_competencia',(isset($fatura) && $fatura->paid ? 'readonly' : (isset($readOnly) && $readOnly ? 'readonly' : ''))]) }}
                    </div>
                  </div>
    
                  <div class="form-group row">
                    <div class="col-md-3">
                      {!! Form::label('data_vencimento', trans('financial::general.data_vencimento'),['class'=>' control-label']) !!}
                    </div>
                    
                    <div class="col-sm-7">
                        {{ Form::date('data_vencimento', isset($fatura) ? $fatura->data_vencimento : \Carbon\Carbon::now(), ['class' => 'form-control input-lg', 'v-model' => 'form.data_vencimento',(isset($fatura) && $fatura->paid ? 'readonly' : (isset($readOnly) && $readOnly ? 'readonly' : ''))]) }}

                    </div>
                  </div>

                  <!-- Datas -->
                  @if (count($fatura->categories) > 0)
                    <div class="form-group">
                        <div class="form-group row">
                          <div class="col-md-3">
                          {{ Form::label('categories', trans("general.categories"), ['class' => 'control-label']) }}
                          </div>
                          
                          <div class="col-md-7">
                            {{Form::select('categories[]',$categories, (isset($fatura) ? $fatura->categories : []), ['class' => 'select2 form-control input-lg','multiple'=>'true', 'id'=>'categories',(isset($fatura) && $fatura->paid ? 'disabled' : (isset($readOnly) && $readOnly ? 'disabled' : ''))])}}
                          </div>
                          
                        </div>
                    </div>
                  @endif

                  <!-- Descrição -->
                  @if ($fatura->description)
                    <div class="form-group row">
                        <div class="col-md-3">
                        {{ Form::label('description',trans("general.description"),['class' => 'control-label']) }}
                        </div>
                        
                        <div class="col-md-7">
                          {{ Form::textarea('description', null,['class' => 'form-control','v-model' => 'form.description',(isset($fatura) && $fatura->paid ? 'readonly' : (isset($readOnly) && $readOnly ? 'readonly' : ''))]) }}
                        </div>
                    </div>
                  @endif

                   <div class="form-group row">
                    <div class="col-md-3">
                      <label class="control-label" for="value">{{trans('general.price.value')}} </label>
                    </div>
                    <div class="col-md-7">

                      {{ Form::text("vlaue", 'R$ ' . number_format($fatura->value,2,$currency->decimal_mark,$currency->thousands_separator),['class' => 'form-control input-lg', 'readonly']) }}

                    </div>
                   </div>

                   <div class="form-group row">
                    <div class="col-md-3">
                      {{ Form::label('account_id', trans('financial::general.account'), ['class' => 'control-label']) }}</div>
                    
                      <div class="col-md-7">

                          {{ Form::text('customer_name',( isset( $fatura->account->name ) ? $fatura->account->name : '' ), ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled']) }}

                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-md-3">
                      {{ Form::label('payment_method', trans('sale-rent::general.payment_method'), ['class' => 'control-label']) }}</div>
                    

                    <div class="col-md-7">
                          {{ Form::text('customer_name',( isset( $fatura->payment_method ) ? getDefaultPaymentsMethods()[$fatura->payment_method] : '' ), ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled']) }}

                    </div>
                  </div>
                  <hr>

                   <div class="form-group row">


                    <div class="col-md-3">
                      {{ Form::label('nota_fiscal', trans('financial::general.nf_emmited'), ['class' => 'control-label']) }}</div>
                    

                    <div class="col-md-7">

                          {{ Form::text('customer_name',( isset( $fatura->nota_fiscal ) ? $fatura->nota_fiscal == 0 ? 'Não' : 'Sim'  : '' ), ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled']) }}

                    </div>

                    
                  </div>
                  <hr>

                  <div class="form-group row">
                  <div class="col-md-3">
                      {{ Form::label('boleto', trans('financial::general.billet_emmited'), ['class' => 'control-label']) }}</div>
                   
                    
                    
                    <div class="col-md-7">
                          {{ Form::text('customer_name',( isset( $fatura->boleto ) ? $fatura->boleto == 0 ? 'Não' : 'Sim'  : '' ), ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled']) }}

                    </div>
                  </div>
                 
                      <hr>
                      <div class="form-group row">
                        <div class="col-md-3">
                          {{ Form::label('recurrent', trans("general.recurrent"),['class' => 'control-label']) }}
                        </div>


                        <div class="col-md-7">

                              {{ Form::text('customer_name',( isset( $fatura->recurrent ) ? $fatura->recurrent == 0 ? 'Não' : 'Sim'  : '' ), ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled']) }}

                        </div>
                        
                      </div>

                  <div id="paymentRecurrent" class="collapse {{ isset($fatura) && $fatura->recurrent ? 'in' : ''}}">
                  <hr>
                    <div class="form-group row">
                      <div class="col-md-3">
                        
                        {{ Form::label('recurrent', 'Frequência de pagamento',['class' => 'control-label']) }}
                      </div>
                      <div class="col-md-7">
                        {{ Form::text('customer_name',( isset( $fatura->payment_frequency ) ? 'Mensal' : '' ), ['class' => 'form-control input-lg','v-model' => 'form.customer_name','disabled']) }}
                      </div>
                      
                    </div>

                    <div class="form-group row">
                      <div class="col-md-3">
                        
                        
                      </div>

                      <div class="col-md-7">
                          {{ Form::number('payment_quantity',null,['class' => 'form-control input-lg', 'v-model' => 'form.payment_quantity',(isset($readOnly) && $readOnly ? 'readonly' : (isset($fatura) && $fatura->recurrent ? 'disabled' : ''))])}}
                          {{ trans('sale-rent::general.quantity_invoices') }}
                      </div>
                    </div>
                  </div>

                  <hr>
                  @if(isset($fatura) && $fatura->status == 'partial')
                 <div class="form-group row">
                   <div class="col-md-3"></div>
                  <div class="col-md-3">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fas fa-money-bill-wave-alt"></i></span>
                        <money
                        decimal="{{$currency->decimal_mark}}"
                        thousands="{{$currency->thousands_separator}}"
                        name="balance"
                        id="balance"
                        readonly
                        v-model="form.balance"
                        value="{{(isset($fatura->total_balance) ? number_format($fatura->total_balance,2,'.','') :(!empty(old('total_balance')) ? old('total_balance'): null))}}"
                        class="form-control input-lg"
                        ></money>
                    </div>
                      {{ trans('financial::general.balance') }}
                      
                  </div>
                 </div>
                  @endif
                 
                  
            </div>
        </div>
    </div>
</div>

<div class="text-right">
  <a href="javascript:window.print()" class="btn btn-primary btn-icon icon-left hidden-print">
    Imprimir Relatório
    <i class="entypo-doc-text"></i>
  </a>
</div>

@section("scripts")
	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="{{asset("assets/css/font-icons/font-awesome/css/font-awesome.min.css")}}">
	<link rel="stylesheet" href="{{asset("assets/js/select2/select2-bootstrap.css")}}">
	<link rel="stylesheet" href="{{asset("assets/js/select2/select2.css")}}">
	<link rel="stylesheet" href="{{asset("assets/js/selectboxit/jquery.selectBoxIt.css")}}">

	<!-- Imported scripts on this page -->
	<script src="{{asset("assets/js/select2/select2.min.js")}}"></script>
	<script src="{{asset("assets/js/jquery.inputmask.bundle.js")}}"></script>
	<script src="{{asset("assets/js/bootstrap-datepicker.js")}}"></script>
  <script src="{{asset("assets/js/selectboxit/jquery.selectBoxIt.min.js")}}"></script>
    <script>
        
        @if(isset($formEdit) or !empty(old()))
        const editStatus = true;
        @else
        const editStatus = false;
        @endif
    </script>
@if(!isset($readOnly))
  <script src="{{asset("js/financial/incomes.js")}}?v={{ filemtime('js/financial/incomes.js') }}"></script>
@endif
@endsection