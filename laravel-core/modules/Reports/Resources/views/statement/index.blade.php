@extends('layouts.app')

@section('title', trans('reports::general.statement') )

@section('breadcrumb')
<li> {{ trans('reports::general.reports') }} </li>
<li class="active"> <strong>{{trans('reports::general.statement')}}</strong> </li>
@endsection

@section('content')
<hr>
<div class="mail-env">
		
   
   <!-- Mail Body -->
    <div class="mail-body">
        
        

        @include('reports::statement.table')
 

    </div>

    <!-- Sidebar -->
    <div class="mail-sidebar">

        <img src="{{isset($company->logo) ? $company->logo : ''}}" alt="company_logo" width="90%">

        <!-- Filtros -->
    

        <h4>Período</h4>

        <div class="mail-menu">
                {{ Form::text('range_date',isset( Request::query()['range_date']) ? Request::query()['range_date'] : null,['class' => ' form-control daterange  daterange-inline add-ranges active', 'id' => 'range_date_selec', 'data-format'=>"DD/M/YYYY"])}}
        </div>
        
    </div>
    
    
</div>
@endsection

