<!DOCTYPE html>
<html>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: left;    
        }
        #valor-total{
            text-align: right;
        }
    </style>
<body>

    <h2>
        Extrato do período {{$periodo}}
    </h2>

    <br>

    <div id="valor-total">
        <h4 style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
            Total: 
        </h4>
        <h3>
            <span style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
                {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div>

    <br>

    <table style="width:100%">
        <tr>
            <th>Cliente</th>
            <th>Categoria</th>
            <th>Data de vencimento</th>
            <th>Montante</th>
        </tr>
        @foreach($dados as $d)
            <tr style="background-color: {{ $d->type == 'expenses' ? '#f4c2bf' : '#add6ad'}}">
                <td>{{( isset( $d->customer->user['name'] ) ? $d->customer->user['name'] : '' )}}</td>
                <td>
                    @php
                        $aux = '-';
                        foreach($faturascateg as $fatcat){
                            if ($d->id == $fatcat->fatura_id) {
                                $aux = $categorias[$fatcat->category_id];
                                break;
                            }
                        }
                    @endphp
                    {{$aux}}
                </td>
                <td>
                    {{$d->data_vencimento->format($companySettings['dateFormat'])}}
                </td>
                <td>{{'R$ ' . number_format($d->value, 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
            </tr>
        @endforeach
    </table>

    <br>

    <div id="valor-total">
        <h4 style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
            Total: 
        </h4>
        <h3>
            <span style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
                {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
            </span>
        </h3>
    </div>


</body>
</html>