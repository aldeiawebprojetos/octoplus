@if($filter)
    <div class="text-right">
        <h3>
            Total: 
                <span style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
                    {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
                </span>
        </h3>
    </div>    



    <table class="table table-bordered table-striped datatable" id="table-3">
        <thead>
            <tr>
                <th>{{ trans('user.client') }}</th>
                <th>Categoria</th>
                <th>{{ trans('financial::general.data_vencimento') }}</th>
                <th>{{ trans('financial::general.amount') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($faturas as $fatura)
            @php
                $fatura_id = $fatura->id;    
            @endphp
                <tr>
                    <td  style="background-color: {{ $fatura->type == 'expenses' ? '#ff000028' : '#00800028'}}">
                        {{ 
                            ( isset( $fatura->customer->user['name'] ) ? $fatura->customer->user['name'] : '' )
                        }}
                    </td>
                    <td  style="background-color: {{ $fatura->type == 'expenses' ? '#ff000028' : '#00800028'}}">
                        @php
                            $aux = '-';
                            foreach ( $faturas_categ as $fat ) {
                                if ( $fatura_id == $fat->fatura_id ) {
                                    $aux = $categories[$fat->category_id];
                                }
                            }
                        @endphp
                        {{$aux}}
                    </td>
                    <td style="background-color: {{ $fatura->type == 'expenses' ? '#ff000028' : '#00800028'}}">{{$fatura->data_vencimento->format($companySettings['dateFormat'])}}</td>
                    <td  style="background-color: {{ $fatura->type == 'expenses' ? '#ff000028' : '#00800028'}}">{{'R$ ' . number_format($fatura->value, 2,$currency->decimal_mark,$currency->thousands_separator)}}</td>
                </div>
                </tr>
            
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>{{ trans('user.client') }}</th>
                <th>Categoria</th>
                <th>{{ trans('financial::general.data_vencimento') }}</th>
                <th>{{ trans('financial::general.amount') }}</th>
            </tr>
        </tfoot>
    </table>


    <div class="text-right">
        <h3>
            Total: 
                <span style="color: {{ $total < 0 ? '#ff5252' : '#329932'}}">
                    {{'R$ ' . number_format($total, 2,$currency->decimal_mark,$currency->thousands_separator)}}
                </span>
        </h3>
    </div>

    <br>
    @if($total != 0)
        <div class="text-center">
            <button class="btn btn-primary btn-icon icon-left hidden-print" onclick="filter('{{ route('reports.statement.generate-pdf') }}', true)">
                Imprimir Relatório
                <i class="entypo-doc-text"></i>
            </button>
        </div>
    @endif
@endif



<form action="{{ route('reports.statement.filter') }}" method="get" id="form-category">
    <input type="hidden" name="range_date" id="aux-date">
</form>

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script>
    jQuery( document ).ready( function( $ ) {


    var $table3 = jQuery("#table-3");

    var table3 = $table3.DataTable( {
        "aLengthMenu": [[ 25, 50, -1], [ 25, 50, "All"]],
        "ordering": false
    } );
    // Initalize Select Dropdown after DataTables is created
    $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
    minimumResultsForSearch: -1
    });

    } );
    
</script>
<link rel="stylesheet" href="{{asset('assets/js/daterangepicker/daterangepicker-bs3.css')}}" >
<script src="{{asset('assets/js/moment.min.js')}}"></script>
<script src="{{asset('assets/js/daterangepicker/daterangepicker.js')}}"></script>

    <script>

        function filter (url, novaAba) {
            $('#aux-date').val($('#range_date_selec').val())
            $('#form-category').attr('action', url);
            if (novaAba){
                $("#form-category").attr("target", "_blank");
            } else {
                $("#form-category").attr("target", "");
            }
            $( "#form-category").submit();
        }

        $(function() {

            $('#range_date_selec').change(function (){

                filter('{{ route('reports.statement.filter') }}', false)
            });

        });

    </script>
@endsection