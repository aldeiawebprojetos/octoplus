<?php

use Illuminate\Support\Facades\Route;

    Route::group(['middleware' =>  ['web','tenant','admin','bindings'], 'prefix'=>'reports', 'as'=>'reports.','namespace'=>'Modules\Reports\Http\Controllers'], function () {
                
        Route::group(['prefix' => 'statement', 'as'=>'statement.'], function () {
            Route::get('/', 'Statement@index')->name('index');
            Route::get('/filter', 'Statement@filter')->name('filter');
            Route::get('/generate-pdf', 'Statement@generatePDF')->name('generate-pdf');
        });

        Route::group(['prefix' => 'financial', 'as'=>'financial.'], function () {
            Route::get('/', 'Financial@index')->name('index');
            Route::get('/filter', 'Financial@filter')->name('filter');
            Route::get('/generate-pdf', 'Financial@generatePDF')->name('generate-pdf');
        });

        Route::group(['prefix' => 'history-items', 'as'=>'history-items.'], function () {
            Route::get('/', 'HistoryItems@index')->name('index');
            Route::get('/filter', 'HistoryItems@filter')->name('filter');
            Route::get('/generate-pdf', 'HistoryItems@generatePDF')->name('generate-pdf');
        });

        Route::group(['prefix' => 'rent-items', 'as'=>'rent-items.'], function () {
            Route::get('/', 'RentItems@index')->name('index');
            Route::get('/filter', 'RentItems@filter')->name('filter');
            Route::get('/generate-pdf', 'RentItems@generatePDF')->name('generate-pdf');
        });

        Route::group(['prefix' => 'clientes-inadimplentes', 'as'=>'clientes-inadimplentes.'], function () {
            Route::get('/', 'ClientesInadimplentes@index')->name('index');
            Route::get('/filter', 'ClientesInadimplentes@filter')->name('filter');
            Route::get('/generate-pdf', 'ClientesInadimplentes@generatePDF')->name('generate-pdf');
        });
    });