<?php

namespace Modules\Reports\Listeners;


use Octoplus\Events\Menu\AdminCreated as Event;

class AddToAdminMenu
{
    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
       
        $menu = $event->menu;
            if(auth()->user()->can('reports.statement_show') || auth()->user()->can('reports.history-items_show') || 
            auth()->user()->can('reports.financial_show') || auth()->user()->can('reports.clientes-inadimplentes_show')){
                $menu->dropdown(trans('reports::general.reports'),function($sub){
                    if(auth()->user()->can('reports.statement_show')){
                        $sub->route('reports.statement.index', trans('reports::general.statement'), [],['icon' => 'entypo-doc-text-inv']);
                    }

                    if(auth()->user()->can('reports.history-items_show')){
                        $sub->route('reports.history-items.index', trans('reports::general.history_items_rent'), [],['icon' => 'fas fa-barcode']);
                    }

                    if(auth()->user()->can('reports.rent-items_show')){
                        $sub->route('reports.rent-items.index', trans('reports::general.rent_items'), [],['icon' => 'fas fa-barcode']);
                    }

                    if(auth()->user()->can('reports.financial_show')){
                        $sub->route('reports.financial.index', trans('reports::general.income/expenses'), [],['icon' => 'fas fa-dollar-sign']);
                    }

                    if(auth()->user()->can('reports.clientes-inadimplentes_show')){
                        $sub->route('reports.clientes-inadimplentes.index', 'Clientes inadimplentes', [],['icon' => 'fas fa-dollar-sign']);
                    }

                },[
                    'icon'=>'far fa-chart-bar'  
                ]);
            }

    }
}