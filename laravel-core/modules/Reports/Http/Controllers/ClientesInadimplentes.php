<?php

namespace Modules\Reports\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Financial\Models\Fatura;
use Modules\SaleRent\Models\Invoice;
use Modules\Customers\Models\Customer;
use Modules\Customers\Models\CustomerUser;
use Octoplus\Domain\Item\Models\Item;
use Octoplus\Domain\Category\Models\Category;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Account;
use PDF;

class ClientesInadimplentes extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {

        $company = Company::find(session("tenant"));
        $filter = false;

        return view('reports::clientes_inadimplentes.index', compact('filter', 'company'));
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function filter(Request $request){

        $query = Fatura::where('type', 'incomes')->where('status',  'open')->where( 'data_vencimento', '<', date('Y-m-d') )->groupBy('customer_id')
        ->selectRaw('sum(value) as sum, customer_id');

        if(isset($request->range_date))
        {
            $query->whereBetween('data_vencimento', $this->formataRangeDate($request));
        }

        $faturasValue = 0;
        foreach ($query->get() as $fatura){
            $faturasValue += $fatura->sum;
        }
        $faturaCount = $query->count();
  
        $faturas = $query->paginate()->withQueryString();

        $company = Company::find(session("tenant"));
        $filter = true;
          
        return view('reports::clientes_inadimplentes.index', compact( 
            'faturas', 'faturasValue','faturaCount', 'filter', 'company'
        ));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function generatePDF(Request $request)
    {

        $query = Fatura::where('type', 'incomes')->where('status',  'open')->where( 'data_vencimento', '<', date('Y-m-d') )->groupBy('customer_id')
        ->selectRaw('sum(value) as sum, customer_id');

        if(isset($request->range_date))
        {
            $query->whereBetween('data_vencimento', $this->formataRangeDate($request));
        }

        $faturasValue = 0;
        foreach ($query->get() as $fatura){
            $faturasValue += $fatura->sum;
        }


        $company = Company::find(session("tenant"));



        $data = [
            'faturas' => $query->get(),
            'company' => $company,
            'faturasValue' => $faturasValue,
            'periodo' => $request->range_date
        ];

		$pdf = PDF::loadView('reports::clientes_inadimplentes.pdf', $data);
		return $pdf->stream('document.pdf');

    }


    private function formataRangeDate(Request $request): array
    {
        
        $dateFormated = array();
        $rangedateArray = explode(' - ', $request->range_date);

        $rangedateArray = array_map(function($value){
            
            return date('Y-m-d',strtotime(str_replace('/','-',$value)));
        }, $rangedateArray);

       //Formata a data para colocar a hora para o SQL ter precisão da busca

        $dateFormated = [$rangedateArray[0]." 00:00:00",
                         $rangedateArray[1]." 23:59:59"];

        return $dateFormated;
    }
}
