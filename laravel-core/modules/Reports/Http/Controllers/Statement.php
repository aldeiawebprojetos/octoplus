<?php

namespace Modules\Reports\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Modules\Financial\Models\Fatura;
use Illuminate\View\View;
use Octoplus\Domain\Category\Models\Category;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Account;
use PDF;


class Statement extends Controller
{


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request){

        $filter = false;
           
        $company = Company::find(session("tenant"));

        return view('reports::statement.index', compact('filter', 'company'
        ));
    }



    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function filter(Request $request){
        
        $query = Fatura::where('status',  'settled' )->orderBy('data_recebimento');
        $receitas = Fatura::where([['type', 'incomes'], ['status',  'settled']]);
        $despesas = Fatura::where([['type', 'expenses'], ['status',  'settled']]);

        if(isset($request->range_date))
        {
            $query->whereBetween('data_recebimento',$this->formataRangeDate($request));
            $receitas->whereBetween('data_recebimento',$this->formataRangeDate($request));
            $despesas->whereBetween('data_recebimento',$this->formataRangeDate($request));
        }

        $total = $receitas->sum('value') - $despesas->sum('value');
        $faturaCount = $query->count();
  
        $faturas = $query->paginate()->withQueryString();


        $faturas_aux = $faturas->pluck('id')->toArray();   
        $faturas_categ = DB::table('fatura_categories')->whereIn('fatura_id', $faturas_aux)->get();
        $categories = Category::where('enabled', true)->pluck('name','id');

        $filter = true;

        $company = Company::find(session("tenant"));

           
        return view('reports::statement.index', compact( 
            'faturas','faturaCount', 'categories', 'faturas_categ', 'total', 'filter', 'company'
        ));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function generatePDF(Request $request)
    {

        $query = Fatura::where('status',  'settled' )->orderBy('data_recebimento');
        $receitas = Fatura::where([['type', 'incomes'], ['status',  'settled']]);
        $despesas = Fatura::where([['type', 'expenses'], ['status',  'settled']]);

        if(isset($request->range_date))
        {
            $query->whereBetween('data_recebimento',$this->formataRangeDate($request));
            $receitas->whereBetween('data_recebimento',$this->formataRangeDate($request));
            $despesas->whereBetween('data_recebimento',$this->formataRangeDate($request));
        }

        $total = $receitas->sum('value') - $despesas->sum('value');


        $faturas_aux = $query->pluck('id')->toArray();   
        $faturas_categ = DB::table('fatura_categories')->whereIn('fatura_id', $faturas_aux)->get();
        $categories = Category::where('enabled', true)->pluck('name','id');

        $data = [
            'dados' => $query->get(),
            'periodo' => $request->range_date,
            'total' => $total,
            'categorias' => $categories,
            'faturascateg' => $faturas_categ
        ];

		$pdf = PDF::loadView('reports::statement.pdf', $data);
		return $pdf->stream('document.pdf');

    }

    private function formataRangeDate(Request $request): array
    {
        
        $dateFormated = array();
        $rangedateArray = explode(' - ', $request->range_date);

        $rangedateArray = array_map(function($value){
            
            return date('Y-m-d',strtotime(str_replace('/','-',$value)));
        }, $rangedateArray);

       //Formata a data para colocar a hora para o SQL ter precisão da busca

        $dateFormated = [$rangedateArray[0]." 00:00:00",
                         $rangedateArray[1]." 23:59:59"];

        return $dateFormated;
    }
}
