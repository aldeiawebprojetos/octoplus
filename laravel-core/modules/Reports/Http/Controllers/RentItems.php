<?php

namespace Modules\Reports\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Financial\Models\Fatura;
use Modules\SaleRent\Models\Invoice;
use Octoplus\Domain\Item\Models\Item;
use Octoplus\Domain\Category\Models\Category;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Account;
use Carbon\Carbon;
use PDF;

class RentItems extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $item_status = ['Disponível', 'Vencendo', 'Locado'];
        $filter = false;
        $company = Company::find(session("tenant"));
        return view('reports::rent_items.index', compact('item_status', 'filter', 'company'));
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function filter(Request $request)
    {

        $item_status = ['Disponível', 'Vencendo', 'Locado'];
        $items_status_history = [];

        switch ($request->item_status) {
            case 0:
                $items = Item::where([['enabled', true], ['item_type', 'rent'], ['rent_status', 1]])->paginate();

                $items_id = Item::where([['enabled', true], ['item_type', 'rent'], ['rent_status', 1]])->pluck('id')->toArray();
                $items_status_history = DB::table('item_status_history')->whereIn('item_id', $items_id)
                    ->where('status', 'closed')->orderBy('data', 'Desc')->get()->toArray();
                break;
            case 1:
                $items = Item::where([['enabled', true], ['item_type', 'rent'], ['rent_status', 0]])
                    ->whereHas('invoices', function($invoice){
                        $invoice
                        ->where('rent_endDate', '!=', null)
                        ->where('active_contract', true)
                        ->whereDate('rent_endDate', '<=', Carbon::now()->startOfDay()->addDays(7)->toDateString())
                        ->whereDate('rent_endDate', '>=', Carbon::now()->startOfDay()->toDateString());
                    })->paginate();
                break;
            case 2:
                $items = Item::where([['enabled', true], ['item_type', 'rent'], ['rent_status', 0]])->paginate();
                break;
        }

        $filter = true;
        $company = Company::find(session("tenant"));
        return view('reports::rent_items.index', compact('item_status', 'filter', 'items', 'company', 'items_status_history'));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function generatePDF(Request $request)
    {

        $item_status = ['Disponíveis', 'Vencendo', 'Locados'];
        $items_status_history = [];

        switch ($request->item_status) {
            case 0:
                $items = Item::where([['enabled', true], ['item_type', 'rent'], ['rent_status', 1]])->paginate();

                $items_id = Item::where([['enabled', true], ['item_type', 'rent'], ['rent_status', 1]])->pluck('id')->toArray();
                $items_status_history = DB::table('item_status_history')->whereIn('item_id', $items_id)
                    ->where('status', 'closed')->orderBy('data', 'Desc')->get()->toArray();
                break;
            case 1:
                $items = Item::where([['enabled', true], ['item_type', 'rent'], ['rent_status', 0]])
                    ->whereHas('invoices', function($invoice){
                        $invoice
                        ->where('rent_endDate', '!=', null)
                        ->where('active_contract', true)
                        ->whereDate('rent_endDate', '<=', Carbon::now()->startOfDay()->addDays(7)->toDateString())
                        ->whereDate('rent_endDate', '>=', Carbon::now()->startOfDay()->toDateString());
                    })->get();
                break;
            case 2:
                $items = Item::where([['enabled', true], ['item_type', 'rent'], ['rent_status', 0]])->get();
                break;
        }

        $company = Company::find(session("tenant"));

        $data = [
            'items' => $items,
            'items_status_history' => $items_status_history,
            'status' => $item_status[$request->item_status]
        ];

		$pdf = PDF::loadView('reports::rent_items.pdf', $data);
		return $pdf->stream('document.pdf');

    }
}
