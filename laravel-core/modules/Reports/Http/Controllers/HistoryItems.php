<?php

namespace Modules\Reports\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Financial\Models\Fatura;
use Modules\SaleRent\Models\Invoice;
use Octoplus\Domain\Item\Models\Item;
use Octoplus\Domain\Category\Models\Category;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Account;
use PDF;

class HistoryItems extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $items = Item::where([['item_type', 'rent'], ['enabled', true]])->pluck('name', 'id');
        $filter = false;
        $company = Company::find(session("tenant"));
        return view('reports::history_items_rent.index', compact('items', 'filter', 'company'));
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function filter(Request $request)
    {

        
        $aux = DB::table('invoice_items')->where('item_id', $request->item_id)->pluck('invoice_id')->toArray();
        
        $invoices = Invoice::whereIn('id', $aux)->get();
        
        $total = 0;
        foreach($invoices as $invoice){
            $total += $invoice->faturas->sum('value');
        }


        $items = Item::where([['item_type', 'rent'], ['enabled', true]])->pluck('name', 'id');
        $filter = true;
        $company = Company::find(session("tenant"));
        return view('reports::history_items_rent.index', compact('invoices', 'filter', 'items', 'total', 'company'));
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function generatePDF(Request $request)
    {

        $aux = DB::table('invoice_items')->where('item_id', $request->item_id)->pluck('invoice_id')->toArray();
        
        $invoices = Invoice::whereIn('id', $aux)->get();
        
        $total = 0;
        foreach($invoices as $invoice){
            $total += $invoice->faturas->sum('value');
        }


        $items = Item::where([['item_type', 'rent'], ['enabled', true]])->pluck('name', 'id');
        $data = [
            'dados' => $invoices,
            'total' => $total,
            'item' => Item::where('id', $request->item_id)->pluck('name')
        ];

		$pdf = PDF::loadView('reports::history_items_rent.pdf', $data);
		return $pdf->stream('document.pdf');

    }
}
