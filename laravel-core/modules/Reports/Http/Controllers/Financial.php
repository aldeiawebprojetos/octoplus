<?php

namespace Modules\Reports\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Financial\Models\Fatura;
use Octoplus\Domain\Category\Models\Category;
use Octoplus\Domain\Company\Models\Company;
use Octoplus\Domain\Account;
use PDF;

class Financial extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        
        $type = 'incomes';

        if(isset($request->type))
        {
            $type = $request->type;
        }

        
                
        $categories = Category::where([['enabled', true],['type', substr($type, 0, -1)]])->pluck('name','id');
      
        $filter = false; 
        $company = Company::find(session("tenant"));
           
        return view('reports::financial.index', compact( 
            'categories', 'type', 'filter', 'company'
        ));
    }




    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function filter(Request $request)
    {
        
        $type = 'incomes';

        if(isset($request->type))
        {
            $type = $request->type;
        }

        $query = Fatura::where([['type', $type], ['status',  'settled']])
                        ->orderBy('data_recebimento');
        
                
        $categories = Category::where([['enabled', true],['type', substr($type, 0, -1)]])->pluck('name','id');
      
        $date = $request->range_date;
        if(isset($request->range_date))
        {
            $query->whereBetween('data_recebimento',$this->formataRangeDate($request));
        }

        $category = $request->category_id;
        if(isset($request->category_id))
        {
            $query->whereHas('categories', function($cat) use ($request){
                $cat->where('id', $request->category_id);
            });
        } 

        $faturas_aux = $query->pluck('id')->toArray();   
        $faturas_categ = DB::table('fatura_categories')->whereIn('fatura_id', $faturas_aux)->get()->toArray();


        $faturasValue = $query->sum('value');
        $faturaCount = $query->count();
  
        $faturas = $query->paginate()->withQueryString();

        $filter = true;

        $company = Company::find(session("tenant"));
           
        return view('reports::financial.index', compact( 
            'faturas','categories','faturasValue','faturaCount', 'type', 'faturas_categ', 'category', 'date', 'filter', 
            'company'
        ));
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request)
    {
        $fatura = Fatura::find($request->fatura_id);
        $categories = Category::where([['type','income'],['enabled',true]])->orderBy('name')->pluck('name','id');
        $accounts = Account::where([['enabled', true]]);
        return view('reports::financial.show',compact('fatura','categories','accounts'));
    }



    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function generatePDF(Request $request)
    {

        $faturas = Fatura::where([['status',  'settled'], ['type', $request->type]])->orderBy('data_recebimento');

        $categories = Category::where([['enabled', true],['type', substr($request->type, 0, -1)]])->pluck('name','id');


        if(isset($request->range_date)){
            $faturas->whereBetween('data_recebimento',$this->formataRangeDate($request));
        }

        $category = null;

        if(isset($request->category_id)){
            $faturas->whereHas('categories', function($cat) use ($request){
                $cat->where('id', $request->category_id);
            });

            $category = $categories[$request->category_id];
        } 

        $faturasValue = $faturas->sum('value');

        $faturas_aux = $faturas->pluck('id')->toArray();   
        $faturas_categ = DB::table('fatura_categories')->whereIn('fatura_id', $faturas_aux)->get()->toArray();
        $categories = Category::where('enabled', true)->pluck('name','id')->toArray();

        $data = [
            'dados' => $faturas->get(),
            'tipo' => $request->type,
            'categoria' => $category,
            'periodo' => $request->range_date,
            'total' => $faturasValue,
            'categorias' => $categories,
            'faturascateg' => $faturas_categ
        ];

		$pdf = PDF::loadView('reports::financial.pdf', $data);
		return $pdf->stream('document.pdf');

    }



    private function formataRangeDate(Request $request): array
    {
        
        $dateFormated = array();
        $rangedateArray = explode(' - ', $request->range_date);

        $rangedateArray = array_map(function($value){
            
            return date('Y-m-d',strtotime(str_replace('/','-',$value)));
        }, $rangedateArray);

       //Formata a data para colocar a hora para o SQL ter precisão da busca

        $dateFormated = [$rangedateArray[0]." 00:00:00",
                         $rangedateArray[1]." 23:59:59"];

        return $dateFormated;
    }
}
