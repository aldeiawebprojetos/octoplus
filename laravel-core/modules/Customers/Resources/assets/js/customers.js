import Form from './plugins/form';

var vm = new Vue({
    el: '#app',
    data: function(){
        return {
            form: new Form('customers'),
            apiCEP: 'https://brasilapi.com.br/api/cep/v1/{cep}',
            edit: false,
        };
    },
    mounted() {
        this.edit = form_edit;

        if(this.edit)
        {
            for(var index in customer_backup){

                this.form[index] = customer_backup[index];

            }
        }
    },
    methods:{
        buscaCep() {
            console.log(this.form.cep)
           if(this.form.cep.length >= 8 && this.form.cep.length <= 10)
           {
               const client = require('axios');
               const api = this.routeAPI(this.form.cep);
               client.get(api)
                     .then(({data}) => {
   
                        for(let endereco in data)
                        {
                            if(endereco == 'street')
                            {
                                this.form['address'] = data[endereco];
                            }else if(endereco == 'state'){
                                this.form['uf'] = data[endereco];
                            }else{
                                this.form[endereco] = data[endereco];
                            }
                        }
                })
                .catch(function (error){
                    const trans = (typeof array_trans != 'undefined' ? array_trans : {});;
                    console.log(`ERRO: ${error}`);
                    sendNotify('error',trans['error'], trans['cepFail']);
                  });
           }
        },
        routeAPI(value)
        {
            return this.apiCEP.replace('{cep}', value);
        },
    },
});