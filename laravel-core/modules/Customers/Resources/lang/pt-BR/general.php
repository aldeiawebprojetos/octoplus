<?php

return [
    'name'                                              => 'Parceiros',
    'customer'                                          => 'Parceiro',
    'cpf'                                               => 'CPF',
    'cnpj'                                              => 'CNPJ',
    'customer_natureza'                                 => 'Natureza',
    'customer_type'                                     => 'Tipo de contato',
    'cep'                                               => 'CEP',
    'cep_fail'                                          => 'CEP não encontrado na base de dados',
    'address'                                           => 'Endereço',
    'complement'                                        => 'Complemento',
    'neighborhood'                                      => 'Bairro',
    'uf'                                                => 'Estado',
    'city'                                              => 'Cidade',
    'website'                                           => 'Website',
    'other_contacts'                                    => 'Outros Contatos',
    'obs'                                               => 'Observações',
    'responsible_name'                                  => 'Nome do Responsável',
    'responsible_document'                              => 'CPF do Responsável',
];