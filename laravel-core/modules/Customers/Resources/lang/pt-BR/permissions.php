<?php
return [
    'customer'                          => 'Parceiros',
    'create'                            => 'Cadastrar',
    'update'                            => 'Atualizar',
    'show'                              => 'Visualizar',
    'delete'                            => 'Apagar',
];