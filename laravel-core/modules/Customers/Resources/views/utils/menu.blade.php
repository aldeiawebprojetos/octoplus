<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="index.html">
						<img src="assets/images/logo@2x.png" width="120" alt="" />
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-switch"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="fa fa-bars"></i>
					</a>
				</div>

			</header>


			<div class="sidebar-user-info">

				<div class="sui-normal">
					<a href="#" class="user-link">
						<img src="assets/images/thumb-4@2x.png" width="55" alt="" class="img-circle" />

						<span>Olá,</span>
						<strong>$Nome_User</strong>
						<strong > <i class="fa fa-unlock-alt"></i> 
						 <i class="fa fa-caret-down"></i> </strong>
						
						
					</a>
				</div>

				<div class="sui-hover inline-links animate-in"><!-- You can remove "inline-links" class to make links appear vertically, class "animate-in" will make A elements animateable when click on user profile -->
					<a href="#">
						<i class="fa fa-user"></i>
						Perfil
					</a>

					<a href="mailbox.html">
						<i class="fa fa-users"></i>
						Usuários
					</a>
					
					<span class="close-sui-popup">&times;</span><!-- this is mandatory -->				
				</div>
			</div>
			
			

									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				
				<li>
					<a href="dashboard.php" >
						<i class="entypo-cd"></i>
						<span class="title">Início</span>
						
					</a>
				</li>

				<li>
					<a href="list.customer.php">
						<i class="entypo-vcard"></i>
						<span class="title">Parceiros</span>
						
					</a>
				</li>


				<li>
					<a href="list.itens.php">
						<i class="fa fa-barcode"></i>
						<span class="title">Itens</span>
					</a>
				</li>



				<li class="has-sub">
					<a href="#">
						<i class="fa fa-rocket"></i>
						<span class="title">Venda/Locação</span>
					</a>
					
					<ul>
						<li>
							<a href="#">
								<i class="fa fa-list"></i>
								<span class="title">Pedidos</span>
							</a>
						</li>

						<li>
							<a href="#">
								<i class="fa fa-history"></i>
								<span class="title">Histórico</span>
							</a>
						</li>

						<li>
							<a href="list.customer.php">
								<i class="entypo-vcard"></i>
								<span class="title">Clientes</span>
								
							</a>
						</li>
						
					</ul>
				</li>

				<li class="has-sub">
					<a href="#">
						<i class="fa fa-dollar"></i>
						<span class="title">Financeiro</span>
					</a>

					<ul>

						<li class="has-sub">
							<a href="#">
								<i class="fa fa-arrow-up"></i>
								<span class="title">Receitas</span>
							</a>

							<ul>
								<li>
									<a href="#">
										<span class="title">Faturas</span>
									</a>
								</li>
								
								<li>
									<a href="list.customer.php">
										<i class="entypo-vcard"></i>
										<span class="title">Clientes</span>
										
									</a>
								</li>

							</ul>
						</li>



						<li class="has-sub">
							<a href="#">
								<i class="fa fa-arrow-down"></i>
								<span class="title">Despesas</span>
							</a>

							<ul>
								<li>
									<a href="#">
										<span class="title">Faturas</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="title">Pagamentos</span>
									</a>
								</li>

								<li>
									<a href="list.customer.php">
										<i class="entypo-vcard"></i>
										<span class="title">Fornecedores</span>
										
									</a>
								</li>

							</ul>
						</li>



						<li class="has-sub">
							<a href="#">
								<i class="fa fa-bank"></i>
								<span class="title">Banco</span>
							</a>

							<ul>
								<li>
									<a href="#">
										<span class="title">Contas</span>
									</a>
								</li>

								<li>
									<a href="#">
										<i class="fa fa-exchange"></i>
										<span class="title">Transações</span>
									</a>
								</li>
								

							</ul>
						</li>
						
						<li>
							<a href="#">
								<i class="fa fa-folder-open"></i>
								<span class="title">Categorias</span>
							</a>
						</li>
						
						
						
					</ul>
				</li>



				<li>
					<a href="charts.html">
						<i class="fa fa-bar-chart"></i>
						<span class="title">Relatórios</span>
					</a>
				</li>

				<li>
					<a href="charts.html">
						<i class="fa fa-cog"></i>
						<span class="title">Configurações</span>
					</a>
				</li>
				
				

				
			</ul>

			
			
		</div>

	</div>