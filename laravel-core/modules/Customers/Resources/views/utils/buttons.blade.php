<!-- Botoes -->
<div class="col-md-6">
	<ul class="list-inline pull-right">
			<li class="list-inline-item"><a class="btn btn-danger btn-lg btn-icon pull-right">
				Excluir
				<i class="entypo-trash"></i>
			</a>
		</li>

		<li class="list-inline-item">
	  		<a href="list.customer.php" class="btn btn-white btn-lg btn-icon pull-right">
				Cancelar e Voltar
				<i class="entypo-block"></i>
			</a>
	  	</li>

			<li class="list-inline-item">
				<a class="btn btn-green btn-lg btn-icon pull-right">
				Salvar
				<i class="entypo-floppy"></i>
			</a>
		</li>
	</ul>
</div>