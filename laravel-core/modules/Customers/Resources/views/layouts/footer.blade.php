


		<!-- Footer -->
		<footer class="main">
			<div class="row">
				<div class="col-md-6 clearfix">
					<label class="control-label">  Modo Noturno </label>

					<div class="make-switch" data-text-label="<i class='entypo-moon'></i>">
						<input type="checkbox" checked />
					</div>

				</div>

				<div class="col-md-6 clearfix">
					&copy; 2020 <strong>8plus.me</strong>  
				</div>
				
			</div>

			<div class="row">
				<div class="col-md-6">
					
				</div>

				<div class="col-md-6">
					<i class="fa fa-spinner"></i> Versão <strong>1.0</strong>  
				</div>
				
			</div>
							
			
			
		
		</footer>