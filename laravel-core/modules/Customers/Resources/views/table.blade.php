<table class="table table-bordered table-striped datatable" id="table-4">
    <thead>
        <tr>
            <th>{{ trans('general.actions') }}</th>
            <th>{{ trans('general.code') }}</th>
            <th>{{ trans('general.name') }}</th>
            <th>{{ trans('customers::general.cpf') }}/{{ trans('customers::general.cnpj') }}</th>
            <th>{{ trans('auth.email') }}</th>
            <th>{{ trans('user.phone') }}</th>
            <th width="1%" class="text-center">
                <div class="checkbox checkbox-replace">
                    <input class="chkbx_customer" type="checkbox"  onchange="changeChkboxes(this,'customer')" />
                </div>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($customers as $customer)
       
            <tr>
                <td>
                    @include('common.tableActions',['options' => [
                        ['text' => trans('general.edit'),'route' => ['customers.edit',true],'permission' => 'customers.customer_update','icon' => 'entypo-pencil'],
                    ], 'name' => $customer->user->name, 'id' => $customer->id, 'delete_permission' => 'customers.customer_delete','route_delete'=>'customers.delete'])
                </td>
                <td onclick="goToEdit('{{ route('customers.show',$customer->id) }}')">#{{ $customer->id }}</td>
                <td onclick="goToEdit('{{ route('customers.show',$customer->id) }}')">{{ $customer->user->name }}</td>
                <td onclick="goToEdit('{{ route('customers.show',$customer->id) }}')">{{ formatar_cpf_cnpj((isset($customer->pessoa->cpf) ? $customer->pessoa->cpf : (isset($customer->pessoa->cnpj) ? $customer->pessoa->cnpj : '') )) }}</td>
                <td onclick="goToEdit('{{ route('customers.show',$customer->id) }}')">{{ $customer->user->email }}</td>
                <td onclick="goToEdit('{{ route('customers.show',$customer->id) }}')">{{ $customer->tel1 }} {{  $customer->tel2 != null ? ' / '. $customer->tel2 : ''}}</td>
                <td class="text-center">
                    <div class="checkbox checkbox-replace">
                        <input type="checkbox" class="chkbx_customer" value="{{$customer->id}}"/>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>{{ trans('general.actions') }}</th>
            <th >{{ trans('general.code') }}</th>
            <th >{{ trans('general.name') }}</th>
            <th >{{ trans('customers::general.cpf') }}/{{ trans('customers::general.cnpj') }}</th>
            <th >{{ trans('auth.email') }}</th>
            <th >{{ trans('user.phone') }}</th>
            <th width="1%" class="text-center">
                <div class="checkbox checkbox-replace">
                    <input class="chkbx_customer" type="checkbox"  onchange="changeChkboxes(this,'customer')"/>
                </div>
            </th>
        </tr>
    </tfoot>
</table>


@section('modals')
    @include('common.modals.delete',['type'=>trans('customers::general.name')])
@endsection
