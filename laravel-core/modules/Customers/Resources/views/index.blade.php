@extends('layouts.app')

@section('title', trans('customers::general.name'))

@section('breadcrumb')
<li> <a href="">{{ trans('customers::general.name') }}</a> </li>
<li class="active"> <strong>{{trans_choice('general.list',2,['item' => trans('customers::general.name')])}}</strong> </li>		
@endsection

@section('content')
<hr>
<div class="mail-env">
	<!-- Mail Body -->
	<div class="mail-body">
		
		<div class="mail-header">
			<!-- Search  form -->
			
			<form method="get" role="form" class="search-form-full">
				<div class="form-group">
					<input type="text" class="form-control input-lg" name="s" id="search-input" placeholder="Pesquise pelo Nome, cpf/cnpj..." value="" />
					<i class="entypo-search"></i>
				</div>
				
			</form>
		</div>


		<div class="mail-header">
			<!-- title -->
			<h3 class="mail-title">
				{{ trans('customers::general.name') }}
				<span class="count text-info">({{$customers->count()}})</span>
			</h3>
			


			<!-- search -->					
			{{-- <div class="mail-search">				
				{{ Form::select('customer_type_id',  typeOfCustomer(), ['class' => 'select2', 'placeholder'  => trans('general.select') ."..."]) }}
			</div> --}}

			

			<!-- search -->					
			{{-- <div class="pull-left">				
				<a href="#" class="btn btn-danger pull-left">
					<i class="entypo-trash"></i>
				</a>
			</div> --}}

		</div>
		
		
		@include('customers::table')

		{{$customers->links()}}
	</div>
	
	<!-- Sidebar -->
	<div class="mail-sidebar">
		
		<!-- compose new email button -->
		<div class="mail-sidebar-row hidden-xs">
			@can('customers.customer_create')
				<a href="{{route('customers.create')}}" class="btn btn-blue btn-lg btn-icon btn-block">
					{{ trans_choice('general.add',2,['item' => '']) }}
					<i class="entypo-plus"></i>
				</a>
			@endcan
		</div>
		

		<!-- menu -->
		<ul class="mail-menu">
			@foreach(typeOfCustomer() as $key => $type)
				<li class="">
					<a href="?{{ http_build_query(collect(['customer_type' => $key])->merge(collect(Request::query())->forget(['customer_type','page']))->toArray()) }}">
						<span class="badge badge-danger pull-right"></span>
						{{$type}}
					</a>
				</li>
			@endforeach			
		</ul>
		
		<div class="mail-distancer"></div>
		
		<!--
		<h4>Status</h4>
		<ul class="mail-menu">
			<li>
				<a href="#">
					<span class="badge badge-danger badge-roundless pull-right"></span>
					Vencido
				</a>
			</li>
			
			<li>
				<a href="#">
					<span class="badge badge-success badge-roundless pull-right"></span>
					ATivo
				</a>
			</li>
			
			<li>
				<a href="#">
					<span class="badge badge-warning badge-roundless pull-right"></span>
					Vencendo
				</a>
			</li>
		</ul>
	-->
		
	</div>
	
</div>
@endsection