
<div class="row">
				
					
    <div class="form-group col-md-2 col-xs-4">
        {{ Form::label('cod', trans('general.code')) }}
        {{ Form::text('cod',isset($customer) ? '#'.$customer->id : null,['class' => 'form-control input-lg','v-model' => 'form.cod', 'readonly']) }}
    </div>

    <div class="form-group col-md-8 col-xs-5">
      {{Form::label("first_name", trans('general.name'))}}<strong class="text-danger">*</strong>
      {{Form::text("first_name", null,['class' => 'form-control input-lg','v-model' => 'form.first_name','required',(isset($readOnly) && $readOnly ? 'readonly' : '')])}}
    </div>

    <div class="form-group col-md-2 col-xs-3">
      <label for="radio1">{{trans('general.status')}}?</label>


      <div>
         <label class="switch switch-left-right">
            {{ Form::checkbox('status', 1, true, ['class'=> 'switch-input',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
        <span class="switch-label switch-label-3" data-on="{{trans('general.yes')}}" data-off="{{trans('general.not')}}"></span> <span class="switch-handle"></span> </label>
      </div>

  </div>





  <div class="form-group col-md-4 col-xs-12">
      {{Form::label('customer_type_id', trans('customers::general.customer_type'))}} <strong class="text-danger">*</strong>
      
      {{ Form::select('customer_types[]', typeOfCustomer(), null,['class' => " form-control input-lg select2",  (isset($readOnly) && $readOnly ? 'disabled' : ''),'multiple','id' => 'customer_type_id']) }}
     
  </div>


    <div class="form-group col-md-4 col-xs-6">
        {{ Form::label('type_of_people', trans('customers::general.customer_natureza')) }} <strong class="text-danger">*</strong>
     
        {{ Form::select('type_of_people', typeOfPeople(), null,['class' => " form-control input-lg", 'v-model' => 'form.type_of_people', 'placeholder' => trans('general.select') ."...",(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
          
  </div>

    <div class="form-group col-md-4 col-xs-6"> 
     
      {{ Form::label('cpf_cnpj', trans('customers::general.cpf')."/".trans('customers::general.cnpj')) }} <strong class="text-danger">*</strong>
      @if(isset($readOnly) && $readOnly)
        {{Form::text("cpf_cnpj", formatar_cpf_cnpj($customer->cpf_cnpj),['class' => 'form-control input-lg','required',(isset($readOnly) && $readOnly ? 'readonly' : '')])}}
      @else
        <the-mask 
          :mask="['###.###.###-##', '##.###.###/####-##']"
          name="cpf_cnpj"
          id="cpf_cnpj"
          class="form-control input-lg"
          v-model="form.cpf_cnpj"
          {{ (isset($readOnly) && $readOnly ? 'readonly' : '') }}
        />
      @endif
     
    </div>
  
</div>


<!-- Metaboxes -->
<div class="row">
  
  <!-- Metabox :: Publish Settings -->
  <div class="col-sm-6">
      
      <div class="panel panel-primary" data-collapsed="0">
  
          <div class="panel-heading">
              <div class="panel-title">
                  Informações de Endereço
              </div>
              
              <div class="panel-options">
                  <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                  <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                  <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                  <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
              </div>
          </div>
          
          <div class="panel-body">


              <div class="form-group">
                  {{ Form::label('cep', trans('customers::general.cep')) }}
                  @if(isset($readOnly) && $readOnly)

                    {{ Form::text('cep',null, ['class' => "form-control input-lg",'v-model' => 'form.cep','readonly']) }}
                  @else
                    <the-mask mask="##.###-###"
                    name="cep"
                    class="form-control input-lg"
                    v-model="form.cep"
                    id = "cep"
                    @keyup.native="buscaCep()"
                    {{ (isset($readOnly) && $readOnly ? 'readonly' : '') }}
                    />
                  @endif
              </div>

              <div class="form-group">
                  {{ Form::label('address', trans('customers::general.address')) }}
                  {{ Form::text('address',null, ['class' => "form-control input-lg",'v-model' => 'form.address',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}

              </div>


              <div class="form-group">
                  {{ Form::label('complement', trans('customers::general.complement')) }}
                  {{ Form::Text('complement', null, ['class' => "form-control input-lg", 'v-model' => 'form.complement',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}

              </div>


              <div class="form-group">
                  {{ Form::label('neighborhood', trans("customers::general.neighborhood")) }}
                  {{ Form::text('neighborhood', null, ['class' => "form-control input-lg", 'v-model' => 'form.neighborhood',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
              </div>

              <div class="form-group">
                {{ Form::label('uf', trans('customers::general.uf')) }}
                {{ Form::select('uf', $states, null, ['class' => "select2 form-control input-lg", 'v-model' => 'form.uf',(isset($readOnly) && $readOnly ? 'disabled' : '')]) }}
          </div>

              <div class="form-group">
                  {{ Form::label('city', trans('customers::general.city')) }}
                  {{ Form::text('city', null, ['class' => "form-control input-lg", 'v-model' => 'form.city',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
              </div>

                           
          </div>
      
      </div>
      
  </div>
  
  
  <!-- Metabox :: Featured Image -->
  <div class="col-sm-6">
      
      <div class="panel panel-primary" data-collapsed="0">
              
          <div class="panel-heading">
              <div class="panel-title">
                  Informações de Contato
              </div>
              
              
              <div class="panel-options">
                  <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                  <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                  <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                  <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
              </div>
          </div>
          
          <div class="panel-body">
            <div class="form-group">
                {{ Form::label('email', trans("auth.email")) }}
                  <div class="input-group">
                    <span class="input-group-addon"><i class="entypo-mail"></i></span>
                    {{ Form::text('email', null, ['class' => "form-control input-lg", 'v-model' => 'form.email',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
                </div>
            </div>
                <div class="form-group">
                    {{ Form::label('tel1', trans('user.phone') . " 1") }}
                  <div class="input-group">
                      <span class="input-group-addon"><i class="entypo-phone"></i></span>
                    {{ Form::text('tel1', null, ['class' => "form-control input-lg", 'v-model' => 'form.tel1',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
                  </div>
                </div>

                <div class="form-group">
                    {{ Form::label('tel2', trans('user.phone') . " 2") }}
                  <div class="input-group">
                      <span class="input-group-addon"><i class="entypo-phone"></i></span>
                    {{ Form::text('tel2', null, ['class' => "form-control input-lg", 'v-model' => 'form.tel2',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
                  </div>
                </div>

              <div class="form-group">
                  {{ Form::label('website', trans('customers::general.website')) }}
                   

                    <div class="input-group">
                      <span class="input-group-addon"><i class="entypo-network"></i></span>
                      {{ Form::text('website', null, ['class' =>"form-control input-lg", 'v-model' => 'form.website',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
                    
                  </div>
              </div>


              <div class="form-group">
                  {{ Form::label('other_contacts', trans('customers::general.other_contacts')) }}
                  {{ Form::textarea('other_contacts', null, ['class' => "form-control",'v-model' => 'form.other_contacts', 'rows' => '5',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}                    
              </div>


          </div>
  
      </div>
      
  </div>
  
  <div class="clear"></div>



   
    <div class="col-md-6">
        
        <div class="panel panel-primary" data-collapsed="0">
    
            <div class="panel-heading">
                <div class="panel-title">
                    {{trans('customers::general.obs')}}
                </div>
                
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                </div>
            </div>
            
            <div class="panel-body">
                
              <p>
                {{ Form::label( 'observation',  trans('customers::general.obs') ) }} : 
              </p>

                {{ Form::textarea('observation', null, ['class' => "form-control", 'v-model' => 'form.observation', 'rows' => '5',(isset($readOnly) && $readOnly ? 'readonly' : '')]) }}
            </div>
        
        </div>
        
    </div>


    <div class="col-md-6">
        
        <div class="panel panel-primary" data-collapsed="0">
        
            <div class="panel-heading">
              <div class="panel-title">
                {{trans('sale-rent::general.responsible')}}
              </div>
              
              <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
              </div>
            </div>
            
            <div class="panel-body">



              <div class="form-group">
                    {{Form::label('responsible',  trans('customers::general.responsible_name'))}}
                    {{Form::text('responsible', null,['class' => 'form-control input-lg',(isset($readOnly) && $readOnly ? 'readonly' : '')])}}
                </div>


              <div class="form-group">

                {{ Form::label( 'responsible_cpf',  trans('customers::general.responsible_document') ) }}

                <the-mask 
                  name = "responsible_cpf"
                  class = "form-control input-lg"
                  mask = "###.###.###-##"
                  id = "responsible_cpf"
                  v-model = "form.responsible_cpf"
                  {{ ( isset( $readOnly ) && $readOnly ? 'readonly' : '') }}
                />       

              </div>

              
            </div>
          
          </div>
        
    </div>

  <div class="clear"></div>
  
  
  
</div>



<hr />

@section('scripts')
<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">
<script>
    const form_edit = {{ (isset($customer) ? 'true' : 'false') }};
    const array_trans = {
        error: "{{ trans('alerts.error') }}",
        cepFail: "{{ trans('customers::general.cep_fail') }}"
    };
    const customer_backup = {!! json_encode(!empty(old()) ? old() : (isset($customer) ? $customer : false)) !!};

    // if( customer_backup != null)
    // {    
    //   var atributos = [ 'cep', 'uf', 'type_of_people', 'cpf_cnpj', 'responsible_cpf' ];
     
    //   atributos.forEach(element => 
    //   {
    //        document.getElementById( element ).value = customer_backup[ element ] ;     
           
    //        console.log( document.getElementById( element ).value );
    //   });
      
    // }

</script>
<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<script>
 
//  if ( form_edit == true )
//       maskCpfCnpj();
 

//  function maskCpfCnpj()
//  {
//     var inputMask = document.getElementById('cpf_cnpj');
//     var typeOfPeople = document.getElementById('type_of_people').value;
    
//     if( typeOfPeople == "f" )
//       Inputmask('999.999.999-99').mask( inputMask ); 
//     else if ( typeOfPeople == "j" )
//       Inputmask('99.999.999/9999-99').mask( inputMask );   
//  }

</script>
@if(!isset($readOnly))
<script src="{{asset('js/customers/customers.js')}}" ></script>
@endif
@endsection

