@extends('layouts.app')

@section('title',  trans('customers::general.name') )

@section('breadcrumb')
<li> <a href="">{{ trans('customers::general.name') }}</a> </li>
<li class="active"> <strong>{{trans('general.show')}}</strong> </li>		
@endsection

@section('content')

		<br />	
		
		<hr />

				
		{{Form::model($customer, ['class' => 'disabled' ])}}
		<div class="row">
			<!-- title -->
			<div class="col-md-6">
			   <h1 class="pull-left">
				   {{trans('customers::general.name')}} <i class="entypo-pencil"></i>
			   </h1>
		   </div>
       </div>
       
		@include('customers::form', ['readOnly' => true])

		{{ Form::close() }}
		
		
		<div class="clear"></div>
		<!-- Footer -->
@endsection