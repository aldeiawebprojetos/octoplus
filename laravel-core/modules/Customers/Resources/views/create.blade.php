@extends('layouts.app')

@section('title', trans_choice('general.add',2,['item' => trans('customers::general.name')]) )

@section('breadcrumb')
<li> <a href="">{{ trans('customers::general.name') }}</a> </li>
<li class="active"> <strong>{{trans_choice('general.add',2,['item' => trans('customers::general.name')])}}</strong> </li>		
@endsection

@section('content')

		<br />	
		
		<hr />

				
		{{Form::open(["route" => 'customers.store', 'id'=>'customers'])}}
		<div class="row">
			<!-- title -->
			<div class="col-md-6">
			   <h1 class="pull-left">
				   {{trans('customers::general.name')}} <i class="entypo-pencil"></i>
			   </h1>
		   </div>
	   
		   @include('common.buttonsForm',['route_return' => 'customers.index'])
	   </div>
		@include('customers::form')
		<div class="row">
			<div class="col-md-6"></div>
			@include('common.buttonsForm',['route_return' => 'customers.index'])
		</div>
		{{ Form::close() }}
		
		
		<div class="clear"></div>
		<!-- Footer -->
@endsection