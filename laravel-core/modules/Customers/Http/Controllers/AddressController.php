<?php

  

namespace Modules\Customers\Http\Controllers;

use Modules\Customers\Models\Address;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public static function store(Request $request)
    {
        
        $validatedData = $request->validate([
            'address' => 'required',
            'neighborhood' => 'required',
            'city' => 'required',
            'cep' => 'required|size:10',
            'uf' => 'required|size:2',
        ]);
        $validatedData['cep'] = str_replace('.','',str_replace('-','',$validatedData['cep']));
        $validatedData['complement'] = $request->complement;


        return Address::create($validatedData);
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  Modules\Customers\Models\Address  $customer
     * @return \Illuminate\Http\Response
     */

    public static function update(Request $request, Address $customer)
    {
        $validatedData = $request->validate([
            'address' => 'required',
            'neighborhood' => 'required',
            'city' => 'required',
            'cep' => 'required|size:10',
            'uf' => 'required|size:2',
        ]);
        $validatedData['cep'] = str_replace('.','',str_replace('-','',$validatedData['cep']));
        
        return $customer->update($validatedData);
    }

     /**
     * Remove the specified resource from storage.
     * @param  Modules\Customers\Models\Address  $Address
     * @return \Illuminate\Http\Response
     */

    public function destroy(Address $Address)
    {
        $Address->delete();
        return true;
    }

}