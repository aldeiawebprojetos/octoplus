<?php

  

namespace Modules\Customers\Http\Controllers;

use Modules\Customers\Models\CustomerUser;
use Modules\Customers\Http\Controllers\PessoaFisicaController;
use Modules\Customers\Http\Controllers\PessoaJuridicaController;
use Modules\Customers\Http\Controllers\AddressController;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Customers\Models\Customer;
use Modules\Customers\Models\PessoaFisica;
use Modules\Customers\Models\PessoaJuridica;

class CustomerUserController extends Controller
{
    public static function store(Request $request)
    {
        $validatedUserData = $request->validate([
            'first_name' => 'required',
            'email' => (!empty($request->email) ? 'unique:users,email' : ''),
            'type_of_people' => 'required',
        ]);
           
        if(Self::verificaEndereco($request))
        {
            $createdAddress = AddressController::store($request);
            $validatedUserData['address_id'] = $createdAddress->id;
        }

        $createdDocumentData = null;

       
        $validatedUserData['password'] = "_Disabled-password_";
        $createdCustomerUser = CustomerUser::create($validatedUserData);

        if($validatedUserData['type_of_people'] == 'f'){
            // Create Pessoa_Fisica
            $createdDocumentData = PessoaFisicaController::store($request, $createdCustomerUser->id);
        }else{
            if($validatedUserData['type_of_people'] == 'j'){
                // Create Pessoa_Juridica
                $createdDocumentData = PessoaJuridicaController::store($request, $createdCustomerUser->id);
            }
        }

        
        return $createdCustomerUser->id;
    }


    public static function verificaEndereco(Request $request)
    {
       
        if(!empty($request->cep))
        {
            $request->validate([
                'address' => 'required',
                'neighborhood' => 'required',
                'city' => 'required',
                'cep' => 'required',
                'uf' => 'required'
            ]);

            return true;
        }

        return false;

    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  Modules\Customers\Models\CustomerUser  $customer
     * @return \Illuminate\Http\Response
     */

    public static function update(Request $request)
    {
        
        $customer = Customer::find($request->customer_id);
        
        $customerUser = CustomerUser::find($customer->user_id);

        $validatedUserData = $request->validate([
            'first_name' => 'required',
            'email' => (!empty($request->email) ?  'unique:users,email,'.$customerUser->id : ''),
           
            'type_of_people' => 'required',
        ]);
        
        if(Self::verificaEndereco($request))
        {
            if(!empty($customerUser->address)){
               AddressController::update($request, $customerUser->address);
                $validatedUserData['address_id'] = $customerUser->address->id;
            }else{
                $createdAddress = AddressController::store($request);
                 $validatedUserData['address_id'] = $createdAddress->id;
            }
            
        }
        
        $createdCustomerUser = $customerUser->update($validatedUserData);

        if($validatedUserData['type_of_people'] == 'f'){
            // Create Pessoa_Fisica
            
            $pessoa_fisica = PessoaFisica::where('user_id', $customer->user_id)->first();

            if( !is_null( $pessoa_fisica ) )
            {
                PessoaFisicaController::update( $request, $pessoa_fisica );
            }
            else
            {
                PessoaFisicaController::store($request, $customer->user_id);
            }

         
        }else{
            if($validatedUserData['type_of_people'] == 'j'){
                // Create Pessoa_Juridica

                $pessoa_juridica = PessoaJuridica::where('user_id', $customer->user_id)->first();
                
                if( !is_null( $pessoa_juridica ) )
                {
                    PessoaJuridicaController::update($request, $pessoa_juridica);
                }
                else
                {
                    PessoaJuridicaController::store($request, $customer->user_id);
                }         
          
            }
        }
        return true;
    }

     /**
     * Remove the specified resource from storage.
     * @param  Modules\Customers\Models\CustomerUser  $CustomerUser
     * @return \Illuminate\Http\Response
     */

    public function destroy(CustomerUser $CustomerUser)
    {
        $CustomerUser->delete();
        return true;
    }

}