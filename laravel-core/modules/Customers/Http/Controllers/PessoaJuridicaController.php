<?php

  

namespace Modules\Customers\Http\Controllers;

   

use Modules\Customers\Models\PessoaJuridica;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

  

class PessoaJuridicaController extends Controller

{

    public static function store(Request $request, int $user_id)
    {
        $request->validate([
            'cpf_cnpj' => 'required',
            'cpf_cnpj' => 'size:18',
           // 'fantasy_name' => 'required',
            //'insc_estadual' => 'required',
        ]);

        PessoaJuridica::create($request->merge(['user_id' => $user_id, 'cnpj' => mb_strimwidth(str_replace('.','', str_replace('-','', str_replace('/','',$request->cpf_cnpj))),0,14)])->all());

        return true;
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  Modules\PessoasJuridicas\Models\PessoaJuridica  $pessoaJuridica
     * @return \Illuminate\Http\Response
     */

    public static function update(Request $request, PessoaJuridica $pessoaJuridica)
    {
        $request->validate([
            'cpf_cnpj' => 'required',
            'cpf_cnpj' => 'size:18',
           // 'fantasy_name' => 'required',
            //'insc_estadual' => 'required',
        ]);

        $pessoaJuridica->update($request->merge(['cnpj' => mb_strimwidth(str_replace('.','', str_replace('-','', str_replace('/','',$request->cpf_cnpj))),0,14)])->all());

        return true;
    }

    /**
     * Remove the specified resource from storage.
     * @param  Modules\PessoasJuridicas\Models\PessoaJuridica  $pessoaJuridica
     * @return \Illuminate\Http\Response
     */

    public function destroy(PessoaJuridica $pessoaJuridica)
    {
        $pessoaJuridica->delete();
        return true;
    }

}