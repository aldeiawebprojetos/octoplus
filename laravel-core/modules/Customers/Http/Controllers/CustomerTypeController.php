<?php
namespace Modules\Customers\Http\Controllers;

   

use Modules\Customers\Models\CustomerType;

use Illuminate\Http\Request;

  

class CustomerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $customerTypes = CustomerType::latest()->paginate(10);

        return view('customerTypes.index',compact('customerTypes'))
               ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()

    {
        return view('customerTypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        CustomerType::create($request->all());

        return redirect()->route('customerTypes.index')
               ->with('success','Customer Type created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  Modules\Customers\Models\CustomerType  $customerType
     * @return \Illuminate\Http\Response
     */

    public function show(CustomerType $customerType)

    {
        return view('customerTypes.show',compact('customer'));
    } 

     

    /**
     * Show the form for editing the specified resource.
     * @param  Modules\Customers\Models\CustomerType  $customerType
     * @return \Illuminate\Http\Response
     */

    public function edit(CustomerType $customerType)
    {
        return view('customerTypes.edit',compact('customer'));
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  Modules\Customers\Models\CustomerType  $customerType
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, CustomerType $customerType)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $customerType->update($request->all());

        return redirect()->route('customerTypes.index')
               ->with('success','Customer Type updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     * @param  Modules\Customers\Models\CustomerType  $customerType
     * @return \Illuminate\Http\Response
     */

    public function destroy(CustomerType $customerType)
    {
        $customerType->delete();

        return redirect()->route('customerTypes.index')
               ->with('success','Customer Type deleted successfully');
    }

}