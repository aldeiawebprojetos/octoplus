<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Customers\Models\Customer;
use Modules\Customers\Models\Addresses;
use Modules\Customers\Models\PessoaFisica;
use Modules\Customers\Models\PessoaJuridica;
use Modules\Financial\Models\Fatura;

use App\Http\Controllers\AddressController;
use Modules\Customers\Http\Controllers\CustomerUserController;
use Modules\Customers\Models\CustomerUser;
use Modules\Customers\Models\State;


class Main extends Controller
{

    public function __construct()
    {
        $this->middleware('can:customers.customer_create')->only(['create','store']);
        $this->middleware('can:customers.customer_update')->only(['edit','update']);
        $this->middleware('can:customers.customer_show')->only(['index']);
        $this->middleware('can:customers.customer_delete')->only(['delete']);       
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    public function index(Request $request)
    {
        $query = Customer::with('customer_types');
        
        if(!empty($request->s))
        {
           
            if(is_numeric($request->s))
            {
                if(strlen($request->s) == 11)
                {
                    $queryPessoa = PessoaFisica::where('cpf',$request->s)->first();
                   $query->where('user_id', $queryPessoa->user_id);
                }elseif(strlen($request->s) == 14){
                    $queryPessoa = PessoaJuridica::where('cnpj',$request->s)->first();
                    $query->where('user_id', $queryPessoa->user_id);
                }

            }else{
                
                $query->whereHas('user', function($q) use($request){
                    $q->where('first_name', 'LIKE', '%'.$request->s.'%');
                });

            }
            
        }

        if(isset($request->customer_type))
        {
           
            $query->whereHas('customer_types',function($ctype) use ($request){
                $ctype->where('customer_type_id', $request->customer_type);
            });
        }
        
        $customers = $query->paginate()->withQueryString();
       

        // data manipulation

        foreach ($customers as $key => $customer) {
          
            
            if($customer->user->type_of_people == 'f'){
                
                $customer['pessoa'] = PessoaFisica::where('user_id', $customer->user_id)->first();
            }else {
               
                if($customer->user->type_of_people == 'j'){
                    $customer['pessoa'] = PessoaJuridica::where('user_id', $customer->user_id)->first();
                }
            }
           
        }
      
        // $customers->setCollection($updatedCustomers);
      
        return view('customers::index',compact('customers'))
               ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $states = State::all()->pluck('nome','sigla');
        return view('customers::create',compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      
        $validatedData = $request->validate([
            'customer_types' => 'required',
        ],[
            'customer_types.required' => trans_choice("validation.required",2,['attribute'=> trans('customers::general.customer_type')])
        ]);
           

        // Create Address & Pessoa_Física/Juridica & User 
        $createdUserId = CustomerUserController::store($request);
        $customerArray = $validatedData + 
        [
            'user_id' => $createdUserId, 
            'tel1' => $request->tel1,
            'tel2' => $request->tel2,
            'other_contacts' => $request->other_contacts,
            'observation' => $request->observation,
            'responsible' => $request->responsible,
            'responsible_cpf' => mb_strimwidth(str_replace('.','', str_replace('-','', str_replace('/','',$request->responsible_cpf))),0,11),
            
        ];
      

        // Create customer

        // Needs user_id customer_type_id observation
        $customer = Customer::create($customerArray);

        if(count($request->customer_types) > 0)
        {
            $customer->customer_types()->sync($request->customer_types);
        }

        return redirect()->route('customers.index')
               ->with('success','Customer created successfully.');
    }

    /**
     * Show the specified resource.
     * @param  Modules\Customers\Models\Customer  $customer
     * @return Response
     */
    public function show(Request $request)
    {
        $states = State::all()->pluck('nome','sigla');
       
        $customer = $this->getCustomerArray(Customer::find($request->customer_id));

        return view('customers::show',compact('customer','states'));

    }

    public function search(Request $request)
    {
       
         $query = Customer::orderBy('created_at')->limit(10);
       
         
         $query->whereHas('user', function($user) use ($request){
             $user->where('first_name',"LIKE",'%'.$request->q.'%');
         });
        
        if(isset($request->type) && $request->type != null)
        {
           $query->whereHas('customer_types', function($ctype) use ($request){
               $ctype->where("customer_type_id", $request->type);
           });
        }
               
                $customers = $query->get()->map(function($customer){

                        if($customer->user->type_of_people == 'f'){
                
                            $customer['pessoa'] = PessoaFisica::where('user_id', $customer->user_id)->first();
                        }else {
                           
                            if($customer->user->type_of_people == 'j'){
                                $customer['pessoa'] = PessoaJuridica::where('user_id', $customer->user_id)->first();
                            }
                        }
                        
                        $customer['address'] = $customer->user->address;
                        $customer['name'] = $customer->user->name;
                        $customer['type_of_people'] = $customer->user->type_of_people;   
                        return $customer;


        });
        return response()->json($customers);

    }

    /**
     * Show the form for editing the specified resource.
     * @param  Modules\Customers\Models\Customer  $customer
     * @return Response
     */
    public function edit(Request $request)
    { 
        $states = State::all()->pluck('nome','sigla');
       
        $customer = $this->getCustomerArray(Customer::find($request->customer_id));

        return view('customers::edit',compact('customer','states'));

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param  Modules\Customers\Models\Customer  $customer
     * @return Response
     */
    public function update(Request $request)
    {
        $customer = Customer::find($request->customer_id);

        $validatedData = $request->validate([
            'customer_types' => 'required',
            
        ]);
      
        if(count($request->customer_types) > 0)
        {
            $customer->customer_types()->sync($request->customer_types);
        }

        // update Address & Pessoa_Física/Juridica & User 
        CustomerUserController::update($request);

        // update customer

        // Needs user_id customer_type_id observation
        $customer->update($request->all());

        sendNotify(trans_choice('alerts.item_updated',2,['item' => trans('customers::general.customer')]),'success',trans('alerts.success'));


        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     * @param  Modules\Customers\Models\Customer  $customer
     * @return Response
     */
    public function destroy($customer)
    {
        $customer->delete();

        return redirect()->route('index')
               ->with('success','Customer deleted successfully');
    }

    public function delete(Request $request)
    {
        $customer = Customer::find($request->customer_id);
       
        $fatura = Fatura::where( 'customer_id', $request->customer_id )->where('status', 'open')->first();
     
        if( is_null( $fatura ) )
        {
            $customer->delete();

            sendNotify(trans('customers::warning.customer.deleted'),'success');
        }
        else
            sendNotify( 'Usuario nao deletado, pois esta com faturas em aberto' ,'error');

        return redirect()->route('customers.index');

        dd( $fatura );
    }

    private function getCustomerArray(Customer $customer): Customer
    {
        foreach($customer->user->toArray() as $key => $value)
        {
           if($key != 'id')
           {
            $customer[$key] = $value;
           }
        }
        if(!empty($customer->address))
        {
            $address = $customer->address->toArray();
            unset($customer->address);
    
            foreach($address as $key => $value)
            {
                if($key != 'id')
                {
                 $customer[$key] = $value;
                }
               
            }
        }
        if($customer->type_of_people == 'f'){
            
            $pessoa_fisica = PessoaFisica::where('user_id', $customer->user_id)->first();

            if( !is_null( $pessoa_fisica ) )
            {
                foreach(PessoaFisica::where('user_id', $customer->user_id)->first()->toArray() as $key => $value)
                {
                    if($key == 'cpf')
                    {
                        $customer['cpf_cnpj'] = $value;
                    }else{
                        if($key != 'id')
                        {
                        $customer[$key] = $value;
                        }
                    }
                }
            }
        }else {
            if($customer->type_of_people == 'j'){
               
                $pessoa_juridica = PessoaJuridica::where('user_id', $customer->user_id)->first();

                if( !is_null( $pessoa_juridica ) )
                {
                    foreach(PessoaJuridica::where('user_id', $customer->user_id)->first()->toArray() as $key => $value)
                    {
                        if($key == 'cnpj')
                        {
                            $customer['cpf_cnpj'] = $value;
                        }else{
                            if($key != 'id')
                            {
                            $customer[$key] = $value;
                            }
                        }
                    }
                }
            }
        }


        return ($customer);

    }
}
