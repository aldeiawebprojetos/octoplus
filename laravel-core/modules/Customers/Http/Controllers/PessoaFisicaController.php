<?php

  

namespace Modules\Customers\Http\Controllers;

   

use Modules\Customers\Models\PessoaFisica;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

  

class PessoaFisicaController extends Controller

{

    public static function store(Request $request, int $user_id)

    {
        $request->validate([
            'cpf_cnpj' => 'required',
            'cpf_cnpj' => 'size:14',
            
        ]);

        PessoaFisica::create($request->merge(['user_id' => $user_id, 'cpf' => mb_strimwidth(str_replace('.','', str_replace('-','', str_replace('/','',$request->cpf_cnpj))),0,11)])->all());

        return true;
    }


    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  Modules\Customers\Models\PessoaFisica  $customer
     * @return \Illuminate\Http\Response
     */

    public static function update(Request $request, PessoaFisica $customer)
    {
        $request->validate([
            'cpf_cnpj' => 'required',
            'cpf_cnpj' => 'size:14',
        ]);

        
        $customer->update($request->merge(['cpf' => mb_strimwidth(str_replace('.','', str_replace('-','', str_replace('/','',$request->cpf_cnpj))),0,14)])->all());

        return true;
    }

     /**
     * Remove the specified resource from storage.
     * @param  Modules\Customers\Models\PessoaFisica  $pessoaFisica
     * @return \Illuminate\Http\Response
     */

    public function destroy(PessoaFisica $pessoaFisica)
    {
        $pessoaFisica->delete();
        return true;
    }

}