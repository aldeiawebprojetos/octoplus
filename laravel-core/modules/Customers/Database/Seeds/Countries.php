<?php

namespace Modules\Customers\Database\Seeds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Customers\Models\Country;

class Countries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
    ['nome' => 'Brasil',
     'sigla2' => 'BR',
     'sigla3' => 'BRA',
     'codigo' => '076',],
        ];
        
        foreach($countries as $country)
        {
            Country::create($country);
        }
    }
}
