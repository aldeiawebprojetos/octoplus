<?php

namespace Modules\Customers\Database\Seeds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Octoplus\Domain\Users\Models\Permission;

class Permissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $permissions = [
          ['name' => 'customers.customer_create'],
          ['name' => 'customers.customer_update'],
           ['name' =>'customers.customer_show'],
           ['name' =>'customers.customer_delete'],
       ];

       foreach($permissions as $permission)
       {
           Permission::create($permission);
       }
    }
}
