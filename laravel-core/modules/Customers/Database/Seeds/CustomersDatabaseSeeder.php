<?php

namespace Modules\Customers\Database\Seeds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Octoplus\Domain\Module\Models\Module;

class CustomersDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
            Module::create(['alias' => 'customers','status' => true]);
           $this->call(CustomerTypes::class);
           $this->call(Countries::class);
           $this->call(EstadosBr::class);
          $this->call(Permissions::class);

        Model::reguard();
    }
}
