<?php

namespace Modules\Customers\Database\Seeds;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Customers\Models\CustomerType;

class CustomerTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $customerTypes = [
            ['name' => 'general.client'],
            ['name' => 'general.provider'],
            ['name' => 'general.salesman']
        ];

        foreach($customerTypes as $type)
        {
            CustomerType::create($type);
        }

        Model::reguard();
    }
}
