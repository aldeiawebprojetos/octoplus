<?php

namespace Modules\Customers\Listeners;


use Octoplus\Events\Menu\AdminCreated as Event;

class AddToAdminMenu
{
    /**
     * Handle the event.
     *
     * @param  Event $event
     * @return void
     */
    public function handle(Event $event)
    {
       
        $menu = $event->menu;
       
        if(module_enabled('customers'))
        {
            if(auth()->user()->can('customers.customer_show'))
            {
                $menu->route("customers.index", trans('customers::general.name'),[],['icon' => 'entypo-vcard']);
            }
        }
   
    }

    
}