<?php

namespace Modules\Customers\Models;


use Illuminate\Database\Eloquent\Model;

class PessoaFisica extends Model
{
    protected $table = 'pessoa_fisicas';
    
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id', 'cpf', 'rg'
    ];

    
    public function user()
    {
        return $this->hasOne('Octoplus\Domain\Users\Models\User');
    }

}