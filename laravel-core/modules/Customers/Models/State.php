<?php

namespace Modules\Customers\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = [
        'sigla',
        'nome',
        'country_id',
    ];

    public function country()
    {
        return $this->belongsTo(Countries::class);
    }
}
