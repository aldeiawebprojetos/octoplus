<?php

namespace Modules\Customers\Models;



use Octoplus\Domain\Users\Models\User;


class CustomerUser extends User
{

    protected $table = 'users';

    protected $fillable = [
        'address_id',
        'first_name',
        'last_name',
        'document_id',
        'type_of_people',
        'email',
        'phone',
        'password',
        'activated',
    ];

   
    public function address()
    {
        return $this->belongsTo(Address::class);
    }

}