<?php

namespace Modules\Customers\Models;

use Illuminate\Database\Eloquent\Model;


class Country extends Model
{
    protected $fillable = [
        'sigla2',
        'sigla3',
        'nome',
        'codigo',
    ];

    public function states()
    {
        return $this->hasMany(States::class);
    }
}
