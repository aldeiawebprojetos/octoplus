<?php

namespace Modules\Customers\Models;


use Illuminate\Database\Eloquent\Model;


class CustomerType extends Model
{


    protected $fillable = [
        'name'
    ];

    public function customer()
    {
        return $this->belongsTo('Modules\PessoasJuridicas\Models\Customers');
    }
}