<?php

namespace Modules\Customers\Models;


use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Address extends Model
{
    use LogsActivity;

    protected $fillable = [
        'address','complement','neighborhood', 'city', 'cep', 'uf'
    ];

    protected static $logFillable = true;
    protected static $logName = 'customers::general.address';

    public function user()
    {
        return $this->belongsTo('Octoplus\Domain\Users\Models\User');
    }
}