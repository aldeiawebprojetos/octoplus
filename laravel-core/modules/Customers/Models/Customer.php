<?php

namespace Modules\Customers\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Octoplus\App\Tenant\Traits\ForTenants;
use Octoplus\Domain\Users\Models\User;
use Spatie\Activitylog\Traits\LogsActivity;

class Customer extends Model
{
    use ForTenants, LogsActivity, SoftDeletes;

    protected $fillable = [
        'user_id',
        'id',
        'company_id',
        'responsible',
        'responsible_cpf',
        'observation',
        'tel1',
        'tel2',
        'other_contacts',
    ];

    protected static $logFillable = true;
    protected static $logName = 'customers::general.customer';

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function address()
    {
        return $this->belongsTo(Address::class);
    }
    
    public function company()
    {
        return $this->hasOne('Octoplus\Domain\Company\Models\Company');
    }

    public function customer_types()
    {
        return $this->belongsToMany(CustomerType::class,'customers_customer_types');
    }
    
    public function getAddressCompleteAttribute()
    {
        $endereco = Address::find($this->user->address_id);

        if($endereco != null)
        {
            return $endereco->address. ', ' . $endereco->neighborhood. ', ' . $endereco->city. '-'.$endereco->uf;
        }else{
            return '';
        }
        
      
    }

}