<?php

namespace Modules\Customers\Models;


use Illuminate\Database\Eloquent\Model;


class PessoaJuridica extends Model
{

    protected $table = 'pessoa_juridicas';

    protected $primaryKey = 'user_id';
    
    protected $fillable = [
        'user_id', 'cnpj', 'insc_estadual', 'fantasy_name'
    ];

    
    public function user()
    {
        return $this->hasOne('Octoplus\Domain\Users\Models\User');
    }
}