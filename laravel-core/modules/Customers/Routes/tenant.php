<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web','tenant','admin','bindings'], 'prefix'=>'customers', 'as'=>'customers.', 'namespace'=>'Modules\Customers\Http\Controllers' ], function () {
       Route::get('/',"Main@index")->name('index');
       Route::get('/create', "Main@create")->name('create');
       Route::post('/store', "Main@store")->name('store');
       Route::get('/search', "Main@search")->name('search');
       Route::get('{customer_id}/edit', "Main@edit")->name('edit');
       Route::put('{customer_id}/update', "Main@update")->name('update');
       Route::delete('{customer_id}/delete', "Main@delete")->name('delete');
       Route::get('{customer_id}/show', "Main@show")->name('show');
});
