<?php

/*
*   Versionamento Semântico Adicionado
*   Versionamento segundo o semver.org
*   major: Alterações que faz a API Pública Ficar incompatível <API Pública -> "laravel/framework": "^7.24">
*   minor: Recursos que não alteram a compatibilidade da API pública
*   path: Correção de bugs (funcionamento inadequado) sem alterar a compatibilidade da API pública
*   Para obter essas informações basta usar a função ().
*/

return [
    "major"              => "1",
    "minor"              => "0",
    "patch"              => "0",
    "build"              => "stable",
    "codename"           => "tentaculus",
];