<?php



return [

    'styles' => [
        'neon' => Octoplus\Presenters\Sidebar::class,
        'neon-settings' => Octoplus\Presenters\SettingsMenu::class,
    ],

    'home_urls' => [
        '/',
        'portal',
    ],

    'ordering' => true,

];
