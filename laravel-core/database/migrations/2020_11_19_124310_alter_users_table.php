<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('username');
            $table->string('document_id')->after('id')-> nullable();
            $table->text('description')->nullable();
            $table->boolean('enabled')->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::table('users', function (Blueprint $table){
                $table->string('username');
                $table->dropColumn('document_id');
                $table->dropColumn('description');
                $table->dropColumn('enabled');
            });
    }
}
