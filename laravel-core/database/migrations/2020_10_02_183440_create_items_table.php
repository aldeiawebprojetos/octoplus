<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id')->unsigned();
            $table->string('name');
            $table->string('sku')->nullable();
            $table->text('description')->nullable();
            $table->double('sale_price', 15, 4)->nullable();
            $table->double('purchase_price', 15, 4)->nullable();
            $table->integer('storage')->nullable();
            $table->integer('storage_min')->nullable();
            $table->string('barcode')->nullable();
            $table->boolean('enabled')->default(1);
            $table->string('unit_type')->nullable();
            $table->string('item_type',7)->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
