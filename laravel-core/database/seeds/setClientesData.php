<?php

use Illuminate\Database\Seeder;

use Modules\Customers\Models\Address;
use Modules\Customers\Models\Customer;
use Modules\Customers\Models\CustomerUser;
use Modules\Customers\Models\PessoaFisica;
use Modules\Customers\Models\PessoaJuridica;


class setClientesData extends Seeder
{
    private $Company_id;

    public function __construct()
    {
        $this->Company_id = 2;
    }

    public function run()
    {
        $clientes = \DB::connection('temporario')->table('cliente')->get();
        foreach($clientes as $cliente){
            
            $address = Address::create([
                'address' => $cliente->Endereco, 
                'neighborhood' => $cliente->Bairro, 
                'city' => $cliente->Cidade, 
                'cep' => mb_strimwidth(str_replace('.','',str_replace('-','',$cliente->CEP)), 0, 8, ""), 
                'uf' => $cliente->UF
            ]);

                $cnpj = str_replace('.','',str_replace('/','',str_replace('-','',str_replace(',','',$cliente->Cnpj))));

            $user = CustomerUser::create([
                'first_name' => $cliente->Nome,
                'password' => '_Disabled-Password_',
                'address_id' =>  $address->id,
                'type_of_people' => ($cliente->TipoPessoa || strlen($cnpj) > 11 ? 'j' : 'f'),
            ]);

            if($user->type_of_people == 'f'){
                PessoaFisica::create([
                    'user_id' => $user->id, 
                    'cpf' => mb_strimwidth($cnpj, 0, 11, "") ,
                ]);
            }
            if($user->type_of_people == 'j')
            {
                PessoaJuridica::create([
                    'user_id' => $user->id, 
                    'cnpj' => mb_strimwidth($cnpj, 0, 14, "") , 
                    'insc_estadual' => $cliente->InscricaoEstadual,
                ]);
            }
            $fone1 = preg_replace("/[^0-9]/", "",str_replace('(','',str_replace(')','',str_replace('-','',$cliente->Fone1))));
            $fone2 = preg_replace("/[^0-9]/", "",str_replace('(','',str_replace(')','',str_replace('-','',$cliente->Fone2))));

           $customer = Customer::create([
                'id' => $cliente->Id,
                'user_id' => $user->id,
                'company_id' => $this->Company_id,
                'customer_type_id' => 1,
                'observation' => $cliente->Obs,
                'tel1' => (strlen($fone1) > 11 ? 0 : $fone1),
                'tel2' => (strlen($fone2) > 11 ? 0 : $fone2),
                'other_contacts' => $cliente->Contato .  (strlen($fone1) > 11 ? "\n" . $fone1 : null)  . (strlen($fone2) > 11 ?  "\n" . $fone2 : null),
            ]);

        }
    }
}