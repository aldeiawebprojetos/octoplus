<?php

use Illuminate\Database\Seeder;
use Octoplus\Domain\Item\Models\Item;


class setItemsData extends Seeder
{
    private $Company_id;

    public function __construct()
    {
        $this->Company_id = 2;
    }

    public function run()
    {
        $containers = \DB::connection('temporario')->table('container')->get();

        foreach($containers as $container){
           
           $item = Item::create([
                'id' => $container->Id,
                'company_id' => $this->Company_id,
                'name' => $container->Referencia,
                'code' => $container->Referencia,
                'item_type' => 'rent',
                'sale_price' => $container->Valor ,
                'unit_type' => 'un',
                'enabled' => $container->Ativo,
            ]);

           if($container->TipoContainer > 0)
           {
               $item->categories()->attach($container->TipoContainer);
           }
        }
    }
}