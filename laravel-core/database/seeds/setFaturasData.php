<?php

use Illuminate\Database\Seeder;
use Modules\Financial\Models\Fatura;
use Modules\SaleRent\Models\Invoice;

class setFaturasData extends Seeder
{
    private $Company_id;

    public function __construct()
    {
        $this->Company_id = 2;
    }

    public function run()
    {

        $faturas = \DB::connection('temporario')->table('faturas')->orderBy('Id',"ASC")->get();

        foreach($faturas as $fatura)
        {
          $paymentMEthod = ($fatura->EspeciePagamento == 'BOLETO' ? 'billet' : ($fatura->EspeciePagamento == 'DINHEIRO' ? 'money' : ($fatura->EspeciePagamento == 'CHEQUE' ? 'bank_check' : false)));
          $invoice = Invoice::find($fatura->Contrato);
            if($invoice){
              $nfatura = Fatura::create([
                'id' => $fatura->Id,
                'customer_id' => $invoice->customer_id,
                'account_id' => 1,
                'code' => $invoice->code .'/'. ($invoice->faturas->count() > 0 ? $invoice->faturas->count() : 1),
                'company_id' => $this->Company_id,
                'data_competencia' => $invoice->sale_date,
                'data_vencimento' => $fatura->DataVencimento,
                'description' => $fatura->Obs . "\n Banco:". $fatura->Banco . "\n Número Cheque: " . $fatura->NumeroCheque. "\n Emitente Cheque: " . $fatura->EmitenteCheque,
                'value' => $fatura->Valor,
                'value_paid' => $fatura->Valor,
                'value_liquid' => $fatura->Valor,
                'type' => 'incomes',
                'payment_method' => $paymentMEthod,
                'status' => ($fatura->ConfirmaPago ? 'settled' : 'open'),
                'paid' => ($fatura->ConfirmaPago ? true : false),
                'data_recebimento' => ($fatura->DataRecebimento == '0000-00-00' ? null : $fatura->DataRecebimento),
              ]);

              $invoice->faturas()->attach($nfatura->id);
            

                if($paymentMEthod)
                {
                  $invoice->update(['payment_method' => $paymentMEthod]);
                }
            }

        }


    }
}