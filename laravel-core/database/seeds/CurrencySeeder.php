<?php

use Illuminate\Database\Seeder;
use Octoplus\Domain\Currency\Currency;

class CurrencySeeder extends Seeder
{
    private $Company_id;

    public function __construct()
    {
        $this->Company_id = session('company_id');
    }

    public function run()
    {
        Currency::create(
            [
                'company_id' => $this->Company_id,
                'name' => "Real",
                'code' => 'BRL',
                'rate' => '1.00',
                'enabled' => '1',
                'precision' => 2,
                'symbol' => 'R$',
                'symbol_first' => '1',
                'decimal_mark' => ",",
                'thousands_separator' => ".",
            ]);
    }
}