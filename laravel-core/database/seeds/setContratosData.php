<?php

use Illuminate\Database\Seeder;
use Modules\Financial\Models\Fatura;
use Modules\SaleRent\Models\Invoice;

class setContratosData extends Seeder
{
    private $Company_id;

    public function __construct()
    {
        $this->Company_id = 2;
    }

    public function run()
    {

        $contratos = \DB::connection('temporario')->table('contrato')->orderBy('Id','ASC')->get();

        foreach($contratos as $contrato)
        {
           
            $invoiceCode = settings('prefix.rent','PA'). str_pad(settings('sequence.rent',1) , 5 , '0' , STR_PAD_LEFT);

            $invoice = Invoice::create([
                'id' => $contrato->Id,
                'company_id' => $this->Company_id,
                'description' => $contrato->Obs . "\n" . $contrato->OutrosDescricao . "\n Fone1 : " . $contrato->Fone1 . "\n Fone2: " . $contrato->Fone2,
                'invoice_type' => 'rent',
                'amount' => $contrato->ValorTotalLocacaoMensal,
                'code'=> $invoiceCode,
                'extra_days'=> ($contrato->Mais15Dias ? 15 : null),
                'sale_date' => $contrato->DataInicial,
                'customer_id' => $contrato->Cliente,
                'shipping_address' => 'delivery',
                'payment_conditions' => 'monthly',
                'shipping_delivery' => $contrato->FreteColPagamento,
                'shipping_withdrawal' => $contrato->FreteRemPagamento,
                'shipping_value' => $contrato->ValorTotalFrete,
                'address_complete' => $contrato->EnderecoObra . ', ' . $contrato->Bairro . ', ' . $contrato->Cidade. ', ' . $contrato->UF. ', ' . $contrato->CEP. ", \n" . $contrato->PontoReferencia ,
                'shipping_status' => ($contrato->ContratoEncerrado ? 'closed' : 'delivered'),
                'payment_method' => ($contrato->Boleto ? 'billet' : 'money'),
                'rent_endDate' => $contrato->DataFinal,
                'quantity_parts' => $contrato->TempoLocacao,
                ]);

            if($invoice)
            {
                $customer = $invoice->customer;

                $customer->responsible = $contrato->Encarregado;
                $invoice->customer_name = $customer->user->name;
                
                $invoice->save();
                $customer->save();
                
                session(['tenant' => $this->Company_id]);
                //incrementa a sequencia do código
                settings(['sequence.rent' => settings('sequence.rent',1) +1])->save(); 
            }
                

        }


    }
}