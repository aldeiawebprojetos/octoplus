<?php

use Illuminate\Database\Seeder;
use Modules\Customers\Models\Customer;
use Octoplus\Domain\Currency\Currency;

class FixCustomerTypes extends Seeder
{


    public function run()
    {
        
        $customers = Customer::all();

        foreach($customers as $customer)
        {
           
           
            $customer->customer_types()->sync($customer->customer_type_id);
        }
    }
}