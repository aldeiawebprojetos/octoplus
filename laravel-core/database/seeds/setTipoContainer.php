<?php

use Illuminate\Database\Seeder;
use Octoplus\Domain\Category\Models\Category;


class setTipoContainerData extends Seeder
{
    private $Company_id;

    public function __construct()
    {
        $this->Company_id = 2;
    }

    public function run()
    {

        $tipoContainers = \DB::connection('temporario')->table('tipocontainer')->get();

        foreach($tipoContainers as $tipo)
        {
            
            Category::create([
                'company_id' => $this->Company_id,
                'id' => $tipo->Id,
                'type' => 'item',
                'name' => $tipo->Nome,
                'enabled' => true,
                'icon' => 'item-container',
            ]);
        }
    }
}