<?php

use Illuminate\Database\Seeder;
use Octoplus\Domain\Users\Models\Permission;
use Octoplus\Domain\Users\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Admin',
                'children' => [
                    [
                        'name' => 'Root',
                    ],
                ]
            ]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }

        $permissions = [
            [
                'name' => 'octoplus.categories_create'
            ],
            [
                'name' => 'octoplus.categories_update'
            ],
            [
                'name' => 'octoplus.categories_show'
            ],
            [
                'name' => 'octoplus.categories_delete'
            ],

            [
                'name' => 'octoplus.items_create'
            ],
            [
                'name' => 'octoplus.items_update'
            ],
            [
                'name' => 'octoplus.items_show'
            ],
            [
                'name' => 'octoplus.items_delete'
            ],

            [
                'name' => 'octoplus.users_create'
            ],
            [
                'name' => 'octoplus.users_update'
            ],
            [
                'name' => 'octoplus.users_show'
            ],
            [
                'name' => 'octoplus.users_delete'
            ],

            [
                'name' => 'octoplus.logs_show'
            ],
            
            [
                'name' => 'octoplus.assign_roles'
            ],
        ];

        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
    }
}
