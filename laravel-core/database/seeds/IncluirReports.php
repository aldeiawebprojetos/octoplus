<?php

use Illuminate\Database\Seeder;

class IncluirReports extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'status' => 1,
            'alias' => 'reports'
        ]);

        DB::table('company_modules')->insert([
            'company_id' => 1,
            'module_id' => 4
        ]);

        DB::table('permissions')->insert([
            'name' => 'reports.financial_show',
            'usable' => 1
        ]);

        DB::table('permissions')->insert([
            'name' => 'reports.history-items_show',
            'usable' => 1
        ]);

        DB::table('permissions')->insert([
            'name' => 'reports.statement_show',
            'usable' => 1
        ]);

        DB::table('permissions')->insert([
            'name' => 'reports.clientes-inadimplentes_show',
            'usable' => 1
        ]);
    }
}
