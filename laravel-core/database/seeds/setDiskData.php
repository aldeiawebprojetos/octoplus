<?php

use Illuminate\Database\Seeder;
use Octoplus\Domain\Item\Models\Item;


class setDiskData extends Seeder
{
    private $Company_id;

    public function __construct()
    {
        $this->Company_id = 2;
    }

    public function run()
    {

        $containers_contrato = \DB::connection('temporario')->table('containers_contrato')->get();

        foreach($containers_contrato as $container_contrato){
            $item = Item::find($container_contrato->Container);
            
            \DB::table('invoice_items')->insert([
                'invoice_id' => $container_contrato->Contrato,
                'item_id' => $container_contrato->Container,
                'name' => $item->name, 
                'quantity' => 1, 
                'price' => $container_contrato->ValorLocacao, 
                'total' => $container_contrato->ValorLocacao, 
                'tax_id' => null,
            ]);

            $item->update(['rent_status' => $container_contrato->Liberado]);
        }
    }
}