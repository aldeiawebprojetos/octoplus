/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 94);
/******/ })
/************************************************************************/
/******/ ({

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(11);


	/***/ }),
	
	/***/ 11:
	/***/ (function(module, exports, __webpack_require__) {
	
	/**
	 * Copyright (c) 2014-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */
	
	// This method of obtaining a reference to the global object needs to be
	// kept identical to the way it is obtained in runtime.js
	var g = (function() { return this })() || Function("return this")();
	
	// Use `getOwnPropertyNames` because not all browsers support calling
	// `hasOwnProperty` on the global `self` object in a worker. See #183.
	var hadRuntime = g.regeneratorRuntime &&
	  Object.getOwnPropertyNames(g).indexOf("regeneratorRuntime") >= 0;
	
	// Save the old regeneratorRuntime in case it needs to be restored later.
	var oldRuntime = hadRuntime && g.regeneratorRuntime;
	
	// Force reevalutation of runtime.js.
	g.regeneratorRuntime = undefined;
	
	module.exports = __webpack_require__(12);
	
	if (hadRuntime) {
	  // Restore the original runtime.
	  g.regeneratorRuntime = oldRuntime;
	} else {
	  // Remove the global property added by runtime.js.
	  try {
		delete g.regeneratorRuntime;
	  } catch(e) {
		g.regeneratorRuntime = undefined;
	  }
	}
	
	
	/***/ }),
	
	/***/ 12:
	/***/ (function(module, exports) {
	
	/**
	 * Copyright (c) 2014-present, Facebook, Inc.
	 *
	 * This source code is licensed under the MIT license found in the
	 * LICENSE file in the root directory of this source tree.
	 */
	
	!(function(global) {
	  "use strict";
	
	  var Op = Object.prototype;
	  var hasOwn = Op.hasOwnProperty;
	  var undefined; // More compressible than void 0.
	  var $Symbol = typeof Symbol === "function" ? Symbol : {};
	  var iteratorSymbol = $Symbol.iterator || "@@iterator";
	  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
	  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";
	
	  var inModule = typeof module === "object";
	  var runtime = global.regeneratorRuntime;
	  if (runtime) {
		if (inModule) {
		  // If regeneratorRuntime is defined globally and we're in a module,
		  // make the exports object identical to regeneratorRuntime.
		  module.exports = runtime;
		}
		// Don't bother evaluating the rest of this file if the runtime was
		// already defined globally.
		return;
	  }
	
	  // Define the runtime globally (as expected by generated code) as either
	  // module.exports (if we're in a module) or a new, empty object.
	  runtime = global.regeneratorRuntime = inModule ? module.exports : {};
	
	  function wrap(innerFn, outerFn, self, tryLocsList) {
		// If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
		var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
		var generator = Object.create(protoGenerator.prototype);
		var context = new Context(tryLocsList || []);
	
		// The ._invoke method unifies the implementations of the .next,
		// .throw, and .return methods.
		generator._invoke = makeInvokeMethod(innerFn, self, context);
	
		return generator;
	  }
	  runtime.wrap = wrap;
	
	  // Try/catch helper to minimize deoptimizations. Returns a completion
	  // record like context.tryEntries[i].completion. This interface could
	  // have been (and was previously) designed to take a closure to be
	  // invoked without arguments, but in all the cases we care about we
	  // already have an existing method we want to call, so there's no need
	  // to create a new function object. We can even get away with assuming
	  // the method takes exactly one argument, since that happens to be true
	  // in every case, so we don't have to touch the arguments object. The
	  // only additional allocation required is the completion record, which
	  // has a stable shape and so hopefully should be cheap to allocate.
	  function tryCatch(fn, obj, arg) {
		try {
		  return { type: "normal", arg: fn.call(obj, arg) };
		} catch (err) {
		  return { type: "throw", arg: err };
		}
	  }
	
	  var GenStateSuspendedStart = "suspendedStart";
	  var GenStateSuspendedYield = "suspendedYield";
	  var GenStateExecuting = "executing";
	  var GenStateCompleted = "completed";
	
	  // Returning this object from the innerFn has the same effect as
	  // breaking out of the dispatch switch statement.
	  var ContinueSentinel = {};
	
	  // Dummy constructor functions that we use as the .constructor and
	  // .constructor.prototype properties for functions that return Generator
	  // objects. For full spec compliance, you may wish to configure your
	  // minifier not to mangle the names of these two functions.
	  function Generator() {}
	  function GeneratorFunction() {}
	  function GeneratorFunctionPrototype() {}
	
	  // This is a polyfill for %IteratorPrototype% for environments that
	  // don't natively support it.
	  var IteratorPrototype = {};
	  IteratorPrototype[iteratorSymbol] = function () {
		return this;
	  };
	
	  var getProto = Object.getPrototypeOf;
	  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
	  if (NativeIteratorPrototype &&
		  NativeIteratorPrototype !== Op &&
		  hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
		// This environment has a native %IteratorPrototype%; use it instead
		// of the polyfill.
		IteratorPrototype = NativeIteratorPrototype;
	  }
	
	  var Gp = GeneratorFunctionPrototype.prototype =
		Generator.prototype = Object.create(IteratorPrototype);
	  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
	  GeneratorFunctionPrototype.constructor = GeneratorFunction;
	  GeneratorFunctionPrototype[toStringTagSymbol] =
		GeneratorFunction.displayName = "GeneratorFunction";
	
	  // Helper for defining the .next, .throw, and .return methods of the
	  // Iterator interface in terms of a single ._invoke method.
	  function defineIteratorMethods(prototype) {
		["next", "throw", "return"].forEach(function(method) {
		  prototype[method] = function(arg) {
			return this._invoke(method, arg);
		  };
		});
	  }
	
	  runtime.isGeneratorFunction = function(genFun) {
		var ctor = typeof genFun === "function" && genFun.constructor;
		return ctor
		  ? ctor === GeneratorFunction ||
			// For the native GeneratorFunction constructor, the best we can
			// do is to check its .name property.
			(ctor.displayName || ctor.name) === "GeneratorFunction"
		  : false;
	  };
	
	  runtime.mark = function(genFun) {
		if (Object.setPrototypeOf) {
		  Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
		} else {
		  genFun.__proto__ = GeneratorFunctionPrototype;
		  if (!(toStringTagSymbol in genFun)) {
			genFun[toStringTagSymbol] = "GeneratorFunction";
		  }
		}
		genFun.prototype = Object.create(Gp);
		return genFun;
	  };
	
	  // Within the body of any async function, `await x` is transformed to
	  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
	  // `hasOwn.call(value, "__await")` to determine if the yielded value is
	  // meant to be awaited.
	  runtime.awrap = function(arg) {
		return { __await: arg };
	  };
	
	  function AsyncIterator(generator) {
		function invoke(method, arg, resolve, reject) {
		  var record = tryCatch(generator[method], generator, arg);
		  if (record.type === "throw") {
			reject(record.arg);
		  } else {
			var result = record.arg;
			var value = result.value;
			if (value &&
				typeof value === "object" &&
				hasOwn.call(value, "__await")) {
			  return Promise.resolve(value.__await).then(function(value) {
				invoke("next", value, resolve, reject);
			  }, function(err) {
				invoke("throw", err, resolve, reject);
			  });
			}
	
			return Promise.resolve(value).then(function(unwrapped) {
			  // When a yielded Promise is resolved, its final value becomes
			  // the .value of the Promise<{value,done}> result for the
			  // current iteration. If the Promise is rejected, however, the
			  // result for this iteration will be rejected with the same
			  // reason. Note that rejections of yielded Promises are not
			  // thrown back into the generator function, as is the case
			  // when an awaited Promise is rejected. This difference in
			  // behavior between yield and await is important, because it
			  // allows the consumer to decide what to do with the yielded
			  // rejection (swallow it and continue, manually .throw it back
			  // into the generator, abandon iteration, whatever). With
			  // await, by contrast, there is no opportunity to examine the
			  // rejection reason outside the generator function, so the
			  // only option is to throw it from the await expression, and
			  // let the generator function handle the exception.
			  result.value = unwrapped;
			  resolve(result);
			}, reject);
		  }
		}
	
		var previousPromise;
	
		function enqueue(method, arg) {
		  function callInvokeWithMethodAndArg() {
			return new Promise(function(resolve, reject) {
			  invoke(method, arg, resolve, reject);
			});
		  }
	
		  return previousPromise =
			// If enqueue has been called before, then we want to wait until
			// all previous Promises have been resolved before calling invoke,
			// so that results are always delivered in the correct order. If
			// enqueue has not been called before, then it is important to
			// call invoke immediately, without waiting on a callback to fire,
			// so that the async generator function has the opportunity to do
			// any necessary setup in a predictable way. This predictability
			// is why the Promise constructor synchronously invokes its
			// executor callback, and why async functions synchronously
			// execute code before the first await. Since we implement simple
			// async functions in terms of async generators, it is especially
			// important to get this right, even though it requires care.
			previousPromise ? previousPromise.then(
			  callInvokeWithMethodAndArg,
			  // Avoid propagating failures to Promises returned by later
			  // invocations of the iterator.
			  callInvokeWithMethodAndArg
			) : callInvokeWithMethodAndArg();
		}
	
		// Define the unified helper method that is used to implement .next,
		// .throw, and .return (see defineIteratorMethods).
		this._invoke = enqueue;
	  }
	
	  defineIteratorMethods(AsyncIterator.prototype);
	  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
		return this;
	  };
	  runtime.AsyncIterator = AsyncIterator;
	
	  // Note that simple async functions are implemented on top of
	  // AsyncIterator objects; they just return a Promise for the value of
	  // the final result produced by the iterator.
	  runtime.async = function(innerFn, outerFn, self, tryLocsList) {
		var iter = new AsyncIterator(
		  wrap(innerFn, outerFn, self, tryLocsList)
		);
	
		return runtime.isGeneratorFunction(outerFn)
		  ? iter // If outerFn is a generator, return the full iterator.
		  : iter.next().then(function(result) {
			  return result.done ? result.value : iter.next();
			});
	  };
	
	  function makeInvokeMethod(innerFn, self, context) {
		var state = GenStateSuspendedStart;
	
		return function invoke(method, arg) {
		  if (state === GenStateExecuting) {
			throw new Error("Generator is already running");
		  }
	
		  if (state === GenStateCompleted) {
			if (method === "throw") {
			  throw arg;
			}
	
			// Be forgiving, per 25.3.3.3.3 of the spec:
			// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
			return doneResult();
		  }
	
		  context.method = method;
		  context.arg = arg;
	
		  while (true) {
			var delegate = context.delegate;
			if (delegate) {
			  var delegateResult = maybeInvokeDelegate(delegate, context);
			  if (delegateResult) {
				if (delegateResult === ContinueSentinel) continue;
				return delegateResult;
			  }
			}
	
			if (context.method === "next") {
			  // Setting context._sent for legacy support of Babel's
			  // function.sent implementation.
			  context.sent = context._sent = context.arg;
	
			} else if (context.method === "throw") {
			  if (state === GenStateSuspendedStart) {
				state = GenStateCompleted;
				throw context.arg;
			  }
	
			  context.dispatchException(context.arg);
	
			} else if (context.method === "return") {
			  context.abrupt("return", context.arg);
			}
	
			state = GenStateExecuting;
	
			var record = tryCatch(innerFn, self, context);
			if (record.type === "normal") {
			  // If an exception is thrown from innerFn, we leave state ===
			  // GenStateExecuting and loop back for another invocation.
			  state = context.done
				? GenStateCompleted
				: GenStateSuspendedYield;
	
			  if (record.arg === ContinueSentinel) {
				continue;
			  }
	
			  return {
				value: record.arg,
				done: context.done
			  };
	
			} else if (record.type === "throw") {
			  state = GenStateCompleted;
			  // Dispatch the exception by looping back around to the
			  // context.dispatchException(context.arg) call above.
			  context.method = "throw";
			  context.arg = record.arg;
			}
		  }
		};
	  }
	
	  // Call delegate.iterator[context.method](context.arg) and handle the
	  // result, either by returning a { value, done } result from the
	  // delegate iterator, or by modifying context.method and context.arg,
	  // setting context.delegate to null, and returning the ContinueSentinel.
	  function maybeInvokeDelegate(delegate, context) {
		var method = delegate.iterator[context.method];
		if (method === undefined) {
		  // A .throw or .return when the delegate iterator has no .throw
		  // method always terminates the yield* loop.
		  context.delegate = null;
	
		  if (context.method === "throw") {
			if (delegate.iterator.return) {
			  // If the delegate iterator has a return method, give it a
			  // chance to clean up.
			  context.method = "return";
			  context.arg = undefined;
			  maybeInvokeDelegate(delegate, context);
	
			  if (context.method === "throw") {
				// If maybeInvokeDelegate(context) changed context.method from
				// "return" to "throw", let that override the TypeError below.
				return ContinueSentinel;
			  }
			}
	
			context.method = "throw";
			context.arg = new TypeError(
			  "The iterator does not provide a 'throw' method");
		  }
	
		  return ContinueSentinel;
		}
	
		var record = tryCatch(method, delegate.iterator, context.arg);
	
		if (record.type === "throw") {
		  context.method = "throw";
		  context.arg = record.arg;
		  context.delegate = null;
		  return ContinueSentinel;
		}
	
		var info = record.arg;
	
		if (! info) {
		  context.method = "throw";
		  context.arg = new TypeError("iterator result is not an object");
		  context.delegate = null;
		  return ContinueSentinel;
		}
	
		if (info.done) {
		  // Assign the result of the finished delegate to the temporary
		  // variable specified by delegate.resultName (see delegateYield).
		  context[delegate.resultName] = info.value;
	
		  // Resume execution at the desired location (see delegateYield).
		  context.next = delegate.nextLoc;
	
		  // If context.method was "throw" but the delegate handled the
		  // exception, let the outer generator proceed normally. If
		  // context.method was "next", forget context.arg since it has been
		  // "consumed" by the delegate iterator. If context.method was
		  // "return", allow the original .return call to continue in the
		  // outer generator.
		  if (context.method !== "return") {
			context.method = "next";
			context.arg = undefined;
		  }
	
		} else {
		  // Re-yield the result returned by the delegate method.
		  return info;
		}
	
		// The delegate iterator is finished, so forget it and continue with
		// the outer generator.
		context.delegate = null;
		return ContinueSentinel;
	  }
	
	  // Define Generator.prototype.{next,throw,return} in terms of the
	  // unified ._invoke helper method.
	  defineIteratorMethods(Gp);
	
	  Gp[toStringTagSymbol] = "Generator";
	
	  // A Generator should always return itself as the iterator object when the
	  // @@iterator function is called on it. Some browsers' implementations of the
	  // iterator prototype chain incorrectly implement this, causing the Generator
	  // object to not be returned from this call. This ensures that doesn't happen.
	  // See https://github.com/facebook/regenerator/issues/274 for more details.
	  Gp[iteratorSymbol] = function() {
		return this;
	  };
	
	  Gp.toString = function() {
		return "[object Generator]";
	  };
	
	  function pushTryEntry(locs) {
		var entry = { tryLoc: locs[0] };
	
		if (1 in locs) {
		  entry.catchLoc = locs[1];
		}
	
		if (2 in locs) {
		  entry.finallyLoc = locs[2];
		  entry.afterLoc = locs[3];
		}
	
		this.tryEntries.push(entry);
	  }
	
	  function resetTryEntry(entry) {
		var record = entry.completion || {};
		record.type = "normal";
		delete record.arg;
		entry.completion = record;
	  }
	
	  function Context(tryLocsList) {
		// The root entry object (effectively a try statement without a catch
		// or a finally block) gives us a place to store values thrown from
		// locations where there is no enclosing try statement.
		this.tryEntries = [{ tryLoc: "root" }];
		tryLocsList.forEach(pushTryEntry, this);
		this.reset(true);
	  }
	
	  runtime.keys = function(object) {
		var keys = [];
		for (var key in object) {
		  keys.push(key);
		}
		keys.reverse();
	
		// Rather than returning an object with a next method, we keep
		// things simple and return the next function itself.
		return function next() {
		  while (keys.length) {
			var key = keys.pop();
			if (key in object) {
			  next.value = key;
			  next.done = false;
			  return next;
			}
		  }
	
		  // To avoid creating an additional object, we just hang the .value
		  // and .done properties off the next function object itself. This
		  // also ensures that the minifier will not anonymize the function.
		  next.done = true;
		  return next;
		};
	  };
	
	  function values(iterable) {
		if (iterable) {
		  var iteratorMethod = iterable[iteratorSymbol];
		  if (iteratorMethod) {
			return iteratorMethod.call(iterable);
		  }
	
		  if (typeof iterable.next === "function") {
			return iterable;
		  }
	
		  if (!isNaN(iterable.length)) {
			var i = -1, next = function next() {
			  while (++i < iterable.length) {
				if (hasOwn.call(iterable, i)) {
				  next.value = iterable[i];
				  next.done = false;
				  return next;
				}
			  }
	
			  next.value = undefined;
			  next.done = true;
	
			  return next;
			};
	
			return next.next = next;
		  }
		}
	
		// Return an iterator with no values.
		return { next: doneResult };
	  }
	  runtime.values = values;
	
	  function doneResult() {
		return { value: undefined, done: true };
	  }
	
	  Context.prototype = {
		constructor: Context,
	
		reset: function(skipTempReset) {
		  this.prev = 0;
		  this.next = 0;
		  // Resetting context._sent for legacy support of Babel's
		  // function.sent implementation.
		  this.sent = this._sent = undefined;
		  this.done = false;
		  this.delegate = null;
	
		  this.method = "next";
		  this.arg = undefined;
	
		  this.tryEntries.forEach(resetTryEntry);
	
		  if (!skipTempReset) {
			for (var name in this) {
			  // Not sure about the optimal order of these conditions:
			  if (name.charAt(0) === "t" &&
				  hasOwn.call(this, name) &&
				  !isNaN(+name.slice(1))) {
				this[name] = undefined;
			  }
			}
		  }
		},
	
		stop: function() {
		  this.done = true;
	
		  var rootEntry = this.tryEntries[0];
		  var rootRecord = rootEntry.completion;
		  if (rootRecord.type === "throw") {
			throw rootRecord.arg;
		  }
	
		  return this.rval;
		},
	
		dispatchException: function(exception) {
		  if (this.done) {
			throw exception;
		  }
	
		  var context = this;
		  function handle(loc, caught) {
			record.type = "throw";
			record.arg = exception;
			context.next = loc;
	
			if (caught) {
			  // If the dispatched exception was caught by a catch block,
			  // then let that catch block handle the exception normally.
			  context.method = "next";
			  context.arg = undefined;
			}
	
			return !! caught;
		  }
	
		  for (var i = this.tryEntries.length - 1; i >= 0; --i) {
			var entry = this.tryEntries[i];
			var record = entry.completion;
	
			if (entry.tryLoc === "root") {
			  // Exception thrown outside of any try block that could handle
			  // it, so set the completion value of the entire function to
			  // throw the exception.
			  return handle("end");
			}
	
			if (entry.tryLoc <= this.prev) {
			  var hasCatch = hasOwn.call(entry, "catchLoc");
			  var hasFinally = hasOwn.call(entry, "finallyLoc");
	
			  if (hasCatch && hasFinally) {
				if (this.prev < entry.catchLoc) {
				  return handle(entry.catchLoc, true);
				} else if (this.prev < entry.finallyLoc) {
				  return handle(entry.finallyLoc);
				}
	
			  } else if (hasCatch) {
				if (this.prev < entry.catchLoc) {
				  return handle(entry.catchLoc, true);
				}
	
			  } else if (hasFinally) {
				if (this.prev < entry.finallyLoc) {
				  return handle(entry.finallyLoc);
				}
	
			  } else {
				throw new Error("try statement without catch or finally");
			  }
			}
		  }
		},
	
		abrupt: function(type, arg) {
		  for (var i = this.tryEntries.length - 1; i >= 0; --i) {
			var entry = this.tryEntries[i];
			if (entry.tryLoc <= this.prev &&
				hasOwn.call(entry, "finallyLoc") &&
				this.prev < entry.finallyLoc) {
			  var finallyEntry = entry;
			  break;
			}
		  }
	
		  if (finallyEntry &&
			  (type === "break" ||
			   type === "continue") &&
			  finallyEntry.tryLoc <= arg &&
			  arg <= finallyEntry.finallyLoc) {
			// Ignore the finally entry if control is not jumping to a
			// location outside the try/catch block.
			finallyEntry = null;
		  }
	
		  var record = finallyEntry ? finallyEntry.completion : {};
		  record.type = type;
		  record.arg = arg;
	
		  if (finallyEntry) {
			this.method = "next";
			this.next = finallyEntry.finallyLoc;
			return ContinueSentinel;
		  }
	
		  return this.complete(record);
		},
	
		complete: function(record, afterLoc) {
		  if (record.type === "throw") {
			throw record.arg;
		  }
	
		  if (record.type === "break" ||
			  record.type === "continue") {
			this.next = record.arg;
		  } else if (record.type === "return") {
			this.rval = this.arg = record.arg;
			this.method = "return";
			this.next = "end";
		  } else if (record.type === "normal" && afterLoc) {
			this.next = afterLoc;
		  }
	
		  return ContinueSentinel;
		},
	
		finish: function(finallyLoc) {
		  for (var i = this.tryEntries.length - 1; i >= 0; --i) {
			var entry = this.tryEntries[i];
			if (entry.finallyLoc === finallyLoc) {
			  this.complete(entry.completion, entry.afterLoc);
			  resetTryEntry(entry);
			  return ContinueSentinel;
			}
		  }
		},
	
		"catch": function(tryLoc) {
		  for (var i = this.tryEntries.length - 1; i >= 0; --i) {
			var entry = this.tryEntries[i];
			if (entry.tryLoc === tryLoc) {
			  var record = entry.completion;
			  if (record.type === "throw") {
				var thrown = record.arg;
				resetTryEntry(entry);
			  }
			  return thrown;
			}
		  }
	
		  // The context.catch method must only be called with a location
		  // argument that corresponds to a known catch block.
		  throw new Error("illegal catch attempt");
		},
	
		delegateYield: function(iterable, resultName, nextLoc) {
		  this.delegate = {
			iterator: values(iterable),
			resultName: resultName,
			nextLoc: nextLoc
		  };
	
		  if (this.method === "next") {
			// Deliberately forget the last sent value so that we don't
			// accidentally pass it on to the delegate.
			this.arg = undefined;
		  }
	
		  return ContinueSentinel;
		}
	  };
	})(
	  // In sloppy mode, unbound `this` refers to the global object, fallback to
	  // Function constructor if we're in global strict mode. That is sadly a form
	  // of indirect eval which violates Content Security Policy.
	  (function() { return this })() || Function("return this")()
	);
	
	
	/***/ }),
	
	/***/ 38:
	/***/ (function(module, __webpack_exports__, __webpack_require__) {
	
	"use strict";
	/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__error__ = __webpack_require__(39);
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	
	
	var Form = function () {
		function Form(form_id) {
			_classCallCheck(this, Form);
	
			var form = document.getElementById(form_id);
	
			if (!form) {
				return;
			}
	
			this['method'] = form.getAttribute('method').toLowerCase();
			this['action'] = form.getAttribute('action');
	
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;
	
			try {
				for (var _iterator = document.getElementById(form_id).getElementsByTagName("input")[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var form_element = _step.value;
	
					if (form_element.getAttribute('id') == 'global-search') {
						continue;
					}
	
					var name = form_element.getAttribute('name');
					var type = form_element.getAttribute('type');
	
					if (name == 'method') {
						continue;
					}
	
					if (form_element.getAttribute('data-item')) {
						if (!this['items']) {
							var item = {};
							var row = {};
	
							item[0] = row;
							this['items'] = item;
						}
	
						if (!this['items'][0][form_element.getAttribute('data-item')]) {
							this['items'][0][form_element.getAttribute('data-item')] = '';
						}
	
						this['item_backup'] = this['items'];
	
						continue;
					}
	
					if (form_element.getAttribute('data-field')) {
						if (!this[form_element.getAttribute('data-field')]) {
							var field = {};
	
							this[form_element.getAttribute('data-field')] = field;
						}
	
						/*
						if (!this[form_element.getAttribute('data-field')][name]) {
							this[form_element.getAttribute('data-field')][name] = '';
						}
						*/
	
						if (type == 'radio') {
							if (!this[form_element.getAttribute('data-field')][name]) {
								this[form_element.getAttribute('data-field')][name] = (form_element.getAttribute('value') ? 1 : 0) || 0;
							}
						} else if (type == 'checkbox') {
							if (this[form_element.getAttribute('data-field')][name]) {
								if (!this[form_element.getAttribute('data-field')][name].push) {
									this[form_element.getAttribute('data-field')][name] = [this[form_element.getAttribute('data-field')][name]];
								}
	
								if (form_element.checked) {
									this[form_element.getAttribute('data-field')][name].push(form_element.value);
								}
							} else {
								if (form_element.checked) {
									this[form_element.getAttribute('data-field')][name] = form_element.value;
								} else {
									this[form_element.getAttribute('data-field')][name] = [];
								}
							}
						} else {
							this[form_element.getAttribute('data-field')][name] = form_element.getAttribute('value') || '';
						}
	
						continue;
					}
	
					if (type == 'radio') {
						if (!this[name]) {
							this[name] = (form_element.getAttribute('value') ? 1 : 0) || 0;
						}
					} else if (type == 'checkbox') {
						if (this[name]) {
							if (!this[name].push) {
								this[name] = [this[name]];
							}
	
							if (form_element.checked) {
								this[name].push(form_element.value);
							}
						} else {
							if (form_element.checked) {
								this[name] = form_element.value;
							} else {
								this[name] = [];
							}
						}
					} else {
						this[name] = form_element.getAttribute('value') || '';
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}
	
			var _iteratorNormalCompletion2 = true;
			var _didIteratorError2 = false;
			var _iteratorError2 = undefined;
	
			try {
				for (var _iterator2 = document.getElementById(form_id).getElementsByTagName("textarea")[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
					var _form_element = _step2.value;
	
					var name = _form_element.getAttribute('name');
	
					if (name == 'method') {
						continue;
					}
	
					if (_form_element.getAttribute('data-item')) {
						console.log(this);
						if (!this['items']) {
							var item = {};
							var row = {};
	
							item[0] = row;
							this['items'] = item;
						}
	
						if (!this['items'][0][_form_element.getAttribute('data-item')]) {
							this['items'][0][_form_element.getAttribute('data-item')] = '';
						}
	
						this['item_backup'] = this['items'];
	
						continue;
					}
	
					if (_form_element.getAttribute('data-field')) {
						if (!this[_form_element.getAttribute('data-field')]) {
							var field = {};
	
							this[_form_element.getAttribute('data-field')] = field;
						}
	
						if (!this[_form_element.getAttribute('data-field')][name]) {
							this[_form_element.getAttribute('data-field')][name] = '';
						}
	
						continue;
					}
	
					if (this[name]) {
						if (!this[name].push) {
							this[name] = [this[name]];
						}
	
						this[name].push(_form_element.value || '');
					} else {
						this[name] = _form_element.value || '';
					}
				}
			} catch (err) {
				_didIteratorError2 = true;
				_iteratorError2 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion2 && _iterator2.return) {
						_iterator2.return();
					}
				} finally {
					if (_didIteratorError2) {
						throw _iteratorError2;
					}
				}
			}
	
			var _iteratorNormalCompletion3 = true;
			var _didIteratorError3 = false;
			var _iteratorError3 = undefined;
	
			try {
				for (var _iterator3 = document.getElementById(form_id).getElementsByTagName("select")[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
					var _form_element2 = _step3.value;
	
					var name = _form_element2.getAttribute('name');
	
					if (name == 'method') {
						continue;
					}
	
					if (_form_element2.getAttribute('data-item')) {
						if (!this['items']) {
							var item = {};
							var row = {};
	
							item[0] = row;
							this['items'] = item;
						}
	
						if (!this['items'][0][_form_element2.getAttribute('data-item')]) {
							this['items'][0][_form_element2.getAttribute('data-item')] = '';
						}
	
						this['item_backup'] = this['items'];
	
						continue;
					}
	
					if (_form_element2.getAttribute('data-field')) {
						if (!this[_form_element2.getAttribute('data-field')]) {
							var field = {};
	
							this[_form_element2.getAttribute('data-field')] = field;
						}
	
						if (!this[_form_element2.getAttribute('data-field')][name]) {
							this[_form_element2.getAttribute('data-field')][name] = '';
						}
	
						continue;
					}
	
					if (this[name]) {
						if (!this[name].push) {
							this[name] = [this[name]];
						}
	
						this[name].push(_form_element2.value || '');
					} else {
						this[name] = _form_element2.value || '';
					}
				}
			} catch (err) {
				_didIteratorError3 = true;
				_iteratorError3 = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion3 && _iterator3.return) {
						_iterator3.return();
					}
				} finally {
					if (_didIteratorError3) {
						throw _iteratorError3;
					}
				}
			}
	
			this.errors = new __WEBPACK_IMPORTED_MODULE_0__error__["a" /* default */]();
	
			this.loading = false;
	
			this.response = {};
		}
	
		_createClass(Form, [{
			key: 'data',
			value: function data() {
				var data = Object.assign({}, this);
	
				delete data.method;
				delete data.action;
				delete data.errors;
				delete data.loading;
				delete data.response;
	
				return data;
			}
		}, {
			key: 'reset',
			value: function reset() {
				var _iteratorNormalCompletion4 = true;
				var _didIteratorError4 = false;
				var _iteratorError4 = undefined;
	
				try {
					for (var _iterator4 = document.getElementsByTagName("input")[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
						var form_element = _step4.value;
	
						var name = form_element.getAttribute('name');
	
						if (this[name]) {
							this[name] = '';
						}
					}
				} catch (err) {
					_didIteratorError4 = true;
					_iteratorError4 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion4 && _iterator4.return) {
							_iterator4.return();
						}
					} finally {
						if (_didIteratorError4) {
							throw _iteratorError4;
						}
					}
				}
	
				var _iteratorNormalCompletion5 = true;
				var _didIteratorError5 = false;
				var _iteratorError5 = undefined;
	
				try {
					for (var _iterator5 = document.getElementsByTagName("textarea")[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
						var _form_element3 = _step5.value;
	
						var name = _form_element3.getAttribute('name');
	
						if (this[name]) {
							this[name] = '';
						}
					}
				} catch (err) {
					_didIteratorError5 = true;
					_iteratorError5 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion5 && _iterator5.return) {
							_iterator5.return();
						}
					} finally {
						if (_didIteratorError5) {
							throw _iteratorError5;
						}
					}
				}
	
				var _iteratorNormalCompletion6 = true;
				var _didIteratorError6 = false;
				var _iteratorError6 = undefined;
	
				try {
					for (var _iterator6 = document.getElementsByTagName("select")[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
						var _form_element4 = _step6.value;
	
						var name = _form_element4.getAttribute('name');
	
						if (this[name]) {
							this[name] = '';
						}
					}
				} catch (err) {
					_didIteratorError6 = true;
					_iteratorError6 = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion6 && _iterator6.return) {
							_iterator6.return();
						}
					} finally {
						if (_didIteratorError6) {
							throw _iteratorError6;
						}
					}
				}
			}
		}, {
			key: 'oldSubmit',
			value: function oldSubmit() {
				this.loading = true;
	
				//    window.axios({
				//         method: this.method,
				//         url: this.action,
				//         data: this.data()
				//     })
				//     .then(this.onSuccess.bind(this))
				//     .catch(this.onFail.bind(this));
			}
		}, {
			key: 'submit',
			value: function submit() {
				FormData.prototype.appendRecursive = function (data) {
					var wrapper = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	
					for (var name in data) {
						if (wrapper) {
							if ((_typeof(data[name]) == 'object' || data[name].constructor === Array) && data[name] instanceof File != true && data[name] instanceof Blob != true) {
								this.appendRecursive(data[name], wrapper + '[' + name + ']');
							} else {
								this.append(wrapper + '[' + name + ']', data[name]);
							}
						} else {
							if ((_typeof(data[name]) == 'object' || data[name].constructor === Array) && data[name] instanceof File != true && data[name] instanceof Blob != true) {
								this.appendRecursive(data[name], name);
							} else {
								this.append(name, data[name]);
							}
						}
					}
				};
	
				this.loading = true;
	
				var data = this.data();
	
				var form_data = new FormData();
				form_data.appendRecursive(data);
	
				window.axios({
					method: this.method,
					url: this.action,
					data: form_data,
					headers: {
						'X-CSRF-TOKEN': window.Laravel.csrfToken,
						'X-Requested-With': 'XMLHttpRequest',
						'Content-Type': 'multipart/form-data'
					}
				}).then(this.onSuccess.bind(this)).catch(this.onFail.bind(this));
			}
		}, {
			key: 'onSuccess',
			value: function onSuccess(response) {
				this.errors.clear();
	
				this.loading = false;
	
				if (response.data.redirect) {
					this.loading = true;
	
					window.location.href = response.data.redirect;
				}
	
				this.response = response.data;
			}
	
			// Form fields check validation issue
	
		}, {
			key: 'onFail',
			value: function onFail(error) {
				this.errors.record(error.response.data.errors);
	
				this.loading = false;
			}
		}]);
	
		return Form;
	}();
	
	/* harmony default export */ __webpack_exports__["a"] = (Form);
	
	/***/ }),
	
	/***/ 39:
	/***/ (function(module, __webpack_exports__, __webpack_require__) {
	
	"use strict";
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Errors = function () {
		function Errors() {
			_classCallCheck(this, Errors);
	
			this.errors = {};
		}
	
		_createClass(Errors, [{
			key: "has",
			value: function has(field) {
				// if this.errors contains as "field" property.
				return this.errors.hasOwnProperty(field);
			}
		}, {
			key: "any",
			value: function any() {
				return Object.keys(this.errors).length > 0;
			}
		}, {
			key: "set",
			value: function set(key, field) {
				return this.errors[key] = field;
			}
		}, {
			key: "get",
			value: function get(field) {
				if (this.errors[field]) {
					return this.errors[field][0];
				}
			}
		}, {
			key: "record",
			value: function record(errors) {
				this.errors = errors;
			}
		}, {
			key: "clear",
			value: function clear(field) {
				if (field) {
					return delete this.errors[field];
				}
	
				this.errors = {};
			}
		}]);
	
		return Errors;
	}();
	
	/* harmony default export */ __webpack_exports__["a"] = (Errors);
	
	/***/ }),
	
	/***/ 94:
	/***/ (function(module, exports, __webpack_require__) {
	
	module.exports = __webpack_require__(95);
	
	
	/***/ }),
	
	/***/ 95:
	/***/ (function(module, __webpack_exports__, __webpack_require__) {
	
	"use strict";
	Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
	/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__ = __webpack_require__(10);
	/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator__);
	/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__plugins_form__ = __webpack_require__(38);
	
	
	function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }
	
	
	
	var vm = new Vue({
		el: '#app',
		data: function data() {
			return {
				form: new __WEBPACK_IMPORTED_MODULE_1__plugins_form__["a" /* default */]('invoice'),
				totals: {
					sub: 0,
					item_discount: '',
					discount: 0,
					discount_text: false,
					tax: 0,
					total: 0
				},
				parts: [],
				transaction: [],
				discount: 0,
				taxes: [],
				edit: {},
				items_bakcup: [],
				colspan: 6
	
			};
		},
		mounted: function () {
			var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee() {
				var row, data_sale;
				return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								if (document.getElementById('items') != null && document.getElementById('items').rows) {
									this.colspan = document.getElementById("items").rows[0].cells.length - 1;
								}
	
								this.items_bakcup = this.form.item_backup;
								this.form.items = [];
								this.form.quantity_parts = 1;
								this.form.extra_days = 0;
	
								if (typeof invoice_data === 'undefined') {
									//this.form.payment_conditions = "monthly";
									this.edit.status = false;
	
									this.onAddItem();
								} else {
	
									for (row in invoice_data) {
										if (row == 'sale_date') {
											data_sale = new Date(this.form.sale_date);
	
											this.form.sale_date = this.formatDate(data_sale, 'yyyy-mm-dd');
										} else if (row == 'shipping_value') {
											this.form.shipping_value = parseFloat(invoice_data['shipping_value']);
										} else {
											this.form[row] = invoice_data[row];
										}
									}
	
									this.taxes = taxes_edit;
									this.edit.status = true;
									this.onSelectConditions();
									this.onSelectFrete();
									this.onAddDiscount();
	
									if (typeof invoice_parts !== 'undefined') {
										this.prepareFaturas();
									}
								}
								this.form.old_quantity_parts = this.form.quantity_parts;
								this.form.customer_adress_formated = null;
	
								if (!(typeof invoice_items !== 'undefined' && invoice_items)) {
									_context.next = 12;
									break;
								}
	
								_context.next = 11;
								return sleep(300);
	
							case 11:
								this.form.items = invoice_items;
	
							case 12:
								this.form.old_rent_endDate = this.form.rent_endDate;
								this.form.old_extra_days = this.form.extra_days;
	
							case 14:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, this);
			}));
	
			function mounted() {
				return _ref.apply(this, arguments);
			}
	
			return mounted;
		}(),
	
	
		methods: {
			onAddItem: function onAddItem() {
				this.form.items.push(Object.assign({}, this.items_bakcup[0]));
			},
			getShowLink: function getShowLink() {
				var link = document.getElementById('showLink');
	
				link.removeAttribute('href');
				link.setAttribute('href', this.form.customer_route);
	
				link.classList.remove('hidden');
			},
			hideShowLink: function hideShowLink() {
				var link = document.getElementById('showLink');
				link.removeAttribute('href');
				link.classList.add('hidden');
			},
			prepareFaturas: function prepareFaturas() {
				for (var row in invoice_parts) {
	
					var data = new Date(invoice_parts[row]['data_vencimento']);
					var data_competencia = this.formatDate(new Date(invoice_parts[row].data_competencia), 'yyyy-mm-dd');
	
					invoice_parts[row]['class'] = invoice_parts[row]['status'] == 'canceled' ? 'primary' : invoice_parts[row]['status'] == 'settled' ? 'success' : invoice_parts[row]['vencendo'] ? 'warning' : invoice_parts[row]['vencido'] ? 'danger' : 'default';
					invoice_parts[row]['name'] = translate_data.part + ' ' + (invoice_parts[row].code.indexOf(translate_data.shipping) > -1 ? translate_data.shipping : parseInt(row) + 1 + '/' + this.form.quantity_parts);
					invoice_parts[row]['block_frete'] = invoice_parts[row].code.indexOf(translate_data.shipping) > -1 || data_competencia < '2021-02-02';
					invoice_parts[row]['data_vencimento'] = this.formatDate(data, 'yyyy-mm-dd');
	
					if (typeof this.form.default_url != 'undefined') {
						invoice_parts[row]['link'] = this.form.default_url.replace('#row_id#', invoice_parts[row]['id']);
					}
				}
	
				this.parts = invoice_parts;
			},
			onSelectFrete: function onSelectFrete() {
	
				var freteDiv = document.getElementById('freteDetails');
				var arrayAddress = this.form.customer_address;
	
				if (typeof arrayAddress !== 'undefined') {
					if (this.form.shipping_address === "delivery_home") {
						this.form.address_complete = arrayAddress.address + (arrayAddress.complement ? ', ' + arrayAddress.complement + ', ' : ', ') + arrayAddress.neighborhood + ', ' + arrayAddress.city + '-' + arrayAddress.uf + ', CEP: ' + arrayAddress.cep;
					} else {
						this.form.address_complete = '';
					}
				}
			},
			onSelectConditions: function onSelectConditions() {
	
				var paymentDetails = document.getElementById("paymentPartDetails");
	
				if (this.form.payment_conditions === 'in_cash') {
					paymentDetails.classList.remove('hidden');
				} else {
	
					paymentDetails.classList.remove('hidden');
				}
	
				this.onPartsQuantity();
			},
	
			//    async  onAddPart(){
			//         this.parts.push({
			//             type: 'part',               
			//             date:  this.formatDate(new Date(),'yyyy-mm-dd') ,
			//             value: 0.00,
			//         });
			//         await sleep(50);
			//         for(var row in this.parts)
			//         {
			//             if(typeof this.parts[row].id == "undefined")
			//             {                    
			//                 const data_input = document.getElementById(`date_${row}`),
			//                 value_input = document.getElementById(`value_${row}`),
			//                 code_input = document.getElementById(`code_${row}`);
	
			//                 data_input.removeAttribute('readonly');
			//                 value_input.removeAttribute('readonly');
			//                 code_input.classList.remove('col-md-2');
			//                 code_input.classList.add('col-md-3');
	
			//             }
			//         }
			//     },
			onRemovePart: function onRemovePart(index) {
				this.parts.splice(index, 1);
			},
			onPartsQuantity: function () {
				var _ref2 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.mark(function _callee2() {
					var index, data_vencimento, row, data_input, value_input, extra_daysValue, _data_vencimento, _data_input, _value_input, block_delivery, block_withdrawal, data, _data_input2, _value_input2, _data_input3, _value_input3, _data, first_data, frete_value, i, _extra_daysValue, endDate, div_paymentDetails;
	
					return __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
						while (1) {
							switch (_context2.prev = _context2.next) {
								case 0:
									this.setFreteAmount();
	
									if (!this.edit.status) {
										_context2.next = 56;
										break;
									}
	
									if (!(parseInt(this.form.old_quantity_parts) < parseInt(this.form.quantity_parts))) {
										_context2.next = 20;
										break;
									}
	
									for (index in this.parts) {
										if (typeof this.parts[index].id == 'undefined') {
											this.parts.splice(index);
										}
									}
	
									data_vencimento = new Date(this.form.faturas[this.form.faturas.length - 1].data_vencimento);
	
	
									for (i = 0; i < parseInt(this.form.quantity_parts) - parseInt(this.form.old_quantity_parts); i++) {
										this.setFrequencyPayment(data_vencimento);
										this.parts.push({
											type: 'part',
											data_vencimento: this.formatDate(data_vencimento, 'yyyy-mm-dd'),
											value: this.totals.total
										});
									}
									this.form.rent_endDate = this.formatDate(data_vencimento, 'yyyy-mm-dd');
									_context2.t0 = __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.keys(this.parts);
	
								case 8:
									if ((_context2.t1 = _context2.t0()).done) {
										_context2.next = 18;
										break;
									}
	
									row = _context2.t1.value;
	
									if (!(typeof this.parts[row].id == 'undefined')) {
										_context2.next = 16;
										break;
									}
	
									_context2.next = 13;
									return sleep(50);
	
								case 13:
									data_input = document.getElementById('date_' + row), value_input = document.getElementById('value_' + row);
	
	
									data_input.removeAttribute('readonly');
									value_input.removeAttribute('readonly');
	
								case 16:
									_context2.next = 8;
									break;
	
								case 18:
									_context2.next = 40;
									break;
	
								case 20:
									if (!(this.form.old_extra_days < this.form.extra_days)) {
										_context2.next = 39;
										break;
									}
	
									for (index in this.parts) {
										if (typeof this.parts[index].id == 'undefined') {
											this.parts.splice(index);
										}
									}
									extra_daysValue = this.totals.total / 30 * this.form.extra_days, _data_vencimento = new Date(this.form.rent_endDate);
	
	
									_data_vencimento.setDate(_data_vencimento.getDate() + parseInt(this.form.extra_days));
	
									this.parts.push({
										type: 'part',
										data_vencimento: this.formatDate(_data_vencimento, 'yyyy-mm-dd'),
										value: extra_daysValue
									});
	
									_context2.t2 = __WEBPACK_IMPORTED_MODULE_0_babel_runtime_regenerator___default.a.keys(this.parts);
	
								case 26:
									if ((_context2.t3 = _context2.t2()).done) {
										_context2.next = 36;
										break;
									}
	
									row = _context2.t3.value;
	
									if (!(typeof this.parts[row].id == 'undefined')) {
										_context2.next = 34;
										break;
									}
	
									_context2.next = 31;
									return sleep(50);
	
								case 31:
									_data_input = document.getElementById('date_' + row), _value_input = document.getElementById('value_' + row);
	
	
									_data_input.removeAttribute('readonly');
									_value_input.removeAttribute('readonly');
	
								case 34:
									_context2.next = 26;
									break;
	
								case 36:
									this.form.rent_endDate = this.formatDate(_data_vencimento, 'yyyy-mm-dd');
									_context2.next = 40;
									break;
	
								case 39:
									for (index in this.parts) {
										if (typeof this.parts[index].id == 'undefined') {
											this.parts.splice(index);
										}
									}
	
								case 40:
									block_delivery = {}, block_withdrawal = {};
									data = new Date(new Date().getFullYear(), new Date().getMonth(), this.form.payment_day !== '' ? this.form.payment_day : new Date().getDate());
	
	
									for (index in this.parts) {
										if (this.parts[index].edit == 'delivery') {
											this.parts.splice(index, 1);
										}
										if (typeof this.parts[index] != 'undefined' && this.parts[index].block_frete) {
											block_delivery = {
												status: true,
												id: this.parts[index].id
											};
										} else {
											if (typeof block_delivery.id == 'undefined') {
												block_delivery = {
													status: false,
													id: this.parts[index].id
												};
											}
										}
									}
	
									for (index in this.parts) {
	
										if (this.parts[index].edit == 'withdrawal') {
											this.parts.splice(index, 1);
										}
	
										if (typeof this.parts[index] != 'undefined' && this.parts[index].block_frete) {
											if (block_delivery.id != this.parts[index].id) {
												block_withdrawal = {
													status: true,
													id: this.parts[index].id
												};
											} else {
												if (typeof block_withdrawal.id == 'undefined') {
													block_withdrawal = {
														status: false,
														id: this.parts[index].id
													};
												}
											}
										} else {
											if (typeof block_withdrawal.id == 'undefined') {
												block_withdrawal = {
													status: false,
													id: this.parts[index].id
												};
											}
										}
									}
	
									if (!(this.form.shipping_delivery && !block_delivery.status)) {
										_context2.next = 49;
										break;
									}
	
									this.parts.push({
										type: 'shipping',
										edit: 'delivery',
										name: translate_data.shipping,
										date: this.formatDate(data, 'yyyy-mm-dd'),
										value: this.form.shipping_value
									});
									_context2.next = 48;
									return sleep(300);
	
								case 48:
									for (index in this.parts) {
										if (this.parts[index].edit == 'delivery') {
											_data_input2 = document.getElementById('date_' + index), _value_input2 = document.getElementById('value_' + index);
	
	
											_data_input2.removeAttribute('readonly');
											_value_input2.removeAttribute('readonly');
										}
									}
	
								case 49:
									if (!(this.form.shipping_withdrawal && !block_withdrawal.status)) {
										_context2.next = 54;
										break;
									}
	
									this.parts.push({
										type: 'shipping',
										edit: 'withdrawal',
	
										name: translate_data.shipping,
										date: this.formatDate(data, 'yyyy-mm-dd'),
										value: this.form.shipping_value
									});
									_context2.next = 53;
									return sleep(300);
	
								case 53:
									for (index in this.parts) {
										if (this.parts[index].edit == 'withdrawal') {
											_data_input3 = document.getElementById('date_' + index), _value_input3 = document.getElementById('value_' + index);
	
	
											_data_input3.removeAttribute('readonly');
											_value_input3.removeAttribute('readonly');
										}
									}
	
								case 54:
									_context2.next = 66;
									break;
	
								case 56:
	
									this.parts = [];
									_data = new Date(new Date().getFullYear(), new Date().getMonth(), this.form.payment_day !== '' ? this.form.payment_day : new Date().getDate());
									first_data = new Date(_data.getTime()), frete_value = typeof this.form.shippig_invoice[0] == 'undefined' ? (!this.form.shipping_entrega_retirada ? typeof this.form.shipping_delivery[0] != "undefined" || typeof this.form.shipping_withdrawal[0] != "undefined" ? this.form.shipping_value : 0 : this.form.shipping_value * 2) / (this.form.quantity_parts > 0 ? this.form.quantity_parts : 1) : 0;
									_extra_daysValue = this.totals.total / 30 * this.form.extra_days;
	
	
									this.form.amount = (!this.form.shipping_entrega_retirada ? typeof this.form.shipping_delivery[0] != "undefined" || typeof this.form.shipping_withdrawal[0] != "undefined" ? this.form.shipping_value : 0 : this.form.shipping_value * 2) + this.totals.total * this.form.quantity_parts;
	
									if (this.form.payment_conditions == 'in_cash') {
										this.parts.push({
											type: 'part',
											date: this.formatDate(_data, 'yyyy-mm-dd'),
											value: this.totals.total * this.form.quantity_parts + frete_value + parseFloat(_extra_daysValue.toFixed(2))
										});
									} else {
										for (i = 0; i < this.form.quantity_parts; i++) {
	
											this.setFrequencyPayment(_data);
	
											this.parts.push({
												type: 'part',
												date: this.formatDate(_data, 'yyyy-mm-dd'),
												value: this.totals.total + frete_value + _extra_daysValue
											});
										}
									}
									endDate = _data;
	
									endDate.setDate(endDate.getDate() + parseInt(this.form.extra_days));
									this.form.rent_endDate = this.formatDate(endDate, 'yyyy-mm-dd');
	
									if (typeof this.form.shippig_invoice[0] !== 'undefined') {
										if (typeof this.form.shipping_delivery[0] !== 'undefined') {
											this.parts.push({
												type: 'shipping',
												date: this.formatDate(first_data, 'yyyy-mm-dd'),
												value: this.form.shipping_value
											});
										}
	
										if (typeof this.form.shipping_withdrawal[0] !== 'undefined') {
											this.parts.push({
												type: 'shipping',
												date: this.formatDate(first_data, 'yyyy-mm-dd'),
												value: this.form.shipping_value
											});
										}
									}
	
								case 66:
									div_paymentDetails = document.getElementById('paymentPartDetails');
	
	
									if (this.form.payment_conditions == 'in_cash') {
										div_paymentDetails.classList.add('hidden');
									} else {
										div_paymentDetails.classList.remove('hidden');
									}
	
								case 68:
								case 'end':
									return _context2.stop();
							}
						}
					}, _callee2, this);
				}));
	
				function onPartsQuantity() {
					return _ref2.apply(this, arguments);
				}
	
				return onPartsQuantity;
			}(),
			setFreteAmount: function setFreteAmount() {
	
				if (typeof this.form.shipping_delivery[0] !== 'undefined' && typeof this.form.shipping_withdrawal[0] !== 'undefined') {
					this.form.shipping_entrega_retirada = true;
				} else {
					this.form.shipping_entrega_retirada = false;
				}
			},
			setFrequencyPayment: function setFrequencyPayment(data) {
				var vencimentos = {
					daily: function daily() {
						data.setDate(data.getDate() + 1);
					},
					weekly: function weekly() {
						data.setDate(data.getDate() + 7);
					},
					monthly: function monthly() {
						data.setMonth(data.getMonth() + 1);
					},
					bimonthly: function bimonthly() {
						data.setMonth(data.getMonth() + 2);
					},
					quarterly: function quarterly() {
						data.setMonth(data.getMonth() + 3);
					},
					semiannual: function semiannual() {
						data.setMonth(data.getMonth() + 6);
					},
					yearly: function yearly() {
						data.setFullYear(data.getFullYear() + 1);
					}
				};
	
				vencimentos[this.form.payment_conditions]();
			},
			formatDate: function formatDate(date) {
				var formato = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'dd/mm/yyyy';
	
				return formato.replace('dd', ('0' + date.getDate()).slice(-2)).replace('mm', ('0' + (date.getMonth() + 1)).slice(-2)).replace('yyyy', date.getFullYear());
			},
			onAddDiscount: function onAddDiscount() {
				var discount = document.getElementById('discount_rate').value;
	
				if (discount < 0) {
					discount = 0;
				} else if (discount > 100) {
					discount = 100;
				}
	
				document.getElementById('discount_rate').value = discount;
	
				this.form.discount = discount;
				this.discount = false;
	
				$('#desconto').modal('hide');
				this.onCalculateTotal();
			},
			onDeleteItem: function onDeleteItem(index) {
				this.form.items.splice(index, 1);
			},
			onCalculateTotal: function onCalculateTotal() {
				var _this = this;
	
				var sub_total = 0;
				var discount_total = 0;
				var line_item_discount_total = 0;
				var tax_total = 0;
				var grand_total = 0;
				var items = this.form.items;
				var discount_in_totals = this.form.discount;
				var old_total = this.totals.total;
	
				if (items.length) {
					var index = 0;
	
					// get all items.
	
					var _loop = function _loop() {
						var discount = 0;
						// get row item and set item variable.
						var item = items[index];
	
						// item sub total calcute.
						var item_total = item.price * item.quantity;
	
						// item discount calculate.
						var line_discount_amount = 0;
	
						if (item.discount) {
							line_discount_amount = item_total * (item.discount / 100);
	
							item_discounted_total = item_total -= line_discount_amount;
							discount = item.discount;
						}
	
						var item_discounted_total = item_total;
	
						if (discount_in_totals) {
							item_discounted_total = item_total - item_total * (discount_in_totals / 100);
							discount = discount_in_totals;
						}
	
						// item tax calculate.
						var item_tax_total = 0;
	
						if (item.tax_id) {
							if (_this.edit.status && typeof item.tax_id == 'number') {
								item.tax_id = [item.tax_id];
							}
							var inclusives = [];
							var compounds = [];
							var index_taxes = 0;
							var taxes = _this.taxes;
	
							item.tax_id.forEach(function (item_tax_id) {
								for (index_taxes = 0; index_taxes < taxes.length; index_taxes++) {
									var tax = taxes[index_taxes];
	
									if (item_tax_id != tax.id) {
										continue;
									}
	
									switch (tax.type) {
										case 'inclusive':
											inclusives.push(tax);
											break;
										case 'compound':
											compounds.push(tax);
											break;
										case 'fixed':
											item_tax_total += tax.rate * item.quantity;
											break;
										case 'withholding':
											item_tax_total += 0 - item_discounted_total * (tax.rate / 100);
											break;
										default:
											item_tax_total += item_discounted_total * (tax.rate / 100);
											break;
									}
								}
							});
	
							if (inclusives.length > 0) {
								var item_sub_and_tax_total = item_discounted_total + item_tax_total;
	
								var inclusive_total = 0;
	
								inclusives.forEach(function (inclusive) {
									inclusive_total += inclusive.rate;
								});
	
								var item_base_rate = item_sub_and_tax_total / (1 + inclusive_total / 100);
	
								item_tax_total = item_sub_and_tax_total - item_base_rate;
	
								item_total = item_base_rate + discount;
							}
	
							if (compounds.length > 0) {
								compounds.forEach(function (compound) {
									item_tax_total += (item_discounted_total + item_tax_total) / 100 * compound.rate;
								});
							}
						}
	
						// set item total
						if (item.discount) {
							items[index].total = item_discounted_total;
						} else {
							items[index].total = item_total;
						}
	
						// calculate sub, tax, discount all items.
						line_item_discount_total += line_discount_amount;
						sub_total += item_total;
						tax_total += item_tax_total;
					};
	
					for (index = 0; index < items.length; index++) {
						_loop();
					}
				}
	
				// set global total variable.
				this.totals.sub = sub_total;
				this.totals.tax = Math.abs(tax_total);
				this.totals.item_discount = line_item_discount_total;
	
				// Apply discount to total
				if (discount_in_totals) {
					discount_total = sub_total * (discount_in_totals / 100);
	
					this.totals.discount = discount_total;
	
					sub_total = sub_total - sub_total * (discount_in_totals / 100);
				}
	
				// set all item grand total.
				grand_total = sub_total + tax_total;
	
				this.totals.total = grand_total;
				if (old_total !== this.totals.total) {
					this.onPartsQuantity();
				}
			},
			onSubmit: function onSubmit() {
				var form = document.getElementById('invoice');
	
				form.submit();
			}
		}
	});
	
	/***/ })
	
	/******/ });