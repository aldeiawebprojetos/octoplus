/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 98);
/******/ })
/************************************************************************/
/******/ ({

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__error__ = __webpack_require__(41);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Form = function () {
    function Form(form_id) {
        _classCallCheck(this, Form);

        var form = document.getElementById(form_id);

        if (!form) {
            return;
        }

        this['method'] = form.getAttribute('method').toLowerCase();
        this['action'] = form.getAttribute('action');

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = document.getElementById(form_id).getElementsByTagName("select")[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var form_element = _step.value;

                var name = form_element.getAttribute('name');

                if (form_element.getAttribute('multiple')) {

                    this[form_element.id] = [form_element.value] || [];
                } else {
                    this[name] = form_element.value || '';
                }
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = document.getElementById(form_id).getElementsByTagName("money")[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var _form_element = _step2.value;

                var name = _form_element.getAttribute('name');

                this[name] = _form_element.getAttribute('value') || '';
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }

        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
            for (var _iterator3 = document.getElementById(form_id).getElementsByTagName("input")[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                var _form_element2 = _step3.value;


                var name = _form_element2.getAttribute('name');
                var type = _form_element2.getAttribute('type');

                if (name == 'method') {
                    continue;
                }

                if (type == 'radio') {
                    if (!this[name]) {
                        this[name] = (_form_element2.getAttribute('value') ? 1 : 0) || 0;
                    }
                } else if (type == 'checkbox') {
                    if (this[name]) {
                        if (!this[name].push) {
                            this[name] = [this[name]];
                        }

                        if (_form_element2.checked) {
                            this[name].push(_form_element2.value);
                        }
                    } else {
                        if (_form_element2.checked) {
                            this[name] = _form_element2.value;
                        } else {
                            this[name] = [];
                        }
                    }
                } else {
                    this[name] = _form_element2.getAttribute('value') || '';
                }
            }
        } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                    _iterator3.return();
                }
            } finally {
                if (_didIteratorError3) {
                    throw _iteratorError3;
                }
            }
        }

        var _iteratorNormalCompletion4 = true;
        var _didIteratorError4 = false;
        var _iteratorError4 = undefined;

        try {
            for (var _iterator4 = document.getElementById(form_id).getElementsByTagName("textarea")[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                var _form_element3 = _step4.value;

                var name = _form_element3.getAttribute('name');

                if (name == 'method') {
                    continue;
                }

                this[name] = _form_element3.value || '';
            }
        } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion4 && _iterator4.return) {
                    _iterator4.return();
                }
            } finally {
                if (_didIteratorError4) {
                    throw _iteratorError4;
                }
            }
        }

        this.errors = new __WEBPACK_IMPORTED_MODULE_0__error__["a" /* default */]();

        this.loading = false;

        this.response = {};
    }

    _createClass(Form, [{
        key: 'data',
        value: function data() {
            var data = Object.assign({}, this);

            delete data.method;
            delete data.action;
            delete data.errors;
            delete data.loading;
            delete data.response;

            return data;
        }
    }, {
        key: 'reset',
        value: function reset() {
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
                for (var _iterator5 = document.getElementsByTagName("input")[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                    var form_element = _step5.value;

                    var name = form_element.getAttribute('name');

                    if (this[name]) {
                        this[name] = '';
                    }
                }
            } catch (err) {
                _didIteratorError5 = true;
                _iteratorError5 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion5 && _iterator5.return) {
                        _iterator5.return();
                    }
                } finally {
                    if (_didIteratorError5) {
                        throw _iteratorError5;
                    }
                }
            }

            var _iteratorNormalCompletion6 = true;
            var _didIteratorError6 = false;
            var _iteratorError6 = undefined;

            try {
                for (var _iterator6 = document.getElementsByTagName("textarea")[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                    var _form_element4 = _step6.value;

                    var name = _form_element4.getAttribute('name');

                    if (this[name]) {
                        this[name] = '';
                    }
                }
            } catch (err) {
                _didIteratorError6 = true;
                _iteratorError6 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion6 && _iterator6.return) {
                        _iterator6.return();
                    }
                } finally {
                    if (_didIteratorError6) {
                        throw _iteratorError6;
                    }
                }
            }

            var _iteratorNormalCompletion7 = true;
            var _didIteratorError7 = false;
            var _iteratorError7 = undefined;

            try {
                for (var _iterator7 = document.getElementsByTagName("select")[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                    var _form_element5 = _step7.value;

                    var name = _form_element5.getAttribute('name');

                    if (this[name]) {
                        this[name] = '';
                    }
                }
            } catch (err) {
                _didIteratorError7 = true;
                _iteratorError7 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion7 && _iterator7.return) {
                        _iterator7.return();
                    }
                } finally {
                    if (_didIteratorError7) {
                        throw _iteratorError7;
                    }
                }
            }
        }
    }, {
        key: 'oldSubmit',
        value: function oldSubmit() {
            this.loading = true;

            //    window.axios({
            //         method: this.method,
            //         url: this.action,
            //         data: this.data()
            //     })
            //     .then(this.onSuccess.bind(this))
            //     .catch(this.onFail.bind(this));
        }
    }, {
        key: 'submit',
        value: function submit() {
            FormData.prototype.appendRecursive = function (data) {
                var wrapper = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

                for (var name in data) {
                    if (wrapper) {
                        if ((_typeof(data[name]) == 'object' || data[name].constructor === Array) && data[name] instanceof File != true && data[name] instanceof Blob != true) {
                            this.appendRecursive(data[name], wrapper + '[' + name + ']');
                        } else {
                            this.append(wrapper + '[' + name + ']', data[name]);
                        }
                    } else {
                        if ((_typeof(data[name]) == 'object' || data[name].constructor === Array) && data[name] instanceof File != true && data[name] instanceof Blob != true) {
                            this.appendRecursive(data[name], name);
                        } else {
                            this.append(name, data[name]);
                        }
                    }
                }
            };

            this.loading = true;

            var data = this.data();

            var form_data = new FormData();
            form_data.appendRecursive(data);

            window.axios({
                method: this.method,
                url: this.action,
                data: form_data,
                headers: {
                    'X-CSRF-TOKEN': window.Laravel.csrfToken,
                    'X-Requested-With': 'XMLHttpRequest',
                    'Content-Type': 'multipart/form-data'
                }
            }).then(this.onSuccess.bind(this)).catch(this.onFail.bind(this));
        }
    }, {
        key: 'onSuccess',
        value: function onSuccess(response) {
            this.errors.clear();

            this.loading = false;

            if (response.data.redirect) {
                this.loading = true;

                window.location.href = response.data.redirect;
            }

            this.response = response.data;
        }

        // Form fields check validation issue

    }, {
        key: 'onFail',
        value: function onFail(error) {
            this.errors.record(error.response.data.errors);

            this.loading = false;
        }
    }]);

    return Form;
}();

/* harmony default export */ __webpack_exports__["a"] = (Form);

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Errors = function () {
    function Errors() {
        _classCallCheck(this, Errors);

        this.errors = {};
    }

    _createClass(Errors, [{
        key: "has",
        value: function has(field) {
            // if this.errors contains as "field" property.
            return this.errors.hasOwnProperty(field);
        }
    }, {
        key: "any",
        value: function any() {
            return Object.keys(this.errors).length > 0;
        }
    }, {
        key: "set",
        value: function set(key, field) {
            return this.errors[key] = field;
        }
    }, {
        key: "get",
        value: function get(field) {
            if (this.errors[field]) {
                return this.errors[field][0];
            }
        }
    }, {
        key: "record",
        value: function record(errors) {
            this.errors = errors;
        }
    }, {
        key: "clear",
        value: function clear(field) {
            if (field) {
                return delete this.errors[field];
            }

            this.errors = {};
        }
    }]);

    return Errors;
}();

/* harmony default export */ __webpack_exports__["a"] = (Errors);

/***/ }),

/***/ 98:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(99);


/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__plugins_form__ = __webpack_require__(40);


var vm = new Vue({
    el: '#app',
    data: function data() {
        return {
            form: new __WEBPACK_IMPORTED_MODULE_0__plugins_form__["a" /* default */]('incomes'),
            edit: {}
        };
    },
    mounted: function mounted() {
        if (typeof editStatus !== 'undefined') {
            this.edit.status = editStatus;
        }
    },

    methods: {
        getCalculo: function getCalculo() {

            if (this.edit.status) {
                if (this.form.balance) {
                    this.form.value_liquid = this.form.value + this.form.juros_multas - this.form.discount;
                    this.form.value_paid = this.form.value_liquid - this.form.balance;
                } else {

                    this.form.value_liquid = this.form.value + this.form.juros_multas - this.form.discount;
                    this.form.value_paid = this.form.value_liquid;
                }
            } else {
                this.form.value_liquid = this.form.value + this.form.juros_multas - this.form.discount;
                this.form.value_paid = this.form.value_liquid;
            }
        },
        getShowLink: function getShowLink() {
            var link = document.getElementById('showLink');

            link.removeAttribute('href');
            link.setAttribute('href', this.form.customer_route);

            link.classList.remove('hidden');
        },
        hideShowLink: function hideShowLink() {
            var link = document.getElementById('showLink');
            link.removeAttribute('href');
            link.classList.add('hidden');
        },
        getPartialPaid: function getPartialPaid() {

            if (this.form.paid[0] || this.form.paid) {
                var data_recebimento = document.getElementById('data_recebimento');
                this.form.value_paid = this.form.balance ? this.form.balance : this.form.value;
                this.form.data_recebimento = this.formatDate(new Date(), 'yyyy-mm-dd');
                data_recebimento.value = this.formatDate(new Date(), 'yyyy-mm-dd');
            } else {
                this.form.value_paid = this.form.value_liquid - (this.form.balance ? this.form.balance : this.form.value);
            }
        },
        formatDate: function formatDate(date) {
            var formato = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'dd/mm/yyyy';

            return formato.replace('dd', ('0' + date.getDate()).slice(-2)).replace('mm', ('0' + (date.getMonth() + 1)).slice(-2)).replace('yyyy', date.getFullYear());
        }
    }
});

/***/ })

/******/ });