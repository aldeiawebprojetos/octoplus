<!DOCTYPE html>
<html lang="en">
<head>


	<?php
		$title = "Incluir/Editar Receita";
		$description = $title . " | Admin Panel 8+";
	?>
	<!-- Header -->
	<?php include("admin.header.php"); ?>


	<!-- Left Menu -->
	<?php
		include("admin.menu.php");
	?>
	
	<div class="main-content">
		<!-- Left Menu -->
		<?php
			include("admin.notification.php");
		?>

		

		<ol class="breadcrumb 2" >
			<li> <a href="index.php"><i class="entypo-cd"></i>Início</a> </li>

			<li> <a href="">Receitas</a> </li>

			<li class="active"> <strong>Pedido</strong> </li>		
		</ol>
				
		<br />
			
		<div class="row">
			
			<!-- title -->
			<div class="col-md-6">
				<h1 class="pull-left">
					Pedido <i class="fa fa-dot-circle-o"></i> <span class="label label-primary"> #12345 </span>
				</h1>
			</div>
			

			<!-- Tipo Contrato -->
			<div class="col-md-6">
				<div class="">
					<label for="tipo2"> Tipo de Contrato<strong class="text-danger">*</strong></label>
					
					<select name="tipo25" id="tipo2" class="input-lg form-control" >
						<option value="Pedido de Venda" selected="true"> Venda </option>
						<option value="Pedido de Locação">Locação</option>
						<option value="Serviço" >Serviço</option>
					</select>	

					<span class="description"> <i class="fa fa-cogs"></i> Os Tipo que você utiliza podem ser ajustados nas configurações. </span>
				</div>
			</div>
		</div>

		<hr />
		
		<div class="row">
			<div class="col-xs-12">
				<!-- Botoes -->
				<?php include("admin.buttons.php"); ?>
			</div>
			
		</div>
	
		<hr />
				
		<form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="">
		
			<div class="row">
				<div class="col-md-12">
					
					<div class="panel panel-primary" data-collapsed="0">
					
						<div class="panel-heading">
							<div class="panel-title">
								Dados Principais
							</div>
							
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
				
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label">Cliente</label>
								
								<div class="col-sm-7">
									<div class="input-group">
										<span class="input-group-addon"><i class="entypo-user"></i></span>
										<input type="text" name="typeahead_local" class="form-control typeahead" data-local="Cliente 1, Cliente 2, Fulano de Tal, Cliclano" />
									</div>
								</div>

								<a href="newedit.customer.php" target="_blank" type="button" class="btn btn-white">
									<i class="fa fa-plus-square"></i>
								</a>
							</div>
			
			

							<div class="form-group">
								<label for="field-4" class="col-sm-3 control-label">Data</label>
								
								<div class="col-sm-5">
									<div class="input-group">
										<input type="text" class="form-control datepicker" data-format="D, dd MM yyyy">
										
										<div class="input-group-addon">
											<a href="#"><i class="entypo-calendar"></i></a>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group">
								<label for="tipo"> <i class="entypo-archive"></i> Categoria <strong class="text-danger">*</strong> </label>
								
								<select name="tipo" class="select2 form-control input-lg" id="tipo" multiple>
									<option value="3" >locação</option>
									<option value="2" >venda</option>
									<option value="5" >receita geral</option>
									
								</select>

								<div class="btn-group">
									<button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
									<i class="fa fa-plus-square"></i>	 <span class="caret"></span>
									</button>
									<ul class="dropdown-menu dropdown-blue" role="menu">
										<li>
											<a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});" class="">Adicionar Categoria</a>
										</li>
										
										<li class="divider"></li>

										<li>
											<a href="#"> Configs </a>
										</li>
									</ul>
								</div>
							</div>


							<div class="form-group">
								<label for="" class="col-sm-3 control-label">Observações</label>
								<div class="col-sm-7">
									<textarea class="form-control" id="inputEnd6xf" placeholder="Endereço do Cliente"></textarea>
								</div>
							</div>
			
							
						</div>
					
					</div>
				
				</div>
			</div>
			
			
		
			<div class="row">
				<div class="col-md-12">
					
					<div class="panel panel-primary" data-collapsed="0">
					
						<div class="panel-heading">
							<div class="panel-title">
								Itens do Pedido
							</div>
							
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
			
							<div class="form-group">
								<label class="col-sm-12 control-label"> <img src="assets/images/akaunting-pedidos.png" /> </label>
								
							</div>
			
							<div class="form-group">
								<label class="col-sm-5 control-label">Frete</label>
								
								<div class="col-sm-5">
								
									<select class="form-control">
										<option>Subscriber</option>
										<option>Author</option>
										<option>Editor</option>
										<option>Administrator</option>
									</select>
									
								</div>
							</div>
			
							<div class="form-group">
								<label class="col-sm-5 control-label">New users</label>
								
								<div class="col-sm-5">
								
									<select class="form-control">
										<option>Will have to activate their account (via email)</option>
										<option>Account is automatically activated</option>
									</select>
									
								</div>
							</div>
			
							<div class="form-group">
								<label for="field-5" class="col-sm-5 control-label">Maximum login attempts</label>
								
								<div class="col-sm-5">
								
									<input type="text" name="max_attempts" id="field-5" class="form-control" data-validate="required,number" value="5" />
									
								</div>
							</div>
						
						</div>
						
					</div>
				
				</div>
				
			</div>
			
		
			<div class="row">
				<div class="col-md-12">
					
					<div class="panel panel-primary" data-collapsed="0">
					
						<div class="panel-heading">
							<div class="panel-title">
								Frete
							</div>
							
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
			
							<div class="form-group">
								<label for="field-2" class="col-sm-3 control-label">Endereço de Entrega</label>
								
								<div class="col-sm-7">
									<select class="form-control">
										<option >Cliente retira na loja</option>
										<option selected="">Entregar no mesmo Endereço do Cliente</option>
										<option>Entregar em um endereço diferente</option>
									</select>
								</div>
							</div>
			
							<div class="form-group">
								
								<div class="col-sm-7">
									<textarea class="form-control" id="inputEnd6x" placeholder="Endereço do Cliente"></textarea>
								</div>
							</div>
			
							<div class="form-group">
								<label class="col-sm-3 control-label">Valor do Frete</label>
								
								<div class="col-sm-7">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-money"></i></span>
										<input type="text" class="form-control input-lg" data-mask="currency" data-sign="R$" />
									</div>
								</div>
							</div>
						</div>
						
					</div>
				
				</div>
				
			</div>
												
			
		
			<div class="row">
				<div class="col-md-12">
					
					<div class="panel panel-primary" data-collapsed="0">
					
						<div class="panel-heading">
							<div class="panel-title">
								Pagamento
							</div>
							
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
			
							<div class="form-group">
								<label for="field-2" class="col-sm-3 control-label">Endereço de Entrega</label>
								
								<div class="col-sm-7">
									<select class="form-control">
										<option >Cliente retira na loja</option>
										<option selected="">Entregar no mesmo Endereço do Cliente</option>
										<option>Entregar em um endereço diferente</option>
									</select>
								</div>
							</div>
			
							<div class="form-group">
								
								<div class="col-sm-7">
									<textarea class="form-control" id="inputEnd6x" placeholder="Endereço do Cliente"></textarea>
								</div>
							</div>
			
							<div class="form-group">
								<label class="col-sm-3 control-label">Valor do Frete</label>
								
								<div class="col-sm-7">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-money"></i></span>
										<input type="text" class="form-control input-lg" data-mask="currency" data-sign="R$" />
									</div>
								</div>
							</div>
						</div>
						
					</div>
				
				</div>
				
			</div>
												
			
						
		</form>


		<hr />
		
		<div class="row">
			<div class="col-xs-12">
				<!-- Botoes -->
				<?php include("admin.buttons.php"); ?>
			</div>
			
		</div>
	
		<hr />
					
		
		<div class="clear"></div>
		<!-- Footer -->
		<?php include("admin.footer.php"); ?>


		
	</div>

		

	
</div>


	<!-- Modal 6 (Long Modal)-->
	<div class="modal fade" id="modal-6">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title"> <i class="entypo-folder"></i> Inserir Categoria </h3>
				</div>
				
				<div class="modal-body">
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-1" class="control-label">Nome</label>
								
								<input type="text" class="form-control" id="field-1" placeholder="Dê um nome a categoria">
							</div>	
							
						</div>
						
						
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<a class="btn btn-green btn-lg btn-icon pull-right">Salvar<i class="entypo-floppy"></i></a>
				</div>
			</div>
		</div>
	</div>


	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	
	





	<!-- Bottom scripts (common) -->
	<script src="assets/js/gsap/TweenMax.min.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>




	<!-- Imported scripts on this page -->
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/jquery.inputmask.bundle.js"></script>
	<script src="assets/js/bootstrap-switch.min.js"></script>
	<script src="assets/js/typeahead.min.js"></script>
	<script src="assets/js/bootstrap-datepicker.js"></script>
	
	


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="assets/js/neon-demo.js"></script>


</body>
</html>