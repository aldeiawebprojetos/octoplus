<!DOCTYPE html>
<html lang="en">
<head>


	<?php
		$title = "Criar/Editar Itens(Produtos)";
		$description = $title . " | Admin Panel 8+";
	?>
	<!-- Header -->
	<?php include("admin.header.php"); ?>


	<!-- Left Menu -->
	<?php
		include("admin.menu.php");
	?>
	
	<div class="main-content">
		<!-- Left Menu -->
		<?php
			include("admin.notification.php");
		?>

		

		<ol class="breadcrumb 2" >
			<li> <a href="index.php"><i class="entypo-cd"></i>Início</a> </li>

			<li> <a href="">Itens</a> </li>

			<li class="active"> <strong>Inserir Item</strong> </li>		
		</ol>
				
		<br />
			
		<div class="row">
			<!-- title -->
			<div class="col-md-6">
				<h1 class="pull-left">
					Itens <i class="entypo-pencil"></i>
				</h1>
			</div>
			
			<!-- Botoes -->
			<?php include("admin.buttons.php"); ?>
			
		</div>
	
		<hr />
				
		<form>

			<div class="row">
				
					
		  		<div class="form-group col-md-2 col-xs-4">
		    		<label for="readonly">Código </label>
		    		<input type="text" readonly class=" form-control input-lg" id="readonly" value="#12345">
		  		</div>

		  		<div class="form-group col-md-6 col-xs-8">
		    		<label for="formGroupExampleInput">Nome <strong class="text-danger">*</strong> </label>
		    		<input type="text" class="form-control input-lg" id="formGroupExampleInput" placeholder="Digite o nome do Item" required>
		  		</div>

		  		<div class="form-group col-md-4">
					<label for="tipo2"> Tipo <strong class="text-danger">*</strong></label>
					
					<select name="tipo25" id="tipo2" class="input-lg form-control" >
						<option value="Produto Compra/Venda" selected="true">Produto Compra/Venda</option>
						<option value="Produto Locação">Produto Locação</option>
						<option value="Serviço" >Serviço</option>
					</select>	

					<span class="description"> <i class="fa fa-cogs"></i> Os Tipo podem ser ativados ou desativados nas configurações. </span>
				</div>


				<!-- ;) -->


				<div class="form-group col-md-6 col-xs-5">
					<label for="tipo"> <i class="entypo-archive"></i> Categoria <strong class="text-danger">*</strong> </label>
					
					<select name="tipo" class="select2 form-control input-lg" id="tipo" multiple>
						<option value="3" >CAtegoria</option>
						<option value="2" >Cat1</option>
						<option value="5" >Cat2</option>
						<option value="6" >Locação</option>
						<option value="7" >Venda</option>
					</select>

					<div class="btn-group">
						<button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-plus-square"></i>	 <span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-blue" role="menu">
							<li>
								<a href="javascript:;" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});" class="">Adicionar Categoria</a>
							</li>
							
							<li class="divider"></li>

							<li>
								<a href="#"> Configs </a>
							</li>
						</ul>
					</div>
				</div>


				<div class="form-group col-md-4 col-xs-4">
			      	<label for="inputZip">Preço de Venda/Locação</label>

			      	<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-money"></i></span>
						<input type="text" class="form-control input-lg" data-mask="currency" data-sign="R$" />
					</div>
			    </div>


			  	<div class="form-group col-md-2 col-xs-3">
					<label for="radio1">Ativo?</label>

					<div class="">
						<div class="make-switch" data-on="primary" data-off="danger" data-on-label="<i class='entypo-check'></i>" data-off-label="<i class='entypo-cancel'></i>">
							<input type="checkbox" checked />
						</div>
					</div>
				</div>

				<div class="form-group col-md-12">
		    		<label for="formGroupExampleInput12">SKU </label>
		    		<input type="text" class="form-control input-lg" id="formGroupExampleInput12" placeholder="Unidade de Manutenção de Estoque">

		    		<span class="description ">   
						<button class="btn btn-default popover-default btn-xs" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Unidade de Manutenção de Estoque é um código personalizado utilizado para auxiliar a gestão dos produtos. Esse item pode ser desativado em configurações" data-original-title="SKU"><i class="entypo-info"></i></button>
					</span>
		  		</div>
		  	</div>


			 <!-- Metaboxes -->
			<div class="row">
				
				<!-- Painel que aparecerá apenas para produtos, serviços não -->
				<div class="col-sm-6">
					
					<div class="panel panel-primary" data-collapsed="0">
				
						<div class="panel-heading">
							<div class="panel-title">
								Características Produtos (Somente Compra/Venda)
							</div>
							
							<div class="panel-options">
								<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
								<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
							</div>
						</div>
						
						<div class="panel-body">

						    <div class="form-group">
						      	<label for="inputZip">Preço de Compra</label>

						      	<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-money"></i></span>
									<input type="text" class="form-control input-lg" data-mask="currency" data-sign="R$" />
								</div>
						    </div>

						    <hr>
						


							<div class="form-group">
						      	<label for="inputEnd3">Estoque</label>

						      	<div class="input-spinner">
									<button type="button" class="btn btn-default" data-step="-1">-</button>
									<input type="text" class="form-control size-2" value="0" />
									<button type="number" class="btn btn-default" data-step="1">+</button>
								</div>

						    </div>


							<div class="form-group">
						      	<label for="inputEnd3">Estoque Mínimo</label>

						      	<div class="input-spinner">
									<button type="button" class="btn btn-default" data-step="-1">-</button>
									<input type="text" class="form-control size-2" value="0" />
									<button type="number" class="btn btn-default" data-step="1">+</button>
								</div>

						    </div>


						    <hr>


					  		<div class="form-group">
					    		<label for="formGroupExampleInput">Cód. de Barras </label>

					    		<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-barcode"></i></span>
									<input type="text" class="form-control input-lg" id="formGroupExampleInput" placeholder="Digite ou faça a leitura do código de barras">
								</div>
					  		</div>

					  		<hr>

					  		<div class="form-group">
						      	<label for="inputZip">Unidade de Medida</label>

						      	<div class="input-group">
									<span class="input-group-addon"><i class="entypo-doc-text"></i></span>
									<select name="tipo3" id="tipo2" class="input-lg form-control" >
										<option value="2" selected="true">Unitário</option>
										<option value="3" >Kg</option>
										<option value="4" >Mts</option>
									</select>	
								</div>
						    </div>

						    <hr>

						</div>
					
					</div>
					
				</div>
				
				
				
				
				<!-- Metabox :: Tags -->
				<div class="col-sm-6">
					
					<div class="panel panel-primary" data-collapsed="0">
				
						<div class="panel-heading">
							<div class="panel-title">
								Informações Adicionais
							</div>
							
							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
							</div>
						</div>
						
						<div class="panel-body">

							<div class="form-group">
								<label for="tipo2"> Descrição  </label>
								
								<textarea class="form-control" id="inputEnd6" placeholder="Coloquei aqui informações."></textarea>
							</div>

							<hr>

							<p>Imagem do Produto:</p>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="max-width: 310px; height: 160px;" data-trigger="fileinput">
									<img src="http://placehold.it/320x160" alt="...">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 320px; max-height: 160px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Escolha a Imagem</span>
										<span class="fileinput-exists">Alterar</span>
										<input type="file" name="..." accept="image/*">
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover</a>
								</div>
							</div>
							
							
						</div>
					
					</div>
					
				</div>

				
			</div>


			<div class="row">
				<!-- title -->
				<div class="col-md-6"> </div>
				
				<!-- Botoes -->
				<?php include("admin.buttons.php"); ?>
				
			</div>
						
			<hr />
		</form>
					
		
		<div class="clear"></div>
		<!-- Footer -->
		<?php include("admin.footer.php"); ?>


		
	</div>

		

	
</div>


	<!-- Modal 6 (Long Modal)-->
	<div class="modal fade" id="modal-6">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="modal-title"> <i class="entypo-folder"></i> Inserir Categoria </h3>
				</div>
				
				<div class="modal-body">
				
					<div class="row">
						<div class="col-md-12">
							
							<div class="form-group">
								<label for="field-1" class="control-label">Nome</label>
								
								<input type="text" class="form-control" id="field-1" placeholder="Dê um nome a categoria">
							</div>	
							
						</div>
						
						
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<a class="btn btn-green btn-lg btn-icon pull-right">Salvar<i class="entypo-floppy"></i></a>
				</div>
			</div>
		</div>
	</div>


	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/css/font-icons/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">




	<!-- Bottom scripts (common) -->
	<script src="assets/js/gsap/TweenMax.min.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>




	<!-- Imported scripts on this page -->
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/jquery.inputmask.bundle.js"></script>
	<script src="assets/js/bootstrap-switch.min.js"></script>
	
	





	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/neon-custom.js"></script>


	<!-- Demo Settings -->
	<script src="assets/js/neon-demo.js"></script>


</body>
</html>